<?php
/*
Plugin Name: import Podcast from Spotify
Plugin URI: https://betasoftsolutions.com/
Description: Plugin to accompany podcast import guide to creating plugins, post imported data as post type.
Version: 1.0
Author: Sunil Kumar
Author URI: https://
License: GPLv2 or later
Text Domain: Podcast Importer
*/

//require_once('../wp-load.php');
//require '../../../vendor/autoload.php';
include($_SERVER['DOCUMENT_ROOT'].'/zipistream/wp-load.php');
include($_SERVER['DOCUMENT_ROOT'].'/zipistream/vendor/autoload.php');



add_action( 'admin_menu', 'spotify_stats_menu' );

function spotify_stats_menu() {
  add_menu_page( 'Spotify', 'import spotify', 'manage_options', 'import-spotify', 'import_spotify', 'dashicons-welcome-widgets-menus', 90 );
}

function import_spotify(){
	
	//'ead8e16aecf1469bb6cfc751a8a10ac2'
	//'9853f3bf7c3b4fb4afe1e5dc83b85466'
	
	global $wpdb;
	$table_name = '0d9Uj_user_access_token';
	

	$data = $wpdb->get_row( "SELECT * FROM $table_name" );
	$bearer = $data->access_token;
	
	$clientId = $_POST['client_id'];
	$clientSecret = $_POST['client_secret'];
	
	$session = new SpotifyWebAPI\Session(
		'ead8e16aecf1469bb6cfc751a8a10ac2',
		'9853f3bf7c3b4fb4afe1e5dc83b85466',
		'http://localhost/zipistream/wp-admin/admin.php?page=import-spotify'
	);

	$api = new SpotifyWebAPI\SpotifyWebAPI();
	
	
	 if (isset($_GET['code'])) 
	{
		
		$session->requestAccessToken($_GET['code']);
		//$accessToken = $api->setAccessToken($session->getAccessToken());
		
		$accessToken = $session->getAccessToken();
		$refreshToken = $session->getRefreshToken();
		
		//print_r($accessToken);
		
		$data = $wpdb->get_results( "SELECT * FROM $table_name" );
		
		if(!empty($data)){
			
			$wpdb->query($wpdb->prepare(" UPDATE $table_name SET access_token='$accessToken', refresh_token= '$refreshToken' WHERE id = 1"));
			
		}else{
			
			$wpdb->insert("$table_name", array( "access_token" => $accessToken, "refresh_token" => $refreshToken ));
		}
		
	} else {
		/* $options = [
			'scope' => [
				'user-read-email',
			],
		]; */

		header('Location: ' . $session->getAuthorizeUrl());
		die();
	}
	
?>
		<div class="wrap" style="min-height: 100vh">
			
			<div class="container" style="max-width: 100rem;">
			
			
			<div class="auth-user" style="display:none">
				<h4>Before Proceed to import podcast Please fill you client id</h4>
				<div class="auth-user">
					<input type="text" name="client_id" placeholder="Client ID" id="client_id">
					<input type="text" name="client_secret" placeholder="Client Secret" id="client_secret">
					<input type="submit" id="request_auth" name="submit" value="Submit">
				</div>				
			</div>
		
			<div id="import_content" style="display:block">
				<h2 class="text-center">Spotify Podcast</h2>
				<h2 class="text-center">Import Podcast from Spotify</h2>
				
				<p class="pp_align-center">The following tool will import your podcast episodes to this website.</p>
				
				<section id="one" class="wrapper">
					<div class="inner">
				
					<form class="form-horizontal" name="import_podcast" method="post" id="import_podcast">
						<input type="hidden" name="ispost" value="1" />
						<input type="hidden" name="web_url" id='web_url' value="<?php echo content_url(); ?>" />
						<div class="row">
							<div class="col-md-12 form-group">
								<label class="control-label">URL</label>
								<input type="text" class="form-group" name="podcast_url" id="podcast_url" required placeholder="https://open.spotify.com/show/64lpnDBikZbmPvBuezUesz" style="width: 50%;margin-bottom:15px">	
								<span style="display:none">Invaild url can't be import data </span>
							</div>
						

							<div class="col-md-6 form-group">
								<label class="control-label">Choose Category</label>
								<select required name="category" class="form-control">
									<option value="">Select</option>
									<?php
									$catList = get_categories();
									
									foreach($catList as $listval)
									{
										echo '<option value="'.$listval->term_id.'">'.$listval->name.'</option>';
									}
									?>
								</select>
							</div>
						
							<div class="col-md-12" style="margin-top:30px">
								<input type="submit" class="btn btn-primary submit-btn" value="Import Podcast" name="import_" />
							</div>
						</div>
					</form>
				</div>
			  </section>
			</div>
		</div>
	</div>
	<div id="pageloader" style="visibility: hidden;  background-color: rgba(255,255,255,0.7);  position: absolute;  z-index: +100;  width: 100%;  height:100%; top:0">
	   <img src="http://localhost/podcast-importer/wp-content/uploads/2020/10/Eclipse-1s-200px2.gif" alt="processing..." style="left: 50%;  margin-left: -32px;  margin-top: -32px;  position: absolute;  top: 50%;"/>
	</div>


		<?php
		

	
 ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script>
		
		jQuery(document).ready(function(){
			
		  jQuery("#request_auth").on("click", function(e){
			
			var url= jQuery('#web_url').val();
			var client_id= jQuery('#client_id').val();
			var client_secret= jQuery('#client_secret').val();
			jQuery("#pageloader").css("visibility", "visible");
			
			//var url = '<?php echo $url= urlencode("http://localhost/podcast-importer/callback/"); ?>';
			
			//window.location.href = "		https://accounts.spotify.com/en/authorize?client_id=ead8e16aecf1469bb6cfc751a8a10ac2&response_type=code&redirect_uri="+url+"";
			
			e.preventDefault();
			
			   jQuery.ajax({
				type: 'post',
				url: url + '/plugins/import-podcasts-spotify/ajex-request.php',
				data: {client_id : client_id, client_secret:client_secret},
				success: function (response) {
					console.log(response);
					jQuery("#pageloader").css("visibility", "hidden");
					if(response == 'success'){
						jQuery("#import_content").show();
						alert('successfully');
					}
					else{
						
						alert(response);
					}
				  //alert();
				}
			  }); 
			
		  });//submit


		//////////////// Import Podcast Form request /////////////////////////////
		
		  jQuery("#import_podcast").on("submit", function(e){
			
			var url= jQuery('#web_url').val();
			jQuery("#pageloader").css("visibility", "visible");
			
			e.preventDefault();
			
			  jQuery.ajax({
				type: 'post',
				url: url + '/plugins/import-spotify-podcasts/ajex-request.php',
				data: $('form').serialize(),
				success: function (response) {
					console.log(response);
					var res = response.split('/');
					//console.log(res[0]);
					jQuery("#pageloader").css("visibility", "hidden");
					
					if(response == 'Invalid access token'){
						
						alert('Invalid access token');
						return false;
					}
					
					if(res[0] == 'success'){
						
						alert(res[1]+' Eposides are imported successfully');
					
					}else if(res[2].length > 0)
					{
						var data = JSON.parse(res[2]);
						alert('Podcast with these names are already exist ! ' +data)
					}
					else{
						
						alert(response);
					}
				  //alert();
				}
			  });
			
		  });//submit
		});

		//var cat= jQuery('#podcast_category :selected').text();
		//console.log(cat);
		
		
	</script>
<?php }