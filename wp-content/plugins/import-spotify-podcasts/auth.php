<?php
require __DIR__ . '/vendor/autoload.php';

	$session = new SpotifyWebAPI\Session(
		'ead8e16aecf1469bb6cfc751a8a10ac2',
		'9853f3bf7c3b4fb4afe1e5dc83b85466',
		'http://localhost/podcast-importer/callback/'
	);

	$api = new SpotifyWebAPI\SpotifyWebAPI();

	if (isset($_GET['code'])) {
		$session->requestAccessToken($_GET['code']);
		$api->setAccessToken($session->getAccessToken());

		print_r($api->me());
	} else {
		/* $options = [
			'scope' => [
				'user-read-email',
			],
		]; */

		header('Location: ' . $session->getAuthorizeUrl());
		die();
	}
?>