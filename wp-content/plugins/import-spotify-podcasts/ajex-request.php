<?php
require_once('../../../wp-load.php'); 
require '../../../vendor/autoload.php';


if ( ! is_admin() ) {
    require_once( ABSPATH . 'wp-admin/includes/post.php' );
}

if(isset($_POST['ispost'])){


		if(filter_var($_POST['podcast_url'], FILTER_VALIDATE_URL)) ////check URL is Valid
		{
			global $wpdb;
			$table_name = '0d9Uj_user_access_token';
			$data = $wpdb->get_row( "SELECT * FROM $table_name" );
			$bearer = $data->access_token;
			
			$m_url= $_POST['podcast_url'];

			$parseUrl = parse_url($m_url, PHP_URL_PATH);
			$host = explode('/', $parseUrl);
			
			//////////// curl request to get all shows /////////////////////////////
			
				$m_url = 'https://api.spotify.com/v1/shows/'.$host[2];
				////Get the details of odcasts from the Spotify API as JSON

				$url = 'curl -s -X "GET" "'.$m_url.'' .
				'" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer ' .
				$bearer .
				'"';

				$json = '';
				$fpin = popen($url, 'r');
				while(!feof($fpin)) {
				$json .= fgets($fpin, 4096);
				}
				
			//////////// end curl request to get all shows /////////////////////////////
			
				//Convert the JSON into a PHP object
				$shows = json_decode($json);
				
				//echo '<pre>';
			   //print_r($shows); die;
			if($shows->error){
				
				echo $shows->error->message;
				
			}else{	
			   
			  
				if (is_array($shows) || is_object($shows)) // create post from the curl response for each episode
				{	
					$i=1;
					
					$podcast_exist = array();
					
					foreach($shows->episodes->items as $oneshow) {
						
						$id = $oneshow->id;
						$name = $oneshow->name; //echo '<br>';
						$audio_preview_url = $oneshow->audio_preview_url;
						$description = $oneshow->description; //echo '<br>';
						$duration_ms = $oneshow->duration_ms;
						$external_urls = $oneshow->external_urls->spotify;
						$images = $oneshow->images[0]->url;
						$release_date = $oneshow->release_date;
						
						$post_title = $name;
						$post_content = $description;
						$category = array($_POST['category']);
		
						$fount_post = post_exists($post_title);
						// print_r($fount_post);
						// die;
						 if(empty($fount_post)){
							
							 $new_post = array(
								'post_title' => $post_title,
								'post_content' => $post_content .' '.$external_urls,
								'post_status' => 'publish',
								'post_name' => $post_title,
								//'post_type' => 'podcast',
								'post_category' => $category
							);

							$pid = wp_insert_post($new_post);
							
							add_post_meta($pid, 'meta_key', true);
							
							add_post_meta($pid, 'audio_file', $audio_preview_url);
							add_post_meta($pid, 'episode_type', 'audio');
							add_post_meta($pid, 'enclosure', $audio_preview_url);
							
							include_once( ABSPATH . 'wp-admin/includes/image.php' );
							
							$imageurl = $images;
							$imagetype = end(explode('/', getimagesize($imageurl)['mime']));
							$uniq_name = date('dmY').''.(int) microtime(true); 
							$filename = $uniq_name.'.'.$imagetype;

							$uploaddir = wp_upload_dir();
							$uploadfile = $uploaddir['path'] . '/' . $filename;
							$contents= file_get_contents($imageurl);
							$savefile = fopen($uploadfile, 'w');
							fwrite($savefile, $contents);
							fclose($savefile);

							$wp_filetype = wp_check_filetype(basename($filename), null );
							$attachment = array(
								'post_mime_type' => $wp_filetype['type'],
								'post_title' => $filename,
								'post_content' => '',
								'post_status' => 'inherit'
							);

							$attach_id = wp_insert_attachment( $attachment, $uploadfile );
							$imagenew = get_post( $attach_id );
							$fullsizepath = get_attached_file( $imagenew->ID );
							$attach_data = wp_generate_attachment_metadata( $attach_id, $fullsizepath );
							wp_update_attachment_metadata( $attach_id, $attach_data ); 
							
							set_post_thumbnail( $pid, $attach_id );
													
							$i++;
						
						}else{
							
							array_push($podcast_exist, $name);
							
						}
					}
					$podcast_exist = json_encode($podcast_exist, true);
					
					echo "success/".$i."/".$podcast_exist;
					//echo $i.' Eposides are imported successfully';
				
				}else
					{
						echo "No show found for the given URL";
					}
			}
				
		}		
		else{
		
				echo "URL is not vaild";
		}		
 }




function Auth(){
	
		$url = "https://accounts.spotify.com/authorize";
		$code = 'AQBz6gHOMNYA7a-AeDUjR11MwMEucxyyi6sbpir4GIsJGka51msXN6H-g9d-LtWUjUtmAvMIjHP8CO8ymgbIn-pKInP9KLRdb9CX_yW9WKhPaLSCZdwdnPuYQcDyu-Gl_KBiIyAnvhqBb_kP2VvRX2o5MkosB9-7MtFo5x1V-vIUqrquDT1SC1Em05X6zZsflln4A7F-Qx4';
		$curl = curl_init($url);
		$params = [
			"grant_type" => "authorization_code",
			"code" => $code,
			"redirect_uri" => "https://zipi.honesttech.co.uk/auth/external/callback"
		];

		$id = "ead8e16aecf1469bb6cfc751a8a10ac2:9853f3bf7c3b4fb4afe1e5dc83b85466";

		$header = [
			"Accept: application/json",
			"Content-Type: application/x-www-form-urlencoded",
			"Authorization: Basic " . base64_encode($id)
		];

		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$res = curl_exec($curl);
		print_r($res);
	
	
}


if(isset($_POST['client_id'])){
	
	//'ead8e16aecf1469bb6cfc751a8a10ac2'
	//'9853f3bf7c3b4fb4afe1e5dc83b85466'
		
	/* $clientId = $_POST['client_id'];
	$clientSecret = $_POST['client_secret'];
	
	$session = new SpotifyWebAPI\Session(
		$clientId,
		$clientSecret,
		'http://localhost/podcast-importer/wp-admin/admin.php?page=import-spotify'
	);

	$api = new SpotifyWebAPI\SpotifyWebAPI();

	if (isset($_GET['code'])) {
		global $wpdb;
		
		$session->requestAccessToken($_GET['code']);
		//$accessToken = $api->setAccessToken($session->getAccessToken());
		$table_name = 'wp_user_access_token';
		
		$accessToken = $session->getAccessToken();
		$refreshToken = $session->getRefreshToken();
		
		//print_r($accessToken);
		
		$data = $wpdb->get_results( "SELECT * FROM $table_name" );
		
		if(!empty($data)){
			
			$wpdb->query($wpdb->prepare(" UPDATE $table_name SET access_token='$accessToken', refresh_token= '$refreshToken' WHERE id = 1"));
			
		}else{
			
			$wpdb->insert("$table_name", array( "access_token" => $accessToken, "refresh_token" => $refreshToken ));
		}
		
	} else {
		/* $options = [
			'scope' => [
				'user-read-email',
			],
		]; */

		//header('Location: ' . $session->getAuthorizeUrl());
		//die();
	//} */
	
	//print_r($_POST);
		
		/* ////////////////////Get access token //////////////
		$client_id = 'ead8e16aecf1469bb6cfc751a8a10ac2';
		$client_secret = '9853f3bf7c3b4fb4afe1e5dc83b85466'; 

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,            'https://accounts.spotify.com/api/token' );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch, CURLOPT_POST,           1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS,     'grant_type=client_credentials' ); 
		curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Authorization: Basic '.base64_encode($client_id.':'.$client_secret))); 

		echo $result=curl_exec($ch);
		//echo $response = json_decode($result, true);
		
		die;
			////////////////////Get access token //////////////
		*/ 

		/* $client_id = $_POST['client_id'];
		$redirect_url = 'https://zipi.honesttech.co.uk/auth/external/callback';

		$data = array(
			'client_id' => $client_id,
			'response_type' => 'code',
			'redirect_uri' => $redirect_url,
			'show_dialog' => 'true'
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://accounts.spotify.com/authorize' . http_build_query($data));
		curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
		$result=curl_exec($ch);
		print_r($result); */
		
		/* $url = "https://accounts.spotify.com/authorize";
		$code = 'AQBz6gHOMNYA7a-AeDUjR11MwMEucxyyi6sbpir4GIsJGka51msXN6H-g9d-LtWUjUtmAvMIjHP8CO8ymgbIn-pKInP9KLRdb9CX_yW9WKhPaLSCZdwdnPuYQcDyu-Gl_KBiIyAnvhqBb_kP2VvRX2o5MkosB9-7MtFo5x1V-vIUqrquDT1SC1Em05X6zZsflln4A7F-Qx4';
		$curl = curl_init($url);
		$params = [
			"grant_type" => "authorization_code",
			"code" => $code,
			"redirect_uri" => "https://zipi.honesttech.co.uk/auth/external/callback"
		];

		$id = "ead8e16aecf1469bb6cfc751a8a10ac2:9853f3bf7c3b4fb4afe1e5dc83b85466";

		$header = [
			"Accept: application/json",
			"Content-Type: application/x-www-form-urlencoded",
			"Authorization: Basic " . base64_encode($id)
		];

		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		//curl_setopt($get, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($get, CURLOPT_FOLLOWLOCATION, true);
		
		$res = curl_exec($curl);
		print_r($res); */

			/* $url = 'https://accounts.spotify.com/api/token';
			$method = 'POST';

			$credentials = "{ead8e16aecf1469bb6cfc751a8a10ac2:{9853f3bf7c3b4fb4afe1e5dc83b85466}";
			$code ='AQBz6gHOMNYA7a-AeDUjR11MwMEucxyyi6sbpir4GIsJGka51msXN6H-g9d-LtWUjUtmAvMIjHP8CO8ymgbIn-pKInP9KLRdb9CX_yW9WKhPaLSCZdwdnPuYQcDyu-Gl_KBiIyAnvhqBb_kP2VvRX2o5MkosB9-7MtFo5x1V-vIUqrquDT1SC1Em05X6zZsflln4A7F-Qx4';
			//$headers = array(
			//		"Accept: *", 
					"Content-Type: application/x-www-form-urlencoded",
					"User-Agent: runscope/0.1",
					"Authorization: Basic " . base64_encode($credentials));
			$data = array(
					'grant_type' => 'authorization_code',
					'code' => $code,
					'redirect_uri' => 'https://zipi.honesttech.co.uk/auth/external/callback',
			);
			
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

			echo $response = curl_exec($ch); */
}


		
?>