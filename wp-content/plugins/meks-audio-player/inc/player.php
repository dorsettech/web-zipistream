<div class="meks-ap meks-ap-bg">

	<a class="meks-ap-toggle" href="javascript:void(0);">
		<span class="meks-ap-collapse-text"><i class="apf apf-minimize"></i></span>
		<span class="meks-ap-show-text"><i class="apf apf-maximize"></i></span>
	</a>

	<?php do_action('meks_ap_player_before'); //theme can hook here ?>
	<?php $current_id = is_singular() ? get_the_ID() : 0;
			$file_path = get_post_meta($current_id,'audio_file',true);
			$file_path_array = explode('?',$file_path);
			$title= get_the_title($current_id);
			//print_r($file_path);
		if(count($file_path_array) > 1){ ?>
		<div id="meks-ap-player" class="meks-ap-player" data-playing-id="<?php echo esc_attr($current_id); ?>">
		<iframe title="Spotify Embed: 4x22: The Shower Curtain - Let's Not Meet" width="100%" height="232" allowtransparency="true" frameborder="0" allow="encrypted-media" src="<?php echo $file_path; ?>"></iframe>
		</div>
			
		<?php }else{
		?>
	<div id="meks-ap-player" class="meks-ap-player" data-playing-id="<?php echo esc_attr($current_id); ?>">

	</div>
		<?php }?>

	<?php do_action('meks_ap_player_after'); //theme can hook here ?>
  
</div>
		