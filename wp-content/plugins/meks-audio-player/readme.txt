=== Meks Audio Player ===

Contributors: mekshq
Donate link: https://mekshq.com/
Tags: audio, player, podcast, radio, music
Requires at least: 5.0
Tested up to: 5.4
Stable tag: 1.0.1
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html

Easily enhance your podcast, music or any audio files with a full-featured and customizable sticky audio player.

== Description ==

Easily enhance your podcast, music or any audio on the website. Meks Auio Player is first created as a support for our [Megaphone theme](https://mekshq.com/demo/megaphone) but now it can be used on any WordPres website. The plugin will automatically detect audio inside the content and play it in a full-featured sticky audio player. Several smart options are provided to fine-tune the functionality as you wish. Perfect for personal podcasts, podcasting newtorks, radio stations or music websites. 

Meks Audio Player WordPress plugin is created by [Meks](https://mekshq.com)

== Features ==

* Color options to style the player to your personal taste and theme design
* Options to fully customize the player controls that you like to display: play/pause, skip back, jump forward, duration/progress bar, current time, duration time, mute/volume, playback speed...
* No setup required, it simply detects your existing audio inside the content
* Supports WordPress native audio block and audio shortocode
* Works on post and pages as well as all other registered custom post types on the website
* Hooks and filters provided for an elegant way to modify the plugin through your own WordPress theme or a plugin

== More features? ==

By using the plugin with our [Megaphone WordPress theme](https://mekshq.com/demo/megaphone), you can also: 

* Get more styling options
* Display the post title inside the player
* Autodetect third-party embeds (i.e. SoundCloud, Spotify, YouTube, etc...)
* Run the player from any page on the website (not only from single posts but from archives too)



== Installation ==

1. Upload meks-audio-player.zip to plugins via WordPress admin panel or upload unzipped folder to your wp-content/plugins/ folder
2. Activate the plugin through the "Plugins" menu in WordPress
3. In dashboard, go to Settings -> Meks Audio Player to manage the plugin settings

== Frequently Asked Questions ==

For any questions, error reports and suggestions please visit https://mekshq.com/contact/

== Changelog ==

= 1.0.1 =
* Minor JavaScript improvements

= 1.0 =
* Initial release