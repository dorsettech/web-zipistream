(function($) {

    "use strict";

    $(document).ready(function() {

        /* Color picker */
        $('.meks_ap-colors').wpColorPicker();

    });

})(jQuery);