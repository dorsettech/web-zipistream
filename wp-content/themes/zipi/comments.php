<?php if ( post_password_required() ) { return; } ?>

<?php if ( comments_open() || get_comments_number() ) : ?>
    
    <div class="megaphone-comments">

        <div class="megaphone-comment-form mb-24 mb-sm-20 mb-md-30">
            <?php

                comment_form( array(
            	    'title_reply_before' => '<h5 id="reply-title" class="h3 section-title section-title-gray">',
            	    'title_reply'        => __megaphone( 'leave_a_reply' ),
                    'label_submit' => __megaphone( 'comment_submit' ),
                    'cancel_reply_link' => __megaphone( 'comment_cancel_reply' ),
            	    'title_reply_after'  => '</h5>',
                    'comment_notes_before' => '',
                    'comment_notes_after' => '',
                    'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . __megaphone( 'comment_text' ) .'</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true">' .'</textarea></p>',
                ) );
            ?>
        </div>

        <?php if ( have_comments() ) : ?>
            <h5 id="comments" class="h3 section-title section-title-gray mt-24 mt-md-20 mt-sm-10">
                <?php comments_number( __megaphone( 'no_comments' ), __megaphone( 'one_comment' ), __megaphone( 'multiple_comments' ) ); ?>
            </h5>

            <ul class="comment-list">
                <?php $args = array(
                    'avatar_size' => 60,
                    'reply_text' => __megaphone( 'comment_reply' ),
                    'format' => 'html5'
                ); ?>
                <?php wp_list_comments( $args ); ?>
            </ul>

            <?php paginate_comments_links( array(  'prev_text' => '<i class="mf mf-chevron-left"></i>', 'next_text' => '<i class="mf mf-chevron-right"></i>', 'type' => 'list' ) ); ?>
        <?php endif; ?>

    </div>

<?php endif; ?>