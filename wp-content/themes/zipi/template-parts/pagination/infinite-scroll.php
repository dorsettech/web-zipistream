<?php if ( $more_link = get_next_posts_link( __megaphone('load_more') ) ) : ?>
    <div class="col-12 text-center megaphone-order-2">
        <nav class="megaphone-pagination megaphone-infinite-scroll">
            <?php echo wp_kses_post( $more_link ); ?>
	        <div class="megaphone-loader">
				<div class="megaphone-ellipsis"><div></div><div></div><div></div><div></div></div>
	        </div>
        </nav>
    </div>
<?php endif; ?>