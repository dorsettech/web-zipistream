<?php if( $pagination = get_the_posts_pagination( array( 'mid_size' => 2, 'prev_text' => __megaphone( 'previous_posts' ), 'next_text' => __megaphone( 'next_posts' ) ) ) ) : ?>
	    <div class="col-12 text-center megaphone-order-2">
            <nav class="megaphone-pagination">

            	<?php if( !get_previous_posts_link() ) : ?>
            		<a href="javascript:void(0);" class="prev megaphone-button page-numbers disabled"><?php echo __megaphone( 'previous_posts' ); ?></a>
	            <?php endif; ?>

                <?php echo wp_kses_post( $pagination ); ?>

                <?php if( !get_next_posts_link() ) : ?>
            		<a href="javascript:void(0);" class="next megaphone-button page-numbers disabled"><?php echo __megaphone( 'next_posts' ); ?></a>
	            <?php endif; ?>

            </nav>
	    </div>
<?php endif; ?>