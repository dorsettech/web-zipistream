<div class="col-12 text-center megaphone-order-2">
    <nav class="megaphone-pagination prev-next nav-links">
        <div class="prev">
        	<?php if( get_previous_posts_link() ) : ?>
            	<?php previous_posts_link( __megaphone( 'newer_entries' ) ); ?>
            <?php else: ?>
            	<a href="javascript:void(0);" class="megaphone-button disabled"><?php echo __megaphone( 'newer_entries' ); ?></a>
            <?php endif; ?>
        </div>
        <div class="next">
            <?php if( get_next_posts_link() ) : ?>
            	<?php next_posts_link( __megaphone( 'older_entries' ) ); ?>
            <?php else: ?>
            	<a href="javascript:void(0);" class="megaphone-button disabled"><?php echo __megaphone( 'older_entries' ); ?></a>
            <?php endif; ?>
        </div>
    </nav>
</div>
