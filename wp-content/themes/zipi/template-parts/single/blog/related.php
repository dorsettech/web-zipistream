<?php $related = megaphone_get_related(); ?>
<?php if ( $related->have_posts() ) : ?>
    <div class="megaphone-section megaphone-bg-alt-1">
        <div class="container">
            
            <div class="section-content row justify-content-center">

                <div class="section-head col-12 <?php echo esc_attr( megaphone_get_loop_col_class( megaphone_get( 'related_layout' ) ) ); ?>">
                    <h3 class="section-title"><?php echo __megaphone( 'related_blog' ); ?></h3>
                </div>

                <div class="col-12 <?php echo esc_attr( megaphone_get_loop_col_class( megaphone_get( 'related_layout' ) ) ); ?>">
                    <div class="row megaphone-items megaphone-posts">
                        <?php while ( $related->have_posts() ) : $related->the_post(); ?>
                        <?php $layout = megaphone_get_loop_params( megaphone_get( 'related_layout' ), $related->current_post ); ?>
                        <div class="<?php echo esc_attr( $layout['col'] ); ?>">
                            <?php get_template_part( 'template-parts/layouts/blog/' . $layout['style'] ); ?>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>