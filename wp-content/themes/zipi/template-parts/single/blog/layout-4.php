<?php get_template_part( 'template-parts/ads/above-singular' ); ?>

<div class="megaphone-section">

	<div class="megaphone-overlay megaphone-overlay-fallback">

		<div class="overlay-container container-self-center d-flex align-items-center justify-content-start">

			<div class="container">

				<div class="row">

					<div class="entry-header col-12 col-md-8">

						<?php echo megaphone_breadcrumbs(); ?>

						<?php if ( megaphone_get( 'meta_up' ) ) : ?>
							<div class="entry-category">
								<?php echo megaphone_get_meta_data( megaphone_get( 'meta_up' ) ); ?>
							</div>
						<?php endif; ?>

						<?php the_title( '<h1 class="entry-title h2 mb-4 mb-sm-4 mb-md-4">', '</h1>' ); ?>

						<?php if ( megaphone_get( 'meta_down' ) ) : ?>
							<div class="entry-meta separator-before separator-align-left">
								<?php echo megaphone_get_meta_data( megaphone_get( 'meta_down' ) ); ?>
							</div>
						<?php endif; ?>

					</div>

				</div>

			</div>

		</div>

		<div class="entry-media alignfull">
			<?php echo megaphone_get_single_media( 'blog', '', '' ); ?>    
		</div>
			
	</div>

</div>

<?php get_template_part( 'template-parts/single/blog/content' ); ?>