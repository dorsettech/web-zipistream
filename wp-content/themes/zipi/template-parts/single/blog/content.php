<div class="megaphone-section">
    <div class="container">
        <div class="section-content row justify-content-center">
            
            <?php if ( megaphone_has_sidebar( 'left' ) ): ?>
		        <div class="col-12 col-lg-4 megaphone-order-2">
		            <?php get_sidebar(); ?>
		        </div>
    		<?php endif; ?>

            <div class="megaphone-content-post col-12 col-lg-8 megaphone-order-1 megaphone-content-height">

                <?php if ( megaphone_get( 'author' ) == 'above' ): ?>
                    <?php get_template_part( 'template-parts/single/blog/author' ); ?>
                <?php endif; ?>
            
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php if ( megaphone_get( 'headline' ) && has_excerpt() ): ?>
						<div class="entry-summary">
						    <?php the_excerpt(); ?>
						</div>
					<?php endif; ?>

                    <div class="entry-content entry-single clearfix">
                        <?php the_content(); ?>
                        <?php wp_link_pages( array( 'before' => '<div class="paginated-post-wrapper pt-20 clearfix">', 'after' => '</div>' ) ); ?>
                    </div>

                </article>

                <?php if ( megaphone_get( 'tags' ) && has_tag() ) : ?>
                    <div class="entry-tags clearfix mb-50 mb-sm-20 mb-md-30">
                        <?php the_tags( '<span>'.__megaphone( 'tagged_as' ).'</span>', '', '' ); ?>
                    </div>
                <?php endif; ?>

                <?php if ( megaphone_get( 'author' ) == 'bellow' ): ?>
                    <?php get_template_part( 'template-parts/single/blog/author' ); ?>
                <?php endif; ?>

                <?php comments_template(); ?>

            </div>

            <?php if ( megaphone_has_sidebar( 'right' ) ): ?>
                <div class="col-12 col-lg-4 megaphone-order-2">
                    <?php get_sidebar(); ?>
                </div>
            <?php endif; ?>

        </div>

    </div>
</div>

<?php if ( megaphone_get( 'related' ) ): ?>
    <?php get_template_part( 'template-parts/single/'. megaphone_get('type') .'/related' ); ?>
<?php endif; ?>