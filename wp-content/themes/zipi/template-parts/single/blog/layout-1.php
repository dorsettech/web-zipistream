<?php get_template_part( 'template-parts/ads/above-singular' ); ?>

<div class="megaphone-section">
	<div class="container">
		<div class="megaphone-content-alt entry-header mb-40 mb-sm-20 mb-md-30 m-mc text-center">

			<?php echo megaphone_breadcrumbs(); ?>

			<?php if ( megaphone_get( 'meta_up' ) ) : ?>
				<div class="entry-category lh-1 mb-10 mb-sm-10 mb-md-10">
					<?php echo megaphone_get_meta_data( megaphone_get( 'meta_up' ) ); ?>
				</div>
			<?php endif; ?>

			<?php the_title( '<h1 class="entry-title h2 mb-4 mb-sm-4 mb-md-4">', '</h1>' ); ?>

			<?php if ( megaphone_get( 'meta_down' ) ) : ?>
				<div class="entry-meta separator-before separator-align-center">
					<?php echo megaphone_get_meta_data( megaphone_get( 'meta_down' ) ); ?>
				</div>
			<?php endif; ?>

		</div>

		<?php echo megaphone_get_single_media( 'blog', '<div class="entry-media text-center">', '</div>' ); ?>

	</div>
</div>

<?php get_template_part( 'template-parts/single/blog/content' ); ?>