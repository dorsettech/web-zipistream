<style>
.claim_btn{
	background:linear-gradient(to left, #fe004e 0%, #8700cd 100%)!important;
	border-radius:50px;
    font-family: 'avertape-bolduploaded_file';
    color: #fff;
	border:0px solid #fff !important;
    text-transform: uppercase;
    padding-top: 7px;
    padding-bottom: 9px;
    font-size: 11px;
    font-size: 12px!important;
    padding-top: 11px!important;
    padding-bottom: 11px!important;
    padding-left: 30px;
    padding-right: 30px;
	text-decoration:none;
	width:130px;
    letter-spacing:1px;
        margin-bottom:20px;
}
div#claimModal {
    z-index: 99999;
}
#claimModal input.form-control {
    outline: none;
    height: 45px;
    font-size: 15px;
    padding-left: 10px;
}
#claimModal input.form-control:focus{
	box-shadow:none;
	outline: none;
	border:1px solid #444;
}
#claimModal textarea:focus{
	box-shadow:none;
	outline: none;
	border:1px solid #444;
}
#claimModal textarea{
	margin-bottom:20px;
}
#claimModal .modal-body{
	padding:2rem;
}
#claimModal .modal-header{
	background:linear-gradient(to left, #fe004e 0%, #8700cd 100%)!important;
}
#claimModal .modal-header h5{
	color:#fff;
	font-size:18px;
}
#claimModal button.close {
    outline: none;
    color: #fff;
    opacity: 1;
    font-size: 18px;
}
#nameError {
    display:none;
    color:red;
}
#emailError{
    display:none;
    color:red;
}
#emailErrorVaild{
    display:none;
    color:red;
}
#msgError{
    display:none;
    color:red;
}
</style>
<?php $related = megaphone_get_related( 'podcast' ); ?>
<?php if ( $related->have_posts() ) : ?>
    <div class="megaphone-section megaphone-bg-alt-1">
        <div class="container">
            
            <div class="section-content row justify-content-center">

                <div class="section-head col-12 <?php echo esc_attr( megaphone_get_loop_col_class( megaphone_get( 'related_layout' ), 'episode' ) ); ?>">
                    <h3 class="section-title"><?php echo __megaphone( 'related_podcast' ); ?></h3>
                </div>

                <div class="col-12 <?php echo esc_attr( megaphone_get_loop_col_class( megaphone_get( 'related_layout' ), 'episode' ) ); ?>">
                    <div class="row megaphone-items megaphone-posts">
                        <?php while ( $related->have_posts() ) : $related->the_post(); ?>
                        <?php $layout = megaphone_get_loop_params( megaphone_get( 'related_layout' ), $related->current_post, 'episode' ); ?>
                        <div class="<?php echo esc_attr( $layout['col'] ); ?>">
                        <?php// echo 'template-parts/layouts/podcast/' . $layout['style']; ?>
                            <?php get_template_part( 'template-parts/layouts/podcast/' . $layout['style'] ); 
                            if ( is_user_logged_in() ) { ?>
                            <!--a  class="btn btn-primary claim_btn" id="podcastId" data-toggle="modal" data-id="<?php the_ID(); ?>" href="#claimModal">
                                Claim
                            </a-->
                            <?php }?>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

<?php $url = get_template_directory_uri();
         $userId = get_current_user_id();
?>
<!-- Modal -->
<div class="modal fade" id="claimModal" tabindex="-1" role="dialog" aria-labelledby="claimModalLabel" aria-hidden="true" style="top: 100px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="claimModalLabel">Claim Podcast</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method='post' id="claimForm2">
      <div class="form-group">
            
            <input type="text" class="form-control" required name="full_name"  id="full_name" placeholder="Enter Full Name">
            <small id="nameError" >Please fill the full Name</small>
        </div>
        <div class="form-group">
            <input type="email" class="form-control"  required name="email" id="email" aria-describedby="emailHelp" placeholder="Enter Email">
            <small id="emailError" >Please fill the email</small>
            <small id="emailErrorVaild" >Please fill vaild email address</small>
            <input type="hidden" name="podcast_id" id="podcast_id" value="">
            <input type="hidden" name="userId" id="userId" value="<?php echo $userId; ?>">
        </div>
        <div class="form-group">    
            <textarea rows="4" cols="50" name="message" id="message" required  placeholder="Enter message here..."></textarea>
            <small id="msgError" >Please fill the message</small>
        </div>
        <input type="submit" id="submit2" class="btn btn-primary" value='Send'>
    </form>
      </div>
    </div>
  </div>
</div>


<script>

jQuery(document).ready(function() {

 $('#claimModal').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        jQuery('#podcast_id').val(id);
 });

jQuery('#submit2').click(function(e) {
    
    var name= jQuery('#full_name').val();
    var email= jQuery('#email').val();
    var msg= jQuery('#message').val();
  
    if( name == null || name == ''){
        jQuery('#nameError').show();
        setTimeout(function(){
            $('#nameError').fadeOut('slow');
        },2000);
        jQuery("#full_name").focus();
        return false;
    }
    if( email == null || email == ''){
        jQuery('#emailError').show();
        setTimeout(function(){
            $('#emailError').fadeOut('slow');
        },2000);
        jQuery("#email").focus();
        return false;
    }else{
        //return = isValidEmailAddress(email);
        if( !isValidEmailAddress( email ) ) { 
            jQuery('#emailErrorVaild').show();
            setTimeout(function(){
                $('#emailErrorVaild').fadeOut('slow');
            },2000);
            jQuery("#email").focus();
            return false;
        }
    }
    if( msg == null || msg == ''){
        jQuery('#msgError').show();
        setTimeout(function(){
            $('#msgError').fadeOut('slow');
        },2000);
        jQuery("#message").focus();
        return false;
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }

    e.preventDefault();
    var data = jQuery("#claimForm2").serialize()    
    jQuery.ajax({
            type: "POST",
            url: "<?php echo $url?>/sendEmail.php",
            data: {'data': data },
            success: function(msg) {
                //console.log(msg);
                alert(msg);
                $('#claimModal').modal('toggle');
                $( '#claimForm2' ).each(function(){
                    this.reset();
                });
            }
        });
        return false;
    });

});

</script>