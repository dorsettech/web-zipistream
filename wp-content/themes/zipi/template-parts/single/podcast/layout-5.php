<?php get_template_part( 'template-parts/ads/above-singular' ); ?>

<div class="megaphone-section single-layout-5">
	<div class="container">
		<div class="row align-items-center">

			<div class="col-12 col-md-6">
				<?php echo megaphone_get_single_media( 'podcast', '<div class="entry-media entry-media-rounded">', '</div>' ); ?>
			</div>

			<div class="col-12 col-md-6 pl-48">

				<div class="entry-header">

					<?php echo megaphone_breadcrumbs(); ?>

					<?php if ( megaphone_get( 'meta_up' ) ) : ?>
						<div class="entry-category"><?php echo megaphone_get_meta_data( megaphone_get( 'meta_up' ) ); ?></div>
					<?php endif; ?>

					<h1 class="entry-title h2 mb-4 mb-sm-4 mb-md-4"><?php echo megaphone_get_episode_title(); ?></h1>

					<?php if ( megaphone_get( 'meta_down' ) ) : ?>
						<div class="entry-meta separator-before separator-align-left">
							<?php echo megaphone_get_meta_data( megaphone_get( 'meta_down' ) ); ?>
						</div>
					<?php endif; ?>

					<?php if( megaphone_is_meks_ap_active() ): ?>

					<a href="<?php the_permalink(); ?>" data-play-id="<?php the_ID(); ?>" class="megaphone-play megaphone-play-<?php the_ID(); ?> megaphone-play-current player-paceholder player-paceholder-medium-single megaphone-center-play-medium player-placeholder-on-white mt-24 mt-sm-0 mt-md-10">
						<span class="placeholder-slot-l">
							<span class="megaphone-button-play megaphone-button-play-medium"><i class="mf mf-play"></i></span>
						</span>
					
						<span class="placeholder-slot-r">
							<?php if ( megaphone_get( 'show_episode_number' ) ) : ?>
								<?php $episodes_ids = megaphone_get( 'episodes_ids' ); ?>
								<?php if ( $episode_number = megaphone_get_episode_number( $episodes_ids, get_the_ID(), true ) ) : ?>
									<span class="entry-episode megaphone-placeholder-label lh-1">
										<?php echo __megaphone( 'episode' ); ?> <?php echo absint( $episode_number ); ?>
									</span>
								<?php endif; ?>
							<?php endif; ?>
							<h2 class="megaphone-placeholder-title h8"><?php echo __megaphone( 'play_episode' ); ?></h2>
						</span>                        
					</a>

					<?php endif; ?>

				</div>

				<?php if ( megaphone_get( 'podcast_subscribe' ) ) : ?>
					<?php get_template_part( 'template-parts/single/podcast/subscribe' ); ?>
				<?php endif; ?>

			</div>

		</div>
	</div>



</div>
<?php get_template_part( 'template-parts/single/podcast/content' ); ?>