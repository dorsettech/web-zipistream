<?php get_template_part( 'template-parts/ads/above-singular' ); ?>

<div class="megaphone-section megaphone-podcast-single-1">
	<div class="container">
		<div class="megaphone-content-alt entry-header mb-40 mb-sm-20 mb-md-30 m-mc text-center">

			<?php echo megaphone_breadcrumbs(); ?>

			<?php if ( megaphone_get( 'meta_up' ) ) : ?>
				<div class="entry-category lh-1 mb-10 mb-sm-10 mb-md-10"><?php echo megaphone_get_meta_data( megaphone_get( 'meta_up' ) ); ?></div>
			<?php endif; ?>
		
			<h1 class="entry-title h2 mb-4 mb-sm-4 mb-md-4"><?php echo megaphone_get_episode_title(); ?></h1>

			<?php if ( megaphone_get( 'meta_down' ) ) : ?>
				<div class="entry-meta separator-before separator-align-center">
					<?php echo megaphone_get_meta_data( megaphone_get( 'meta_down' ) ); ?>
				</div>
			<?php endif; ?>

		</div>

		<div class="mb-40 mb-sm-20 mb-md-30">
			<div class="megaphone-overlay">

				<?php 
				$fimg = megaphone_get_single_media( 'podcast', '<div class="entry-media">', '</div>' );
				$classes = $fimg ? 'overlay-container container-self-center' : '';
				?>
				<?php if( megaphone_is_meks_ap_active() ): ?>
					<div class="container d-flex align-items-center <?php echo esc_attr( $classes ); ?>">

						<a href="<?php the_permalink(); ?>" data-play-id="<?php the_ID(); ?>" class="megaphone-play megaphone-play-<?php the_ID(); ?> megaphone-play-current player-paceholder player-paceholder-medium-single m-mc megaphone-center-play-medium">
							<span class="placeholder-slot-l">
								<span class="megaphone-button-play megaphone-button-play-medium"><i class="mf mf-play"></i></span>
							</span>

							<span class="placeholder-slot-r">
								<?php if ( megaphone_get( 'show_episode_number' ) ) : ?>
									<?php $episodes_ids = megaphone_get( 'episodes_ids' ); ?>
									<?php if ( $episode_number = megaphone_get_episode_number( $episodes_ids, get_the_ID(), true ) ) : ?>
										<span class="entry-episode megaphone-placeholder-label lh-1">
											<?php echo __megaphone( 'episode' ); ?> <?php echo absint( $episode_number ); ?>
										</span>
									<?php endif; ?>
								<?php endif; ?>
								<h2 class="megaphone-placeholder-title h8"><?php echo __megaphone( 'play_episode' ); ?></h2>
							</span>
						</a>

					</div>

				<?php endif; ?>

				<?php echo megaphone_wp_kses( $fimg ); ?>

			</div>
			<?php if ( megaphone_get( 'podcast_subscribe' ) ) : ?>
				<?php get_template_part( 'template-parts/single/podcast/subscribe' ); ?>
			<?php endif; ?>
		</div>

	</div>
</div>

<?php get_template_part( 'template-parts/single/podcast/content' ); ?>