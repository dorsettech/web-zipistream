<?php ?>
<div class="megaphone-section megaphone-podcast-single-3">

	<div class="megaphone-overlay megaphone-overlay-fallback">

		<div class="overlay-container container-self-center d-flex align-items-center justify-content-center text-center">

			<div class="entry-header megaphone-content-alt">

				<?php echo megaphone_breadcrumbs(); ?>

				<?php if ( megaphone_get( 'meta_up' ) ) : ?>
					<div class="entry-category"><?php echo megaphone_get_meta_data( megaphone_get( 'meta_up' ) ); ?></div>
				<?php endif; ?>

				<h1 class="entry-title h2 mb-4 mb-sm-4 mb-md-4"><?php echo megaphone_get_episode_title(); ?></h1>

				<?php if ( megaphone_get( 'meta_down' ) ) : ?>
					<div class="entry-meta separator-before separator-align-center">
						<?php echo megaphone_get_meta_data( megaphone_get( 'meta_down' ) ); ?>
					</div>
				<?php endif; ?>

				<?php if( megaphone_is_meks_ap_active() ): ?>

					<?php megaphone_play_button('megaphone-play-current megaphone-button mt-24 mt-md-12 mt-sm-10'); ?>
					
				<?php endif; ?>
				<button class="btn"><i class="fa fa-exclamation-circle"></i> Claim</button>
			</div>

		</div>

		<div class="entry-media alignfull">	 
			<?php echo megaphone_get_single_media( 'podcast', '', '' ); ?>
		</div>
		
	</div>
	
	<?php if ( megaphone_get( 'podcast_subscribe' ) ) : ?>
		<?php get_template_part( 'template-parts/single/podcast/subscribe' ); ?>
	<?php endif; ?>
	
</div>

<?php get_template_part( 'template-parts/ads/above-singular' ); ?>

<?php get_template_part( 'template-parts/single/podcast/content' ); ?>