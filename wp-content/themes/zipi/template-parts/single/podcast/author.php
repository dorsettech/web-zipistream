<div class="megaphone-author-box megaphone-bg-alt-1 mb-50 mb-sm-20 mb-md-30">

    <?php if ($co_authors = megaphone_get_coauthors()) : ?>

        <?php if (!empty($co_authors)) : ?>

            <?php foreach ($co_authors as $author) :  ?>

                <div class="author-item">

                <div class="author-header">
                   
                    <div class="author-avatar">
                        <?php echo get_avatar( $author->ID, 60 ); ?>
                    </div>
                    
                    <div class="author-title">
                        <span class="text-small"><?php echo esc_html( $author->type ) == 'guest-author' ? __megaphone('guest') : __megaphone('hosted_by'); ?></span>
                            <h6 class="h8">
                                <?php echo coauthors_posts_links_single($author); ?>
                            </h6>
                            <div class="social-icons-clean section-subnav">
                                <?php echo megaphone_get_co_authors_links($author); ?>
                            </div>
                    </div>

                </div>

                <div class="author-description mt-12 mt-sm-8 mt-md-12">
                    <?php echo wpautop(wp_kses_post($author->description)); ?>
                </div>


                </div>

            <?php endforeach; ?>

        <?php endif; ?>

    <?php else : ?>

        <div class="author-item">

        <div class="author-header">
            
            <div class="author-avatar">
                <?php echo get_avatar(get_the_author_meta('ID'), 60 ); ?>
            </div>

            <div class="author-title">
                <span class="text-small"><?php echo __megaphone('hosted_by'); ?></span>
                <h6 class="h8">
                    <?php global $authordata; ?>
                    <a href="<?php echo esc_url(get_author_posts_url($authordata->ID, $authordata->user_nicename)); ?>">
                        <?php echo get_the_author_meta('display_name'); ?>
                    </a>
                </h6>
                <div class="social-icons-clean section-subnav">
                    <?php echo megaphone_get_author_links(get_the_author_meta('ID'), false); ?>
                </div>
            </div>
        </div>

        <div class="author-description mt-12 mt-sm-8 mt-md-12">
            <?php echo wpautop(get_the_author_meta('description')); ?>
        </div>


        </div>

    <?php endif; ?>

</div>