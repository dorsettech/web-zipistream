<div class="megaphone-section section-color megaphone-section-subscribe megaphone-bg-alt-1">

	<div class="container">

		<div class="section-content row justify-content-center">
		
				<div class="col-12 megaphone-buttons">

					<?php get_template_part( 'template-parts/header/elements/subscribe' ); ?>

				</div>
		</div>

	</div>

</div>