<?php if ( $page->have_posts()  ) : ?>

    <?php while ( $page->have_posts() ) : $page->the_post() ?>
    
            <?php if ( $has_title ) : ?>
                <div class="row">    
                    <div class="section-head col-12">
                        <div class="section-head-l">
                            <h3 class="section-title h2">
                                <?php the_title(); ?>
                            </h3>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            
            <div class="row">
                <div class="col-12 megaphone-items megaphone-shows entry-content">
                    <?php the_content(); ?>
                </div>
            </div>

    <?php endwhile; ?>

<?php endif; ?>