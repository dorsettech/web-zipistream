<div class="megaphone-section <?php echo megaphone_get_background_css_class( megaphone_get( 'posts_bg' ) ); ?>">
    <div class="container">
        <div class="section-content row justify-content-center">
            
            <?php if ( megaphone_has_sidebar( 'left', 'posts_sidebar' ) ): ?>
            <div class="col-12 col-lg-4 megaphone-order-3">
                <?php get_template_part( 'template-parts/front-page/sidebar-posts' ); ?>
            </div>
            <?php endif; ?>
            
            <div class="col-12 megaphone-order-1 <?php echo esc_attr( megaphone_get_loop_col_class( megaphone_get( 'posts_loop' ) ) ); ?>">
                
                <?php if ( megaphone_get( 'posts_has_section_header' ) ) : ?>
                    <div class="row">    
                        <div class="section-head col-12">
                            <div class="section-head-l">
                                <?php if ( megaphone_get( 'posts_display_title' ) ): ?>
                                    <h3 class="section-title h2"><?php echo esc_html( __megaphone('front_page_posts_title') ); ?></h3>
                                    <?php if ( megaphone_get( 'posts_view_all_link' ) ): ?>
                                        <a href="<?php echo megaphone_get('posts_view_all_link_url'); ?>" class="link-uppercase">
                                            <?php echo esc_html( __megaphone('front_page_posts_view_all_link_label') ); ?> <i class="mf mf-chevron-right"></i>
                                        </a>
                                    <?php endif; ?>
                                    <?php endif; ?>
                            </div>
                            <?php if ( megaphone_get( 'posts_slider' ) ): ?>
                                <div class="section-header-r megaphone-slider-nav">
                                    <a href="javascript:void(0);" class="megaphone-button-circle prev">
                                        <i class="mf mf-chevron-left"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="megaphone-button-circle next">
                                        <i class="mf mf-chevron-right"></i>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?> 

                <div class="row megaphone-items megaphone-posts megaphone-content-height <?php echo esc_attr( megaphone_get_slider_class( megaphone_get( 'posts_slider' ) ) ); ?>">
                    <?php $front_page_posts = new WP_Query( megaphone_get( 'posts_query_args' ) ); ?>
                    <?php if ( $front_page_posts->have_posts() ) : ?>
                        <?php while ( $front_page_posts->have_posts() ) : $front_page_posts->the_post(); ?>
                            <?php $layout = megaphone_get_loop_params( megaphone_get( 'posts_loop' ), $front_page_posts->current_post ); ?>
                            <div class="<?php echo esc_attr( $layout['col'] ); ?>">
                                <?php get_template_part( 'template-parts/layouts/blog/' . $layout['style'] ); ?>
                            </div>
                            
                            <?php if( $front_page_posts->current_post === megaphone_get('ads', 'between_position') ) : ?>
                                <?php get_template_part( 'template-parts/ads/between-posts' ); ?>
                            <?php endif; ?>
                            
                        <?php endwhile; ?>
                    <?php else: ?>
                        <div class="col-12">
                            <div class="megaphone-empty-message">
                                <p><?php echo wp_kses_post( sprintf( __( 'Sorry, there are no regular blog posts found. Please make sure you set at least one blog category in <a href="%s"> Theme Options -> Podcast -> General </a>', 'megaphone' ), add_query_arg( array( 'autofocus[section]' => 'megaphone_podcast_general' ), admin_url( 'customize.php' ) ) ) ); ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>
                </div>

            </div>

            <?php if ( megaphone_has_sidebar( 'right', 'posts_sidebar' ) ): ?>
                <div class="col-12 col-lg-4 megaphone-order-3">
                    <?php get_template_part( 'template-parts/front-page/sidebar-posts' ); ?>
                </div>
            <?php endif; ?>

            <?php if ( megaphone_get( 'pagination_section' ) == 'posts' ) : ?>
                <?php 
                    $pagination_type = megaphone_get( 'pagination_type' );
                    $temp_query = $wp_query;
                    $wp_query = $front_page_posts;
                    get_template_part( 'template-parts/pagination/' . $pagination_type );
                    $wp_query = $temp_query;
                ?>
            <?php endif; ?>
            
        </div>
    </div>
</div>