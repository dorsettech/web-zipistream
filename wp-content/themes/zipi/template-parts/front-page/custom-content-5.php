<div class="megaphone-section megaphone-shows <?php echo megaphone_get_background_css_class( megaphone_get( 'custom_content_bg_5' ) ); ?>">
    <div class="container">
        <div class="section-content row justify-content-center">
       
            <div class="col-12 megaphone-order-1">

                <?php if ( !empty( megaphone_get( 'custom_content_page_5' ) ) ) : ?>

                    <?php $page = new WP_Query( array( 'page_id' => megaphone_get( 'custom_content_page_5' ) ) ); ?>
                    <?php $has_title = megaphone_get( 'custom_content_display_title_5' ); ?>
                    <?php include( locate_template( 'template-parts/front-page/custom-content-page.php' ) ); ?>
                
                <?php else : ?>

                    <?php if ( megaphone_get( 'custom_content_display_title_5' ) ): ?>
                        <div class="row">    
                            <div class="section-head col-12">
                                <div class="section-head-l">
                                    <h3 class="section-title h2"><?php echo esc_html( __megaphone('front_page_custom_content_title_5') ); ?></h3>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?> 

                    <div class="row">
                        <div class="col-12 megaphone-items megaphone-shows">
                            <?php echo do_shortcode( __megaphone('front_page_custom_content_5') ); ?>
                        </div>
                    </div>
                  
                <?php endif; ?>

            </div> <!-- end col-12 megaphone-order-1 -->
                
        </div>
    </div>
</div>