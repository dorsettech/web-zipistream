<div class="megaphone-section megaphone-section-featured <?php echo megaphone_get_background_css_class( megaphone_get( 'fa_bg' ) ); ?>">
    <div class="container">
        
        <?php if ( megaphone_get( 'fa_has_section_header' ) ): ?>
            <div class="section-head">
                <div class="section-head-l">
                    <?php if ( megaphone_get( 'fa_display_title' ) ): ?>
                        <h3 class="section-title h2"><?php echo esc_html( __megaphone('front_page_featured_title') ); ?></h3>
                        <?php if ( megaphone_get( 'fa_view_all_link' ) ): ?>
                            <a href="<?php echo megaphone_get('fa_view_all_link_url'); ?>" class="link-uppercase">
                                <?php echo esc_html( __megaphone('front_page_featured_view_all_link_label') ); ?> <i class="mf mf- mf-chevron-right"></i>
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php if ( megaphone_get( 'fa_has_section_header_slider_nav' ) ): ?>
                    <div class="section-header-r megaphone-slider-nav">
                        <a href="javascript:void(0);" class="megaphone-button-circle prev">
                            <i class="mf mf- mf-chevron-left"></i>
                        </a>
                        <a href="javascript:void(0);" class="megaphone-button-circle next">
                            <i class="mf mf- mf-chevron-right"></i>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>   

        <?php $front_page_fa = new WP_Query( megaphone_get( 'fa_query_args' ) ); ?>
            
        <?php if ( $front_page_fa->have_posts() ) : ?>

            <div class="section-content row justify-content-md-center <?php echo esc_attr( megaphone_get_fa_class( megaphone_get( 'fa_loop' ), megaphone_get( 'fa_display_title' ) ) ); ?>">
          
                <?php while ( $front_page_fa->have_posts() ) : $front_page_fa->the_post(); ?>
                    <?php $layout = megaphone_get_loop_params( megaphone_get( 'fa_loop' ), $front_page_fa->current_post, 'fa' ); ?>
                    <div class="<?php echo esc_attr( $layout['col'] ); ?>">
                        <?php get_template_part( 'template-parts/layouts/featured/' . $layout['style'] ); ?>
                    </div>
                <?php endwhile; ?>
            
            </div>

        <?php else: ?>
            <div class="section-content row justify-content-md-center">
                <div class="col-12">
                    <div class="megaphone-empty-message">
                        <p><?php esc_html_e( 'Sorry, there is no content found on this section. Please make sure that you add some posts/episodes.', 'megaphone' ); ?></p>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php wp_reset_postdata(); ?>

        <?php if( megaphone_get( 'fa_subscribe') ) : ?>
            <?php get_template_part( 'template-parts/front-page/subscribe-fa' ); ?>
        <?php endif; ?> 

    </div>
</div>