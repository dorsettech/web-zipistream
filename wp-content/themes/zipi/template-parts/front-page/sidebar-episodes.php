<aside class="megaphone-sidebar row">

    <?php $sidebar = megaphone_get( 'episodes_sidebar' ); ?>

    <?php  if ( isset( $sidebar['classic'] ) && is_active_sidebar( $sidebar['classic'] ) ): ?>
    	<?php dynamic_sidebar( $sidebar['classic'] ); ?>
    <?php endif; ?>

    <?php  if ( isset( $sidebar['sticky'] ) && is_active_sidebar( $sidebar['sticky'] ) ): ?>
	    <div class="megaphone-sticky">
	    	<?php dynamic_sidebar( $sidebar['sticky'] ); ?>
	    </div>
    <?php endif; ?>

</aside>