<div class="megaphone-section megaphone-shows <?php echo megaphone_get_background_css_class( megaphone_get( 'shows_bg' ) ); ?>">
    <div class="container">
        <div class="section-content row justify-content-center">
       
            <?php if ( megaphone_has_sidebar( 'left', 'shows_sidebar' ) ): ?>
                <div class="col-12 col-lg-4 megaphone-order-3">
                    <?php get_template_part( 'template-parts/front-page/sidebar-shows' ); ?>
                </div>
            <?php endif; ?>

            <div class="col-12 megaphone-order-1 <?php echo esc_attr( megaphone_get_loop_col_class( megaphone_get( 'shows_loop' ) ) ); ?>">

                <?php if ( megaphone_get( 'shows_has_section_header' ) ) : ?>
                    <div class="row">    
                        <div class="section-head col-12">
                            <div class="section-head-l">
                                <?php if ( megaphone_get( 'shows_display_title' ) ): ?>
                                    <h3 class="section-title h2"><?php echo esc_html( __megaphone('front_page_shows_title') ); ?></h3>
                                    <?php if ( megaphone_get( 'shows_view_all_link' ) ): ?>
                                        <a href="<?php echo megaphone_get('shows_view_all_link_url'); ?>" class="link-uppercase">
                                            <?php echo esc_html( __megaphone('front_page_shows_view_all_link_label') ); ?> <i class="mf mf-chevron-right"></i>
                                        </a>
                                    <?php endif; ?>
                                    <?php endif; ?>
                            </div>
                            <?php if ( megaphone_get( 'shows_slider' ) ): ?>
                                <div class="section-header-r megaphone-slider-nav">
                                    <a href="javascript:void(0);" class="megaphone-button-circle prev">
                                        <i class="mf mf-chevron-left"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="megaphone-button-circle next">
                                        <i class="mf mf-chevron-right"></i>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?> 

                <?php $front_page_shows = megaphone_get( 'shows_query' ); ?>
                    
                <?php if ( !empty( $front_page_shows ) ) : ?>

                    <div class="row megaphone-items megaphone-shows megaphone-content-height <?php echo esc_attr( megaphone_get_slider_class( megaphone_get( 'shows_slider' ) ) ); ?>">
                        
                        <?php foreach ( $front_page_shows as $show ) : ?>
                            <?php $layout = megaphone_get_loop_params( megaphone_get( 'shows_loop' ) ); ?>
                            <div class="<?php echo esc_attr( $layout['col'] ); ?>">
                                <?php include( locate_template('template-parts/layouts/show/' . $layout['style'] . '.php') ); ?>
                            </div>
                        <?php endforeach; ?>
                        
                    </div>

                <?php else: ?>

                    <div class="megaphone-empty-message">
                        <p><?php esc_html_e( 'Sorry, there is no content found on this section. Please make sure that you add some posts/episodes.', 'megaphone' ); ?></p>
                    </div>
                        
                <?php endif; ?>

            </div>
                
            <?php if ( megaphone_has_sidebar( 'right', 'shows_sidebar' ) ): ?>
                <div class="col-12 col-lg-4 megaphone-order-3">
                    <?php get_template_part( 'template-parts/front-page/sidebar-shows' ); ?>
                </div>
            <?php endif; ?>
                
        </div>
    </div>
</div>