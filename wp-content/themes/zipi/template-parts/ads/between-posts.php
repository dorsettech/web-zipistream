<?php if( $ad = megaphone_get('ads', 'between_posts') ): ?>
	<div class="col-12 megaphone-ad ad-between-posts d-flex justify-content-center vertical-gutter-flow"><?php echo do_shortcode( $ad ); ?></div>
<?php endif; ?>