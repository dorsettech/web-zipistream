<?php if( $ad = megaphone_get('ads', 'above_footer') ): ?>
	<div class="container">
	    <div class="megaphone-ad ad-above-footer d-flex justify-content-center vertical-gutter-flow mt-50 mt-sm-25 mt-md-30"><?php echo do_shortcode( $ad ); ?></div>
	</div>
<?php endif; ?>