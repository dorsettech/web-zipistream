<?php if( $ad = megaphone_get('ads', 'above_singular') ): ?>
	<div class="container">
		<div class="megaphone-ad ad-above-singular d-flex justify-content-center vertical-gutter-flow mt-50 mt-sm-25 mt-md-30"><?php echo do_shortcode( $ad ); ?></div>
	</div>
<?php endif; ?>