<?php if( $ad = megaphone_get('ads', 'header') ): ?>
	<div class="megaphone-ad ad-header"><?php echo do_shortcode( $ad ); ?></div>
<?php endif; ?>