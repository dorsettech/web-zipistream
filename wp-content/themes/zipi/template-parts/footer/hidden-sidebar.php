<div class="megaphone-sidebar megaphone-sidebar-hidden">

	<div class="megaphone-sidebar-branding">
	    <?php echo megaphone_get_branding( 'sidebar' ); ?>
	    <span class="megaphone-action-close"><i class="mf mf-close" aria-hidden="true"></i></span>
	</div>	

	<?php $display_class = megaphone_get('header', 'nav') ? 'd-md-block d-lg-none' : ''; ?>
	<div class="megaphone-menu-mobile widget <?php echo esc_attr( $display_class ); ?>">
		<div class="widget-inside <?php echo esc_attr( megaphone_get_background_css_class( megaphone_get_option( 'widget_bg' ) )); ?>">
		<h4 class="widget-title"><?php echo __megaphone('menu_label'); ?></h4>
			<?php get_template_part('template-parts/header/elements/menu-primary'); ?>
			<?php if( megaphone_get('header', 'menu_elements_responsive') ): ?>
                <?php foreach( megaphone_get('header', 'menu_elements_responsive') as $element ): ?>
                    <?php get_template_part('template-parts/header/elements/' . $element ); ?>
                <?php endforeach; ?>
            <?php endif; ?>
		</div>
	</div>

	<?php if ( is_active_sidebar('megaphone_sidebar_hidden') ) : ?>
	    <?php dynamic_sidebar( 'megaphone_sidebar_hidden' ); ?>
    <?php endif; ?>

</div>