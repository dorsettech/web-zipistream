<ul class="megaphone-player-action list-reset">
	<li class="mejs-button"><button type="button" title="share" aria-label="share" tabindex="0"><i class="mf mf-social"></i></button>
		<?php if( function_exists('meks_ess_share') ) : ?> 
			<?php meks_ess_share(); ?>
		<?php endif; ?>
	</li>
</ul>