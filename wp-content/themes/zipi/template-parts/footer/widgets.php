<?php $footer_widgets = megaphone_get( 'footer', 'widgets' ); ?>

<?php if ( !empty( $footer_widgets ) ): ?>
	
	<div class="row megaphone-footer-widgets justify-content-center <?php echo megaphone_get( 'footer','widgets_class');?>">
	    <?php foreach ( $footer_widgets as $i => $column ) :?>
	        <?php if ( is_active_sidebar( 'megaphone_sidebar_footer_'.( $i+1 ) ) ): ?>
			
				 <div class="col-12 col-md-6 col-lg-3 <?php echo esc_attr( 'col-lg-' . $column ); ?>">
					<?php dynamic_sidebar( 'megaphone_sidebar_footer_'.( $i+1 ) ); ?>
				</div>
				  
	        <?php endif; ?>
	    <?php endforeach; ?>
	</div>
	
<?php endif; ?>