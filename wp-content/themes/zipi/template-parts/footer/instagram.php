<?php if ( class_exists( 'Meks_Instagram_Widget' ) ): ?>
    
    <?php

        $instagram_username = str_replace('@', '', megaphone_get('footer', 'instagram') );
        $instagram_title = __megaphone('instagram_follow') . ' ' . '<a class="megaphone-button megaphone-button-hollow" href="https://www.instagram.com/'.$instagram_username.'" target="_blank">@'.$instagram_username.'</a>';
    
        the_widget(
                    'Meks_Instagram_Widget',
                    array(
                        'title' => ' ',
                        'username_hashtag' => megaphone_get('footer', 'instagram'),
                        'photos_number' => 12,
                        'columns' => 6,
                        'photo_space' => 10,
                        'container_size' => 1200,
                        'link_text' => '',
                    ),
                    array(
                        'before_widget' => '<div id="%1$s" class="megaphone-section-instagram megaphone-section megaphone-bg-alt-1"><div class="container"><div class="megaphone-section-separator"></div>',
                        'after_widget' => '</div></div>',
                        'before_title' => '<div class="section-head"><h3 class="section-title h2">' . $instagram_title,
                        'after_title' => '</h3></div>',
                    )
                ); 
    ?>

<?php else: ?>

    <?php if( current_user_can( 'manage_options' ) ): ?>
        <div class="megaphone-section-instagram megaphone-section megaphone-bg-alt-1">
            <div class="container">
                <div class="megaphone-empty-message">
                    <p><?php echo wp_kses_post( sprintf( __( 'In order to use the Instagram feature, please install and activate <a href="%s">Meks Instagram Widget plugin </a>.', 'megaphone' ), admin_url( 'themes.php?page=megaphone-plugins' ) ) ); ?></p>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php endif; ?>