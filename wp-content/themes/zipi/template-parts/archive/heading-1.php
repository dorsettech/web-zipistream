<div class="megaphone-section megaphone-archive-1 overlay-small-md-height">

	<div class="<?php echo megaphone_get( 'archive_img_class' ); ?>">

		<?php if ( megaphone_get( 'archive_content' ) ): ?>

		<div class="overlay-container container-self-center d-flex align-items-center justify-content-start">

			<div class="container">
				<div class="row">
					<div class="col-lg-8">

						<div class="archive-section-head megaphone-content-alt">

							<?php echo megaphone_breadcrumbs(); ?>

							<?php if ( megaphone_get( 'archive_avatar' ) ) : ?>
							<span><?php echo megaphone_get( 'archive_avatar' ); ?></span>
							<?php endif; ?>

							<?php if ( megaphone_get( 'archive_title' ) ) : ?>
							<h1 class="h2 lh-1"><?php echo megaphone_get( 'archive_title' ); ?></h1>
							<?php endif; ?>

							<?php if ( megaphone_get( 'archive_meta' ) ) : ?>
							<span class="section-meta separator-before separator-align-left">
								<?php echo megaphone_get( 'archive_meta' ); ?>
								<?php echo megaphone_get('archive_type') == 'podcast' ? __megaphone( 'episodes' ) :  __megaphone( 'articles' ); ?>
							</span>
							<?php endif; ?>

							<?php if ( megaphone_get( 'archive_subnav' ) ) : ?>
							<span class="section-subnav social-icons-clean">
								<?php echo megaphone_get( 'archive_subnav' ); ?>
							</span>
							<?php endif; ?>

							<?php if (  megaphone_get( 'archive_description' ) ) : ?>
							<div class="section-description megaphone-content paragraph-small mt-20 mt-sm-10 mt-md-20">
								<?php echo wpautop( megaphone_get( 'archive_description' ) ); ?></div>
							<?php endif; ?>


						</div>

					</div>
				</div>
			</div>

		</div>

		<?php endif; ?>

		<?php if( $fimg = megaphone_get( 'featured_image' ) ) : ?>
		    <div class="entry-media alignfull"><?php echo wp_kses_post( $fimg ); ?></div>
		<?php endif; ?>

	</div>
</div>
