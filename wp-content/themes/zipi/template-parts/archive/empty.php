<div class="col-12">
	<article <?php post_class( 'megaphone-post' ); ?>>
	        <div class="entry-content">
	            <?php if ( is_search() ) : ?>
						<p><?php echo __megaphone( 'content_none_search' ); ?></p>
				<?php else: ?>
					<p><?php echo __megaphone( 'content_none' ); ?></p>
				<?php endif; ?>
	        </div>
	</article>
</div>
