<div class="megaphone-section section-archive">
    <?php get_template_part('template-parts/archive/heading-' . megaphone_get('layout')); ?>
    <?php get_template_part('template-parts/ads/above-archive'); ?>
    <div class="container">
    	<?php if( megaphone_get('archive_type') == 'blog'): ?>
        <?php get_template_part('template-parts/archive/loop'); ?>
        <?php else: ?>
        <?php get_template_part('template-parts/archive/loop-podcast'); ?>
        <?php endif; ?>
    </div>
</div>