<div class="section-content row justify-content-center">

    <?php if ( megaphone_has_sidebar( 'left' ) ): ?>
        <div class="col-12 col-lg-4 megaphone-order-3">
            <?php get_sidebar(); ?>
        </div>
    <?php endif; ?>

    <div class="col-12 megaphone-order-1 megaphone-content-height <?php echo esc_attr( megaphone_get_loop_col_class( megaphone_get( 'loop' ) ) ); ?>">
        <div class="row megaphone-items megaphone-posts">
            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>
                    
                    <?php $layout = megaphone_get_loop_params( megaphone_get( 'loop' ), $wp_query->current_post ); ?>

                    <div class="<?php echo esc_attr( $layout['col'] ); ?>">
                        <?php include( locate_template( 'template-parts/layouts/blog/'. $layout['style'] . '.php' ) ); ?>
                    </div>

                    <?php if( $wp_query->current_post === megaphone_get('ads', 'between_position') ) : ?>
                        <?php get_template_part( 'template-parts/ads/between-posts' ); ?>
                    <?php endif; ?>

                <?php endwhile; ?>
            <?php else: ?>
                <?php get_template_part( 'template-parts/archive/empty' ); ?>
            <?php endif; ?>
        </div>
    </div>

    <?php if ( megaphone_has_sidebar( 'right' ) ): ?>
        <div class="col-12 col-lg-4 megaphone-order-3">
            <?php get_sidebar(); ?>
        </div>
    <?php endif; ?>

    <?php get_template_part( 'template-parts/pagination/'. megaphone_get( 'pagination' ) ); ?>

</div>