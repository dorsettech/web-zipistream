<div class="header-middle">
	<div class="container d-flex justify-content-center align-items-center">
		<div class="slot-c">
			<?php get_template_part('template-parts/header/elements/branding'); ?>
		</div>
	</div>
</div>
<div class="header-bottom">
	<div class="container d-flex justify-content-center align-items-center">
	
		<div class="slot-c">
			<?php if( megaphone_get('header', 'nav') ): ?>
				<?php get_template_part('template-parts/header/elements/menu-primary'); ?>
			<?php endif; ?>
		</div>

		<div class="slot-r">
			<?php if( megaphone_get('header', 'actions') ): ?>
				<?php foreach( megaphone_get('header', 'actions') as $element ): ?>
					<?php get_template_part('template-parts/header/elements/' . $element ); ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>

	</div>
</div>