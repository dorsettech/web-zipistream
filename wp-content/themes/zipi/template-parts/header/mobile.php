<div class="megaphone-header header-mobile d-lg-none">
	<div class="container d-flex justify-content-between align-items-center">
		<div class="slot-l">
			<?php get_template_part( 'template-parts/header/elements/branding-mobile' ); ?>
		</div>
		<div class="slot-r">
			<?php if ( megaphone_get( 'header', 'elements_responsive' ) ) : ?>
				<?php foreach ( megaphone_get( 'header', 'elements_responsive' ) as $element ) : ?>
					<?php get_template_part( 'template-parts/header/elements/' . $element ); ?>
				<?php endforeach; ?>
			<?php endif; ?>
			<?php get_template_part( 'template-parts/header/elements/hamburger' ); ?>
		</div>
	</div>
</div>