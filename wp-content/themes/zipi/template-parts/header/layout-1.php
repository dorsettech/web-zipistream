<div class="header-middle">
	<div class="container d-flex justify-content-between align-items-center">
		<div class="slot-l">
			<?php get_template_part('template-parts/header/elements/branding'); ?>
		</div>
		<div class="slot-r">
			<?php if( megaphone_get('header', 'nav') ): ?>
            	<?php get_template_part('template-parts/header/elements/menu-primary'); ?>
            <?php endif; ?>
			<?php if( megaphone_get('header', 'actions') ): ?>
            	<?php foreach( megaphone_get('header', 'actions') as $element ): ?>
             		<?php get_template_part('template-parts/header/elements/' . $element ); ?>
             	<?php endforeach; ?>
			<?php endif; ?>
			<ul class="userOptions">
                <a href="/myaccount/"><li class="userIcon dashboard"> Dashboard</li></a>
                <a href="/view-podcast"><li class="userIcon podcasts"> Podcasts</li></a>
                <!--a ><li class="userIcon signinAccount">Login</li></a-->
				<?php if(!is_user_logged_in()){?><a href="/myaccount/"><li class="userIcon signinAccount1">Login</li></a> <?php }?>
                <a href="/my-account/customer-logout/" class="signoutAccount"> <li class="userIcon signoutAccount"> Log out</li></a>
            </ul>
		</div>
	</div>
</div>

