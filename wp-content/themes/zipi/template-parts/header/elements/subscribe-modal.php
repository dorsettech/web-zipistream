<ul class="megaphone-menu-action megaphone-modal-opener megaphone-subscribe">
	<li>
		<a href="javascript:void(0);">
			<span class="header-el-label"><?php echo __megaphone( 'subscribe_title' ); ?></span>
			<i class="mf mf-subscribe"></i>
		</a>
	</li>
</ul>
<div class="megaphone-modal">
	<a href="javascript:void(0);" class="megaphone-modal-close"><i class="mf mf-close"></i></a>
	<div class="megaphone-content-alt">
		
		<?php $subscribe_title = __megaphone( 'subscribe_title' ); ?>
		<?php if ( $subscribe_title ) : ?>
			<div class="section-head">
				<h5 class="section-title h2"><?php echo esc_html( $subscribe_title ); ?></h5>
			</div>
		<?php endif; ?>

		<div class="megaphone-buttons">
			<?php get_template_part( 'template-parts/header/elements/subscribe' ); ?>
		</div>
	</div>
</div>
