<?php if ( has_nav_menu( 'megaphone_menu_primary' ) ) : ?>
    <?php wp_nav_menu( array( 'theme_location' => 'megaphone_menu_primary', 'container'=> 'nav', 'menu_class' => 'megaphone-menu megaphone-menu-primary',  ) ); ?>
<?php else: ?>
    <nav>
	    <?php megaphone_menu_placeholder( esc_html__('Set Main menu', 'megaphone' ) ); ?>
	</nav>
<?php endif; ?>
