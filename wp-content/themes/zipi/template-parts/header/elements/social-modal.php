<ul class="megaphone-menu-action megaphone-modal-opener megaphone-social">
	<li><a href="javascript:void(0);"><span class="header-el-label"><?php echo __megaphone( 'social_label' ); ?></span><i class="mf mf-social"></i></a></li>
</ul>
<div class="megaphone-modal">
	<a href="javascript:void(0);" class="megaphone-modal-close"><i class="mf mf-close"></i></a>
	<div class="megaphone-content">

	<div class="section-head">
        <h5 class="section-title h2"><?php echo __megaphone( 'social_label' ); ?></h5>
	</div>	
		
	<?php get_template_part('template-parts/header/elements/social'); ?>
	</div>
</div>