<?php if ( has_nav_menu( 'megaphone_menu_donate' ) ): ?>
    <?php
	wp_nav_menu(
		array(
			'theme_location' => 'megaphone_menu_donate',
			'container'      => '',
			'items_wrap' => '<ul class="%2$s">%3$s</ul>',
			'menu_class'     => 'megaphone-menu-donate megaphone-menu-action',
		)
	);
?>
<?php else: ?>
	<?php megaphone_menu_placeholder( esc_html__('Set Donate menu', 'megaphone' ) ); ?>
<?php endif; ?>
