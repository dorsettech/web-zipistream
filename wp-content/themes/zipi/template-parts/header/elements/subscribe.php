<?php if ( has_nav_menu( 'megaphone_menu_subscribe' ) ): ?>
    <?php
	wp_nav_menu(
		array(
			'theme_location' => 'megaphone_menu_subscribe',
			'container'      => '',
			'items_wrap' => '<ul id="%1$s" class="%2$s"><li class="header-el-label">'.__megaphone( 'subscribe_title' ).'</li>%3$s</ul>',
			'menu_class'     => 'megaphone-menu megaphone-menu-subscribe list-reset d-flex flex-wrap',
			'link_before'    => '<span>',
			'link_after'     => '</span>'
		)
	);
?>
<?php else: ?>
	<?php megaphone_menu_placeholder( esc_html__('Set Subscribe menu', 'megaphone' ) ); ?>
<?php endif; ?>
