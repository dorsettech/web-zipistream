<?php if(has_nav_menu('megaphone_menu_secondary_1')): ?>
	<?php 
		wp_nav_menu( array(
	            'theme_location' => 'megaphone_menu_secondary_1',
	            'container'=> 'nav',
	            'menu_class' => 'megaphone-menu megaphone-menu-secondary-1' ) ); 
	 ?>
<?php else: ?>
	<nav>
		<?php megaphone_menu_placeholder( esc_html__('Set Secondary menu', 'megaphone' ) ); ?>
	</nav>
<?php endif; ?>
