<?php if ( has_nav_menu( 'megaphone_menu_social' ) ): ?>
    <?php
	wp_nav_menu(
		array(
			'theme_location' => 'megaphone_menu_social',
			'container'      => '',
			'items_wrap' => '<ul id="%1$s" class="%2$s"><li class="header-el-label">'.__megaphone( 'social_label' ).'</li>%3$s</ul>',
			'menu_class'     => 'megaphone-menu megaphone-menu-social',
			'link_before'    => '<span>',
			'link_after'     => '</span>'
		)
	);
?>
<?php else: ?>
	<?php megaphone_menu_placeholder( esc_html__('Set Social menu', 'megaphone' ) ); ?>
<?php endif; ?>
