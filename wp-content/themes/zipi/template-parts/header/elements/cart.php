<?php $cart_elements = megaphone_woocommerce_cart_elements(); ?>
<ul class="megaphone-menu-action megaphone-cart">
	<li>
		<a href="<?php echo esc_url( $cart_elements['cart_url'] ); ?>"><span class="header-el-label"><?php echo __megaphone( 'cart_label' ); ?></span><i class="mf mf-cart"></i></a>
		<?php if ( $cart_elements['products_count'] > 0 ) : ?>
			<span class="megaphone-cart-count"><?php echo absint( $cart_elements['products_count'] ); ?></span>
		<?php endif; ?>
	</li>
</ul>