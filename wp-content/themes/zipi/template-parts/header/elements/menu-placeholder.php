<?php if ( current_user_can( 'manage_options' ) ): ?>
    <ul class="megaphone-menu list-reset empty-list mb-0">
        <li>
            <a href="<?php echo esc_url( admin_url( 'nav-menus.php' )); ?>">
			   <?php echo esc_html( $label ); ?>
			</a>
        </li>
    </ul>
<?php endif; ?>
