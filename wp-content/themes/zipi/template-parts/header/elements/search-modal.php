<ul class="megaphone-menu-action megaphone-modal-opener megaphone-search">
	<li><a href="javascript:void(0);"><span class="header-el-label"><?php echo __megaphone( 'search_label' ); ?></span><i class="mf mf-search"></i></a></li>
</ul>
<div class="megaphone-modal search-alt">
	<a href="javascript:void(0);" class="megaphone-modal-close"><i class="mf mf-close"></i></a>
	<div class="megaphone-content">
	<div class="section-head">
        <h5 class="section-title searchTitle h2">ZipiStream Search</h5>
	</div>
        <p class="searchSub">Enhance your listening experience. Find the best podcasts, search by popularity or subject.</p>

	<?php //get_search_form(); 
		echo do_shortcode('[wpdreams_ajaxsearchlite]');
	?>
	</div>
</div>
