<div class="megaphone-header header-sticky">
	<div class="header-sticky-main">
		<div class="container d-flex justify-content-center align-items-center">
			<div class="slot-l">
				<?php get_template_part( 'template-parts/header/elements/branding-sticky' ); ?>
			</div>
			<div class="slot-c">
				<div class="d-none d-lg-flex">
					<?php if ( megaphone_get( 'header', 'nav' ) ) : ?>
						<?php get_template_part( 'template-parts/header/elements/menu-primary' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="slot-r">
				<div class="d-none d-lg-flex">
					<?php if ( megaphone_get( 'header', 'actions' ) ) : ?>
						<?php foreach ( megaphone_get( 'header', 'actions' ) as $element ) : ?>
							<?php get_template_part( 'template-parts/header/elements/' . $element ); ?>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<div class="d-flex d-lg-none">
					<?php if ( megaphone_get( 'header', 'elements_responsive' ) ) : ?>
						<?php foreach ( megaphone_get( 'header', 'elements_responsive' ) as $element ) : ?>
							<?php get_template_part( 'template-parts/header/elements/' . $element ); ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php get_template_part( 'template-parts/header/elements/hamburger' ); ?>
				</div>
			</div>
		</div>
	</div>
</div>