<?php
$podcast = megaphone_get('podcast');
$episodes_query = new WP_Query(
	array(
		'ignore_sticky_posts' => '1',
		'posts_per_page' => $display['episodes_limit'],
		'order'          => megaphone_get_option( 'episodes_post_order' ),
		'post_type'   	 => $podcast['post_type'],
		'tax_query'      => array(
			array(
				'taxonomy' => $podcast['taxonomy'],
				'field'    => 'term_id',
				'terms'    => $show->term_id,
			)
		)
	)
);
?>

<?php if ( $episodes_query->have_posts() ) : ?>

	<div class="megaphone-shows-list megaphone-shows-list-small">

		<?php
		while ( $episodes_query->have_posts() ) :
			$episodes_query->the_post();
			?>
			<article <?php post_class('megaphone-show-item')?>>
				<?php if ( $display['play_btn'] ) : ?>
					<?php megaphone_play_button( 'megaphone-button-play megaphone-button-play-small megaphone-center-play-medium', false ); ?>
				<?php endif; ?>
				<div class="megaphone-show-header">
					<?php 
						$episodes_ids = megaphone_get( 'episodes_ids' ); 
						$sponsored = megaphone_is_sponsored_episode() ? megaphone_get_meta_data( array('sponsored') ) : ''; 
					?>
					<?php if ( $display['episode_number'] && $episode_number = megaphone_get_episode_number( $episodes_ids[$show->term_id], get_the_ID() ) ) : ?>
						<?php echo wp_kses_post( $sponsored ); ?>
						<span class="entry-episode">
							<?php echo __megaphone( 'episode' ); ?> <?php echo absint( $episode_number ); ?>
						</span>
					<?php endif; ?>
					<h3 class="entry-title">
						<a href="<?php the_permalink(); ?>"><?php echo megaphone_get_episode_title(); ?></a>
					</h3>
				</div>
			</article>
		<?php endwhile; ?>

	</div>

<?php endif; ?>

<?php wp_reset_postdata(); ?>