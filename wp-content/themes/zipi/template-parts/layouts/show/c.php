<?php $display = megaphone_get_show_layout_options(); ?>

<div class="megaphone-item megaphone-show megaphone-show-c show-item show-item-<?php echo esc_attr($show->term_id) . ' ' . esc_attr($display['duotone_overlay']); ?>">

    <?php if ($display['featured_image']) : ?>
        <div class="megaphone-overlay mb-32 mb-sm-15 mb-md-20">
            <div class="overlay-container container-self-center d-flex align-items-center justify-content-center text-center">
                <div class="megaphone-content">
                    <h2 class="h4"><a href="<?php echo get_category_link($show->term_id); ?>"><?php echo esc_html($show->name); ?></a></h2>
                    <?php if ($display['meta']) : ?>
                        <div class="entry-meta separator-before">
                            <span class="meta-item"><?php echo esc_html($show->count); ?> <?php echo __megaphone('episodes'); ?></span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="entry-media">
                <a href="<?php echo get_category_link($show->term_id); ?>"><?php echo megaphone_get_category_featured_image('megaphone-show', $show->term_id); ?></a>
            </div>
        </div>
    <?php else : ?>
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-lg-12 mb-20 mb-sm-10 mb-md-20">
                <h2 class="h5 shows-title"><a href="<?php echo get_category_link($show->term_id); ?>"><?php echo esc_html($show->name); ?></a></h2>
                <?php if ($display['meta']) : ?>
                    <div class="entry-meta separator-before">
                        <span class="meta-item"><?php echo esc_html($show->count); ?> <?php echo __megaphone('episodes'); ?></span>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="row justify-content-start">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="pl-40 pr-40 pr-sm-20 pl-sm-20 pl-md-20 pr-md-20">
                <?php if ($display['description'] && !empty($show->description)) : ?>
                    <div class="entry-content mb-30 mb-sm-20 paragraph-small">
                        <?php echo wp_kses_post( megaphone_get_category_description( $show->description ) ); ?>
                    </div>
                <?php endif; ?>

                <?php if ( $display['episodes'] ) : ?>
                    <?php include( locate_template('template-parts/layouts/show/episodes.php') ); ?>
                <?php endif; ?>

                <?php if ( $display['episode_view_all'] ) : ?>
                    <div class="entry-footer mt-30 mt-sm-20 mt-md-20 separator-before">
                        <a class="megaphone-link-special" href="<?php echo get_category_link( $show->term_id ); ?>"><?php echo __megaphone( 'view_all_episodes' ); ?><i class="mf mf-chevron-right"></i></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>