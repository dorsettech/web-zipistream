<?php $display = megaphone_get_episode_layout_options('f'); ?>

<article <?php post_class('megaphone-item megaphone-post megaphone-layout-f mb-50 mb-sm-20 mb-md-30 '.$display['featured_image_class'].' '); ?>>
    <div class="row justify-content-center">

        <div class="col-5"> 
            <div class="entry-media">
                <?php if ( $display['featured_image'] ): ?>
                    <a href="<?php the_permalink(); ?>"><?php echo megaphone_wp_kses( $display['featured_image'] ); ?>
                        <?php if ( $display['episode_number']  ) : ?>
                            <?php $episodes_ids = megaphone_get( 'episodes_ids' ); ?>
                            <?php if ( $episode_number = megaphone_get_episode_number( $episodes_ids, get_the_ID(), true ) ) : ?>
                                <span class="entry-episode">
                                    <span class="episode-item">
                                        <?php echo __megaphone( 'episode' ); ?> 
                                        <strong><?php echo absint( $episode_number ); ?></strong>
                                    </span>
                                </span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </a>
                <?php else: ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php if ( $display['episode_number']  ) : ?>
                            <?php $episodes_ids = megaphone_get( 'episodes_ids' ); ?>
                            <?php if ( $episode_number = megaphone_get_episode_number( $episodes_ids, get_the_ID(), true ) ) : ?>
                                <span class="megaphone-show-episode-number">
                                    <strong><?php echo absint( $episode_number ); ?></strong>
                                    <?php echo __megaphone( 'episode' ); ?> 
                                </span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </a>                
                <?php endif; ?>

                <?php if ( $display['play_icon']  ) : ?>
                    <?php megaphone_play_button( 'megaphone-button-play megaphone-button-play-white megaphone-button-play-medium megaphone-mega-menu-play', false ); ?>
                <?php endif; ?>

            </div>
        </div>

        <div class="col-7">            
        
            <div class="entry-header mb-16 mb-sm-8 mb-md-10">
                
                <?php if( $display['meta_up'] ): ?>
                    <div class="entry-category"><?php echo megaphone_get_meta_data( $display['meta_up'] ); ?></div>
                <?php endif; ?>
                
                <h2 class="entry-title h4 mb-4 mb-sm-4 mb-md-4"><a href="<?php the_permalink(); ?>"><?php echo megaphone_get_episode_title(); ?></a></h2>
                
                <?php if( $display['meta_down'] ): ?>
                    <div class="entry-meta">
                        <?php echo megaphone_get_meta_data( $display['meta_down'] ); ?>
                    </div>
                <?php endif; ?>

            </div>

            <?php if( $display['play_btn'] ): ?>
                <div class="entry-footer mt-24 mt-sm-8 mt-md-20">
                   <?php megaphone_play_button( 'megaphone-button megaphone-button-medium megaphone-button-hollow' ); ?>
                </div>
            <?php endif; ?>

        </div>
        
	</div>
</article>