<?php $display = megaphone_get_episode_layout_options( 'g' ); ?>

<article <?php post_class( 'megaphone-item megaphone-layout-g megaphone-shows-list megaphone-shows-list-medium megaphone-show-item' ); ?>>

	<?php if ( $display['play_btn'] ) : ?>
		<?php megaphone_play_button( 'megaphone-button-play megaphone-button-play-medium megaphone-center-play-medium', false ); ?>
	<?php endif; ?>

	<div class="megaphone-show-header">
		<?php if ( $display['meta_up'] ) : ?>
			<?php echo megaphone_get_meta_data( $display['meta_up'] ); ?>
		<?php endif; ?>
		<?php if ( $display['episode_number'] ) : ?>
			<?php $episodes_ids = megaphone_get( 'episodes_ids' ); ?>
			<?php if ( $episode_number = megaphone_get_episode_number( $episodes_ids, get_the_ID(), true ) ) : ?>
				<span class="entry-episode"><?php echo __megaphone( 'episode' ); ?> <?php echo absint( $episode_number ); ?></span>
			<?php endif; ?>
		<?php endif; ?>
		<h2 class="h7 entry-title"><a href="<?php the_permalink(); ?>"><?php echo megaphone_get_episode_title(); ?></a></h2>
	</div>

</article>
