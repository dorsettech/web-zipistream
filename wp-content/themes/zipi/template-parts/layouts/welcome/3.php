<?php if ( megaphone_get( 'wa_display_title' ) ): ?>
		<div class="container">
			<div class="section-head">
					<div class="section-head-l">
						<h3 class="section-title h2"><?php echo esc_html( __megaphone('front_page_wa_title') ); ?></h3>
					</div>
			</div>
		</div>
<?php endif; ?>  

<div class="megaphone-section megaphone-section-welcome wa-layout wa-layout-3 d-flex <?php echo megaphone_get_background_css_class( megaphone_get( 'wa_bg' ) ); ?>">
    
	<div class="container">


	<div class="row d-flex align-items-center">

		<div class="col-12 col-md-6"> 
			<div class="entry-media entry-media-rounded">
				<?php if ( !empty( megaphone_get('wa_img') )  ) : ?>
					<?php echo megaphone_get('wa_img'); ?>
				<?php endif; ?>
			</div>
		</div>

        <div class="col-12 col-md-6 pl-48">            
			
			<h2 class="entry-title lh-1 mb-24 mb-sm-12 mb-md-20 h0"><?php echo esc_html( __megaphone('front_page_wa_punchline') ); ?></h2>
			
            <div class="entry-content">
				<?php echo wpautop( __megaphone( 'front_page_wa_text') ); ?>
				<?php if( megaphone_get( 'wa_cta') ) : ?>
					<a href="<?php echo esc_url( megaphone_get( 'wa_cta_url') ); ?>" class="megaphone-button mt-10 mt-sm-5 mt-md-10"><?php echo __megaphone( 'front_page_wa_cta_label'); ?></a>
                <?php endif; ?>
            </div>

			<?php if( megaphone_get( 'wa_latest_episode') ) : ?>
               <?php $latest_episode = megaphone_get( 'wa_latest_episode_obj'); ?>
                <?php if( $latest_episode->have_posts() ) : while( $latest_episode->have_posts() ): $latest_episode->the_post(); ?>
                        <div class="player-paceholder player-paceholder-medium player-placeholder-on-white mt-24 mt-sm-20 mt-md-10">
                                <div class="placeholder-slot-l">
                                	<?php megaphone_play_button( 'megaphone-button-play megaphone-button-play-medium', false ); ?>
                                </div>
                            
                                <div class="placeholder-slot-r">
									<span class="megaphone-placeholder-label mb-4 mb-sm-0 lh-1"><?php echo __megaphone('front_page_wa_latest_episode'); ?></span>
									<h2 class="megaphone-placeholder-title h8"><a href="<?php the_permalink(); ?>"><?php echo megaphone_get_episode_title(); ?></a></h2>
                                </div>
                        
                        </div>
                	<?php endwhile; ?>
				<?php endif; ?>
                <?php wp_reset_postdata(); ?>
			<?php endif; ?>
			
			<?php if( megaphone_get( 'wa_subscribe') ) : ?>
        		<?php get_template_part( 'template-parts/front-page/subscribe-fa' ); ?>
    		<?php endif; ?>  	

		</div>
		
	</div>

	</div>

</div>