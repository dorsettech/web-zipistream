<?php if ( megaphone_get( 'wa_display_title' ) ) : ?>
	<div class="container">
		<div class="section-head">
				<div class="section-head-l">
					<h3 class="section-title h2"><?php echo esc_html( __megaphone( 'front_page_wa_title' ) ); ?></h3>
				</div>
		</div>
	</div>
<?php endif; ?>  

<?php $subscribe_class = megaphone_get( 'wa_subscribe' ) ? 'megaphone-subscribe-on' : ''; ?>

<div class="megaphone-section megaphone-section-welcome megaphone-overlay wa-layout wa-layout-1 d-flex justify-content-center align-items-center flex-lg-column <?php echo esc_attr( $subscribe_class ); ?>">
	
	<div class="entry-media">
		<?php if ( ! empty( megaphone_get( 'wa_img' ) ) ) : ?>
			<?php echo megaphone_get( 'wa_img' ); ?>
		<?php endif; ?>
	</div>

	<div class="container overlay-container container-self-center d-flex align-items-center justify-content-lg-between">

		<div class="col-12 col-md-9 col-lg-6 ">            
			
			<h2 class="entry-title h0 lh-1 mb-24 mb-sm-12 mb-md-20"><?php echo esc_html( __megaphone( 'front_page_wa_punchline' ) ); ?></h2>
			
			<div class="entry-content">
				<?php echo wpautop( __megaphone( 'front_page_wa_text' ) ); ?>
				<?php if ( megaphone_get( 'wa_cta' ) ) : ?>
					<a href="<?php echo esc_url( megaphone_get( 'wa_cta_url' ) ); ?>" class="megaphone-button mt-12 mt-sm-6 mt-md-12"><?php echo __megaphone( 'front_page_wa_cta_label' ); ?></a>
				<?php endif; ?>
			</div>

		</div>

		<div class="col-12 col-md-12 col-lg-5 mt-md-30 mt-sm-20">
			<?php if ( megaphone_get( 'wa_latest_episode' ) ) : ?>
				<?php $latest_episode = megaphone_get( 'wa_latest_episode_obj' ); ?>
				<?php
				if ( $latest_episode->have_posts() ) :
					while ( $latest_episode->have_posts() ) : $latest_episode->the_post(); ?>
						<div class="player-paceholder">
								<div class="placeholder-slot-l">
									<?php megaphone_play_button( 'megaphone-button-play megaphone-button-play-big', false ); ?>
								</div>
							
								<div class="placeholder-slot-r">
									<span class="megaphone-placeholder-label mb-4 mb-sm-0 mb-md-4 lh-1"><?php echo __megaphone( 'front_page_wa_latest_episode' ); ?></span>
									<h2 class="megaphone-placeholder-title h7 d-none d-md-inline"><a href="<?php the_permalink(); ?>"><?php echo megaphone_get_episode_title(); ?></a></h2>
								</div>
						
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>
		</div>

	</div>

	<?php if ( megaphone_get( 'wa_subscribe' ) ) : ?>
		<?php get_template_part( 'template-parts/front-page/subscribe-wa' ); ?>
	<?php endif; ?>
	
</div>