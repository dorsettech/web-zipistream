<?php $display = megaphone_get_episode_layout_options( 'fa_2' ); ?>

<article <?php post_class( 'megaphone-layout-fa-2' ); ?>>

	<div class="megaphone-overlay">

		<div class="overlay-container container-self-center d-flex align-items-center justify-content-center flex-column text-center">
			
		<div class="megaphone-content-alt">
			<div class="entry-header">

					<?php if ( $display['meta_up'] ) : ?>
						<div class="entry-category"><?php echo megaphone_get_meta_data( $display['meta_up'] ); ?></div>
					<?php endif; ?>
					
					<h2 class="entry-title mb-4 mb-sm-4 mb-md-4">
						<a href="<?php the_permalink(); ?>"><?php echo megaphone_get_episode_title(); ?></a>
					</h2>

					<?php if ( $display['meta_down'] ) : ?>
						<div class="entry-meta">
							<?php echo megaphone_get_meta_data( $display['meta_down'] ); ?>
						</div>
					<?php endif; ?>
					
			</div>
				<?php if ( $display['excerpt'] ) : ?>
					<div class="entry-content mt-16 mt-sm-8 mt-md-12 paragraph-small megaphone-content m-mc">
						<?php echo megaphone_get_excerpt( $display['excerpt'] ); ?>
					</div>
				<?php endif; ?>
				<?php if ( $display['play_btn'] ) : ?>
					<div class="entry-footer mt-24 mt-sm-20 mt-md-12">
						<?php megaphone_play_button( 'megaphone-button' ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>

		<div class="entry-media">
			<?php if ( $fimg = megaphone_get_featured_image( 'megaphone-fa-2' ) ) : ?>
				<a href="<?php the_permalink(); ?>"><?php echo megaphone_wp_kses( $fimg ); ?></a>
			<?php endif; ?>

			<?php if ( $display['episode_number'] ) : ?>
					
				<?php $episodes_ids = megaphone_get( 'fa_episodes_ids' ); ?>
				<?php $podcast = megaphone_get( 'podcast' ); ?>
				<?php $terms = get_the_terms( get_the_ID(), $podcast['taxonomy'] ); ?>
				<?php $term = megaphone_get_podcast_term( $episodes_ids, $terms ) ; ?>
				<?php $id = isset( $term['id'] ) ? $term['id'] : false; ?>
				<?php $episode_id = ! empty( $episodes_ids ) && isset( $episodes_ids[ $id ] ) ? $episodes_ids[ $id ] : false; ?>
				<?php $episodes_ids = $episode_id; ?>

				<?php if ( $episode_number = megaphone_get_episode_number( $episodes_ids, get_the_ID() ) ) : ?>
					<span class="entry-episode">
						<span class="episode-item">
							<?php echo __megaphone( 'episode' ); ?> 
							<strong><?php echo absint( $episode_number ); ?></strong>
						</span>
					</span>
				<?php endif; ?>

			<?php endif; ?>

		</div>

	</div>

</article>
