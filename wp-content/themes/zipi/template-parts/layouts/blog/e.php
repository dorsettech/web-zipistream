<?php $display = megaphone_get_post_layout_options('e'); ?>

<article <?php post_class('megaphone-item megaphone-post megaphone-layout-e mb-50 mb-sm-20 mb-md-30'); ?>>
    <div class="row justify-content-center">

        <div class="col-12 col-md-6 col-lg-6"> 
            <?php if ( $fimg = megaphone_get_featured_image( 'megaphone-e' ) ): ?>
                <div class="entry-media mb-sm-10">
                    <a href="<?php the_permalink(); ?>"><?php echo megaphone_wp_kses( $fimg ); ?></a>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-12 col-md-6 col-lg-6">            
        
            <div class="entry-header mb-16 mb-sm-8 mb-md-10">
                
                <?php if( $display['meta_up'] ): ?>
                    <div class="entry-category"><?php echo megaphone_get_meta_data( $display['meta_up'] ); ?></div>
                <?php endif; ?>
                
                <?php the_title( sprintf( '<h2 class="entry-title h4 mb-4 mb-sm-4 mb-md-4"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                
                <?php if( $display['meta_down'] ): ?>
                    <div class="entry-meta">
                        <?php echo megaphone_get_meta_data( $display['meta_down'] ); ?>
                    </div>
                <?php endif; ?>

            </div>

            <?php if( $display['excerpt'] ): ?>
                <div class="entry-content">
                    <?php echo megaphone_get_excerpt( $display['excerpt'] ); ?>    
                </div>
            <?php endif; ?>

            <?php if( $display['rm'] ): ?>
                <div class="entry-footer mt-24 mt-sm-20 mt-md-20">
                    <a href="<?php the_permalink(); ?>" class="megaphone-button megaphone-button-hollow megaphone-button-medium"><?php echo __megaphone( 'read_more'); ?></a>
                </div>
            <?php endif; ?>

        </div>
        
	</div>
</article>