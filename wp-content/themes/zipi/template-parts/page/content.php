<div class="megaphone-section">
    <div class="container">
        <div class="section-content row justify-content-center">
            
            <?php if ( megaphone_has_sidebar( 'left' ) ): ?>
		        <div class="col-12 col-lg-3 megaphone-order-2">
		            <?php get_sidebar(); ?>
		        </div>
    		<?php endif; ?>

            <div class="megaphone-content-page col-12 col-lg-9 megaphone-order-1 megaphone-content-height">

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php if ( megaphone_get( 'layout' ) == 2 ): ?>
                        <div class="entry-header">
                            <?php echo megaphone_breadcrumbs(); ?>
                            <?php the_title( '<h1 class="entry-title h1 mb-50 mb-sm-20 mb-md-30">', '</h1>' ); ?>
                        </div>
                    <?php endif; ?>

                    <?php if(get_the_content()): ?>
                        <div class="entry-content entry-single clearfix">
                            <?php the_content(); ?> 
                            <?php wp_link_pages( array( 'before' => '<div class="paginated-post-wrapper clearfix pt-20">', 'after' => '</div>' ) ); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( is_page_template('template-authors.php') ): ?>
                            <?php get_template_part( 'template-parts/page/authors' ); ?>
                    <?php endif; ?>

                
                </article>

                <?php comments_template(); ?>
                
            </div>

            <?php if ( megaphone_has_sidebar( 'right' ) ): ?>
		        <div class="col-12 col-lg-3 megaphone-order-2">
		            <?php get_sidebar(); ?>
		        </div>
    		<?php endif; ?>

        </div>
    </div>
</div>