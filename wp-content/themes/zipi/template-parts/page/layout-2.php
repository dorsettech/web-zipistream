<?php get_template_part( 'template-parts/ads/above-singular' ); ?>

<div class="megaphone-section">
    <div class="container">
        <?php if ( megaphone_get( 'fimg' ) && $fimg = megaphone_get_featured_image( 'megaphone-page-2', true ) ): ?>
            <div class="entry-media mb-40 mb-sm-20 mb-md-30 text-center">
                <?php echo megaphone_wp_kses( $fimg ); ?>
                <?php if ( megaphone_get( 'fimg_cap' ) && $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
                    <figure class="wp-caption-text">
                        <?php echo wp_kses_post( $caption );  ?>
                    </figure>
                <?php endif; ?>
            </div>
        <?php endif; ?>

    </div>
</div>

<?php if( is_page_template('template-shows.php') ): ?>
    <?php get_template_part( 'template-parts/page/shows' ); ?>
<?php else: ?>
    <?php get_template_part('template-parts/page/content'); ?>
<?php endif; ?>