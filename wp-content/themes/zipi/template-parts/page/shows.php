<div class="megaphone-section megaphone-shows">
	<div class="container">
		<div class="section-content row justify-content-center">

			<?php if ( megaphone_has_sidebar( 'left' ) ) : ?>
			<div class="col-12 col-lg-4 megaphone-order-3">
				<?php get_sidebar(); ?>
			</div>
			<?php endif; ?>

			<div class="col-12 megaphone-order-1 megaphone-content-height <?php echo esc_attr( megaphone_get_loop_col_class( megaphone_get( 'shows_loop' ) ) ); ?>">

				<div class="row megaphone-items megaphone-shows">

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'col-12' ); ?>>

						<?php if ( megaphone_get( 'layout' ) == 2 ) : ?>
						<div class="entry-header">
							<?php echo megaphone_breadcrumbs(); ?>
							<?php the_title( '<h1 class="entry-title h1">', '</h1>' ); ?>
						</div>
						<?php endif; ?>

						<?php if ( ! empty( get_the_content() ) ) : ?>
						<div class="entry-content entry-single clearfix">
							<?php the_content(); ?>
						</div>
						<?php endif; ?>

					</article>

					<?php $shows = megaphone_get( 'shows_query' ); ?>

					<?php if ( ! empty( $shows ) ) : ?>

						<?php foreach ( $shows as $show ) : ?>

							<?php $layout = megaphone_get_loop_params( megaphone_get( 'shows_loop' ) ); ?>

							<div class="<?php echo esc_attr( $layout['col'] ); ?>">
									<?php include locate_template( 'template-parts/layouts/show/' . $layout['style'] . '.php' ); ?>
							</div>

					<?php endforeach; ?>

					<?php endif; ?>

				</div>
			</div>

			<?php if ( megaphone_has_sidebar( 'right' ) ) : ?>
			<div class="col-12 col-lg-4 megaphone-order-3">
				<?php get_sidebar(); ?>
			</div>
			<?php endif; ?>

		</div>
	</div>
</div>