<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link href="/wp-content/themes/zipi/style.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<!-- 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
  
    
	<?php wp_head(); ?>
	<style>
	.megaphone-logo img.ls-is-cached{
		width:280px;
	}
	</style>
</head>

<body <?php body_class(); ?>>

<div class="megaphone-main-wrapper">

	<?php if (megaphone_get('display', 'header')) : ?>
		<header class="megaphone-header megaphone-header-main d-none d-lg-block">
			<?php if (megaphone_get('header', 'top')) : ?>
				<?php get_template_part('template-parts/header/top'); ?>
			<?php endif; ?>
			<?php get_template_part('template-parts/header/layout-' . megaphone_get('header', 'layout')); ?>
		</header>

		<?php get_template_part( 'template-parts/header/mobile' ); ?>

		<?php if (megaphone_get('header', 'sticky')) : ?>
			<?php get_template_part('template-parts/header/sticky-layout-' . megaphone_get('header', 'sticky_layout')); ?>
		<?php endif; ?>

	<?php endif; ?>