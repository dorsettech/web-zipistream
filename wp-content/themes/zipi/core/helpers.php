<?php

/**
 * Debug (log) function
 *
 * Outputs any content into log file in theme root directory
 *
 * @param mixed   $mixed Content to output
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_log' ) ) :
	function megaphone_log( $mixed ) {

		if ( ! function_exists( 'WP_Filesystem' ) || ! WP_Filesystem() ) {
			return false;
		}

		if ( is_array( $mixed ) ) {
			$mixed = print_r( $mixed, 1 );
		} elseif ( is_object( $mixed ) ) {
				ob_start();
				var_dump( $mixed );
				$mixed = ob_get_clean();
		}

		global $wp_filesystem;
		$existing = $wp_filesystem->get_contents( get_parent_theme_file_path( 'log' ) );
		$wp_filesystem->put_contents( get_parent_theme_file_path( 'log' ), $existing . $mixed . PHP_EOL );
	}
endif;

/**
 * Get option value from theme options
 *
 * A wrapper function for WordPress native get_option()
 * which gets an option from specific option key (set in theme options panel)
 *
 * @param string  $option Name of the option
 * @param string  $format How to parse the option based on its type
 * @return mixed Specific option value or "false" (if option is not found)
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_option' ) ) :
	function megaphone_get_option( $option, $format = false ) {

		global $megaphone_settings, $megaphone_default_settings;

		if ( empty( $megaphone_settings ) || is_customize_preview() ) {
			$megaphone_settings = get_option( 'megaphone_settings' );
		}

		if ( ! isset( $megaphone_settings[ $option ] ) ) {

			if ( empty( $megaphone_default_settings ) ) {
				$megaphone_default_settings = megaphone_get_default_option();
			}

			$megaphone_settings[ $option ] = isset( $megaphone_default_settings[ $option ] ) ? $megaphone_default_settings[ $option ] : false;
		}

		if ( empty( $format ) ) {
			return $megaphone_settings[ $option ];
		}

		$value = $megaphone_settings[ $option ];

		switch ( $format ) {

			case 'image':
				$value = is_array( $megaphone_settings[ $option ] ) && isset( $megaphone_settings[ $option ]['url'] ) ? $megaphone_settings[ $option ]['url'] : '';
				break;
			case 'multi':
				$value = is_array( $megaphone_settings[ $option ] ) && ! empty( $megaphone_settings[ $option ] ) ? array_keys( array_filter( $megaphone_settings[ $option ] ) ) : array();
				break;

			case 'font':
				$native_fonts = megaphone_get_native_fonts();
				if ( ! in_array( $value['font-family'], $native_fonts ) ) {
					$value['font-family'] = "'" . $value['font-family'] . "'";
				}

				break;

			default:
				$value = false;
				break;
		}

		return $value;

	}
endif;


/**
 * Get grid vars
 *
 * We use grid vars for dynamic sizes of specific elements such as generating image sizes and breakpoints etc...
 *
 * @return array
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_grid_vars' ) ) :
	function megaphone_grid_vars() {

		$grid['column'] = 46;

		$grid['gutter'] = array(
			'xs' => 15,
			'sm' => 15,
			'md' => 30,
			'lg' => 30,
			'xl' => 50,
		);

		$grid['breakpoint'] = array(
			'xs' => 0,
			'sm' => 374,
			'md' => 600,
			'lg' => 900,
			'xl' => 1102,
		);

		$grid = apply_filters( 'megaphone_modify_grid_vars', $grid );

		return $grid;

	}
endif;


if ( ! function_exists( 'megaphone_size_by_col' ) ) :
	function megaphone_size_by_col( $cols, $breakpoint = 'xl' ) {
		$grid = megaphone_grid_vars();
		return ceil( ( $cols * $grid['column'] ) + ( ( $cols - 1 ) * $grid['gutter'][ $breakpoint ] ) );
	}
endif;



/**
 * Get presets of options for bulk selection of designs
 *
 * @param mixed   $preset_id
 * @return array
 * @param since   1.0
 */

if ( ! function_exists( 'megaphone_get_option_presets' ) ) :
	function megaphone_get_option_presets( $args = array(), $ignore_prefixing = false ) {

		global $megaphone_translate;

		$presets = array(

			'layouts' => array(

				'1' => array(
					'alt' => esc_html__( 'Layout 1', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_layout_1.jpg' ),
				),

				'2' => array(
					'alt' => esc_html__( 'Layout 2', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_layout_2.jpg' ),
				),

				'3' => array(
					'alt' => esc_html__( 'Layout 3', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_layout_3.jpg' ),
				),

				'4' => array(
					'alt' => esc_html__( 'Layout 4', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_layout_4.jpg' ),
				),

				'5' => array(
					'alt' => esc_html__( 'Layout 5', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_layout_5.jpg' ),
				),

				'6' => array(
					'alt' => esc_html__( 'Layout 6', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_layout_6.jpg' ),
				),

			),

			'colors'  => array(
				'1'  => array(
					'alt' => esc_html__( 'Fire', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_1.svg' ),
				),

				'2'  => array(
					'alt' => esc_html__( 'Red', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_2.svg' ),
				),

				'3'  => array(
					'alt' => esc_html__( 'Pink', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_3.svg' ),
				),

				'4'  => array(
					'alt' => esc_html__( 'Purple', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_4.svg' ),
				),

				'5'  => array(
					'alt' => esc_html__( 'Blue', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_5.svg' ),
				),

				'6'  => array(
					'alt' => esc_html__( 'Teal', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_6.svg' ),
				),

				'7'  => array(
					'alt' => esc_html__( 'Olive', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_7.svg' ),
				),

				'8'  => array(
					'alt' => esc_html__( 'Green', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_8.svg' ),
				),
				'9'  => array(
					'alt' => esc_html__( 'Gold', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_9.svg' ),
				),

				'10' => array(
					'alt' => esc_html__( 'Dark Fire', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_10.svg' ),
				),

				'11' => array(
					'alt' => esc_html__( 'Dark Red', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_11.svg' ),
				),
				'12' => array(
					'alt' => esc_html__( 'Dark Pink', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_12.svg' ),
				),

				'13' => array(
					'alt' => esc_html__( 'Dark Purple', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_13.svg' ),
				),

				'14' => array(
					'alt' => esc_html__( 'Dark Blue', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_14.svg' ),
				),

				'15' => array(
					'alt' => esc_html__( 'Dark Teal', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_15.svg' ),
				),

				'16' => array(
					'alt' => esc_html__( 'Dark Olive', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_16.svg' ),
				),

				'17' => array(
					'alt' => esc_html__( 'Dark Green', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_17.svg' ),
				),

				'18' => array(
					'alt' => esc_html__( 'Dark Gold', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_color_18.svg' ),
				),

			),

			'fonts'   => array(

				'1'  => array(
					'alt' => esc_html__( 'PT Serif (default)', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_1.svg' ),
				),

				'2'  => array(
					'alt' => esc_html__( 'Open Sans', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_2.svg' ),
				),

				'3'  => array(
					'alt' => esc_html__( 'Source Serif Pro', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_3.svg' ),
				),

				'4'  => array(
					'alt' => esc_html__( 'Dekko', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_4.svg' ),
				),

				'5'  => array(
					'alt' => esc_html__( 'Playfair Display', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_5.svg' ),
				),

				'6'  => array(
					'alt' => esc_html__( 'Abril', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_6.svg' ),
				),

				'7'  => array(
					'alt' => esc_html__( 'Roboto', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_7.svg' ),
				),

				'8'  => array(
					'alt' => esc_html__( 'Exo 2', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_8.svg' ),
				),

				'9'  => array(
					'alt' => esc_html__( 'Roboto Slab', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_9.svg' ),
				),

				'10' => array(
					'alt' => esc_html__( 'Alegreya', 'megaphone' ),
					'src' => get_parent_theme_file_uri( '/assets/img/admin/preset_font_10.svg' ),
				),

			),
		);

		if ( ! empty( $args ) ) {
			$new_presets = array();

			foreach ( $args as $type => $id ) {

				if ( array_key_exists( $type, $presets ) && array_key_exists( $id, $presets[ $type ] ) ) {
					$new_presets[ $type ][ $id ] = array();
				}
			}

			$presets = $new_presets;

		}

		foreach ( $presets as $type => $items ) {

			foreach ( $items as $slug => $item ) {

				$preset = array();
				$packed = array();

				include get_parent_theme_file_path( '/core/admin/presets/' . $type . '/' . $slug . '.php' );

				if ( ! $ignore_prefixing ) {

					foreach ( $preset as $key => $value ) {
						$packed[ 'megaphone_settings[' . $key . ']' ] = $value; // Kirki why???
					}

					$presets[ $type ][ $slug ]['settings'] = $packed;

				} else {
					$presets[ $type ][ $slug ]['settings'] = $preset;
				}
			}
		}

		$presets = apply_filters( 'megaphone_modify_option_presets', $presets, $args );

		return $presets;

	}
endif;


/**
 * Check if RTL mode is enabled
 *
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_is_rtl' ) ) :
	function megaphone_is_rtl() {

		if ( megaphone_get_option( 'rtl_mode' ) ) {
			$rtl = true;
			// Check if current language is excluded from RTL
			$rtl_lang_skip = explode( ',', megaphone_get_option( 'rtl_lang_skip' ) );
			if ( ! empty( $rtl_lang_skip ) ) {
				$locale = get_locale();
				if ( in_array( $locale, $rtl_lang_skip ) ) {
					$rtl = false;
				}
			}
		} else {
			$rtl = false;
		}

		return $rtl;
	}
endif;



/**
 * Generate dynamic css
 *
 * Function parses theme options and generates css code dynamically
 *
 * @return string Generated css code
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_generate_dynamic_css' ) ) :
	function megaphone_generate_dynamic_css() {
		ob_start();
		get_template_part( 'assets/css/dynamic-css' );
		$output = ob_get_contents();
		ob_end_clean();

		$output = megaphone_compress_css_code( $output );

		return $output;
	}
endif;

/**
 * Generate dynamic css
 *
 * Function parses theme options and generates css code dynamically
 *
 * @return string Generated css code
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_generate_dynamic_editor_css' ) ) :
	function megaphone_generate_dynamic_editor_css() {
		ob_start();
		get_template_part( 'assets/css/admin/dynamic-editor-css' );
		$output = ob_get_contents();
		ob_end_clean();
		$output = megaphone_compress_css_code( $output );

		return $output;
	}
endif;


/**
 * Get JS settings
 *
 * Function creates list of settings from thme options to pass
 * them to global JS variable so we can use it in JS files
 *
 * @return array List of JS settings
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_js_settings' ) ) :
	function megaphone_get_js_settings() {
		$js_settings = array();

		$protocol                            = is_ssl() ? 'https://' : 'http://';
		$js_settings['ajax_url']             = admin_url( 'admin-ajax.php', $protocol );
		$js_settings['rtl_mode']             = megaphone_is_rtl() ? true : false;
		$js_settings['header_sticky']        = megaphone_get_option( 'header_sticky' ) ? true : false;
		$js_settings['header_sticky_offset'] = absint( megaphone_get_option( 'header_sticky_offset' ) );
		$js_settings['header_sticky_up']     = megaphone_get_option( 'header_sticky_up' ) ? true : false;
		$js_settings['popup']                = megaphone_get_option( 'popup' ) ? true : false;
		$js_settings['go_to_top']            = megaphone_get_option( 'go_to_top' ) ? true : false;
		$js_settings['audio_error_msg']      = __megaphone( 'audio_error_msg' );
		$js_settings['grid']                 = megaphone_grid_vars();

		$js_settings = apply_filters( 'megaphone_modify_js_settings', $js_settings );

		return $js_settings;
	}
endif;

/**
 * Get Customize JS settings
 *
 * Function creates list of settings from thme options to pass
 * them to global JS variable so we can use it in JS files
 *
 * @return array List of JS settings
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_customize_js_settings' ) ) :
	function megaphone_get_customize_js_settings() {
		
		$js_settings = array();
		$js_settings['front_page_sections'] = megaphone_get_option( 'front_page_sections' );
		$js_settings['custom_fields_count'] = megaphone_get_option( 'front_page_general_custom_fields_number' );

		$js_settings = apply_filters( 'megaphone_modify_customize_js_settings', $js_settings );

		return $js_settings;
	}
endif;


/**
 * Generate fonts link
 *
 * Function creates font link from fonts selected in theme options
 *
 * @return string
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_generate_fonts_link' ) ) :
	function megaphone_generate_fonts_link() {

		$fonts    = array();
		$fonts[]  = megaphone_get_option( 'main_font' );
		$fonts[]  = megaphone_get_option( 'h_font' );
		$fonts[]  = megaphone_get_option( 'nav_font' );
		$fonts[]  = megaphone_get_option( 'button_font' );
		$unique   = array(); // do not add same font links
		$link     = array();
		$native   = megaphone_get_native_fonts();
		$protocol = is_ssl() ? 'https://' : 'http://';

		// print_r($fonts);

		foreach ( $fonts as $font ) {
			if ( ! in_array( $font['font-family'], $native ) ) {
				$unique[ $font['font-family'] ][] = $font['variant'];
			}
		}

		foreach ( $unique as $family => $variants ) {
			$link[ $family ]  = $family;
			$link[ $family ] .= ':' . implode( ',', array_unique( $variants ) );
		}

		if ( ! empty( $link ) ) {
			$query_args = array(
				'family' => implode( '|', $link ),
			);
			$fonts_url  = add_query_arg( $query_args, $protocol . 'fonts.googleapis.com/css' );

			return esc_url_raw( $fonts_url );
		}

		return '';

	}
endif;


/**
 * Get native fonts
 *
 * @return array List of native fonts
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_native_fonts' ) ) :
	function megaphone_get_native_fonts() {

		$fonts = array(
			'Arial, Helvetica, sans-serif',
			"'Arial Black', Gadget, sans-serif",
			"'Bookman Old Style', serif",
			"'Comic Sans MS', cursive",
			'Courier, monospace',
			'Garamond, serif',
			'Georgia, serif',
			'Impact, Charcoal, sans-serif',
			"'Lucida Console', Monaco, monospace",
			"'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
			"'MS Sans Serif', Geneva, sans-serif",
			"'MS Serif', 'New York', sans-serif",
			"'Palatino Linotype', 'Book Antiqua', Palatino, serif",
			'Tahoma,Geneva, sans-serif',
			"'Times New Roman', Times,serif",
			"'Trebuchet MS', Helvetica, sans-serif",
			'Verdana, Geneva, sans-serif',
		);

		return $fonts;
	}
endif;


/**
 * Get list of image sizes
 *
 * @return array
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_image_sizes' ) ) :
	function megaphone_get_image_sizes() {

		$sizes = array(
			'megaphone-a'                => array(
				'title' => esc_html__( 'Blog A', 'megaphone' ),
				'w'     => megaphone_size_by_col( 12 ),
				'ratio' => megaphone_get_image_ratio( 'layout_a' ),
				'crop'  => true,
			),
			'megaphone-b'                => array(
				'title' => esc_html__( 'Blog B', 'megaphone' ),
				'w'     => megaphone_size_by_col( 8 ),
				'ratio' => megaphone_get_image_ratio( 'layout_b' ),
				'crop'  => true,
			),
			'megaphone-c'                => array(
				'title' => esc_html__( 'Blog C', 'megaphone' ),
				'w'     => megaphone_size_by_col( 6 ),
				'ratio' => megaphone_get_image_ratio( 'layout_c' ),
				'crop'  => true,
			),
			'megaphone-d'                => array(
				'title' => esc_html__( 'Blog D', 'megaphone' ),
				'w'     => megaphone_size_by_col( 4 ),
				'ratio' => megaphone_get_image_ratio( 'layout_d' ),
				'crop'  => true,
			),
			'megaphone-e'                => array(
				'title' => esc_html__( 'Blog E', 'megaphone' ),
				'w'     => megaphone_size_by_col( 6 ),
				'ratio' => megaphone_get_image_ratio( 'layout_e' ),
				'crop'  => true,
			),
			'megaphone-f'                => array(
				'title' => esc_html__( 'Blog F', 'megaphone' ),
				'w'     => megaphone_size_by_col( 3.33 ),
				'ratio' => megaphone_get_image_ratio( 'layout_f' ),
				'crop'  => true,
			),

			'megaphone-a-episode'        => array(
				'title' => esc_html__( 'Podcast A', 'megaphone' ),
				'w'     => megaphone_size_by_col( 12 ),
				'ratio' => megaphone_get_image_ratio( 'layout_a_episode' ),
				'crop'  => true,
			),
			'megaphone-b-episode'        => array(
				'title' => esc_html__( 'Podcast B', 'megaphone' ),
				'w'     => megaphone_size_by_col( 8 ),
				'ratio' => megaphone_get_image_ratio( 'layout_b_episode' ),
				'crop'  => true,
			),
			'megaphone-c-episode'        => array(
				'title' => esc_html__( 'Podcast C', 'megaphone' ),
				'w'     => megaphone_size_by_col( 6 ),
				'ratio' => megaphone_get_image_ratio( 'layout_c_episode' ),
				'crop'  => true,
			),
			'megaphone-d-episode'        => array(
				'title' => esc_html__( 'Podcast D', 'megaphone' ),
				'w'     => megaphone_size_by_col( 4 ),
				'ratio' => megaphone_get_image_ratio( 'layout_d_episode' ),
				'crop'  => true,
			),
			'megaphone-e-episode'        => array(
				'title' => esc_html__( 'Podcast E', 'megaphone' ),
				'w'     => megaphone_size_by_col( 6 ),
				'ratio' => megaphone_get_image_ratio( 'layout_e_episode' ),
				'crop'  => true,
			),
			'megaphone-f-episode'        => array(
				'title' => esc_html__( 'Podcast F', 'megaphone' ),
				'w'     => megaphone_size_by_col( 3.33 ),
				'ratio' => megaphone_get_image_ratio( 'layout_f_episode' ),
				'crop'  => true,
			),
			'megaphone-fa-1'             => array(
				'title' => esc_html__( 'Featured A', 'megaphone' ),
				'w'     => 1920,
				'h'     => megaphone_get_option( 'layout_fa_1_episode_height' ),
				'crop'  => true,
			),
			'megaphone-fa-2'             => array(
				'title' => esc_html__( 'Featured B', 'megaphone' ),
				'w'     => megaphone_size_by_col( 12 ),
				'ratio' => megaphone_get_image_ratio( 'layout_fa_2_episode' ),
				'crop'  => true,
			),

			'megaphone-single-blog-1'    => array(
				'title' => esc_html__( 'Single post layout 1', 'megaphone' ),
				'w'     => megaphone_size_by_col( 12 ),
				'ratio' => megaphone_get_image_ratio( 'single_blog_layout_1' ),
				'crop'  => true,
			),
			'megaphone-single-blog-2'    => array(
				'title' => esc_html__( 'Single post layout 2', 'megaphone' ),
				'w'     => megaphone_size_by_col( 12 ),
				'ratio' => megaphone_get_image_ratio( 'single_blog_layout_2' ),
				'crop'  => true,
			),
			'megaphone-single-blog-3'    => array(
				'title' => esc_html__( 'Single post layout 3', 'megaphone' ),
				'w'     => 1920,
				'h'     => megaphone_get_option( 'single_blog_layout_3_height' ),
				'crop'  => true,
			),
			'megaphone-single-blog-4'    => array(
				'title' => esc_html__( 'Single post layout 4', 'megaphone' ),
				'w'     => 1920,
				'h'     => megaphone_get_option( 'single_blog_layout_4_height' ),
				'crop'  => true,
			),
			'megaphone-single-blog-5'    => array(
				'title' => esc_html__( 'Single post layout 5', 'megaphone' ),
				'w'     => megaphone_size_by_col( 6 ),
				'ratio' => megaphone_get_image_ratio( 'single_blog_layout_5' ),
				'crop'  => true,
			),

			'megaphone-single-podcast-1' => array(
				'title' => esc_html__( 'Single episode layout 1', 'megaphone' ),
				'w'     => megaphone_size_by_col( 12 ),
				'ratio' => megaphone_get_image_ratio( 'single_podcast_layout_1' ),
				'crop'  => true,
			),
			'megaphone-single-podcast-2' => array(
				'title' => esc_html__( 'Single episode layout 2', 'megaphone' ),
				'w'     => megaphone_size_by_col( 12 ),
				'ratio' => megaphone_get_image_ratio( 'single_podcast_layout_2' ),
				'crop'  => true,
			),
			'megaphone-single-podcast-3' => array(
				'title' => esc_html__( 'Single episode layout 3', 'megaphone' ),
				'w'     => 1920,
				'h'     => megaphone_get_option( 'single_podcast_layout_3_height' ),
				'crop'  => true,
			),
			'megaphone-single-podcast-4' => array(
				'title' => esc_html__( 'Single podcast layout 4', 'megaphone' ),
				'w'     => 1920,
				'h'     => megaphone_get_option( 'single_podcast_layout_4_height' ),
				'crop'  => true,
			),
			'megaphone-single-podcast-5' => array(
				'title' => esc_html__( 'Single episode layout 5', 'megaphone' ),
				'w'     => megaphone_size_by_col( 6 ),
				'ratio' => megaphone_get_image_ratio( 'single_podcast_layout_5' ),
				'crop'  => true,
			),

			'megaphone-page-1'           => array(
				'title' => esc_html__( 'Page layout 1', 'megaphone' ),
				'w'     => megaphone_size_by_col( 12 ),
				'ratio' => megaphone_get_image_ratio( 'page_layout_1' ),
				'crop'  => true,
			),
			'megaphone-page-2'           => array(
				'title' => esc_html__( 'Page layout 2', 'megaphone' ),
				'w'     => megaphone_size_by_col( 12 ),
				'ratio' => megaphone_get_image_ratio( 'page_layout_2' ),
				'crop'  => true,
			),
			'megaphone-page-3'           => array(
				'title' => esc_html__( 'Page layout 3', 'megaphone' ),
				'w'     => 1920,
				'h'     => megaphone_get_option( 'page_layout_3_height' ),
				'crop'  => true,
			),
			'megaphone-page-4'           => array(
				'title' => esc_html__( 'Page layout 4', 'megaphone' ),
				'w'     => 1920,
				'h'     => megaphone_get_option( 'page_layout_4_height' ),
				'crop'  => true,
			),

			'megaphone-wa-1'             => array(
				'title' => esc_html__( 'Welcome area layout 1 ', 'megaphone' ),
				'w'     => 1920,
				'h'     => megaphone_get_option( 'wa_layout_1_height' ),
				'crop'  => true,
			),
			'megaphone-wa-2'             => array(
				'title' => esc_html__( 'Welcome area layout 2', 'megaphone' ),
				'w'     => 1920,
				'h'     => megaphone_get_option( 'wa_layout_2_height' ),
				'crop'  => true,
			),
			'megaphone-wa-3'             => array(
				'title' => esc_html__( 'Welcome area layout 3', 'megaphone' ),
				'w'     => megaphone_size_by_col( 6 ),
				'ratio' => megaphone_get_image_ratio( 'wa_layout_3' ),
				'crop'  => true,
			),

		);

		$show_layout    = megaphone_get_option( 'show_layout_loop' );
		$show_layouts   = megaphone_get_show_layouts_map();
		$show_img_width = $show_layouts[ $show_layout ]['width'];

		$sizes['megaphone-show'] = array(
			'title' => esc_html__( 'Shows layout', 'megaphone' ),
			'w'     => megaphone_size_by_col( $show_img_width ),
			'ratio' => megaphone_get_image_ratio( 'show_layout' ),
			'crop'  => true,
		);

		$category_height = megaphone_get_option( 'category_settings' ) == 'inherit' ? megaphone_get_option( 'archive_layout_height' ) : megaphone_get_option( 'category_layout_height' );
		$show_height     = megaphone_get_option( 'show_settings' ) == 'inherit' ? megaphone_get_option( 'archive_layout_height' ) : megaphone_get_option( 'show_layout_height' );

		$sizes['megaphone-archive-category'] = array(
			'title' => esc_html__( 'Category archive layout', 'megaphone' ),
			'w'     => 1920,
			'h'     => $category_height,
			'crop'  => true,
		);
		$sizes['megaphone-archive-show']     = array(
			'title' => esc_html__( 'Show archive layout', 'megaphone' ),
			'w'     => 1920,
			'h'     => $show_height,
			'crop'  => true,
		);

		$disable_img_sizes = megaphone_get_option( 'disable_img_sizes' );

		if ( ! empty( $disable_img_sizes ) ) {
			$disable_img_sizes = array_keys( array_filter( $disable_img_sizes ) );
		}

		if ( ! empty( $disable_img_sizes ) ) {
			foreach ( $disable_img_sizes as $size_id ) {
				unset( $sizes[ $size_id ] );
			}
		}

		foreach ( $sizes as $key => $size ) {

			if ( ! isset( $size['ratio'] ) ) {
				$sizes[ $key ]['cover'] = true;
				continue;
			}

			if ( $size['ratio'] == 'original' ) {
				$size['h']    = 99999;
				$size['crop'] = false;
			} else {
				$size['h'] = megaphone_calculate_image_height( $size['w'], $size['ratio'] );
			}

			unset( $size['ratio'] );
			$sizes[ $key ] = $size;
		}

		// echo "<pre style=\"font-size:11px; line-height:1; \">";
		// print_r( $sizes );
		// echo "</pre>";

		$sizes = apply_filters( 'megaphone_modify_image_sizes', $sizes );

		return $sizes;
	}
endif;


/**
 * Gets an image ratio setting for a specific layout
 *
 * @param string  $option ID
 * @return string
 */
if ( ! function_exists( 'megaphone_get_image_ratio' ) ) :
	function megaphone_get_image_ratio( $layout ) {

		$ratio        = megaphone_get_option( $layout . '_img_ratio' );
		$custom_ratio = megaphone_get_option( $layout . '_img_custom' );

		if ( $ratio === 'custom' && ! empty( $custom_ratio ) ) {
			$ratio = str_replace( ':', '_', $custom_ratio );
		}

		$ratio = apply_filters( 'megaphone_modify_' . $layout . '_image_ratio', $ratio );

		return $ratio;
	}
endif;


/**
 * Parse image height
 *
 * Calculate an image size based on a given ratio and width
 *
 * @param int     $width
 * @param string  $ration in 'w_h' format
 * @return int $height
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_calculate_image_height' ) ) :
	function megaphone_calculate_image_height( $width = 1200, $ratio = '16_9' ) {

		$ratio = explode( '_', $ratio );

		if ( ! isset( $ratio[0] ) || ! is_numeric( $ratio[0] ) || ! isset( $ratio[1] ) || ! is_numeric( $ratio[1] ) ) {
			$ratio[0] = 16;
			$ratio[1] = 9;
		}

		$height = ceil( $width * absint( $ratio[1] ) / absint( $ratio[0] ) );

		return $height;
	}
endif;


/**
 * Get editor font sizes
 *
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_editor_font_sizes' ) ) :
	function megaphone_get_editor_font_sizes() {

		$regular = absint( megaphone_get_option( 'font_size_p' ) );

		$s  = $regular * 0.8;
		$l  = $regular * 1.8;
		$xl = $regular * 2.4;

		$sizes = array(
			array(
				'name'      => esc_html__( 'Small', 'megaphone' ),
				'shortName' => esc_html__( 'S', 'megaphone' ),
				'size'      => $s,
				'slug'      => 'small',
			),

			array(
				'name'      => esc_html__( 'Normal', 'megaphone' ),
				'shortName' => esc_html__( 'M', 'megaphone' ),
				'size'      => $regular,
				'slug'      => 'normal',
			),

			array(
				'name'      => esc_html__( 'Large', 'megaphone' ),
				'shortName' => esc_html__( 'L', 'megaphone' ),
				'size'      => $l,
				'slug'      => 'large',
			),
			array(
				'name'      => esc_html__( 'Huge', 'megaphone' ),
				'shortName' => esc_html__( 'XL', 'megaphone' ),
				'size'      => $xl,
				'slug'      => 'huge',
			),
		);

		$sizes = apply_filters( 'megaphone_modify_editor_font_sizes', $sizes );

		return $sizes;

	}
endif;


/**
 * Get editor colors
 *
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_editor_colors' ) ) :
	function megaphone_get_editor_colors() {

		$colors = array(
			array(
				'name'  => esc_html__( 'Accent', 'megaphone' ),
				'slug'  => 'megaphone-acc',
				'color' => megaphone_get_option( 'color_acc' ),
			),
			array(
				'name'  => esc_html__( 'Text', 'megaphone' ),
				'slug'  => 'megaphone-text',
				'color' => megaphone_get_option( 'color_txt' ),
			),
			array(
				'name'  => esc_html__( 'Background', 'megaphone' ),
				'slug'  => 'megaphone-bg',
				'color' => megaphone_get_option( 'color_bg' ),
			),
			array(
				'name'  => esc_html__( 'Background Alt 1', 'megaphone' ),
				'slug'  => 'megaphone-bg-alt-1',
				'color' => megaphone_get_option( 'color_bg_alt_1' ),
			),
			array(
				'name'  => esc_html__( 'Background Alt 2', 'megaphone' ),
				'slug'  => 'megaphone-bg-alt-2',
				'color' => megaphone_get_option( 'color_bg_alt_2' ),
			),
		);

		$colors = apply_filters( 'megaphone_modify_editor_colors', $colors );

		return $colors;

	}
endif;



/**
 * Get image ID from URL
 *
 * It gets image/attachment ID based on URL
 *
 * @param string  $image_url URL of image/attachment
 * @return int|bool Attachment ID or "false" if not found
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_image_id_by_url' ) ) :
	function megaphone_get_image_id_by_url( $image_url ) {
		global $wpdb;

		$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ) );

		if ( isset( $attachment[0] ) ) {
			return $attachment[0];
		}

		return false;
	}
endif;


/**
 * Calculate reading time by content length
 *
 * @param string  $text Content to calculate
 * @return int Number of minutes
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_read_time' ) ) :
	function megaphone_read_time( $text ) {

		$words                   = count( preg_split( "/[\n\r\t ]+/", wp_strip_all_tags( $text ) ) );
		$number_words_per_minute = megaphone_get_option( 'words_read_per_minute' );
		$number_words_per_minute = ! empty( $number_words_per_minute ) ? absint( $number_words_per_minute ) : 200;

		if ( ! empty( $words ) ) {
			$time_in_minutes = ceil( $words / $number_words_per_minute );
			return $time_in_minutes;
		}

		return false;
	}
endif;


/**
 * Trim chars of a string
 *
 * @param string  $string Content to trim
 * @param int     $limit  Number of characters to limit
 * @param string  $more   Chars to append after trimed string
 * @return string Trimmed part of the string
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_trim_chars' ) ) :
	function megaphone_trim_chars( $string, $limit, $more = '...' ) {

		if ( ! empty( $limit ) ) {

			$text = trim( preg_replace( "/[\n\r\t ]+/", ' ', $string ), ' ' );
			preg_match_all( '/./u', $text, $chars );
			$chars = $chars[0];
			$count = count( $chars );

			if ( $count > $limit ) {

				$chars = array_slice( $chars, 0, $limit );

				for ( $i = ( $limit - 1 ); $i >= 0; $i-- ) {
					if ( in_array( $chars[ $i ], array( '.', ' ', '-', '?', '!' ) ) ) {
						break;
					}
				}

				$chars   = array_slice( $chars, 0, $i );
				$string  = implode( '', $chars );
				$string  = rtrim( $string, '.,-?!' );
				$string .= $more;
			}
		}

		return $string;
	}
endif;


/**
 * Parse args ( merge arrays )
 *
 * Similar to wp_parse_args() but extended to also merge multidimensional arrays
 *
 * @param array   $a - set of values to merge
 * @param array   $b - set of default values
 * @return array Merged set of elements
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_parse_args' ) ) :
	function megaphone_parse_args( &$a, $b ) {
		$a = (array) $a;
		$b = (array) $b;
		$r = $b;
		foreach ( $a as $k => &$v ) {
			if ( is_array( $v ) && isset( $r[ $k ] ) ) {
				$r[ $k ] = megaphone_parse_args( $v, $r[ $k ] );
			} else {
				$r[ $k ] = $v;
			}
		}
		return $r;
	}
endif;


/**
 * Compare two values
 *
 * Fucntion compares two values and sanitazes 0
 *
 * @param mixed   $a
 * @param mixed   $b
 * @return bool Returns true if equal
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_compare' ) ) :
	function megaphone_compare( $a, $b ) {
		return (string) $a === (string) $b;
	}
endif;


/**
 * Compare two values and return a string if true
 *
 * @param mixed   $a
 * @param mixed   $b
 * @param string  $output
 * @return string Returns output if true
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_selected' ) ) :
	function megaphone_selected( $a, $b, $output ) {
		return megaphone_compare( $a, $b ) ? $output : '';
	}
endif;


/**
 * Sort option items
 *
 * Use this function to properly order sortable options
 *
 * @param array   $items    Array of items
 * @param array   $selected Array of IDs of currently selected items
 * @return array ordered items
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_sort_option_items' ) ) :
	function megaphone_sort_option_items( $items, $selected, $field = 'term_id' ) {

		if ( empty( $selected ) ) {
			return $items;
		}

		$new_items      = array();
		$temp_items     = array();
		$temp_items_ids = array();

		foreach ( $selected as $selected_item_id ) {

			foreach ( $items as $item ) {
				if ( $selected_item_id == $item->$field ) {
					$new_items[] = $item;
				} else {
					if ( ! in_array( $item->$field, $selected ) && ! in_array( $item->$field, $temp_items_ids ) ) {
						$temp_items[]     = $item;
						$temp_items_ids[] = $item->$field;
					}
				}
			}
		}

		$new_items = array_merge( $new_items, $temp_items );

		return $new_items;
	}
endif;


/**
 * Compress CSS Code
 *
 * @param string  $code Uncompressed css code
 * @return string Compressed css code
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_compress_css_code' ) ) :
	function megaphone_compress_css_code( $code ) {

		// Remove Comments
		$code = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $code );

		// Remove tabs, spaces, newlines, etc.
		$code = str_replace( array( "\r\n", "\r", "\n", "\t", '  ', '    ', '    ' ), '', $code );

		return $code;
	}
endif;


/**
 * Get list of social options
 *
 * Used for user social profiles
 *
 * @return array
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_social' ) ) :
	function megaphone_get_social() {
		$social = array(
			'behance'      => 'Behance',
			'delicious'    => 'Delicious',
			'deviantart'   => 'DeviantArt',
			'digg'         => 'Digg',
			'dribbble'     => 'Dribbble',
			'facebook'     => 'Facebook',
			'flickr'       => 'Flickr',
			'github'       => 'Github',
			'google'       => 'GooglePlus',
			'instagram'    => 'Instagram',
			'linkedin'     => 'LinkedIN',
			'pinterest'    => 'Pinterest',
			'reddit'       => 'ReddIT',
			'rss'          => 'Rss',
			'skype'        => 'Skype',
			'snapchat'     => 'Snapchat',
			'slack'        => 'Slack',
			'stumbleupon'  => 'StumbleUpon',
			'soundcloud'   => 'SoundCloud',
			'spotify'      => 'Spotify',
			'tumblr'       => 'Tumblr',
			'twitter'      => 'Twitter',
			'vimeo-square' => 'Vimeo',
			'vk'           => 'vKontakte',
			'vine'         => 'Vine',
			'weibo'        => 'Weibo',
			'wordpress'    => 'WordPress',
			'xing'         => 'Xing',
			'yahoo'        => 'Yahoo',
			'youtube'      => 'Youtube',
		);

		return $social;
	}
endif;



/**
 * Calculate time difference
 *
 * @param string  $timestring String to calculate difference from
 * @return  int Time difference in miliseconds
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_calculate_time_diff' ) ) :
	function megaphone_calculate_time_diff( $timestring ) {

		$now = current_time( 'timestamp' );

		switch ( $timestring ) {
			case '-1 day':
				$time = $now - DAY_IN_SECONDS;
				break;
			case '-3 days':
				$time = $now - ( 3 * DAY_IN_SECONDS );
				break;
			case '-1 week':
				$time = $now - WEEK_IN_SECONDS;
				break;
			case '-1 month':
				$time = $now - ( YEAR_IN_SECONDS / 12 );
				break;
			case '-3 months':
				$time = $now - ( 3 * YEAR_IN_SECONDS / 12 );
				break;
			case '-6 months':
				$time = $now - ( 6 * YEAR_IN_SECONDS / 12 );
				break;
			case '-1 year':
				$time = $now - ( YEAR_IN_SECONDS );
				break;
			default:
				$time = $now;
		}

		return $time;
	}
endif;


/**
 * Get post format
 *
 * Checks format of current post and possibly modify it based on specific options
 *
 * @param unknown $restriction_check bool Wheter to check for post restriction (if restricted we threat it as standard)
 * @return string Format value
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_post_format' ) ) :
	function megaphone_get_post_format() {

		$format = get_post_format();

		$supported_formats = get_theme_support( 'post-formats' );

		if ( ! empty( $supported_formats ) && is_array( $supported_formats[0] ) && ! in_array( $format, $supported_formats[0] ) ) {
			$format = '';
		}

		return $format;
	}
endif;


/**
 * Get the css class for a specific background type
 *
 * @param string  $id
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_background_css_class' ) ) :
	function megaphone_get_background_css_class( $id = 'none' ) {

		$classes = array(
			'none'  => '',
			'alt-1' => 'megaphone-bg-alt-1',
			'alt-2' => 'megaphone-bg-alt-2',
		);

		if ( empty( $id ) || ! array_key_exists( $id, $classes ) ) {
			return '';
		}

		return $classes[ $id ];
	}
endif;


/**
 * Get page meta data
 *
 * @param string  $field specific option array key
 * @return mixed meta data value or set of values
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_page_meta' ) ) :
	function megaphone_get_page_meta( $post_id = false, $field = false ) {

		if ( empty( $post_id ) ) {
			$post_id = get_the_ID();
		}

		$defaults = array(
			'settings' => 'inherit',
			'layout'   => megaphone_get_option( 'page_layout' ),
			'sidebar'  => array(
				'position' => megaphone_get_option( 'page_sidebar_position' ),
				'classic'  => megaphone_get_option( 'page_sidebar_standard' ),
				'sticky'   => megaphone_get_option( 'page_sidebar_sticky' ),
			),
			'authors' => array(
				'type'	  => 'host'
			),
		);

		$meta = get_post_meta( $post_id, '_megaphone_meta', true );
		$meta = megaphone_parse_args( $meta, $defaults );

		if ( $field ) {
			if ( isset( $meta[ $field ] ) ) {
				return $meta[ $field ];
			} else {
				return false;
			}
		}

		return $meta;
	}
endif;

/**
 * Get post meta data
 *
 * @param string  $field specific option array key
 * @return mixed meta data value or set of values
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_post_meta' ) ) :
	function megaphone_get_post_meta( $post_id = false, $field = false ) {

		if ( empty( $post_id ) ) {
			$post_id = get_the_ID();
		}

		$args['type'] = megaphone_is_podcast() ? 'podcast' : 'blog';

		$defaults = array(
			'settings' => 'inherit',
			'layout'   => megaphone_get_option( 'single_' . $args['type'] . '_layout' ),
			'sidebar'  => array(
				'position' => megaphone_get_option( 'single_' . $args['type'] . '_sidebar_position' ),
				'classic'  => megaphone_get_option( 'single_' . $args['type'] . '_sidebar_standard' ),
				'sticky'   => megaphone_get_option( 'single_' . $args['type'] . '_sidebar_sticky' ),
			),
		);

		$meta = get_post_meta( $post_id, '_megaphone_meta', true );

		$meta = megaphone_parse_args( $meta, $defaults );

		if ( $field ) {
			if ( isset( $meta[ $field ] ) ) {
				return $meta[ $field ];
			} else {
				return false;
			}
		}

		return $meta;
	}
endif;

/**
 * Get category meta data
 *
 * @param string  $field specific option array key
 * @return mixed meta data value or set of values
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_category_meta' ) ) :
	function megaphone_get_category_meta( $cat_id = false, $field = false ) {

		$inherit_from = megaphone_get_option( 'category_settings' ) == 'custom' ? 'category' : 'archive';

		$defaults = array(
			'settings'   => 'inherit',
			'image'      => '',
			'loop'       => megaphone_get_option( $inherit_from . '_loop' ),
			'layout'     => megaphone_get_option( $inherit_from . '_layout' ),
			'sidebar'    => array(
				'position' => megaphone_get_option( $inherit_from . '_sidebar_position' ),
				'classic'  => megaphone_get_option( $inherit_from . '_sidebar_standard' ),
				'sticky'   => megaphone_get_option( $inherit_from . '_sidebar_sticky' ),
			),
			'pagination' => megaphone_get_option( $inherit_from . '_pagination' ),
			'ppp_num'    => megaphone_get_option( $inherit_from . '_ppp' ) == 'inherit' ? megaphone_get_default_option( $inherit_from . '_ppp_num' ) : megaphone_get_option( $inherit_from . '_ppp_num' ),
			'order'      => 'DESC',
			'archive'    => array(
				'description' => megaphone_get_option( $inherit_from . '_description' ),
				'meta'        => megaphone_get_option( $inherit_from . '_meta' ),
			),
		);

		if ( $cat_id ) {
			$meta = get_term_meta( $cat_id, '_megaphone_meta', true );
			$meta = megaphone_parse_args( $meta, $defaults );
		} else {
			$meta = $defaults;
		}

		if ( $field ) {
			if ( isset( $meta[ $field ] ) ) {
				return $meta[ $field ];
			} else {
				return false;
			}
		}

		return $meta;
	}
endif;

/**
 * Get show meta data
 *
 * @param string  $field specific option array key
 * @return mixed meta data value or set of values
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_show_meta' ) ) :
	function megaphone_get_show_meta( $cat_id = false, $field = false ) {

		$inherit_from = megaphone_get_option( 'show_settings' ) == 'custom' ? 'show' : 'archive';

		$defaults = array(
			'settings'   => 'inherit',
			'image'      => '',
			'loop'       => megaphone_get_option( $inherit_from . '_loop' ),
			'layout'     => megaphone_get_option( $inherit_from . '_layout' ),
			'sidebar'    => array(
				'position' => megaphone_get_option( $inherit_from . '_sidebar_position' ),
				'classic'  => megaphone_get_option( $inherit_from . '_sidebar_standard' ),
				'sticky'   => megaphone_get_option( $inherit_from . '_sidebar_sticky' ),
			),
			'pagination' => megaphone_get_option( $inherit_from . '_pagination' ),
			'ppp_num'    => megaphone_get_option( $inherit_from . '_ppp' ) == 'inherit' ? megaphone_get_default_option( $inherit_from . '_ppp_num' ) : megaphone_get_option( $inherit_from . '_ppp_num' ),
			'order'      => megaphone_get_option( 'episodes_post_order' ), // globally set in Misc.
			'archive'    => array(
				'description' => megaphone_get_option( $inherit_from . '_description' ),
				'meta'        => megaphone_get_option( $inherit_from . '_meta' ),
			),
		);

		if ( $cat_id ) {
			$meta = get_term_meta( $cat_id, '_megaphone_meta', true );
			$meta = megaphone_parse_args( $meta, $defaults );
		} else {
			$meta = $defaults;
		}

		if ( $field ) {
			if ( isset( $meta[ $field ] ) ) {
				return $meta[ $field ];
			} else {
				return false;
			}
		}

		return $meta;
	}
endif;

if ( ! function_exists( 'megaphone_hex_to_hsl' ) ) :
	function megaphone_hex_to_hsla( $hex, $lightness = false, $opacity = 1, $raw = false ) {
		$rgb = megaphone_hex_to_rgba( $hex, false, false, true );

		$hsl = megaphone_rgb_to_hsl( $rgb, $lightness );
		if ( $raw ) {
			return $hsl;
		}

		if ( $opacity !== false ) {
			if ( abs( $opacity ) > 1 ) {
				$opacity = 1.0;
			}
			return 'hsla( ' . $hsl[0] . ', ' . $hsl[1] . '%, ' . $hsl[2] . '%, ' . $opacity . ')';
		} else {
			return 'hsl(' . $hsl[0] . ', ' . $hsl[1] . '%, ' . $hsl[2] . '%)';
		}
	}
endif;

/**
 * Hex to rgba
 *
 * Convert hexadecimal color to rgba
 *
 * @param string  $color   Hexadecimal color value
 * @param float   $opacity Opacity value
 * @return string RGBA color value
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_hex_to_rgba' ) ) :
	function megaphone_hex_to_rgba( $color, $opacity = false, $array = false ) {
		$default = 'rgb(0,0,0)';

		// Return default if no color provided
		if ( empty( $color ) ) {
			return $default;
		}

		// Sanitize $color if "#" is provided
		if ( $color[0] == '#' ) {
			$color = substr( $color, 1 );
		}

		// Check if color has 6 or 3 characters and get values
		if ( strlen( $color ) == 6 ) {
			$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
		} elseif ( strlen( $color ) == 3 ) {
			$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
		} else {
			return $default;
		}

		// Convert hexadec to rgb
		$rgb = array_map( 'hexdec', $hex );

		if ( $array ) {
			return $rgb;
		}

		// Check if opacity is set(rgba or rgb)
		if ( $opacity !== false ) {
			if ( abs( $opacity ) > 1 ) {
				$opacity = 1.0;
			}
			$output = 'rgba(' . implode( ',', $rgb ) . ',' . $opacity . ')';
		} else {
			$output = 'rgb(' . implode( ',', $rgb ) . ')';
		}

		// Return rgb(a) color string
		return $output;
	}
endif;

/**
 * It converts rgb to hex color mode.
 *
 * @param unknown $rgb array
 * @return string
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_rgb_to_hex' ) ) :
	function megaphone_rgb_to_hex( array $rgb ) {
		return sprintf( '#%02x%02x%02x', $rgb[0], $rgb[1], $rgb[2] );
	}
endif;

/**
 * Convert RGB to HSL color code
 *
 * @param unknown $rgb
 * @return array HSL color
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_rgb_to_hsl' ) ) :
	function megaphone_rgb_to_hsl( $rgb, $lightness = false ) {
		list( $r, $g, $b ) = $rgb;

		$r  /= 255;
		$g  /= 255;
		$b  /= 255;
		$max = max( $r, $g, $b );
		$min = min( $r, $g, $b );
		$h   = 0;
		$l   = ( $max + $min ) / 2;
		$d   = $max - $min;
		if ( $d == 0 ) {
			$h = $s = 0; // achromatic
		} else {
			$s = $d / ( 1 - abs( 2 * $l - 1 ) ) * 100;
			switch ( $max ) {
				case $r:
					$h = 60 * fmod( ( ( $g - $b ) / $d ), 6 );
					if ( $b > $g ) {
						$h += 360;
					}
					break;
				case $g:
					$h = 60 * ( ( $b - $r ) / $d + 2 );
					break;
				case $b:
					$h = 60 * ( ( $r - $g ) / $d + 4 );
					break;
			}
		}

		$l *= 100;

		if ( $lightness ) {

			$percentage = ( absint( $lightness ) / 100 ) * $l;

			if ( $lightness < 0 ) {
				$l = $l - $percentage;
			} else {
				$l = $l + $percentage;
			}
			$l = ( $l > 100 ) ? 100 : $l;
			$l = ( $l < 0 ) ? 0 : $l;
		}

		return array( round( $h, 2 ), round( $s, 2 ), round( $l, 2 ) );
	}
endif;

/**
 * Convert HSL to RGB color code
 *
 * @param unknown $hsl
 * @return array RGB color
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_hsl_to_rgb' ) ) :
	function megaphone_hsl_to_rgb( $hsl ) {
		list( $h, $s, $l ) = $hsl;

		$c = ( 1 - abs( 2 * $l - 1 ) ) * $s;
		$x = $c * ( 1 - abs( fmod( ( $h / 60 ), 2 ) - 1 ) );
		$m = $l - ( $c / 2 );

		if ( $h < 60 ) {
			$r = $c;
			$g = $x;
			$b = 0;
		} elseif ( $h < 120 ) {
				$r = $x;
				$g = $c;
				$b = 0;
		} elseif ( $h < 180 ) {
			$r = 0;
			$g = $c;
			$b = $x;
		} elseif ( $h < 240 ) {
			$r = 0;
			$g = $x;
			$b = $c;
		} elseif ( $h < 300 ) {
			$r = $x;
			$g = 0;
			$b = $c;
		} else {
			$r = $c;
			$g = 0;
			$b = $x;
		}

		$r = ( $r + $m ) * 255;
		$g = ( $g + $m ) * 255;
		$b = ( $b + $m ) * 255;
		return array( floor( $r ), floor( $g ), floor( $b ) );
	}
endif;

/**
 * Check if color is light
 *
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_is_color_light' ) ) :
	function megaphone_is_color_light( $color = false ) {
		$hsl = megaphone_rgb_to_hsl( megaphone_hex_to_rgba( $color, false, true ) );
		return $hsl[2] >= 70;
	}
endif;


/**
 * Check if post is currently restricted
 *
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_is_restricted_post' ) ) :
	function megaphone_is_restricted_post() {

		// Check if password protected
		if ( post_password_required() ) {
			return true;
		}

		return false;
	}
endif;

/**
 * Get number of posts for the current archive
 *
 * @return int
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_archive_posts_count' ) ) :
	function megaphone_get_archive_posts_count() {

		global $wp_query;

		return isset( $wp_query->found_posts ) ? $wp_query->found_posts : 0;
	}
endif;


/**
 * Get category/taxonomy child links
 *
 * @return string HTML output
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_archive_subnav' ) ) :
	function megaphone_get_archive_subnav() {

		$obj = get_queried_object();

		if ( empty( $obj ) ) {
			return '';
		}

		$terms = get_terms(
			array(
				'taxonomy' => $obj->taxonomy,
				'child_of' => $obj->term_id,
			)
		);

		if ( is_wp_error( $terms ) ) {
			return '';
		}

		if ( empty( $terms ) ) {
			return '';
		}

		$links = array();

		foreach ( $terms as $term ) {
			$link = get_term_link( $term, $obj->taxonomy );
			if ( ! is_wp_error( $link ) ) {
				$links[] = '<li><a href="' . esc_url( $link ) . '" rel="tag" class="cat-' . esc_attr( $term->term_id ) . '">' . $term->name . '</a></li>';
			}
		}

		if ( ! empty( $links ) ) {
			return implode( '', $links );
		}

		return '';

	}
endif;




/**
 * Woocommerce  Cart Elements
 *
 * @return array
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_woocommerce_cart_elements' ) ) :
	function megaphone_woocommerce_cart_elements() {

		if ( ! megaphone_is_woocommerce_active() ) {
			return false;
		}

		$elements                   = array();
		$elements['cart_url']       = wc_get_cart_url();
		$elements['products_count'] = WC()->cart->get_cart_contents_count();
		return $elements;
	}
endif;

/**
 * Check if we are on WooCommerce page
 *
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_is_woocommerce_page' ) ) :
	function megaphone_is_woocommerce_page() {

		return megaphone_is_woocommerce_active() && ( is_woocommerce() || is_shop() || is_cart() || is_checkout() );
	}
endif;


/**
 * Check if WooCommerce is active
 *
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_is_woocommerce_active' ) ) :
	function megaphone_is_woocommerce_active() {
		return function_exists( 'WC' );
	}
endif;

/**
 * Check if Meks Audio Player Plugin is active
 *
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_is_meks_ap_active' ) ) :
	function megaphone_is_meks_ap_active() {
		return class_exists( 'Meks_AP' );
	}
endif;


/**
 * Check if Yet Another Related Posts Plugin (YARPP) is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_yarpp_active' ) ) :
	function megaphone_is_yarpp_active() {
		return function_exists( 'yarpp_init' );
	}
endif;

/**
 * Check if Contextual Related Posts is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_crp_active' ) ) :
	function megaphone_is_crp_active() {
		return function_exists( 'crp_get_settings' );
	}
endif;

/**
 * Check if WordPress Related Posts is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_wrpr_active' ) ) :
	function megaphone_is_wrpr_active() {
		return function_exists( 'wp_rp_init_zemanta' );
	}
endif;

/**
 * Check if Jetpack is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_jetpack_active' ) ) :
	function megaphone_is_jetpack_active() {
		return class_exists( 'Jetpack' );
	}
endif;

/**
 * Check if Yoast SEO is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_yoast_active' ) ) :
	function megaphone_is_yoast_active() {
		return class_exists( 'WPSEO_Frontend' ) || class_exists( 'WPSEO_Admin' );
	}
endif;

/**
 * Check if Breadcrumb NavXT is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_breadcrumbs_navxt_active' ) ) :
	function megaphone_is_breadcrumbs_navxt_active() {
		return class_exists( 'breadcrumb_navxt' );
	}
endif;

/**
 * Check if Kirki customozer framework is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_kirki_active' ) ) :
	function megaphone_is_kirki_active() {
		if ( class_exists( 'Kirki' ) ) {
			return true;
		}

		return false;
	}
endif;

/**
 * Check if Meks Easy Social Share is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_meks_ess_active' ) ) :
	function megaphone_is_meks_ess_active() {
		return function_exists( 'meks_ess_init' );
	}
endif;

/**
 * Get first post from a specific taxonomy
 *
 * @since  1.0
 * @param $term_id
 * @param $taxonomy
 * @return object WP Post Object
 */
if ( ! function_exists( 'megaphone_is_co_authors_active' ) ) :
	function megaphone_is_co_authors_active() {
		return class_exists( 'CoAuthors_Plus' );
	}
endif;

/**
 * Check if Podlove podcast plugin is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_podlove_active' ) ) :
	function megaphone_is_podlove_active() {
		if ( function_exists( 'load_podlove_podcast_publisher' ) ) {
			return true;
		}

		return false;
	}
endif;

/**
 * Check if Simple podcast plugin is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_simple_podcast_active' ) ) :
	function megaphone_is_simple_podcast_active() {
		if ( function_exists( 'ss_podcast' ) ) {
			return true;
		}

		return false;
	}
endif;

/**
 * Check if is Gutenberg page
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_gutenberg_page' ) ) :
	function megaphone_is_gutenberg_page() {

		if ( function_exists( 'is_gutenberg_page' ) ) {
			return is_gutenberg_page();
		}

		global $wp_version;

		if ( version_compare( $wp_version, '5', '<' ) ) {
			return false;
		}

		global $current_screen;

		if ( ( $current_screen instanceof WP_Screen ) && ! $current_screen->is_block_editor() ) {
			return false;
		}

		return true;

		return is_gutenberg_page();
	}
endif;


/**
 * Get layouts map
 *
 * Function which keeps the definition parameters for each of post listing layouts
 *
 * @param int     $layout_id
 * @param int     $loop_index current post in the loop
 * @return array set of parameters
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_post_layouts_map' ) ) :
	function megaphone_get_post_layouts_map() {

		$params = array(

			// Layout A
			1 => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_a.svg' ),
				'alt'  => esc_html__( 'A', 'megaphone' ),
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => 'a',
					),
				),
			),

			// Layout B
			2 => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_b.svg' ),
				'alt'  => esc_html__( 'B', 'megaphone' ),
				'col'  => 'col-lg-8',
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => 'b',
					),
				),
			),

			// Layout B with sidebar
			3 => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_b_sid.svg' ),
				'alt'     => esc_html__( 'B (w/ sidebar)', 'megaphone' ),
				'sidebar' => true,
				'col'     => 'col-lg-8',
				'loop'    => array(
					array(
						'col'   => 'col-12',
						'style' => 'b',
					),
				),
			),

			// Layout C
			4 => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_c.svg' ),
				'alt'  => esc_html__( 'C', 'megaphone' ),
				'loop' => array(
					array(
						'col'   => 'col-12 col-md-6 col-lg-6',
						'style' => 'c',
					),
				),
			),

			// Layout D
			5 => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_d.svg' ),
				'alt'  => esc_html__( 'D', 'megaphone' ),
				'loop' => array(
					array(
						'col'   => 'col-12 col-md-6 col-lg-4',
						'style' => 'd',
					),
				),
			),

			// Layout D with sidebar
			6 => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_d_sid.svg' ),
				'alt'     => esc_html__( 'D (w/ sidebar)', 'megaphone' ),
				'sidebar' => true,
				'col'     => 'col-lg-8',
				'loop'    => array(
					array(
						'col'   => 'col-12 col-md-6 col-lg-6',
						'style' => 'd',
					),
				),
			),

			// Layout E
			7 => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_e.svg' ),
				'alt'  => esc_html__( 'E', 'megaphone' ),
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => 'e',
					),
				),
			),

			// Layout F
			8 => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_f.svg' ),
				'alt'  => esc_html__( 'F', 'megaphone' ),
				'col'  => 'col-lg-8',
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => 'f',
					),
				),
			),

			// Layout F with sidebar
			9 => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_f_sid.svg' ),
				'alt'     => esc_html__( 'F (w/ sidebar)', 'megaphone' ),
				'sidebar' => true,
				'col'     => 'col-lg-8',
				'loop'    => array(
					array(
						'col'   => 'col-12',
						'style' => 'f',
					),
				),
			),

		);

		return apply_filters( 'megaphone_modify_layouts_map', $params );

	}
endif;


/**
 * Get episode layouts map
 *
 * Function which keeps the definition parameters for each of episode listing layouts
 *
 * @param int     $layout_id
 * @param int     $loop_index current post in the loop
 * @return array set of parameters
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_episode_layouts_map' ) ) :
	function megaphone_get_episode_layouts_map() {

		$params = array(

			// Layout A
			1  => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_a.svg' ),
				'alt'  => esc_html__( 'A', 'megaphone' ),
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => 'a',
					),
				),
			),

			// Layout B
			2  => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_b.svg' ),
				'alt'  => esc_html__( 'B', 'megaphone' ),
				'col'  => 'col-lg-8',
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => 'b',
					),
				),
			),

			// Layout B with sidebar
			3  => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_b_sid.svg' ),
				'alt'     => esc_html__( 'B (w/ sidebar)', 'megaphone' ),
				'sidebar' => true,
				'col'     => 'col-lg-8',
				'loop'    => array(
					array(
						'col'   => 'col-12',
						'style' => 'b',
					),
				),
			),

			// Layout C
			4  => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_c.svg' ),
				'alt'  => esc_html__( 'C', 'megaphone' ),
				'loop' => array(
					array(
						'col'   => 'col-12 col-lg-6',
						'style' => 'c',
					),
				),
			),

			// Layout D
			5  => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_d.svg' ),
				'alt'  => esc_html__( 'D', 'megaphone' ),
				'loop' => array(
					array(
						'col'   => 'col-12 col-md-6 col-lg-4',
						'style' => 'd',
					),
				),
			),

			// Layout D with sidebar
			6  => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_d_sid.svg' ),
				'alt'     => esc_html__( 'D (w/ sidebar)', 'megaphone' ),
				'sidebar' => true,
				'col'     => 'col-lg-8',
				'loop'    => array(
					array(
						'col'   => 'col-12 col-lg-6',
						'style' => 'd',
					),
				),
			),

			// Layout E
			7  => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_e.svg' ),
				'alt'  => esc_html__( 'E', 'megaphone' ),
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => 'e',
					),
				),
			),

			// Layout F
			8  => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_f.svg' ),
				'alt'  => esc_html__( 'F', 'megaphone' ),
				'col'  => 'col-lg-8',
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => 'f',
					),
				),
			),

			// Layout F with sidebar
			9  => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_f_sid.svg' ),
				'alt'     => esc_html__( 'F (w/ sidebar)', 'megaphone' ),
				'sidebar' => true,
				'col'     => 'col-lg-8',
				'loop'    => array(
					array(
						'col'   => 'col-12',
						'style' => 'f',
					),
				),
			),

			// Layout G
			10 => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_g.svg' ),
				'alt'  => esc_html__( 'G', 'megaphone' ),
				'col'  => 'col-lg-6',
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => 'g',
					),
				),
			),

			// Layout G
			11 => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_g_sid.svg' ),
				'alt'     => esc_html__( 'G (w/ sidebar)', 'megaphone' ),
				'sidebar' => true,
				'col'     => 'col-lg-6',
				'loop'    => array(
					array(
						'col'   => 'col-12',
						'style' => 'g',
					),
				),
			),

		);

		return apply_filters( 'megaphone_modify_episode_layouts_map', $params );

	}
endif;


/**
 * Get show layouts map
 *
 * Function which keeps the definition parameters for each of show listing layouts
 *
 * @param int     $layout_id
 * @param int     $loop_index current post in the loop
 * @return array set of parameters
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_show_layouts_map' ) ) :
	function megaphone_get_show_layouts_map() {

		$params = array(

			// Layout C
			4 => array(
				'src'   => get_parent_theme_file_uri( '/assets/img/admin/layout_show_c.svg' ),
				'alt'   => esc_html__( 'C', 'megaphone' ),
				'loop'  => array(
					array(
						'col'   => 'col-12 col-lg-6',
						'style' => 'c',
					),
				),
				'width' => 6,
			),

			// Layout D
			5 => array(
				'src'   => get_parent_theme_file_uri( '/assets/img/admin/layout_show_d.svg' ),
				'alt'   => esc_html__( 'D', 'megaphone' ),
				'loop'  => array(
					array(
						'col'   => 'col-12 col-md-6 col-lg-4',
						'style' => 'd',
					),
				),
				'width' => 4,
			),

			// Layout D with sidebar
			6 => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_show_d_sid.svg' ),
				'alt'     => esc_html__( 'D (w/ sidebar)', 'megaphone' ),
				'sidebar' => true,
				'col'     => 'col-lg-8',
				'loop'    => array(
					array(
						'col'   => 'col-12 col-lg-6',
						'style' => 'd',
					),
				),
				'width'   => 4,
			),

		);

		return apply_filters( 'megaphone_modify_show_layouts_map', $params );

	}
endif;


/**
 * Get layouts map
 *
 * Function which keeps the definition parameters for each of post listing layouts
 *
 * @return array set of parameters
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_featured_layouts_map' ) ) :
	function megaphone_get_featured_layouts_map() {

		$params = array(

			1 => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_fa_a.svg' ),
				'alt'     => esc_html__( 'A (1 item)', 'megaphone' ),
				'ppp'     => 1,
				'classes' => 'alignfull',
				'loop'    => array(
					array(
						'col'   => 'col-12',
						'style' => '1',
					),
				),
			),

			2 => array(
				'src'     => get_parent_theme_file_uri( '/assets/img/admin/layout_fa_a_slider.svg' ),
				'alt'     => esc_html__( 'A (slider)', 'megaphone' ),
				'slider'  => true,
				'classes' => 'alignfull',
				'loop'    => array(
					array(
						'col'   => 'col-12',
						'style' => '1',
					),
				),
			),

			3 => array(
				'src'  => get_parent_theme_file_uri( '/assets/img/admin/layout_fa_b.svg' ),
				'alt'  => esc_html__( 'B (1 item)', 'megaphone' ),
				'ppp'  => 1,
				'loop' => array(
					array(
						'col'   => 'col-12',
						'style' => '2',
					),
				),
			),

			4 => array(
				'src'    => get_parent_theme_file_uri( '/assets/img/admin/layout_fa_b_slider.svg' ),
				'alt'    => esc_html__( 'B (slider)', 'megaphone' ),
				'slider' => true,
				'loop'   => array(
					array(
						'col'   => 'col-12',
						'style' => '2',
					),
				),
			),

			5 => array(
				'src'      => get_parent_theme_file_uri( '/assets/img/admin/layout_fa_b_carousel.svg' ),
				'alt'      => esc_html__( 'B (carousel)', 'megaphone' ),
				'slider'   => true,
				'carousel' => true,
				'center'   => true,
				'loop'     => array(
					array(
						'col'   => 'col-12',
						'style' => '2',
					),
				),
			),

		);

		return apply_filters( 'megaphone_modify_featured_layouts_map', $params );

	}
endif;


/**
 * Function for escaping through WordPress's KSES API
 * wp_kses() and wp_kses_allowed_html()
 *
 * @param string $content
 * @param bool $echo
 * @return string
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_wp_kses' ) ) :
	function megaphone_wp_kses( $content, $echo = false ) {

		$allowed_tags                  = wp_kses_allowed_html( 'post' );
		$allowed_tags['img']['srcset'] = array();
		$allowed_tags['img']['sizes']  = array();

		$tags = apply_filters( 'megaphone_wp_kses_allowed_html', $allowed_tags );

		if ( ! $echo ) {
			return wp_kses( $content, $tags );
		}

		echo wp_kses( $content, $tags );

	}
endif;

/**
 * Function to get homepage sections query arguments
 *
 * @param int $posts_per_page
 * @param string|array $blog
 * @param string|array $tag
 * @param array $specific_podcast_terms
 * @return array
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_frontpage_query_args' ) ) :
	function megaphone_get_frontpage_query_args( $type, $posts_per_page = 10, $tag = null, $specific_podcast_terms = array() ) {

		$podcast_args = megaphone_get_podcast_arguments();
		$args         = $podcast_args['podcast'];

		if ( $type == 'blog' ) {
			$args = $podcast_args['blog'];
		}

		if ( ! empty( $specific_podcast_terms ) ) {
			$args['terms']    = $specific_podcast_terms;
			$args['operator'] = 'IN';
		}

		$query = array(
			'post_type'           => $args['post_type'],
			'posts_per_page'      => $posts_per_page,
			'ignore_sticky_posts' => 1,
		);

		$tax_query = array();

		$tax_query[] = array(
			'taxonomy' => $args['taxonomy'],
			'field'    => 'term_id',
			'terms'    => $args['terms'],
			'operator' => $args['operator'],
		);

		if ( ! empty( $tag ) ) {
			$tax_query[] = array(
				'taxonomy' => 'post_tag',
				'field'    => 'term_id',
				'terms'    => $tag,
			);
		}

		if ( ! empty( $tax_query ) ) {
			$query['tax_query'] = $tax_query;
		}

		return $query;

	}
endif;

/**
 * Function check if current post is podcast
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_podcast' ) ) :
	function megaphone_is_podcast() { 

		$podcast_args = megaphone_get_podcast_arguments();

		if ( is_page() || !is_singular( $podcast_args['podcast']['post_type'] ) ) {
			return false;
		}

		if( $podcast_args['podcast']['operator'] == 'IN' ){
			if( $podcast_args['podcast']['terms'] == 'all' || has_term( $podcast_args['podcast']['terms'], $podcast_args['podcast']['taxonomy'], get_the_ID() ) ){
				return true;
			}
		} else if( $podcast_args['podcast']['operator'] == 'NOT IN' ){
			if( empty( $podcast_args['podcast']['terms'] ) && is_singular( $podcast_args['podcast']['post_type'] ) ){
				return true;
			} else if( !has_term( $podcast_args['podcast']['terms'], $podcast_args['podcast']['taxonomy'], get_the_ID() ) ){
				return true;
			}
		}

		return false;
		
	}
endif;

/**
 * Function escape special characters in regular expression
 *
 * @return string
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_esc_regex_chars' ) ) :
	function megaphone_esc_regex_chars( $string ) {

		$search  = array( '[', ']', '(', ')' );
		$replace = array( '\[', '\]', '\(', '\)' );

		return str_replace( $search, $replace, $string );

	}
endif;


/**
 * Prevent Kirki from breaking ad slot options
 *
 * @return string
 * @since  1.0
 */
function megaphone_sanitize_ad( $string ) {
	return $string;
}


/**
 * Get first post from a specific taxonomy
 *
 * @since  1.0
 * @param $term_id
 * @param $taxonomy
 * @return object WP Post Object
 */
if ( ! function_exists( 'megaphone_get_first_post' ) ) :
	function megaphone_get_first_post_from_taxonomy( $term_id, $taxonomy ) {

		$args = array(
			'tax_query'            => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'term_id',
					'terms'    => $term_id,
				),
			),
			'posts_per_page'       => 1,
			'ignore_sticky_posts ' => 1,
		);

		$sort          = megaphone_get_option( 'order_asc_desc' ) ? 'ASC' : 'DESC';
		$sort          = $taxonomy == 'series' ? $sort : 'DESC';
		$args['order'] = $sort;

		$query = new WP_Query( $args );

		if ( ! $query->have_posts() ) {
			return false;
		}

		while ( $query->have_posts() ) {
			$query->the_post();
			$post_obj = $query->post;
		}

		wp_reset_postdata();
		return $post_obj;
	}
endif;

/**
 * Check if current post is sponsored based on theme options
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_is_sponsored_episode' ) ) :
	function megaphone_is_sponsored_episode() {

		if ( ! megaphone_get_option( 'podcast_sponsored' ) ) {
			return false;
		}

		// Tags
		$sponsored_tags = megaphone_get_option( 'podcast_sponsored_tags' );
		if ( ! empty( $sponsored_tags ) ) {
			$tags = get_the_tags();
			if ( ! empty( $tags ) ) {
				foreach ( $tags as $k => $tag ) {

					if ( in_array( $tag->term_id, $sponsored_tags ) ) {
						if ( is_tag() ) {
							$obj = get_queried_object();
							if ( $tag->term_id == $obj->term_id ) {
								continue; // skip
							}
						}

						return true;
					}
				}
			}
		}

		// Manual
		if ( $manual_posts = megaphone_get_option( 'podcast_sponsored_manual' ) ) {
			if ( in_array( get_the_ID(), $manual_posts ) ) {
				return true;
			}
		}

		return false;
	}
endif;


/**
 * Sort items
 *
 * Use this function to properly order sortable options
 *
 * @param array   $items    Array of items
 * @param array   $selected Array of IDs of currently selected items
 * @return array ordered items
 * @since  1.2.3
 */
if ( ! function_exists( 'megaphone_sort_items' ) ) :
	function megaphone_sort_items( $items, $selected, $field = 'term_id' ) {

		if ( empty( $selected ) ) {
			return $items;
		}

		$new_items      = array();
		$temp_items     = array();
		$temp_items_ids = array();

		foreach ( $selected as $selected_item_id ) {

			foreach ( $items as $item ) {
				if ( $selected_item_id == $item->$field ) {
					$new_items[] = $item;
				} else {
					if ( ! in_array( $item->$field, $selected ) && ! in_array( $item->$field, $temp_items_ids ) ) {
						$temp_items[]     = $item;
						$temp_items_ids[] = $item->$field;
					}
				}
			}
		}

		$new_items = array_merge( $new_items, $temp_items );

		return $new_items;
	}
endif;

/**
 * Get typography uppercase options
 *
 * @since  1.0
 * @return array
 */
if ( ! function_exists( 'megaphone_get_typography_uppercase_options' ) ) :
	function megaphone_get_typography_uppercase_options() {
		return array(
			'.megaphone-header .site-title a'       => esc_html__( 'Site title (when logo is not used)', 'megaphone' ),
			'.site-description'                     => esc_html__( 'Site description', 'megaphone' ),
			'.megaphone-header li a'                => esc_html__( 'Main site navigation', 'megaphone' ),
			'.megaphone-header .header-top, .megaphone-header .header-top li a' => esc_html__( 'Header top bar', 'megaphone' ),
			'.widget-title, .megaphone-footer .widget-title' => esc_html__( 'Widget title', 'megaphone' ),
			'.section-title'                        => esc_html__( 'Section title', 'megaphone' ),
			'.entry-title, .meks-ap-title'          => esc_html__( 'Post/page title', 'megaphone' ),
			'h1, h2, h3, h4, h5, h6, .fn, .h7, .h8' => esc_html__( 'Text headings', 'megaphone' ),
			'buttons'                               => esc_html__( 'Buttons/special labels', 'megaphone' ),
		);
	}
endif;


/**
 * Main podcast setup function
 * Returns blog and podcast parameters
 *
 * @since  1.0
 * @return array
 */
if ( ! function_exists( 'megaphone_get_podcast_arguments' ) ) :
	function megaphone_get_podcast_arguments() {

		$args    = array();
		$options = megaphone_get_option( 'megaphone_podcast_setup' );

		switch ( true ) {

			case ( $options == 'podlove' && megaphone_is_podlove_active() ):

				$podlove_terms = megaphone_get_option( 'podlove_terms' );

				$args['blog']['post_type'] = 'post';
				$args['blog']['taxonomy']  = 'category';
				$args['blog']['terms']     = 'all';
				$args['blog']['operator']  = 'IN';

				$args['podcast']['post_type'] = 'podcast';
				$args['podcast']['taxonomy']  = 'category';
				$args['podcast']['terms']     = !empty( $podlove_terms ) ? $podlove_terms : array();
				$args['podcast']['operator']  = !empty( $podlove_terms ) ? 'IN' : 'NOT IN';
				$args['podcast']['shows']     = !empty( $podlove_terms ) ? true : false;
				
				break;
				

			case ( $options == 'simple' && megaphone_is_simple_podcast_active() ):

				$args['blog']['post_type'] = 'post';
				$args['blog']['taxonomy']  = 'category';
				$args['blog']['terms']     = 'all';
				$args['blog']['operator']  = 'IN';


				$terms = get_terms( array( 'taxonomy' => 'series' ) );
				$operator = 'NOT IN';

				if ( !empty( $terms ) && !is_wp_error( $terms ) ) {
					$operator = 'IN';
				}

				$args['podcast']['post_type'] = 'podcast';
				$args['podcast']['taxonomy']  = 'series';
				$args['podcast']['terms']     = 'all';
				$args['podcast']['operator']  = $operator;
				$args['podcast']['shows']     = true;
				
				break;


			//case 'theme_organization':
			default:
			
				$blog_terms = megaphone_get_option( 'blog_terms' );
				
				if ( empty( $blog_terms ) ) {
					$args['blog'] = false;
				} else {
					$args['blog']['post_type'] = 'post';
					$args['blog']['taxonomy']  = 'category';
					$args['blog']['terms']     = $blog_terms;
					$args['blog']['operator']  = 'IN';
				}

				$args['podcast']['post_type'] = 'post';
				$args['podcast']['taxonomy']  = 'category';
				$args['podcast']['terms']     = $blog_terms;
				$args['podcast']['operator']  = 'NOT IN';
				$args['podcast']['shows']     = true;

				break;

		}

		return $args;

	}
endif;

/**
 * Get category/show query arguments for podcast
 *
 * @param array $selected_categories
 * @since  1.0
 * @return array
 */
if ( ! function_exists( 'megaphone_get_shows_args' ) ) :
	function megaphone_get_shows_args( $selected = array() ) { 
		
		$podcast_args 		 = megaphone_get_podcast_arguments();
		$show_args   		 = array();	

		if ( ! $podcast_args['podcast']['shows'] ) {
			return $show_args;
		}

		if ( ! empty( $selected ) ) {
			$show_args = array( 'include' => $selected );
		} else if ( $podcast_args['podcast']['operator'] == 'IN'  ) {
			$show_args['include'] = $podcast_args['podcast']['terms'];
		} else if ( $podcast_args['podcast']['operator'] == 'NOT IN'  ) {
			$show_args['exclude'] = $podcast_args['podcast']['terms'];
		}
		
		$show_args['taxonomy'] = $podcast_args['podcast']['taxonomy']; 

		return $show_args;
	}
endif;


/*
 * Replacement for get_adjacent_post()
 */
if ( ! function_exists( 'megaphone_get_adjacent_post' ) ) :
	function megaphone_get_adjacent_post( $in_same_term = false, $excluded_terms = '', $previous = true, $taxonomy = 'category', $post_type = 'post' ) {
		
		if ( ( ! $post = get_post() ) || ! taxonomy_exists( $taxonomy ) ) {
			return null;
		}

		$current_post_date = get_post_field( 'post_date', $post );

		$args = array(
				'posts_per_page'   => 1,
				'post_status'      => 'publish',
				'post_type'        => $post_type,
				'orderby'          => 'date',
				'order'            => $previous ? 'DESC' : 'ASC',
				'no_found_rows'    => true,
				'cache_results'    => true,
				'suppress_filters' => false,
			);

		$tax_query = array();

		// Set up for requests limited to posts that share terms
		if ( $in_same_term ) {
			$terms = get_the_terms( get_the_ID(), $taxonomy );

			if ( is_array( $terms ) && ! empty( $terms ) ) {
				$terms = wp_list_pluck( $terms, 'term_id' );
				$terms = array_values( $terms );
				$terms = array_map( 'intval', $terms );
			} else {
				unset( $terms );
			}
		}

		// Handle excluded terms
		if ( ! empty( $excluded_terms ) ) {

			if ( ! is_array( $excluded_terms ) ) {
				// back-compat, $excluded_terms used to be IDs separated by " and "
				if ( false !== strpos( $excluded_terms, ' and ' ) ) {
					_deprecated_argument( __FUNCTION__, '3.3', sprintf( __( 'Use commas instead of %s to separate excluded terms.' ), "'and'" ) );
					$excluded_terms = explode( ' and ', $excluded_terms );
				} else {
					$excluded_terms = explode( ',', $excluded_terms );
				}
			}

			$excluded_terms = array_map( 'intval', $excluded_terms );

			$tax_query[] = array(
							'taxonomy' => $tax_query,
							'slugs'    => $excluded_terms,
							'compare'  => 'NOT IN',
						);
					
		}

		// If requesting same term, ensure that excluded terms don't appear in term list list
		if ( isset( $terms ) ) {
			if ( isset( $excluded_terms ) && is_array( $excluded_terms ) ) {
				$terms = array_diff( $terms, $excluded_terms );
				}

			if ( ! empty( $terms ) ) {
				$tax_query[] = array(
					'taxonomy' => $taxonomy,
					'terms'    => $terms,
				);
			}
		}

		// If we have a tax query, add it to our query args
		if ( ! empty( $tax_query ) ) {
			$args['tax_query'] = $tax_query;
		}

		// And now, the date constraint
		if ( $previous ) {
			$args['date_query'][] = array(
				'before'    => $current_post_date,
				'inclusive' => true,
			);
		} else {
			$args['date_query'][] = array(
				'after'     => $current_post_date,
				'inclusive' => true,
			);
		}
		
		// Ensure the current post isn't returned, since we're using an inclusive date query
		$args['post__not_in'] = array( get_the_ID() );

		// Get the posts and return either the post object or null
		$args = apply_filters( 'get_adjacent_post_args', $args, compact( 'in_same_term', 'excluded_terms', 'previous', 'taxonomy' ) );

		$results = get_posts( $args );

		if ( is_array( $results ) && ! empty( $results ) ) {
			return array_shift( $results );
		} else {
			return '';
		}
	}
endif;

/**
 * Check if is front page on pre_get_posts filter 
 * where WP function is_front_page doesn't work properly
 *
 * @param obj $query
 * @return bool
 * @since  1.0.0
 */
function megaphone_is_front_page( $query ) {

	if ( !$query->is_posts_page && !$query->is_home() && !$query->is_paged() ) {
		return false;
	}
	
	if ( megaphone_get_option( 'front_page_template' ) && 'posts' === get_option( 'show_on_front' ) ) {
		return true;
	} 
	
	if ( megaphone_get_option( 'front_page_template' ) && 'page' == get_option( 'show_on_front' ) && get_option( 'page_on_front' ) == $query->get( 'page_id' ) ) {
		return true;
	} 

	return false;
}
