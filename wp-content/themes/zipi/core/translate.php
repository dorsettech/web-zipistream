<?php
/**
 * Get all translation options
 *
 * @return array Returns list of all translation strings available in theme options panel
 * @since  1.0
 */

if ( !function_exists( 'megaphone_get_translate_options' ) ):
	function megaphone_get_translate_options() {
		global $megaphone_translate;

		if ( !empty( $megaphone_translate ) ) {
			return $megaphone_translate;
		}

		$translate = array(

			// hidden
			'front_page_wa_title' => array( 'text' => esc_html__( 'About', 'megaphone' ), 'desc' => 'Home page - Welcome area section title', 'hidden' => true ),
			'front_page_wa_punchline' => array( 'text' => esc_html__( 'Start your podcast today!', 'megaphone' ), 'desc' => 'Home page - Welcome area punchline', 'hidden' => true ),
			'front_page_wa_text' => array( 'text' => wp_kses_post( __( 'Megaphone is a fully-featured WordPress theme created to help you set up and manage your audio podcast website in no time. No coding knowledge required - choose a layout, add some content and let your voice be heard!', 'megaphone' ) ), 'desc' => 'Home page - Welcome area text', 'hidden' => true ),
			'front_page_wa_cta_label' => array( 'text' => esc_html__( 'Browse episodes', 'megaphone' ), 'desc' => 'Home page - Welcome area button label', 'hidden' => true ),
			'front_page_wa_latest_episode' => array( 'text' => esc_html__( 'Latest episode', 'megaphone' ), 'desc' => 'Home page - Welcome area latest episode label' ),
			'front_page_featured_title' => array( 'text' => esc_html__( 'Featured episodes', 'megaphone' ), 'desc' => 'Home page - Featured articles section title', 'hidden' => true ),
			'front_page_featured_view_all_link_label' => array( 'text' => esc_html__( 'View all', 'megaphone' ), 'desc' => 'Home page - Featured section link label', 'hidden' => true ),
			'front_page_posts_title' => array( 'text' => esc_html__( 'Latest from the blog', 'megaphone' ), 'desc' => 'Home page - Articles section title', 'hidden' => true ),
			'front_page_posts_view_all_link_label' => array( 'text' => esc_html__( 'View all', 'megaphone' ), 'desc' => 'Home page - Posts section link label', 'hidden' => true ),
			'front_page_episodes_title' => array( 'text' => esc_html__( 'Latest episodes', 'megaphone' ), 'desc' => 'Home page - Episodes section title', 'hidden' => true ),
			'front_page_episodes_view_all_link_label' => array( 'text' => esc_html__( 'View all', 'megaphone' ), 'desc' => 'Home page - Episodes section link label', 'hidden' => true ),
			'front_page_shows_title' => array( 'text' => esc_html__( 'Featured shows', 'megaphone' ), 'desc' => 'Home page - Shows section title', 'hidden' => true ),
			'front_page_shows_view_all_link_label' => array( 'text' => esc_html__( 'View all shows', 'megaphone' ), 'desc' => 'Home page - Shows section link label', 'hidden' => true ),
			'front_page_custom_content_title' => array( 'text' => esc_html__( 'Custom content', 'megaphone' ), 'desc' => 'Home page - Custom content section title', 'hidden' => true ),
			'front_page_custom_content' => array( 'text' => esc_html__( 'Custom content goes here...', 'megaphone' ), 'desc' => 'Home page - Custom content section text', 'hidden' => true ),
			'front_page_custom_content_title_2' => array( 'text' => esc_html__( 'Custom content 2', 'megaphone' ), 'desc' => 'Home page - Custom content 2 section title', 'hidden' => true ),
			'front_page_custom_content_2' => array( 'text' => esc_html__( 'Custom content 2 goes here...', 'megaphone' ), 'desc' => 'Home page - Custom content 2 section text', 'hidden' => true ),
			'front_page_custom_content_title_3' => array( 'text' => esc_html__( 'Custom content 3', 'megaphone' ), 'desc' => 'Home page - Custom content 3 section title', 'hidden' => true ),
			'front_page_custom_content_3' => array( 'text' => esc_html__( 'Custom content 3 goes here...', 'megaphone' ), 'desc' => 'Home page - Custom content 3 section text', 'hidden' => true ),
			'front_page_custom_content_title_4' => array( 'text' => esc_html__( 'Custom content 4', 'megaphone' ), 'desc' => 'Home page - Custom content 4 section title', 'hidden' => true ),
			'front_page_custom_content_4' => array( 'text' => esc_html__( 'Custom content 4 goes here...', 'megaphone' ), 'desc' => 'Home page - Custom content 4 section text', 'hidden' => true ),
			'front_page_custom_content_title_5' => array( 'text' => esc_html__( 'Custom content 5', 'megaphone' ), 'desc' => 'Home page - Custom content 5 section title', 'hidden' => true ),
			'front_page_custom_content_5' => array( 'text' => esc_html__( 'Custom content 5 goes here...', 'megaphone' ), 'desc' => 'Home page - Custom content 5 section text', 'hidden' => true ),

			// translation options
			'view_all_episodes' => array( 'text' => esc_html__( 'View all episodes', 'megaphone' ), 'desc' => 'Home page - Shows section view all button text' ),
			'subscribe_title' => array( 'text' => esc_html__( 'Listen on:', 'megaphone' ), 'desc' => 'Subscribe menu label' ),
			'search_label' => array( 'text' => esc_html__( 'Search', 'megaphone' ), 'desc' => 'Search label inside header' ),
			'menu_label' => array( 'text' => esc_html__( 'Menu', 'megaphone' ), 'desc' => 'Menu (hamburger) label inside header' ),
			'social_label' => array( 'text' => esc_html__( 'Follow me: ', 'megaphone' ), 'desc' => 'Social icons label inside header' ),
			'cart_label' => array( 'text' => esc_html__( 'Cart', 'megaphone' ), 'desc' => 'Cart (shop) label inside header' ),
			'by' => array( 'text' => esc_html__( 'by', 'megaphone' ), 'desc' => 'By author name prefix' ),
			'newer_entries' => array( 'text' => esc_html__( 'Newer Entries', 'megaphone' ), 'desc' => 'Pagination (prev/next) link text' ),
			'older_entries' => array( 'text' => esc_html__( 'Older Entries', 'megaphone' ), 'desc' => 'Pagination (prev/next) link text' ),
			'load_more' => array( 'text' => esc_html__( 'Load More', 'megaphone' ), 'desc' => 'Pagination (load more) link text' ),
			'previous_posts' => array( 'text' => esc_html__( 'Previous', 'megaphone' ), 'desc' => 'Pagination (numeric) link text' ),
			'next_posts' => array( 'text' => esc_html__( 'Next', 'megaphone' ), 'desc' => 'Pagination (numeric) link text' ),
			'read_more' => array( 'text' => esc_html__( 'Read more', 'megaphone' ), 'desc' => 'Read more button text' ),
			'category' => array( 'text' => esc_html__( 'Category', 'megaphone' ), 'desc' => 'Category archive title prefix', 'default' => '' ),
			'show' => array( 'text' => esc_html__( 'Show', 'megaphone' ), 'desc' => 'Show archive title prefix', 'default' => '' ),
			'tag' => array( 'text' => esc_html__( 'Tag', 'megaphone' ), 'desc' => 'Tag archive title prefix' ),
			'author' => array( 'text' => esc_html__( 'Author', 'megaphone' ), 'desc' => 'Author archive title prefix', 'default' => '' ),
			'archive' => array( 'text' => esc_html__( 'Archive', 'megaphone' ), 'desc' => 'Archive title prefix' ),
			'search_results_for' => array( 'text' => esc_html__( 'Search results for: ', 'megaphone' ), 'desc' => 'Title for search results template' ),
			'search_placeholder' => array( 'text' => esc_html__( 'Type here to search...', 'megaphone' ), 'desc' => 'Search placeholder text' ),
			'search_button' => array( 'text' => esc_html__( 'Search', 'megaphone' ), 'desc' => 'Search button text' ),
			'search_again' => array( 'text' => esc_html__( 'Search again', 'megaphone' ), 'desc' => 'Search results button text' ),
			'min_read' => array( 'text' => esc_html__( 'min read', 'megaphone' ), 'desc' => 'Used in post meta data (reading time)' ),
			'hosted_by' => array( 'text' => esc_html__( 'Hosted by', 'megaphone' ), 'desc' => 'Line before host author name' ),
			'guest' => array( 'text' => esc_html__( 'Guest', 'megaphone' ), 'desc' => 'Line before guest name' ),
			'author_box_label' => array( 'text' => esc_html__( 'Author', 'megaphone' ), 'desc' => 'Line before author name' ),
			'tagged_as' => array( 'text' => esc_html__( 'Tagged as:', 'megaphone' ), 'desc' => 'Tags label' ),
			'show_more' => array( 'text' => esc_html__( 'Show more', 'megaphone' ), 'desc' => 'Show more label' ),
			'show_less' => array( 'text' => esc_html__( 'Show less', 'megaphone' ), 'desc' => 'Show less label' ),
			'instagram_follow' => array( 'text' => esc_html__( 'Instagram feed', 'megaphone' ), 'desc' => 'Instagram pre-footer "follow" label' ),
			'related_blog' => array( 'text' => esc_html__( 'Further reading', 'megaphone' ), 'desc' => 'Title of the Related box area on single blog template' ),
			'related_podcast' => array( 'text' => esc_html__( 'More from this show', 'megaphone' ), 'desc' => 'Title of the Related box area on single podcast template' ),
			'leave_a_reply' => array( 'text' => esc_html__( 'Join the discussion', 'megaphone' ), 'desc' => 'Title of comments box' ),
			'comment_reply' => array( 'text' => esc_html__( 'Reply', 'megaphone' ), 'desc' => 'Comment reply label' ),
			'comment_submit' => array( 'text' => esc_html__( 'Submit Comment', 'megaphone' ), 'desc' => 'Comment form submit button label' ),
			'comment_text' => array( 'text' => esc_html__( 'Comment', 'megaphone' ), 'desc' => 'Comment form text area label' ),
			'comment_email' => array( 'text' => esc_html__( 'Email', 'megaphone' ), 'desc' => 'Comment form email label' ),
			'comment_name' => array( 'text' => esc_html__( 'Name', 'megaphone' ), 'desc' => 'Comment form name label' ),
			'comment_website' => array( 'text' => esc_html__( 'Website', 'megaphone' ), 'desc' => 'Comment form website label' ),
			'comment_cookie_gdpr' => array( 'text' => esc_html__( 'Save my name, email, and website in this browser for the next time I comment.', 'megaphone' ), 'desc' => 'Comment form checkbox cookie consent label' ),
			'comment_cancel_reply' => array( 'text' => esc_html__( 'Cancel reply', 'megaphone' ), 'desc' => 'Comment cancel reply label' ),
			'no_comments' => array( 'text' => esc_html__( 'Add comment', 'megaphone' ), 'desc' => 'Comment meta data (if zero comments)' ),
			'one_comment' => array( 'text' => esc_html__( '1 comment', 'megaphone' ), 'desc' => 'Comment meta data (if 1 comment)' ),
			'multiple_comments' => array( 'text' => esc_html__( '% comments', 'megaphone' ), 'desc' => 'Comment meta data (if more than 1 comments)' ),
			'episodes' => array( 'text' => esc_html__( 'episodes', 'megaphone' ), 'desc' => 'Number of podcasts/episodes label for categories/shows' ),
			'episode' => array( 'text' => esc_html__( 'Episode', 'megaphone' ), 'desc' => 'Number of episode label for category/show listing' ),
			'play_episode' => array( 'text' => esc_html__( 'Play episode', 'megaphone' ), 'desc' => 'Play button text' ),
			'play_latest_episode' => array( 'text' => esc_html__( 'Play latest episode', 'megaphone' ), 'desc' => 'Play button latest episode text' ),
			'sponsored_episode' => array( 'text' => esc_html__( 'Sponsored episode', 'megaphone' ), 'desc' => 'Sponsored episode label' ),
			'articles' => array( 'text' => esc_html__( 'articles', 'megaphone' ), 'desc' => 'Number of posts/articles label for categories' ),
			'audio_error_msg' => array( 'text' => esc_html__( 'The player couldn\'t recognize and retrieve audio data.', 'megaphone' ), 'desc' => 'Error message in player when audio is not found' ),
			'content_none' => array( 'text' => esc_html__( 'Sorry, there is no content found on this page. Feel free to contact the website administrator regarding this issue.', 'megaphone' ), 'desc' => 'Message when there are no posts on archive pages. i.e an empty Category' ),
			'content_none_search' => array( 'text' => esc_html__( 'No results found. Please try again with a different keyword.', 'megaphone' ), 'desc' => 'Message when there are no search results.' ),
			'404_title' => array( 'text' => esc_html__( 'Page not found', 'megaphone' ), 'desc' => '404 page title' ),
			'404_text' => array( 'text' => esc_html__( 'The page that you are looking for does not exist on this website. You may have accidentally mistyped the page address, or followed an expired link. Anyway, we will help you get back on track. Why not try to search for the page you were looking for:', 'megaphone' ), 'desc' => '404 page text' ),

		);

		$megaphone_translate = apply_filters( 'megaphone_modify_translate_options', $translate );

		return $megaphone_translate;

	}
endif;