<?php

/**
 * Main get function for front-end display and checking
 *
 * It gets the value from our theme global variable which contains all the settings for the current template
 *
 * @param string  $option An option value to get
 * @param string  $part   An option part to get, i.e. if option is an array
 * @return mixed
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get' ) ) :
	function megaphone_get( $option = '', $part = '' ) {

		if ( empty( $option ) ) {
			return false;
		}

		$megaphone = get_query_var( 'megaphone' );

		if ( empty( $megaphone ) ) {
			$megaphone = megaphone_templates_setup();
		}

		if ( ! empty( $part ) ) {

			if ( ! isset( $megaphone[ $option ][ $part ] ) ) {
				return false;
			}

			return $megaphone[ $option ][ $part ];
		}

		if ( isset( $megaphone[ $option ] ) ) {
			return $megaphone[ $option ];
		}

		return false;
	}
endif;


/**
 * Function to set a specific option/value to our global front-end settings variable
 *
 * @param string  $option name of the option to set
 * @param mixed   $value  option value
 * @return void
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_set' ) ) :
	function megaphone_set( $option, $value ) {
		global $wp_query;
		$megaphone = get_query_var( 'megaphone', array() );
		if ( ! empty( $option ) ) {
			$megaphone[ $option ] = $value;
			set_query_var( 'megaphone', $megaphone );
		}

	}
endif;


/**
 * Wrapper function for __()
 *
 * It checks if specific text is translated via options panel
 * If option is set, it returns translated text from theme options
 * If option is not set, it returns default translation string (from language file)
 *
 * @param string  $string_key Key name (id) of translation option
 * @return string Returns translated string
 * @since  1.0
 */

if ( ! function_exists( '__megaphone' ) ) :
	function __megaphone( $string_key ) {

		$translate = megaphone_get_translate_options();

		if ( ! megaphone_get_option( 'enable_translate' ) ) {
			return $translate[ $string_key ]['text'];
		}

		$translated_string = megaphone_get_option( 'tr_' . $string_key );

		if ( isset( $translate[ $string_key ]['hidden'] ) && trim( $translated_string ) == '' ) {
			return '';
		}

		if ( $translated_string == '-1' ) {
			return '';
		}

		if ( ! empty( $translated_string ) ) {
			return $translated_string;
		}

		return $translate[ $string_key ]['text'];
	}
endif;


/**
 * Get featured image
 *
 * Function gets featured image depending on the size and post id.
 * If image is not set, it gets the default featured image placehloder from theme options.
 *
 * @param string  $size               Image size ID
 * @param bool    $ignore_default_img Wheter to apply default featured image if post doesn't have featured image
 * @param bool    $post_id            If is not provided it will pull the image from the current post
 * @return string Image HTML output
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_featured_image' ) ) :
	function megaphone_get_featured_image( $size = 'full', $ignore_default_img = false, $post_id = false ) {

		if ( empty( $post_id ) ) {
			$post_id = get_the_ID();
		}

		if ( has_post_thumbnail( $post_id ) ) {

			return get_the_post_thumbnail( $post_id, $size );

		} elseif ( ! $ignore_default_img && ( $placeholder = megaphone_get_option( 'default_fimg', 'image' ) ) ) {

			// If there is no featured image, try to get default placeholder from theme options

			global $placeholder_img, $placeholder_imgs;

			if ( empty( $placeholder_img ) ) {
				$img_id = megaphone_get_image_id_by_url( $placeholder );
			} else {
				$img_id = $placeholder_img;
			}

			if ( ! empty( $img_id ) ) {
				if ( ! isset( $placeholder_imgs[ $size ] ) ) {
					$def_img = wp_get_attachment_image( $img_id, $size );
				} else {
					$def_img = $placeholder_imgs[ $size ];
				}

				if ( ! empty( $def_img ) ) {
					$placeholder_imgs[ $size ] = $def_img;

					return wp_kses_post( $def_img );
				}
			}

			return wp_kses_post( '<img src="' . esc_attr( $placeholder ) . '" class="size-' . esc_attr( $size ) . '" alt="' . esc_attr( get_the_title( $post_id ) ) . '" />' );
		}

		return '';
	}
endif;



/**
 * Get category featured image
 *
 * Function gets category featured image depending on the size
 *
 * @param string  $size   Image size ID
 * @param int     $cat_id
 * @return string Image HTML output
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_category_featured_image' ) ) :
	function megaphone_get_category_featured_image( $size = 'full', $cat_id = false, $first_post_fallback = true ) {

		if ( empty( $cat_id ) ) {
			$cat_id = get_queried_object_id();
		}

		$img_html = '';

		$img_url = megaphone_get_category_meta( $cat_id, 'image' );

		if ( ! empty( $img_url ) ) {
			$img_id   = megaphone_get_image_id_by_url( $img_url );
			$img_html = wp_get_attachment_image( $img_id, $size );
		}

		if ( empty( $img_html ) && $first_post_fallback ) {
			$first_post = megaphone_get_first_post_from_taxonomy( $cat_id, 'category' );
			$post_id    = false;
			if ( ! empty( $first_post ) && isset( $first_post->ID ) ) {
				$post_id = $first_post->ID;
			}
			$img_html = megaphone_get_featured_image( $size, false, $post_id );
		}

		return megaphone_wp_kses( $img_html );
	}
endif;


/**
 * Get meta data
 *
 * Function outputs meta data HTML
 *
 * @param array   $meta_data
 * @return string HTML output of meta data
 * @since  1.0
 */


if ( ! function_exists( 'megaphone_get_meta_data' ) ) :
	function megaphone_get_meta_data( $meta_data = array() ) {

		$output = '';

		if ( empty( $meta_data ) ) {
			return $output;
		}

		foreach ( $meta_data as $mkey ) {

			$meta = '';

			switch ( $mkey ) {

			case 'sponsored':
				$meta = '<span>' . __megaphone( 'sponsored_episode' ) . '</span>';
				break;

			case 'category':
				$meta = megaphone_get_category();
				break;

			case 'date':
				$meta = '<span class="updated">' . get_the_date() . '</span>';
				break;

			case 'author':
				if ( megaphone_is_co_authors_active() ) {
					global $authordata;

					$author_id = $authordata->ID;

					if ( is_author() ) {
						$author_id                 = absint( get_post_field( 'post_author', get_the_ID() ) );
						$authordata->user_nicename = get_the_author_meta( 'user_nicename', $author_id );
					}

					$meta = '<span class="vcard author">' . __megaphone( 'by' ) . ' <a href="' . esc_url( get_author_posts_url( $author_id, $authordata->user_nicename ) ) . '">' . get_the_author_meta( 'display_name', $author_id ) . '</a></span>';

				} else {
					$author_id = get_post_field( 'post_author', get_the_ID() );
					$meta      = '<span class="vcard author">' . __megaphone( 'by' ) . ' <a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID', $author_id ) ) ) . '">' . get_the_author_meta( 'display_name' ) . '</a></span>';
				}

				break;

			case 'rtime':
				$meta = megaphone_read_time( get_post_field( 'post_content', get_the_ID() ) );
				if ( ! empty( $meta ) ) {
					$meta .= ' ' . __megaphone( 'min_read' );
				}
				break;

			case 'comments':
				if ( comments_open() || get_comments_number() ) {
					ob_start();
					$scroll_class = is_single() && is_main_query() ? 'megaphone-scroll-animate' : '';
					comments_popup_link( __megaphone( 'no_comments' ), __megaphone( 'one_comment' ), __megaphone( 'multiple_comments' ), $scroll_class, '' );
					$meta = ob_get_contents();
					ob_end_clean();
				} else {
					$meta = '';
				}
				break;

			default:
				break;
			}

			if ( ! empty( $meta ) ) {
				$output .= '<span class="meta-item meta-' . $mkey . '">' . $meta . '</span>';
			}
		}

		return wp_kses_post( $output );

	}
endif;




/**
 * Get post categories
 *
 * Function outputs category links with HTML
 *
 * @param int     $post_id
 * @return string HTML output of category links
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_category' ) ) :
	function megaphone_get_category( $post_id = false ) {

		if ( empty( $post_id ) ) {
			$post_id = get_the_ID();
		}

		$terms        = array();
		$podcast_args = megaphone_get_podcast_arguments();
		$taxonomy     = megaphone_is_podcast() || megaphone_is_show( $post_id ) ? $podcast_args['podcast']['taxonomy'] : 'category';

		$can_primary_category = megaphone_get_option( 'primary_category' ) && ! is_single() && megaphone_is_yoast_active();
		$primary_category_id  = $can_primary_category ? get_post_meta( $post_id, '_yoast_wpseo_primary_category', true ) : false;
		

		if ( ! empty( $primary_category_id ) ) {
			$term = get_term( $primary_category_id, $taxonomy );
			if ( ! is_wp_error( $term ) && ! empty( $term ) ) {
				$terms[0] = $term;
			}
		}

		if ( empty( $terms ) ) {
			$terms = get_the_terms( $post_id, $taxonomy );
		}

		if ( is_wp_error( $terms ) || empty( $terms ) ) {
			return '';
		}

		$links = array();

		foreach ( $terms as $term ) {
			$link = get_term_link( $term, 'category' );
			if ( ! is_wp_error( $link ) ) {
				$links[] = '<a href="' . esc_url( $link ) . '" rel="tag" class="cat-item cat-' . esc_attr( $term->term_id ) . '">' . $term->name . '</a>';
			}
		}

		if ( ! empty( $links ) ) {
			return implode( '', $links );
		}

		return '';

	}
endif;


/**
 * Get post format icon
 *
 * Function outputs post format icon HTML
 *
 * @return string HTML output of post format icons
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_format_icon' ) ) :
	function megaphone_get_format_icon( $format = '' ) {

		$icons = array(
			'video'   => 'jf jf-video',
			'audio'   => 'jf jf-audio',
			'gallery' => 'jf jf-image',
		);

		$icons = apply_filters( 'megaphone_modify_format_icons', $icons );

		if ( ! array_key_exists( $format, $icons ) ) {
			return '';
		}

		return '<i class="' . esc_attr( $icons[ $format ] ) . '"></i>';

	}
endif;


/**
 * Get post excerpt
 *
 * Function outputs post excerpt for specific layout
 *
 * @param int     $limit Number of characters to limit excerpt
 * @return string HTML output of category links
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_excerpt' ) ) :
	function megaphone_get_excerpt( $limit = 250 ) {

		$manual_excerpt = false;

		if ( has_excerpt() ) {
			$content        = get_the_excerpt();
			$manual_excerpt = true;
		} else {
			$text    = get_the_content( '' );
			$text    = strip_shortcodes( $text );
			$text    = apply_filters( 'the_content', $text );
			$content = str_replace( ']]>', ']]&gt;', $text );
		}

		if ( ! empty( $content ) ) {
			if ( ! empty( $limit ) || ! $manual_excerpt ) {
				$more    = megaphone_get_option( 'more_string' );
				$content = wp_strip_all_tags( $content );
				$content = preg_replace( '/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $content );
				$content = megaphone_trim_chars( $content, $limit, $more );
			}

			return wp_kses_post( wpautop( $content ) );
		}

		return '';

	}
endif;

/**
 * Get category excerpt
 *
 * Function outputs category excerpt
 *
 * @param string  $description
 * @param int     $limit Number of characters to limit excerpt
 * @return string HTML output of category links
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_category_description' ) ) :
	function megaphone_get_category_description( $description, $limit = 250 ) {

		if ( !megaphone_get_option( 'show_layout_description_excerpt' ) ) {
			return wpautop( $description );
		}

		$user_limit = megaphone_get_option( 'show_layout_description_excerpt_limit' );

		if( !empty( $user_limit ) ) {
			$limit = $user_limit;
		}
		
		$text    = strip_shortcodes( $description );
		$text    = apply_filters( 'the_content', $text );
		$content = str_replace( ']]>', ']]&gt;', $text );

		if ( ! empty( $content ) ) {
			if ( ! empty( $limit ) ) {
				$more    = megaphone_get_option( 'more_string' );
				$content = wp_strip_all_tags( $content );
				$content = preg_replace( '/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $content );
				$content = megaphone_trim_chars( $content, $limit, $more );
			}

			return wpautop( $content );
		}

		return '';

	}
endif;

/**
 * Get branding
 *
 * Returns HTML of logo or website title based on theme options
 *
 * @param string  $use_mini_logo Whether to use mini logo
 * @return string HTML
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_branding' ) ) :
	function megaphone_get_branding( $type = 'default' ) {

		//mobile. sticky. sidebar. default

		$use_mini_logo =  false;
		$use_alt_logo = false;

		switch ( $type ) {
		case 'mobile':
			$use_mini_logo = true;
			if ( megaphone_get( 'header_indent' ) ) {
				$use_alt_logo = true;
			}
			break;

		case 'sticky':

			if ( 'header_sticky_logo' == 'mini' ) {
				$use_mini_logo = true;
			}
			break;

		case 'sidebar':
			$use_mini_logo = true;
			break;

		default:

			if ( megaphone_get( 'header_indent' ) ) {
				$use_alt_logo = true;
			}
			break;
		}


		// Get all logos
		if ( !$use_alt_logo ) {
			$logo             = megaphone_get_option( 'logo', 'image' );
			$logo_retina      = megaphone_get_option( 'logo_retina', 'image' );
			$logo_mini        = megaphone_get_option( 'logo_mini', 'image' );
			$logo_mini_retina = megaphone_get_option( 'logo_mini_retina', 'image' );
		} else {
			$logo             = megaphone_get_option( 'logo_alt', 'image' );
			$logo_retina      = megaphone_get_option( 'logo_alt_retina', 'image' );
			$logo_mini        = megaphone_get_option( 'logo_alt_mini', 'image' );
			$logo_mini_retina = megaphone_get_option( 'logo_alt_mini_retina', 'image' );
		}

		$logo_text_class  = ''; // if there is no image we use textual class

		if ( empty( $logo_mini ) ) {
			$logo_mini = $logo;
		}

		if ( $use_mini_logo ) {
			$logo        = $logo_mini;
			$logo_retina = $logo_mini_retina;
		}

		if ( empty( $logo ) ) {

			$brand           = get_bloginfo( 'name' );
			$logo_text_class = 'logo-img-none';

		} else {

			$grid = megaphone_grid_vars();

			$brand  = '<picture class="megaphone-logo">';
			$brand .= '<source media="(min-width: '.esc_attr( $grid['breakpoint']['md'] ).'px)" srcset="' . esc_attr( $logo );

			if ( ! empty( $logo_retina ) ) {
				$brand .= ', ' . esc_attr( $logo_retina ) . ' 2x';
			}

			$brand .= '">';
			$brand .= '<source srcset="' . esc_attr( $logo_mini );

			if ( ! empty( $logo_mini_retina ) ) {
				$brand .= ', ' . esc_attr( $logo_mini_retina ) . ' 2x';
			}

			$brand .= '">';
			$brand .= '<img src="' . esc_attr( $logo ) . '" alt="' . esc_attr( get_bloginfo( 'name' ) ) . '">';
			$brand .= '</picture>';
		}

		$element   = is_front_page() && ! megaphone_get( 'logo_is_displayed' ) ? 'h1' : 'span';
		$url       = megaphone_get_option( 'logo_custom_url' ) ? megaphone_get_option( 'logo_custom_url' ) : home_url( '/' );
		$site_desc = ! megaphone_get( 'logo_is_displayed' ) && megaphone_get_option( 'header_site_desc' ) ? '<span class="site-description d-none d-lg-block">' . get_bloginfo( 'description' ) . '</span>' : '';

		$output = '<' . esc_attr( $element ) . ' class="site-title h4 ' . esc_attr( $logo_text_class ) . '"><a href="' . esc_url( $url ) . '" rel="home">' . $brand . '</a></' . esc_attr( $element ) . '>' . $site_desc;

		megaphone_set( 'logo_is_displayed', true );

		return apply_filters( 'megaphone_modify_branding', $output );

	}
endif;


/**
 * Breadcrumbs
 *
 * Function provides support for several breadcrumb plugins
 * and gets its content to display on frontend
 *
 * @return string HTML output
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_breadcrumbs' ) ) :
	function megaphone_breadcrumbs() {

		$has_breadcrumbs = megaphone_get_option( 'breadcrumbs' );

		if ( $has_breadcrumbs == 'none' ) {
			return '';
		}

		$breadcrumbs = '';

		if ( $has_breadcrumbs == 'yoast' && function_exists( 'yoast_breadcrumb' ) ) {
			$breadcrumbs = yoast_breadcrumb( '<div class="megaphone-breadcrumbs"><div class="row"><div class="col-12">', '</div></div></div>', false );
		}

		if ( $has_breadcrumbs == 'bcn' && function_exists( 'bcn_display' ) ) {
			$breadcrumbs = '<div class="megaphone-breadcrumbs">' . bcn_display( true ) . '</div>';
		}

		return $breadcrumbs;
	}
endif;

/**
 * Get author social links
 *
 * @param int     $author_id ID of an author/user
 * @return string HTML output of social links
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_author_links' ) ) :
	function megaphone_get_author_links( $author_id, $archive_link = true, $social = true ) {

		$output = '';

		if ( $archive_link ) {

			$output .= '<a href="' . esc_url( get_author_posts_url( $author_id, get_the_author_meta( 'user_nicename', $author_id ) ) ) . '" class="megaphone-button megaphone-button-secondary megaphone-button-medium">' . __megaphone( 'view_all' ) . '</a>';
		}

		if ( $social ) {

			if ( $url = get_the_author_meta( 'url', $author_id ) ) {
				$output .= '<a href="' . esc_url( $url ) . '" target="_blank" class="megaphone-author-button"><i class="megaphone-icon mf mf-website"></i></a>';
			}

			$social = megaphone_get_social();

			if ( ! empty( $social ) ) {
				foreach ( $social as $id => $name ) {
					if ( $social_url = get_the_author_meta( $id, $author_id ) ) {

						if ( $id == 'twitter' ) {
							$social_url = ( strpos( $social_url, 'http' ) === false ) ? 'https://twitter.com/' . $social_url : $social_url;
						}

						$output .= '<a href="' . esc_url( $social_url ) . '" target="_blank" class="megaphone-author-button"><i class="fa fa-' . $id . '"></i></a>';
					}
				}
			}
		}

		return wp_kses_post( $output );
	}
endif;

/**
 * Get co author social links
 *
 * @param object  $author
 * @return string HTML output of social links
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_co_authors_links' ) ) :
	function megaphone_get_co_authors_links( $author ) {

		$social           = megaphone_get_social();
		$social['aim']    = 'aim';
		$social['jabber'] = 'jabber';

		$output = '';

		if ( $author->website ) {
			$output .= '<a href="' . esc_url( $author->website ) . '" target="_blank" class="megaphone-author-button"><i class="megaphone-icon mf mf-website"></i></a>';
		}

		foreach ( $social as $key => $name ) {

			if ( isset( $author->$key ) && ! empty( $author->$key ) ) {

				$output .= '<a href="' . esc_url( $author->$key ) . '" target="_blank" class="megaphone-author-button"><i class="fa fa-' . $key . '"></i></a>';

			}
		}

		return wp_kses_post( $output );
	}
endif;

/**
 * Generate related posts query
 *
 * Depending on post ID generate related posts using theme options
 *
 * @param int     $post_id
 * @return object WP_Query
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_related' ) ) :
	function megaphone_get_related( $podcast = 'blog', $post_id = false ) {

		if ( empty( $post_id ) ) {
			$post_id = get_the_ID();
		}

		$podcast_args = megaphone_get_podcast_arguments();

		$args['post_type']           = $podcast_args[$podcast]['post_type'];
		$args['ignore_sticky_posts'] = 1;

		// Exclude current post from query
		$args['post__not_in']  = array( $post_id );
		$args['related_type']  = megaphone_is_podcast() ? 'podcast' : 'blog';
	
		$num_posts = absint( megaphone_get_option( 'single_' . $args['related_type'] . '_related_limit' ) );
		if ( $num_posts > 100 ) {
			$num_posts = 100;
		}
		$args['posts_per_page'] = $num_posts;
		
		$args['orderby'] = megaphone_get_option( 'single_' . $args['related_type'] . '_related_order' );
		$args['order']   = 'DESC';

		if ( $args['orderby'] == 'title' ) {
			$args['order'] = 'ASC';
		}

		$type = megaphone_is_podcast() ? 'podcast' : megaphone_get_option( 'single_blog_related_type' );

		if ( $type ) {

			switch ( $type ) {

			case 'cat':
				$cats     = get_the_category( $post_id );
				$cat_args = array();
				if ( ! empty( $cats ) ) {
					foreach ( $cats as $k => $cat ) {
						$cat_args[] = $cat->term_id;
					}
				}
				$args['category__in'] = $cat_args;
				break;

			case 'tag':
				$tags     = get_the_tags( $post_id );
				$tag_args = array();
				if ( ! empty( $tags ) ) {
					foreach ( $tags as $tag ) {
						$tag_args[] = $tag->term_id;
					}
				}
				$args['tag__in'] = $tag_args;
				break;

			case 'cat_and_tag':
				$cats     = get_the_category( $post_id );
				$cat_args = array();
				if ( ! empty( $cats ) ) {
					foreach ( $cats as $k => $cat ) {
						$cat_args[] = $cat->term_id;
					}
				}
				$tags     = get_the_tags( $post_id );
				$tag_args = array();
				if ( ! empty( $tags ) ) {
					foreach ( $tags as $tag ) {
						$tag_args[] = $tag->term_id;
					}
				}
				$args['tax_query'] = array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'category',
						'field'    => 'id',
						'terms'    => $cat_args,
					),
					array(
						'taxonomy' => 'post_tag',
						'field'    => 'id',
						'terms'    => $tag_args,
					),
				);
				break;

			case 'cat_or_tag':
				$cats     = get_the_category( $post_id );
				$cat_args = array();
				if ( ! empty( $cats ) ) {
					foreach ( $cats as $k => $cat ) {
						$cat_args[] = $cat->term_id;
					}
				}
				$tags     = get_the_tags( $post_id );
				$tag_args = array();
				if ( ! empty( $tags ) ) {
					foreach ( $tags as $tag ) {
						$tag_args[] = $tag->term_id;
					}
				}
				$args['tax_query'] = array(
					'relation' => 'OR',
					array(
						'taxonomy' => 'category',
						'field'    => 'id',
						'terms'    => $cat_args,
					),
					array(
						'taxonomy' => 'post_tag',
						'field'    => 'id',
						'terms'    => $tag_args,
					),
				);
				break;

			case 'author':
				global $post;
				$author_id      = isset( $post->post_author ) ? $post->post_author : 0;
				$args['author'] = $author_id;
				break;

			case 'podcast':
				$terms = get_the_terms( $post_id, $podcast_args['podcast']['taxonomy'] );
				$term  = megaphone_get_podcast_term( megaphone_get( 'episodes_ids' ), $terms );

				$args['tax_query'] = array(
					array(
						'taxonomy' => $podcast_args['podcast']['taxonomy'],
						'field'    => 'id',
						'terms'    => $term['id'],
					)
				);

			case 'default':
				break;
			}
		}

		$related_query = new WP_Query( $args );

		return $related_query;
	}
endif;


/**
 * Get post layout options
 * Return post layout params based on theme options
 *
 * @return string
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_post_layout_options' ) ) :
	function megaphone_get_post_layout_options( $layout ) {
		$args = array();

		$get_meta_up       = megaphone_get_option( 'layout_' . $layout . '_meta_up' );
		$args['meta_up']   = $get_meta_up !== 'none' ? array( $get_meta_up ) : array();
		$args['meta_down'] = megaphone_get_option( 'layout_' . $layout . '_meta_down' );

		$args['excerpt']      = megaphone_get_option( 'layout_' . $layout . '_excerpt' ) ? megaphone_get_option( 'layout_' . $layout . '_excerpt_limit' ) : false;
		$args['excerpt_type'] = $args['excerpt'] ? megaphone_get_option( 'layout_' . $layout . '_excerpt_type' ) : 'auto';
		$args['rm']           = megaphone_get_option( 'layout_' . $layout . '_rm' );
		$args['width']        = megaphone_get_option( 'layout_' . $layout . '_width' );

		$args = apply_filters( 'megaphone_modify_post_layout_' . $layout . '_options', $args );

		return $args;
	}
endif;

/**
 * Get episode layout options
 * Return post layout params based on theme options
 *
 * @return string $layout
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_episode_layout_options' ) ) :
	function megaphone_get_episode_layout_options( $layout ) {
		$args = array();

		$args['excerpt']      = megaphone_get_option( 'layout_' . $layout . '_episode_excerpt' ) ? megaphone_get_option( 'layout_' . $layout . '_episode_excerpt_limit' ) : false;
		$args['excerpt_type'] = $args['excerpt'] ? megaphone_get_option( 'layout_' . $layout . '_episode_excerpt_type' ) : 'auto';
		$args['width']        = megaphone_get_option( 'layout_' . $layout . '_episode_width' );

		$args['meta_up'] = array();

		if ( megaphone_is_sponsored_episode() ) {
			$args['meta_up'][] = 'sponsored';
		}

		$get_meta_up       = megaphone_get_option( 'layout_' . $layout . '_episode_meta_up' );
		$args['meta_up'][] = $get_meta_up !== 'none' ? $get_meta_up : '';

		$args['meta_down']      = megaphone_get_option( 'layout_' . $layout . '_episode_meta_down' );
		$args['episode_number'] = megaphone_get_option( 'layout_' . $layout . '_episode_number' );
		$args['play_btn']       = megaphone_get_option( 'layout_' . $layout . '_episode_play_btn' );
		$args['play_icon']       = megaphone_get_option( 'layout_' . $layout . '_episode_play_icon' );

		$args['featured_image']       = megaphone_get_option( 'layout_' . $layout . '_episode_img_type' ) === 'image' ? megaphone_get_featured_image( 'megaphone-' . $layout . '-episode' ) : false;
		$args['featured_image_class'] = empty( $args['featured_image'] ) ? 'megaphone-no-image megaphone-' . $layout . '-episode' : '';

		$args = apply_filters( 'megaphone_modify_episode_layout_' . $layout . '_options', $args );

		return $args;
	}
endif;

/**
 * Get shows layout options
 * Return post layout params based on theme options
 *
 * @return string $layout
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_show_layout_options' ) ) :
	function megaphone_get_show_layout_options() {
		$args = array();

		$args['layout']              = megaphone_get_option( 'show_layout_loop' );
		$args['episodes']            = megaphone_get_option( 'show_layout_display_episodes' );
		$args['episodes_limit']      = megaphone_get_option( 'show_layout_display_episodes_limit' );
		$args['play_btn']            = megaphone_get_option( 'show_layout_display_play_btn' );
		$args['episode_number']      = megaphone_get_option( 'show_layout_display_episodes_number' );
		$args['episode_view_all']    = megaphone_get_option( 'show_layout_display_view_episodes_btn' );
		$args['meta']                = megaphone_get_option( 'show_layout_meta' );
		$args['description']         = megaphone_get_option( 'show_layout_description' );
		$args['featured_image']      = in_array( $args['layout'], array( 4, 5, 6 ) ) && !megaphone_get_option( 'show_layout_display_image' ) ? false : true;
		$args['duotone_overlay']     = $args['featured_image'] && megaphone_get_option( 'show_layout_duotone_overlay' ) ? 'megaphone-duotone-overlay' : '';

		$args = apply_filters( 'megaphone_modify_show_layouts_options', $args );

		return $args;
	}
endif;

/**
 * Check if is sidebar enabled
 *
 * @param string  $position_to_check sidebar position to check
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_has_sidebar' ) ) :
	function megaphone_has_sidebar( $position_to_check = 'right', $sidebar_name = 'sidebar' ) {

		$sidebar = megaphone_get( $sidebar_name );

		if ( empty( $sidebar ) ) {
			return false;
		}

		if ( $sidebar['position'] != $position_to_check ) {
			return false;
		}

		return true;
	}
endif;

/**
 * Get slider class
 *
 * @return string|bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_slider_class' ) ) :
	function megaphone_get_slider_class( $slider ) {

		if ( empty( $slider ) ) {
			return '';
		}

		return 'megaphone-slider';

	}
endif;

/**
 * Get boostrap wrapper col class depending
 * on what layout is used
 *
 * @return string
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_loop_col_class' ) ) :
	function megaphone_get_loop_col_class( $layout_id = 1, $type = 'post' ) {

		if ( $type == 'episode' ) {
			$params = megaphone_get_episode_layouts_map();
		} else {
			$params = megaphone_get_post_layouts_map();
		}

		if ( ! array_key_exists( $layout_id, $params ) ) {
			return '';
		}

		if ( isset( $params[ $layout_id ]['col'] ) && $params[ $layout_id ]['col'] ) {
			return $params[ $layout_id ]['col'];
		}

		return '';

	}
endif;


/**
 * Get class for featured area layout wrapper
 *
 * @return string
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_fa_class' ) ) :
	function megaphone_get_fa_class( $layout_id = 1, $section_title = false ) {

		$params = megaphone_get_featured_layouts_map();

		if ( ! array_key_exists( $layout_id, $params ) ) {
			return '';
		}

		$classes = array();

		if ( isset( $params[ $layout_id ]['slider'] ) ) {
			$classes[] = 'megaphone-slider';
		}

		if ( isset( $params[ $layout_id ]['slider'] ) && empty( $section_title ) || ! empty( $params[ $layout_id ]['slider_nav'] ) ) {
			$classes[] = 'has-arrows';
		}

		if ( isset( $params[ $layout_id ]['carousel'] ) ) {
			$classes[] = 'megaphone-carousel';
		}

		if ( isset( $params[ $layout_id ]['center'] ) ) {
			$classes[] = 'slider-center';
		}

		if ( isset( $params[ $layout_id ]['classes'] ) ) {
			$classes[] = $params[ $layout_id ]['classes'];
		}

		return implode( ' ', $classes );
	}
endif;


/**
 * Get loop layout params
 *
 * @param int     $layout_id
 * @param int     $loop_index current post in the loop
 * @return array set of parameters
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_loop_params' ) ) :
	function megaphone_get_loop_params( $layout_id = 1, $loop_index = 0, $type = 'default' ) {

		if ( $type == 'fa' ) {
			$params = megaphone_get_featured_layouts_map();
		} elseif ( $type == 'episode' ) {
			$params = megaphone_get_episode_layouts_map();
		} else {
			$params = megaphone_get_post_layouts_map();
		}

		//todo check shows and episodes

		if ( array_key_exists( $layout_id, $params ) ) {

			$layout = $params[ $layout_id ]['loop'];

			if ( count( $layout ) > $loop_index && ! is_paged() ) {
				return $layout[ $loop_index ];
			}

			return $layout[ count( $layout ) - 1 ];
		}

		return false;

	}
endif;

/**
 * Check if specified loop layout can display sidebar
 *
 * @param int     $layout_id
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_loop_has_sidebar' ) ) :
	function megaphone_loop_has_sidebar( $layout_id = 1 ) {

		$params = megaphone_get_post_layouts_map();

		//todo check shows and episodes

		if ( ! array_key_exists( $layout_id, $params ) ) {
			return false;
		}

		if ( isset( $params[ $layout_id ]['sidebar'] ) && $params[ $layout_id ]['sidebar'] ) {
			return true;
		}

		return false;

	}
endif;


/**
 * Check if specified layout is indented
 *
 * @param int     $layout_id
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_layout_is_indented' ) ) :
	function megaphone_layout_is_indented( $layout_id = 1 ) {

		return in_array( $layout_id, array( 1, 9, 10, 15, 16, 17, 18, 21, 22, 28, 29, 32, 33, 34, 35 ) );

	}
endif;



/**
 * Get archive content
 *
 * Function gets parts of the archive content like ttitle, description, post count, etc...
 *
 * @return array Args
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_archive_content' ) ) :
	function megaphone_get_archive_content( $part = false ) {

		global $wp_query;

		if ( ( is_home() && is_front_page() ) || ( is_home() && ! $wp_query->is_posts_page ) ) {
			return false;
		}

		$defaults = array(
			'avatar'      => '',
			'title'       => '',
			'meta'        => megaphone_get_archive_posts_count(),
			'description' => '',
			'subnav'      => '',
		);

		$args = array();

		$title_prefix = '';

		if ( is_category() ) {

			$title_prefix        = megaphone_get_archive_type() == 'blog' ? __megaphone( 'category' ) : __megaphone( 'show' );
			$args['title']       = single_cat_title( '', false );
			$args['description'] = category_description();

		} elseif ( is_tag() ) {
			$title_prefix        = __megaphone( 'tag' );
			$args['title']       = single_tag_title( '', false );
			$args['description'] = tag_description();

		} elseif ( is_author() ) {
			$title_prefix = __megaphone( 'author' );

			if ( megaphone_is_co_authors_active() && $coauthors = get_coauthors() ) {

				$args['title']       = get_the_author();
				$args['description'] = get_the_author_meta( 'description' );
				$args['avatar']      = get_avatar( get_the_author_meta( 'ID' ), 100 );
				$args['subnav']      = megaphone_get_author_links( get_the_author_meta( 'ID' ), false );

				$current_author_id = get_queried_object_id();
				array_shift( $coauthors );

				foreach ( $coauthors as $author ) {

					if ( $author->ID === $current_author_id ) {
						$args['title']       = esc_html( $author->display_name );
						$args['description'] = wpautop( wp_kses_post( $author->description ) );
						$args['avatar']      = get_avatar( $current_author_id, 100 );
						$args['subnav']      = megaphone_get_co_authors_links( $author );
					}
				}
			} else {
				$args['title']       = get_the_author();
				$args['description'] = get_the_author_meta( 'description' );
				$args['avatar']      = get_avatar( get_the_author_meta( 'ID' ), 100 );
				$args['subnav']      = megaphone_get_author_links( get_the_author_meta( 'ID' ), false );
			}
		} elseif ( is_tax() ) {
			$title_prefix  = megaphone_get_archive_type() == 'blog' ? __megaphone( 'archive' ) : __megaphone( 'show' );
			$args['title'] = single_term_title( '', false );

		} elseif ( is_search() ) {
			$args['description'] =
				'<span class="archive-label">' . __megaphone( 'search_results_for' ) . '</span>' .
				'<form class="search-form search-alt" action="' . esc_url( home_url( '/' ) ) . '" method="get">
                    <input name="s" type="text" value="" placeholder="' . esc_attr( get_search_query() ) . '" />';

			if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
				$args['description'] .= '<input type="hidden" name="lang" value="' . esc_attr( ICL_LANGUAGE_CODE ) . '">';
			}

			$args['description'] .= '<button type="submit" class="megaphone-button megaphone-button-hollow">' . esc_attr( __megaphone( 'search_again' ) ) . '</button></form>';

			$args['meta'] = '';

		} elseif ( is_day() ) {
			$title_prefix  = __megaphone( 'archive' );
			$args['title'] = get_the_date();

		} elseif ( is_month() ) {
			$title_prefix  = __megaphone( 'archive' );
			$args['title'] = get_the_date( 'F Y' );

		} elseif ( is_year() ) {
			$title_prefix  = __megaphone( 'archive' );
			$args['title'] = get_the_date( 'Y' );

		} elseif ( $wp_query->is_posts_page ) {
			$posts_page    = get_option( 'page_for_posts' );
			$args['title'] = get_the_title( $posts_page );

		} elseif ( is_archive() ) {

			$args['title'] = __megaphone( 'archive' );
		}

		if ( $title_prefix ) {
			$args['title'] = '<span class="archive-label mb-4 mb-sm-4 mb-md-4">' . $title_prefix . '</span>' . $args['title'];
		}

		$args = apply_filters( 'megaphone_modify_archive_content', wp_parse_args( $args, $defaults ) );

		if ( $part && isset( $args[ $part ] ) ) {
			return $args[ $part ];
		}

		return $args;

	}
endif;


/**
 * Get media
 *
 * Function gets featured image
 *
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_media' ) ) :

	function megaphone_get_single_media( $type = 'blog', $before = '', $after = '' ) {

		$output = '';

		if ( megaphone_get( 'fimg' ) && $fimg = megaphone_get_featured_image( 'megaphone-single-' . $type . '-' . megaphone_get( 'layout' ), true ) ) {
			$output = megaphone_wp_kses( $fimg );

			if ( megaphone_get( 'fimg_cap' ) && $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) {
				$output .= '<figure class="wp-caption-text">' . wp_kses_post( $caption ) . '</figure>';
			}
		}

		if ( empty( $output ) ) {
			return '';
		}

		return $before . $output . $after;

	}

endif;


/**
 * Check archive type base on selected taxonomy and terms
 * Use it on frontend and on backed (on category edit page to disable some posts layouts)
 *
 * @param int     $id Archive ID
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_archive_type' ) ) :
	function megaphone_get_archive_type( $id = false ) {

		$podcast_args = megaphone_get_podcast_arguments();
		
		if ( ! $podcast_args['podcast']['shows'] ) {
			return 'blog';
		}

		if ( empty( $id ) ) {
			$id = get_queried_object_id();
		}

		if ( is_tax( $podcast_args['podcast']['taxonomy'], array( $id ) ) || ( $podcast_args['podcast']['taxonomy'] == 'category' && is_category() ) ) {
		
			if ( $podcast_args['podcast']['operator'] == 'IN' ) {
				if ( $podcast_args['podcast']['terms'] == 'all' || in_array( $id, $podcast_args['podcast']['terms'] ) ) {
					return 'podcast';
				}
			} 
	
			if ( $podcast_args['podcast']['operator'] == 'NOT IN' ) {
				if (  $podcast_args['podcast']['terms'] != 'all' && !in_array( $id, $podcast_args['podcast']['terms'] ) ) {
					return 'podcast';
				}
			} 
		}

		return 'blog';

	}
endif;


/**
 * Check for show
 * Function check is how or category
 *
 * @param int     $id Archive ID
 * @return bool
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_is_show' ) ) :
	function megaphone_is_show( $id ) {

		$podcast_args = megaphone_get_podcast_arguments();

		if ( ! $podcast_args['podcast']['shows'] ) {
			return false;
		}

		// case IN
		if ( $podcast_args['podcast']['operator'] == 'IN' && $podcast_args['podcast']['terms'] == 'all' && $podcast_args['podcast']['taxonomy'] == 'category' ) {
			return true;
		} 
		
		if ( $podcast_args['podcast']['operator'] == 'IN' && $podcast_args['podcast']['terms'] == 'all' && $podcast_args['podcast']['taxonomy'] != 'category' ) {
			return has_term( '', $podcast_args['podcast']['taxonomy'], $id );
		}

		if ( $podcast_args['podcast']['operator'] == 'IN' && $podcast_args['podcast']['terms'] != 'all' ) {
			return in_array( $id, $podcast_args['podcast']['terms'] );
		}

		// case NOT IN
		if ( $podcast_args['podcast']['operator'] == 'NOT IN' && $podcast_args['podcast']['terms'] != 'all' && !in_array( $id, $podcast_args['podcast']['terms'] ) ) {
				return true;
		} 

		return false;
		
	}
endif;

/**
 * Get episode number
 *
 * @param int     $cat_id
 * @return array
 */
if ( ! function_exists( 'megaphone_get_episode_number' ) ) :
	function megaphone_get_episode_number( $post_ids, $current_post_id, $multi = false ) {

		if ( megaphone_get_option( 'episode_number_from_title' ) ) {
			return megaphone_get_episode_number_from_title( $current_post_id );
		}

		if ( empty( $post_ids ) && empty( $current_post_id ) ) {
			return false;
		}

		$episode_ids  = $post_ids;
		$podcast_args = megaphone_get_podcast_arguments();
		$terms  	  = get_the_terms( $current_post_id, $podcast_args['podcast']['taxonomy'] );
		$term 		  = megaphone_get_podcast_term( $post_ids, $terms );

		if ( ! empty( $multi ) && is_tax( $podcast_args['podcast']['taxonomy'], $term['id'] )  ) {
			$term_id  = !empty( $term['obj']->parent ) ? $term['obj']->parent : $term['id'];
			$episode_ids = $post_ids[ $term_id ];
		}

		if ( ! empty( $multi ) && ! is_tax( $podcast_args['podcast']['taxonomy'], $term['id'] ) ) {
			$term_id  = !empty( $term['obj']->parent ) ? $term['obj']->parent : $term['id'];
			if( isset($post_ids[ $term_id ]) ) {
				$episode_ids = $post_ids[ $term_id ];
			}
		}

		// if ( ! empty( $multi ) && is_category() ) {
		// 	//$cat_id   = get_queried_object_id();
		// 	$term_id  = !empty( $term['obj']->parent ) ? $term['obj']->parent : $term['id'];
		// 	$episode_ids = $post_ids[ $term_id ];
		// }

		if ( ! isset( $episode_ids[ $current_post_id ] ) || is_array( $episode_ids[ $current_post_id ] ) ) {
			return false;
		}

		return $episode_ids[ $current_post_id ] + 1;

	}
endif;

/**
 * Preg match for episode number if that option is enabled
 *
 * @param int     $current_post_id
 * @return array
 */
if ( ! function_exists( 'megaphone_get_episode_number_from_title' ) ) :
	function megaphone_get_episode_number_from_title( $current_post_id = '' ) {

		$title = empty( $current_post_id ) ? get_the_title() : get_the_title( $current_post_id );

		$string = megaphone_get_option( 'episode_number_format' );

		if ( empty( $string ) ) {
			return false;
		}

		$string = megaphone_esc_regex_chars( $string );

		$pattern = '/' . str_replace( '{number}', '([0-9]*)', $string ) . '/';

		preg_match( $pattern, $title, $matches );

		if ( empty( $matches ) ) {
			return false;
		}

		return $matches[1];

	}
endif;

/**
 * Preg match get episodes title
 *
 * @param int     $current_post_id
 * @return array
 */
if ( ! function_exists( 'megaphone_get_episode_title' ) ) :
	function megaphone_get_episode_title( $current_post_id = '' ) {

		$title = empty( $current_post_id ) ? get_the_title() : get_the_title( $current_post_id );

		if ( ! megaphone_get_option( 'episode_number_from_title' ) ) {
			return $title;
		}

		if ( ! megaphone_get_option( 'strip_episode_number_from_title' ) ) {
			return $title;
		}

		$string = megaphone_get_option( 'episode_number_format' );

		if ( empty( $string ) ) {
			return $title;
		}

		$string = megaphone_esc_regex_chars( $string );

		$pattern = '/(.*)(' . str_replace( '{number}', '([0-9]*)', $string ) . ')(.*)/';

		return preg_replace( $pattern, '$1$4', $title );

	}
endif;

/**
 * Get episodes ids from all categories
 *
 * @param array   $categories
 * @param string  $exclude
 *
 * @return array
 */
if ( ! function_exists( 'megaphone_get_episodes_ids' ) ) :
	function megaphone_get_episodes_ids( $terms = array(), $exclude = '' ) {

		$podcast_args = megaphone_get_podcast_arguments();
		$term_args    = array(
			'hide_empty' => true,
			'fields'     => 'ids',
			'taxonomy' 	 => $podcast_args['podcast']['taxonomy']
		);

		if ( ! empty( $terms ) ) {
			$term_args['include'] = $terms;
		} else if ( $podcast_args['podcast']['operator'] == 'IN'  ) {
			$term_args['include'] = $podcast_args['podcast']['terms'];
		} else if ( $podcast_args['podcast']['operator'] == 'NOT IN' ) {
			//$include_exclude = $podcast_args['podcast']['post_type'] == 'post' ? 'include'
			$term_args['exclude'] = $podcast_args['podcast']['terms'];
		}

		
		$term_ids = get_terms( $term_args );

		$episodes = array();

		if ( ! empty( $term_ids ) ) {
			foreach ( $term_ids as $id ) {

				$args = array(
					'posts_per_page' => -1,
					'fields'         => 'ids',
					'order_by'       => 'date',
					'order'          => 'ASC',
					'post_type'      => $podcast_args['podcast']['post_type'],
				);

				$args['tax_query'][0]['taxonomy'] = $podcast_args['podcast']['taxonomy'];
				$args['tax_query'][0]['terms']    = $id;
				$args['tax_query'][0]['field']    = 'term_id';				

				$posts_ids = new WP_Query( $args );

				$episodes[ $id ] = array_flip( $posts_ids->posts );
			}
		}

		return $episodes;

	}
endif;

/**
 * Set pagination on last posts or episodes section on front page.
 *
 * @param array   $sections
 *
 * @return int
 */
if ( ! function_exists( 'megaphone_frontpage_paginated_section' ) ) :
	function megaphone_frontpage_paginated_section( $sections = array() ) {

		if ( empty( $sections ) ) {
			return false;
		}

		$last_section_index = false;

		foreach ( $sections as $number => $section ) {

			$has_slider = ! megaphone_get_option( 'front_page_' . $section . '_slider' );

			if ( in_array( $section, array( 'posts', 'episodes' ) ) && $has_slider ) {
				$last_section_index = $number;
			}
		}

		if ( $last_section_index !== false ) {
			return $sections[ $last_section_index ];
		}

		return false;

	}
endif;


/**
 * Get co authors and order it by host and guest author
 *
 * @return array
 */
if ( ! function_exists( 'megaphone_get_coauthors' ) ) :
	function megaphone_get_coauthors( $only_guests = false ) {

		$co_authors = array();

		if ( ! megaphone_is_co_authors_active() ) {
			return $co_authors;
		}

		$hosts  = array();
		$guests = array();

		$coauthors = get_coauthors();

		if ( empty( $coauthors ) ) {
			return $co_authors;
		}

		foreach ( $coauthors as $author ) {
			if ( $author->type == 'guest-author' ) {
				$guests[] = $author;
			} else {
				$hosts[] = $author;
			}
		}

		if ( $only_guests ) {
			return $guests;
		}

		$co_authors = array_merge( $hosts, $guests );

		return $co_authors;
	}
endif;

/**
 * Get archive play episode button
 *
 * @param string  $classes
 * @return array
 */
if ( ! function_exists( 'megaphone_play_button' ) ) :
	function megaphone_play_button( $classes = '', $label = 'default' ) {

		$play_trigger_class = megaphone_is_meks_ap_active() && !megaphone_get_option( 'play_button_is_link' )  ? 'megaphone-play' : '';
?>

		<a class="<?php echo esc_attr( $play_trigger_class ); ?> megaphone-play-<?php the_ID(); ?> <?php echo esc_attr( $classes ); ?>"  data-play-id="<?php the_ID(); ?>" href="<?php the_permalink(); ?>">
			<i class="mf mf-play"></i>
			<?php if ( !empty( $label ) ) : ?>
				<?php if ( $label == 'default' ): ?>
				<?php echo __megaphone( 'play_episode' ); ?>
				<?php else: ?>
				<?php echo wp_kses_post( $label ); ?>
				<?php endif; ?>
			<?php endif; ?>
		</a>

		<?php

	}
endif;

/**
 * Get prev next episodes
 *
 * @return array
 */
if ( ! function_exists( 'megaphone_get_prev_next_episodes' ) ) :
	function megaphone_get_prev_next_episodes( ) {


		$prev = array();
		$next = array();
		
		$podcast_args = megaphone_get_podcast_arguments();

		if ( is_single() ) {

			$order = megaphone_get_option( 'player_prev_next' ) == 'asc' ? true : false;
			$order_prev = $order ? 'DESC' : 'ASC';
			$order_next = $order ? 'ASC' : 'DESC';
			
			$prev_post = megaphone_get_adjacent_post( true, '', $order, $podcast_args['podcast']['taxonomy'], $podcast_args['podcast']['post_type']  );
			$next_post = megaphone_get_adjacent_post( true, '', !$order, $podcast_args['podcast']['taxonomy'], $podcast_args['podcast']['post_type'] );

			$terms = get_the_terms( get_the_ID(), $podcast_args['podcast']['taxonomy'] );
			$id = isset( $terms[0]->term_id ) ? $terms[0]->term_id : false;
 
			$query_args = array( 
				'post_type' => $podcast_args['podcast']['post_type'], 
				'posts_per_page' => 1,
				'tax_query' => array(
					array(
						'taxonomy' => $podcast_args['podcast']['taxonomy'],
						'field'    => 'term_id',
						'terms'    => $id
					)
				)
			);

			if ( !empty( $prev_post ) ) {
				$prev['id'] = $prev_post->ID;
				$prev['permalink'] = get_permalink( $prev_post->ID );
			} else {
				$query_args['order'] = $order_prev;
				$first = new WP_Query( $query_args );
				$first->the_post();
				$prev['id'] = get_the_ID();
				$prev['permalink'] = get_permalink();
				wp_reset_postdata();
			}

			if ( !empty( $next_post ) ) {
				$next['id'] = $next_post->ID;
				$next['permalink'] = get_permalink( $next_post->ID );
			} else {
				$query_args['order'] = $order_next;
				$first = new WP_Query( $query_args );
				$first->the_post();
				$next['id'] = get_the_ID();
				$next['permalink'] = get_permalink();
				wp_reset_postdata();
			}

		}

		return array( 'prev' => $prev, 'next' => $next );

	}
endif;


/**
 * Generate menu paceholder (if there is no menu set)
 *
 * @return string
 */
if ( ! function_exists( 'megaphone_menu_placeholder' ) ) :
	function megaphone_menu_placeholder( $label = '' ) {

		include locate_template( 'template-parts/header/elements/menu-placeholder.php', false, false );

	}
endif;

/**
 * Get podcast term
 *
 * @param array $terms
 * @return int
 */
if ( ! function_exists( 'megaphone_get_podcast_term' ) ) :
	function megaphone_get_podcast_term( $post_ids = array(), $terms ) {

		$term = array();

		if ( empty( $post_ids ) ) {
			$post_ids = megaphone_get( 'episodes_ids' );
		}

		if ( empty( $terms ) ) {
			return false;
		}
		
		foreach ( $terms as $obj ) {
			if ( isset( $post_ids[ $obj->term_id ] ) ){
				$term['obj']  = $obj;
				$term['id']   = $obj->term_id;
			}
		}

		return $term;

	}
endif;
