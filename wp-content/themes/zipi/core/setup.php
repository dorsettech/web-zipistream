<?php

/**
 * After Theme Setup
 *
 * Callback for after_theme_setup hook
 *
 * @since  1.0
 */

add_action( 'after_setup_theme', 'megaphone_theme_setup' );

function megaphone_theme_setup() {

	/* Define default content width */
	$GLOBALS['content_width'] = megaphone_size_by_col( 12 );

	/* Localization */
	load_theme_textdomain( 'megaphone', get_parent_theme_file_path( '/languages' ) );

	/* Add thumbnails support */
	add_theme_support( 'post-thumbnails' );

	/* Add theme support for title tag */
	add_theme_support( 'title-tag' );

	/* Add image sizes */
	$image_sizes = megaphone_get_image_sizes();
	// print_r( $image_sizes );
	if ( ! empty( $image_sizes ) ) {
		foreach ( $image_sizes as $id => $size ) {
			add_image_size( $id, $size['w'], $size['h'], $size['crop'] );
		}
	}

	/* Support for HTML5 elements */
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	/* Automatic feed links */
	add_theme_support( 'automatic-feed-links' );

	/* Load editor styles */
	add_theme_support( 'editor-styles' );

	if ( is_admin() ) {
		megaphone_load_editor_styles();
	}

	/* Support for align wide elements */
	add_theme_support( 'align-wide' );

	 /* Support for responsive embeds */
	add_theme_support( 'responsive-embeds' );

	/*
	 Support for predefined colors in editor */
	add_theme_support( 'editor-color-palette', megaphone_get_editor_colors() );

	/*
	 Support for predefined font-sizes in editor */
	add_theme_support( 'editor-font-sizes', megaphone_get_editor_font_sizes() );

	/* WooCommerce features support */
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );

}


/**
 * Check all display settings from theme options
 * and store it globally as a query var so we can access it from any template file
 *
 * @since  1.0
 */

add_action( 'template_redirect', 'megaphone_templates_setup' );

if ( ! function_exists( 'megaphone_templates_setup' ) ) :
	function megaphone_templates_setup() {

		// delete_option('megaphone_settings');
		// delete_option('megaphone_welcome_box_displayed');
		// delete_option('merlin_megaphone_completed');

		$defaults = megaphone_get_default_template_options();
		$podcast_args = megaphone_get_podcast_arguments();

		if ( is_front_page() ) {

			if ( megaphone_get_option( 'front_page_template' ) ) {

				$megaphone = megaphone_get_front_page_template_options();

			} else {

				if ( 'posts' == get_option( 'show_on_front' ) ) {
					$megaphone = megaphone_get_archive_template_options();
				} else {
					$megaphone = megaphone_get_page_template_options();
				}
			}
		} elseif ( is_page_template( 'template-shows.php' ) ) {
			$megaphone = megaphone_get_shows_template_options();
		} elseif ( is_page_template( 'template-authors.php' ) ) {
			$megaphone = megaphone_get_authors_template_options();
		} elseif ( is_page_template( 'template-blank.php' ) ) {
			$megaphone = megaphone_get_blank_template_options();
		} elseif ( megaphone_is_woocommerce_page() ) {
			 $megaphone = megaphone_get_woocommerce_template_options();
		} elseif ( is_page() ) {
			$megaphone = megaphone_get_page_template_options();
		} elseif ( is_single() ) {
			$megaphone = megaphone_get_single_template_options();
		} elseif ( is_category() || is_tax( 'series' ) ) {
			$megaphone = megaphone_get_category_template_options();
		} elseif ( is_404() ) {
			$megaphone = megaphone_get_404_template_options();
		} else {
			$megaphone = megaphone_get_archive_template_options();
		}

		$megaphone['header'] = megaphone_get_header_options();
		$megaphone['footer'] = megaphone_get_footer_options();
		$megaphone['ads']    = megaphone_get_ads_options();
		
		$megaphone['podcast'] = $podcast_args['podcast'];
		
		$megaphone = megaphone_parse_args( $megaphone, $defaults );
		$megaphone = apply_filters( 'megaphone_modify_templates_setup', $megaphone );

		//print_r($megaphone);
		set_query_var( 'megaphone', $megaphone );

	}
endif;



/**
 * Get default display options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_default_template_options' ) ) :
	function megaphone_get_default_template_options() {

		$args        = array();
		$podcast_args = megaphone_get_podcast_arguments();

		$args['sidebar']      = array( 'position' => 'none' );
		$args['display']      = array(
			'header' => true,
			'footer' => true,
			'title'  => true,
		);
		$args['episodes_ids'] = megaphone_get_episodes_ids( array(), $podcast_args['blog']['terms'] );

		return apply_filters( 'megaphone_modify_default_template_options', $args );
	}
endif;


/**
 * Get single template options
 * Return single post params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_single_template_options' ) ) :
	function megaphone_get_single_template_options() {

		$meta        = megaphone_get_post_meta();
		$podcast_args = megaphone_get_podcast_arguments();

		$args = array();

		$args['type'] = megaphone_is_podcast() ? 'podcast' : 'blog';

		$args['layout']  = $meta['layout'];
		$args['sidebar'] = $meta['sidebar'];

		$args['show_episode_number'] = megaphone_get_option( 'single_' . $args['type'] . '_number' );
		$args['episodes_ids']        = megaphone_get_episodes_ids( array(), $podcast_args['blog']['terms'] );
		$args['podcast_subscribe']   = megaphone_get_option( 'single_podcast_subscribe' );

		if ( megaphone_is_sponsored_episode() ) {
			$args['meta_up'][] = 'sponsored';
		}
		$get_meta_up       = megaphone_get_option( 'single_podcast_meta_up' );
		$args['meta_up'][] = $get_meta_up !== 'none' ? $get_meta_up : '';

		$args['meta_down']      = megaphone_get_option( 'single_' . $args['type'] . '_meta_down' );
		$args['headline']       = megaphone_get_option( 'single_' . $args['type'] . '_headline' );
		$args['fimg']           = megaphone_get_option( 'single_' . $args['type'] . '_fimg' );
		$args['fimg_cap']       = megaphone_get_option( 'single_' . $args['type'] . '_fimg_cap' );
		$args['avatar']         = megaphone_get_option( 'single_' . $args['type'] . '_avatar' );
		$args['tags']           = megaphone_get_option( 'single_' . $args['type'] . '_tags' );
		$args['author']         = megaphone_get_option( 'single_' . $args['type'] . '_author' );
		$args['related']        = megaphone_get_option( 'single_' . $args['type'] . '_related' );
		$args['related_layout'] = megaphone_get_option( 'single_' . $args['type'] . '_related_layout' );

		$args['show_more']       = megaphone_get_option( 'single_' . $args['type'] . '_show_more' );
		$args['show_more_class'] = $args['show_more'] ? 'megaphone-hidden-content' : '';

		if ( in_array( $args['layout'], array( '3', '4' ) ) ) {
			$args['cover'] = true;
		}

		return apply_filters( 'megaphone_modify_single_template_options', $args );
	}
endif;


/**
 * Get page template options
 * Return page template params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_page_template_options' ) ) :
	function megaphone_get_page_template_options() {

		$meta = megaphone_get_page_meta();

		$args = array();

		$args['layout']   = $meta['layout'];
		$args['sidebar']  = $meta['sidebar'];
		$args['fimg']     = megaphone_get_option( 'page_fimg' );
		$args['fimg_cap'] = megaphone_get_option( 'page_fimg_cap' );

		if ( in_array( $args['layout'], array( '3', '4' ) ) ) {
			$args['cover'] = true;
		}

		return apply_filters( 'megaphone_modify_page_template_options', $args );
	}
endif;

/**
 * Get authors page template options
 * Return page template params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_authors_template_options' ) ) :
	function megaphone_get_authors_template_options() {

		$args 		  = megaphone_get_page_template_options();
		$authors_meta = megaphone_get_page_meta( false, 'authors');

		$args['authors_type'] = $authors_meta['type']; 

		$args['authors_query_args'] = array(
			'fields'              => array( 'ID' ),
			'orderby'             => 'post_count',
			'order'               => 'DESC',
			'has_published_posts' => array( 'post' ),
		);

		return apply_filters( 'megaphone_modify_authors_template_options', $args );
	}
endif;

/**
 * Get shows page template options
 * Return page template params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_shows_template_options' ) ) :
	function megaphone_get_shows_template_options() {

		$args         = megaphone_get_page_template_options();

		$selected_categories = megaphone_get_option( 'show_layout_categories' );
		$cat_args    	 	 = megaphone_get_shows_args( $selected_categories );

		if ( !empty( $cat_args ) ) {
			$categories = get_terms( $cat_args );
			$categories = megaphone_sort_items( $categories, $selected_categories );
		} else {
			$categories = $cat_args;
		}
		
		$args['shows_loop']  = megaphone_get_option( 'show_layout_loop' );
		$args['shows_query'] = $categories;

		if ( megaphone_loop_has_sidebar( $args['shows_loop'] ) ) {
			$args['sidebar'] = array(
				'position' => megaphone_get_option( 'show_layout_sidebar_position' ),
				'classic'  => megaphone_get_option( 'show_layout_sidebar_standard' ),
				'sticky'   => megaphone_get_option( 'show_layout_sidebar_sticky' ),
			);
		} else {
			$args['sidebar'] = array();
		}

		return apply_filters( 'megaphone_modify_shows_template_options', $args );
	}
endif;

/**
 * Get archives options
 * Return archives params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_archive_template_options' ) ) :
	function megaphone_get_archive_template_options() {

		$args = array();

		$args['layout']            = megaphone_get_option( 'archive_layout' );
		$args['loop']              = megaphone_get_option( 'archive_loop' );
		$args['pagination']        = megaphone_get_option( 'archive_pagination' );
		$args['archive_type']      = megaphone_get_archive_type();
		$args['archive_img_class'] = 'megaphone-archive-no-image';

		if ( megaphone_loop_has_sidebar( $args['loop'] ) ) {
			$args['sidebar'] = array(
				'position' => megaphone_get_option( 'archive_sidebar_position' ),
				'classic'  => megaphone_get_option( 'archive_sidebar_standard' ),
				'sticky'   => megaphone_get_option( 'archive_sidebar_sticky' ),
			);
		}

		$archive = megaphone_get_archive_content();

		if ( $archive ) {
			$args['archive_content']     = true;
			$args['archive_title']       = $archive['title'];
			$args['archive_description'] = megaphone_get_option( 'archive_description' ) ? $archive['description'] : '';
			$args['archive_meta']        = megaphone_get_option( 'archive_meta' ) ? $archive['meta'] : '';
			$args['archive_avatar']      = $archive['avatar'];
			$args['archive_subnav']      = $archive['subnav'];
		} else {
			$args['archive_content'] = false;
		}

		$args = apply_filters( 'megaphone_modify_archive_template_options', $args );

		return $args;
	}
endif;

/**
 * Get category options
 * Return category params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_category_template_options' ) ) :
	function megaphone_get_category_template_options() {

		$cat_id       = get_queried_object_id();
		$archive_type = megaphone_get_archive_type();
		$podcast_args  = megaphone_get_podcast_arguments();
		$meta         = $archive_type == 'blog' ? megaphone_get_category_meta( $cat_id ) : megaphone_get_show_meta( $cat_id );

		$args = array();

		$args['layout']       = $meta['layout'];
		$args['loop']         = $meta['loop'];
		$args['archive_type'] = $archive_type;
		$args['pagination']   = $meta['pagination'];

		$image_size                = $archive_type == 'podcast' ? 'megaphone-archive-show' : 'megaphone-archive-category';
		$args['featured_image']    = megaphone_get_category_featured_image( $image_size, false, false );
		$args['archive_img_class'] = $args['featured_image'] ? 'megaphone-overlay' : 'megaphone-archive-no-image';

		$args['episodes_ids'] = megaphone_get_episodes_ids( array( $cat_id ), $podcast_args['blog']['terms'] );

		if ( megaphone_loop_has_sidebar( $args['loop'] ) ) {
			$args['sidebar'] = array(
				'position' => $meta['sidebar']['position'],
				'classic'  => $meta['sidebar']['classic'],
				'sticky'   => $meta['sidebar']['sticky'],
			);
		}

		$archive = megaphone_get_archive_content();

		if ( $archive ) {
			$args['archive_content']     = true;
			$args['archive_title']       = $archive['title'];
			$args['archive_description'] = $meta['archive']['description'] ? $archive['description'] : '';
			$args['archive_meta']        = $meta['archive']['meta'] ? $archive['meta'] : '';
		} else {
			$args['archive_content'] = false;
		}

		if ( ! empty( $args['featured_image'] ) ) {
			$args['cover'] = true;
		}

		return apply_filters( 'megaphone_modify_category_template_options', $args );

	}
endif;

/**
 * Get front page options
 * Return front page template params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_front_page_template_options' ) ) :
	function megaphone_get_front_page_template_options() {

		$has_pagination    = megaphone_get_option( 'front_page_pagination' ) != 'none' ? true : false;
		$paginated_section = $has_pagination ? megaphone_frontpage_paginated_section( megaphone_get_option( 'front_page_sections' ) ) : false;

		$args = array();

		$args['front_page_sections'] = ! is_paged() ? megaphone_get_option( 'front_page_sections' ) : array( $paginated_section );
		$args['pagination_section']  = $paginated_section;
		$args['pagination_type']     = $has_pagination ? megaphone_get_option( 'front_page_pagination' ) : false;

		foreach ( $args['front_page_sections'] as $section ) {

			switch ( $section ) {

				case 'posts':
					// POSTS SECTION
					$posts = (array) megaphone_get_frontpage_posts_args( $paginated_section );
					$args  = array_merge( $args, $posts );
					break;

				case 'episodes':
					// EPISODES SECTION
					$episodes = (array) megaphone_get_frontpage_episodes_args( $paginated_section );
					$args     = array_merge( $args, $episodes );
					break;

				case 'shows':
					// SHOWS SECTION
					$shows = (array) megaphone_get_frontpage_shows_args();
					$args  = array_merge( $args, $shows );
					break;

				case 'featured':
					// FEATURED SECTION
					$featured = (array) megaphone_get_frontpage_featured_args();
					$args     = array_merge( $args, $featured );
					break;

				case 'subscribe':
					// subscribe ON SECTION
					break;

				case 'welcome':
					// WELCOME SECTION
					$welcome = (array) megaphone_get_frontpage_welcome_args();
					$args    = array_merge( $args, $welcome );
					break;

				case 'custom-content':
					// CUSTOM CONTENT SECTION
					$content_type = megaphone_get_option( 'front_page_custom_content_type' );
					$args['custom_content_page'] = $content_type == 'page' ? megaphone_get_option( 'front_page_custom_content_page' ) : '';
					$args['custom_content_display_title'] = megaphone_get_option( 'front_page_custom_content_display_title' );
					$args['custom_content_bg']            = megaphone_get_option( 'front_page_custom_content_bg' );
					break;

				case 'custom-content-2':
					// CUSTOM CONTENT SECTION 2
					$content_type_2 = megaphone_get_option( 'front_page_custom_content_type_2' );
					$args['custom_content_page_2'] = $content_type_2 == 'page' ? megaphone_get_option( 'front_page_custom_content_page_2' ) : '';
					$args['custom_content_display_title_2'] = megaphone_get_option( 'front_page_custom_content_display_title_2' );
					$args['custom_content_bg_2']            = megaphone_get_option( 'front_page_custom_content_bg_2' );
					break;

				case 'custom-content-3':
					// CUSTOM CONTENT SECTION 3
					$content_type_3 = megaphone_get_option( 'front_page_custom_content_type_3' );
					$args['custom_content_page_3'] = $content_type_3 == 'page' ? megaphone_get_option( 'front_page_custom_content_page_3' ) : '';
					$args['custom_content_display_title_3'] = megaphone_get_option( 'front_page_custom_content_display_title_3' );
					$args['custom_content_bg_3']            = megaphone_get_option( 'front_page_custom_content_bg_3' );
					break;

				case 'custom-content-4':
					// CUSTOM CONTENT SECTION 4
					$content_type_4 = megaphone_get_option( 'front_page_custom_content_type_4' );
					$args['custom_content_page_4'] = $content_type_4 == 'page' ? megaphone_get_option( 'front_page_custom_content_page_4' ) : '';
					$args['custom_content_display_title_4'] = megaphone_get_option( 'front_page_custom_content_display_title_4' );
					$args['custom_content_bg_4']            = megaphone_get_option( 'front_page_custom_content_bg_4' );
					break;

				case 'custom-content-5':
					// CUSTOM CONTENT SECTION 5
					$content_type_5 = megaphone_get_option( 'front_page_custom_content_type_5' );
					$args['custom_content_page_5'] = $content_type_5 == 'page' ? megaphone_get_option( 'front_page_custom_content_page_5' ) : '';
					$args['custom_content_display_title_5'] = megaphone_get_option( 'front_page_custom_content_display_title_5' );
					$args['custom_content_bg_5']            = megaphone_get_option( 'front_page_custom_content_bg_5' );
					break;

				default:
					break;
			}
		}

		$args['cover'] = false;

		if ( $args['front_page_sections'][0] == 'welcome' && ! $args['wa_display_title'] && in_array( $args['wa_layout'], array( '1', '2' ) ) ) {
			$args['cover'] = true;
		}

		if ( $args['front_page_sections'][0] == 'featured' && ! $args['fa_display_title'] && in_array( $args['fa_loop'], array( '1', '2' ) ) ) {
			$args['cover'] = true;
		}

		$args = apply_filters( 'megaphone_modify_front_page_template_options', $args );

		return $args;
	}
endif;

/**
 * Get front page posts section arguments
 *
 * @param mixed $blog_args
 * @param mixed $paginated_section
 *
 * @return array
 */
if ( ! function_exists( 'megaphone_get_frontpage_posts_args' ) ) :
	function megaphone_get_frontpage_posts_args( $paginated_section ) {

		$args = array();

		$args['posts_display_title']     = megaphone_get_option( 'front_page_posts_display_title' );
		$args['posts_view_all_link']     = megaphone_get_option( 'front_page_posts_view_all_link' );
		$args['posts_view_all_link_url'] = megaphone_get_option( 'front_page_posts_view_all_link_url' );

		$args['posts_slider']             = megaphone_get_option( 'front_page_posts_slider' );
		$args['posts_has_section_header'] = $args['posts_display_title'] || $args['posts_slider'];
		$args['posts_bg']                 = megaphone_get_option( 'front_page_posts_bg' );

		$args['posts_loop'] = megaphone_get_option( 'front_page_loop' );
		$posts_ppp          = megaphone_get_option( 'front_page_ppp' ) == 'inherit' ? get_option( 'posts_per_page' ) : absint( megaphone_get_option( 'front_page_ppp_num' ) );

		$podcast_args = megaphone_get_podcast_arguments();

		if ( $podcast_args['blog'] ) {

			$tag                      = megaphone_get_option( 'front_page_tag' );
			$posts_query              = megaphone_get_frontpage_query_args( 'blog', $posts_ppp, $tag );
			$args['posts_query_args'] = $posts_query;

			if ( $paginated_section == 'posts' ) {
				$paged                             = 'posts' == get_option( 'show_on_front' ) ? absint( get_query_var( 'paged' ) ) : absint( get_query_var( 'page' ) );
				$args['posts_query_args']['paged'] = $paged;
			}
		}

		if ( megaphone_loop_has_sidebar( $args['posts_loop'] ) ) {
			$args['posts_sidebar'] = array(
				'position' => megaphone_get_option( 'front_page_sidebar_position' ),
				'classic'  => megaphone_get_option( 'front_page_sidebar_standard' ),
				'sticky'   => megaphone_get_option( 'front_page_sidebar_sticky' ),
			);
		}

		return $args;
	}
endif;


/**
 * Get front page episodes section arguments
 *
 * @param mixed $paginated_section
 *
 * @return array
 */
if ( ! function_exists( 'megaphone_get_frontpage_episodes_args' ) ) :
	function megaphone_get_frontpage_episodes_args( $paginated_section ) {

		$args = array();

		$args['episodes_display_title']     = megaphone_get_option( 'front_page_episodes_display_title' );
		$args['episodes_view_all_link']     = megaphone_get_option( 'front_page_episodes_view_all_link' );
		$args['episodes_view_all_link_url'] = megaphone_get_option( 'front_page_episodes_view_all_link_url' );

		$args['episodes_slider']             = megaphone_get_option( 'front_page_episodes_slider' );
		$args['episodes_has_section_header'] = $args['episodes_display_title'] || $args['episodes_slider'];
		$args['episodes_bg']                 = megaphone_get_option( 'front_page_episodes_bg' );

		$args['episodes_loop'] = megaphone_get_option( 'front_page_episodes_loop' );
		$episodes_ppp          = megaphone_get_option( 'front_page_episodes_ppp' ) == 'inherit' ? get_option( 'posts_per_page' ) : absint( megaphone_get_option( 'front_page_episodes_ppp_num' ) );

		$episodes_tag     = megaphone_get_option( 'front_page_episodes_tag' );
		$episodes_cats    = megaphone_get_option( 'front_page_episodes_cat');
		$episodes_query   = megaphone_get_frontpage_query_args( 'podcast', $episodes_ppp, $episodes_tag, $episodes_cats );
		$podcast_args 	  = megaphone_get_podcast_arguments();
		
		if ( !empty( $episodes_cats ) ) {
			$podcast_args['podcast']['terms'] = $episodes_cats;
		}

		$args['episodes_query_args'] = $episodes_query;
		$args['episodes_ids'] 		 = megaphone_get_episodes_ids();

		if ( $paginated_section == 'episodes' ) {
			$paged                                = 'posts' == get_option( 'show_on_front' ) ? absint( get_query_var( 'paged' ) ) : absint( get_query_var( 'page' ) );
			$args['episodes_query_args']['paged'] = $paged;
		}

		if ( $paginated_section == 'episodes' && megaphone_get_option( 'front_page_fa_unique' ) ) {

			if ( $paged > 1 ) {
				megaphone_get_frontpage_featured_args();
			}

			global $fa_unique_episodes;
			$fa_query = new WP_Query( $fa_unique_episodes );
			
			if ( ! empty( $fa_query->posts ) ) {
				$fa_posts_ids                                = wp_list_pluck( $fa_query->posts, 'ID' );
				$args['episodes_query_args']['post__not_in'] = $fa_posts_ids;
			}
		}

		if ( megaphone_loop_has_sidebar( $args['episodes_loop'] ) ) {
			$args['episodes_sidebar'] = array(
				'position' => megaphone_get_option( 'front_page_episodes_sidebar_position' ),
				'classic'  => megaphone_get_option( 'front_page_episodes_sidebar_standard' ),
				'sticky'   => megaphone_get_option( 'front_page_episodes_sidebar_sticky' ),
			);
		}

		return $args;
	}
endif;


/**
 * Get front page featured section arguments
 *
 * @param mixed $podcast_args
 *
 * @return array
 */
if ( ! function_exists( 'megaphone_get_frontpage_featured_args' ) ) :
	function megaphone_get_frontpage_featured_args() {

		$args = array();

		$args['fa_display_title']     = megaphone_get_option( 'front_page_fa_display_title' );
		$args['fa_view_all_link']     = megaphone_get_option( 'front_page_fa_view_all_link' );
		$args['fa_view_all_link_url'] = megaphone_get_option( 'front_page_fa_view_all_link_url' );
		$args['fa_subscribe']         = megaphone_get_option( 'front_page_fa_subscribe' );
		$args['fa_loop']              = megaphone_get_option( 'front_page_fa_loop' );
		$args['fa_bg']                = $args['fa_loop'] > 2 ? megaphone_get_option( 'front_page_fa_bg' ) : '';
		$fa_params                    = megaphone_get_featured_layouts_map();

		$args['fa_has_section_header']            = $args['fa_display_title'] || ( $args['fa_display_title'] && isset( $fa_params[ $args['fa_loop'] ]['slider'] ) );
		$args['fa_has_section_header_slider_nav'] = $args['fa_display_title'] && isset( $fa_params[ $args['fa_loop'] ]['slider'] );

		$fa_ppp     = isset( $fa_params[ $args['fa_loop'] ]['ppp'] ) ? $fa_params[ $args['fa_loop'] ]['ppp'] : absint( megaphone_get_option( 'front_page_fa_ppp' ) );
		$fa_orderby = megaphone_get_option( 'front_page_fa_orderby' );
		$fa_order   = $fa_orderby == 'title' ? 'ASC' : 'DESC';

		$fa_cat   = megaphone_get_option( 'front_page_fa_cat' );
		$fa_tag   = megaphone_get_option( 'front_page_fa_tag' );
		$fa_query = megaphone_get_frontpage_query_args( 'podcast', $fa_ppp, $fa_tag, $fa_cat );

		$args['fa_query_args'] 			  = $fa_query;
		$args['fa_query_args']['orderby'] = $fa_orderby;
		$args['fa_query_args']['order']   = $fa_order;

		global $fa_unique_episodes;
		$fa_unique_episodes = $args['fa_query_args'];

		$podcast_args = megaphone_get_podcast_arguments();
		
		$args['fa_episodes_ids'] = megaphone_get_episodes_ids();

		if ( ! $args['fa_display_title'] && in_array( $args['fa_loop'], array( '1', '2', '5' ) ) ) {
			$args['cover'] = true;
		}

		return $args;
	}
endif;


/**
 * Get front page shows section arguments
 *
 * @return array
 */
if ( ! function_exists( 'megaphone_get_frontpage_shows_args' ) ) :
	function megaphone_get_frontpage_shows_args() {

		$args = array();

		$args['shows_display_title']     = megaphone_get_option( 'front_page_shows_display_title' );
		$args['shows_view_all_link']     = megaphone_get_option( 'front_page_shows_view_all_link' );
		$args['shows_view_all_link_url'] = megaphone_get_option( 'front_page_shows_view_all_link_url' );

		$args['shows_slider']             = megaphone_get_option( 'front_page_shows_slider' );
		$args['shows_has_section_header'] = $args['shows_display_title'] || $args['shows_slider'];
		$args['shows_bg']                 = megaphone_get_option( 'front_page_shows_bg' );

		$args['shows_loop']  = megaphone_get_option( 'show_layout_loop' );
		$selected_categories = megaphone_get_option( 'front_page_shows_categories' );
		$cat_args    	 	 = megaphone_get_shows_args( $selected_categories );

		if ( !empty( $cat_args ) ) {
			$categories = get_terms( $cat_args );
			$categories = megaphone_sort_items( $categories, $selected_categories );
		} else {
			$categories = $cat_args;
		}
		
		$args['shows_query'] = $categories;

		if ( megaphone_loop_has_sidebar( $args['shows_loop'] ) ) {
			$args['shows_sidebar'] = array(
				'position' => megaphone_get_option( 'show_layout_sidebar_position' ),
				'classic'  => megaphone_get_option( 'show_layout_sidebar_standard' ),
				'sticky'   => megaphone_get_option( 'show_layout_sidebar_sticky' ),
			);
		}

		return $args;
	}
endif;


/**
 * Get front page welcome/about section arguments
 *
 * @return array
 */
if ( ! function_exists( 'megaphone_get_frontpage_welcome_args' ) ) :
	function megaphone_get_frontpage_welcome_args() {

		$args         = array();
		$podcast_args = megaphone_get_podcast_arguments();

		$args['wa_display_title']  = megaphone_get_option( 'front_page_wa_display_title' );
		$args['wa_layout']         = megaphone_get_option( 'front_page_wa_layout' );
		$args['wa_cta']            = megaphone_get_option( 'front_page_wa_cta' );
		$args['wa_cta_url']        = megaphone_get_option( 'front_page_wa_cta_url' );
		$args['wa_subscribe']      = megaphone_get_option( 'front_page_wa_subscribe' );
		$args['wa_bg']             = megaphone_get_option( 'front_page_wa_bg' );
		$args['wa_latest_episode'] = megaphone_get_option( 'front_page_wa_latest_episode' );
		$args['wa_img']            = '';

		if ( megaphone_get_option( 'front_page_wa_img', 'image' ) ) {
			$wa_img_id = megaphone_get_image_id_by_url( megaphone_get_option( 'front_page_wa_img', 'image' ) );
			if ( ! empty( $wa_img_id ) ) {
				$args['wa_img'] = wp_get_attachment_image( $wa_img_id, 'megaphone-wa-' . $args['wa_layout'] );
			} else {
				$args['wa_img'] = '<img src="' . esc_url( megaphone_get_option( 'front_page_wa_img', 'image' ) ) . '" class="' . esc_attr( 'size-megaphone-wa-' . $args['wa_layout'] ) . '" />';
			}
		}

		$latest_podcast_query_args = array(
			'post_type' 		  => $podcast_args['podcast']['post_type'],
			'ignore_sticky_posts' => '1',
			'posts_per_page'      => 1,
		);

		//if ( $podcast_args['podcast']['tax_query'] ) {
			$latest_podcast_query_args['tax_query'] = array(
				array(
					'taxonomy' => $podcast_args['podcast']['taxonomy'],
					'field'    => 'term_id',
					'terms'    => $podcast_args['podcast']['terms'],
					'operator' => $podcast_args['podcast']['operator']
				)
				);
		//}

		$args['wa_latest_episode_obj'] = $args['wa_latest_episode'] ? new WP_Query( $latest_podcast_query_args ) : '';

		return $args;
	}
endif;

/**
 * Get 404 template options
 * Return page template params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_404_template_options' ) ) :
	function megaphone_get_404_template_options() {

		$args = array();

		$args['title']     = __megaphone( '404_title' );
		$args['404_image'] = megaphone_get_option( '404_image', 'image' );
		$args['text']      = __megaphone( '404_text' );

		return apply_filters( 'megaphone_modify_404_template_options', $args );
	}
endif;


/**
 * Get page template options
 * Return page template params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_blank_template_options' ) ) :
	function megaphone_get_blank_template_options() {
		$args = array();

		$args['display'] = array(
			'header' => false,
			'footer' => false,
			'title'  => false,
		);

		return apply_filters( 'megaphone_modify_blank_template_options', $args );
	}
endif;


/**
 * Get header options
 * Return header params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_header_options' ) ) :
	function megaphone_get_header_options() {

		$args = array();

		if ( megaphone_get_option( 'header_top' ) ) {
			$args['top'] = array(
				'l' => megaphone_get_option( 'header_top_l' ),
				'c' => megaphone_get_option( 'header_top_c' ),
				'r' => megaphone_get_option( 'header_top_r' ),
			);
		}

		$args['actions'] = megaphone_get_option( 'header_actions' );

		if ( megaphone_get_option( 'woocommerce_cart_force' ) && megaphone_is_woocommerce_page() && ! in_array( 'cart', $args['actions'] ) ) {
			$args['actions'][] = 'cart';
		}

		$args['actions_l'] = megaphone_get_option( 'header_actions_l' );
		$args['actions_r'] = megaphone_get_option( 'header_actions_r' );

		if ( ! in_array( 'hamburger', $args['actions_l'] ) && ! in_array( 'hamburger', $args['actions_r'] ) ) {
			$args['actions_r'][] = 'hamburger';
		}

		$args['layout']                   = megaphone_get_option( 'header_layout' );
		$args['nav']                      = megaphone_get_option( 'header_main_nav' );
		$args['elements_responsive']      = megaphone_get_option( 'header_elements_responsive' );
		$args['menu_elements_responsive'] = megaphone_get_option( 'header_menu_elements_responsive' );

		$args['sticky']        = megaphone_get_option( 'header_sticky' );
		$args['sticky_layout'] = megaphone_get_option( 'header_sticky_layout' );

		$args = apply_filters( 'megaphone_modify_header_options', $args );

		return $args;
	}
endif;

/**
 * Get footer options
 * Return header params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_footer_options' ) ) :
	function megaphone_get_footer_options() {
		$args = array();

		$args['subscribe'] = megaphone_get_option( 'footer_subscribe' );

		$args['instagram'] = ! is_404() && megaphone_get_option( 'footer_instagram' ) ? megaphone_get_option( 'footer_instagram_username' ) : false;

		if ( ! is_front_page() && megaphone_get_option( 'footer_instagram_front' ) ) {
			$args['instagram'] = false;
		}

		$args['widgets'] = megaphone_get_option( 'footer_widgets' ) ? explode( '-', megaphone_get_option( 'footer_widgets_layout' ) ) : false;

		$args['widgets_class'] = megaphone_get_option( 'footer_widgets_layout' ) == '4' ? 'footer-widgets-center' : false;

		$args['copyright'] = megaphone_get_option( 'footer_copyright' ) ? str_replace( '{current_year}', date( 'Y' ), megaphone_get_option( 'footer_copyright' ) ) : '';

		$args['go_to_top'] = megaphone_get_option( 'go_to_top' );

		if ( megaphone_get_option( 'popup' ) && function_exists( 'has_block' ) && is_singular() ) {

			$id = get_the_ID();

			if ( has_block( 'image', $id ) || has_block( 'gallery', $id ) ) {
				$args['popup'] = true;
			}
		}

		$args = apply_filters( 'megaphone_modify_footer_options', $args );

		return $args;
	}
endif;


/**
 * Get ads options
 * Return ad slots content based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_ads_options' ) ) :
	function megaphone_get_ads_options() {
		$args = array();

		if ( ! is_404() ) {

			$args['header']           = megaphone_get_option( 'ad_header' );
			$args['above_archive']    = megaphone_get_option( 'ad_above_archive' );
			$args['above_singular']   = megaphone_get_option( 'ad_above_singular' );
			$args['above_footer']     = megaphone_get_option( 'ad_above_footer' );
			$args['between_posts']    = megaphone_get_option( 'ad_between_posts' );
			$args['between_position'] = ! empty( $args['between_posts'] ) ? absint( megaphone_get_option( 'ad_between_position' ) ) - 1 : false;

			$args['header']         = wp_specialchars_decode( $args['header'], ENT_QUOTES );
			$args['above_archive']  = wp_specialchars_decode( $args['above_archive'], ENT_QUOTES );
			$args['above_singular'] = wp_specialchars_decode( $args['above_singular'], ENT_QUOTES );
			$args['above_footer']   = wp_specialchars_decode( $args['above_footer'], ENT_QUOTES );
			$args['between_posts']  = wp_specialchars_decode( $args['between_posts'], ENT_QUOTES );

			if ( is_page() && in_array( get_the_ID(), megaphone_get_option( 'ad_exclude' ) ) ) {
				$args = array();
			}
		}

		$args = apply_filters( 'megaphone_modify_ads_options', $args );

		return $args;
	}
endif;


/**
 * Get woocommerce template options
 * Return woocommerce template params based on theme options
 *
 * @since  1.0
 * @return array
 */

if ( ! function_exists( 'megaphone_get_woocommerce_template_options' ) ) :
	function megaphone_get_woocommerce_template_options() {

		$args = array();

		$args['sidebar'] = array(
			'position' => megaphone_get_option( 'woocommerce_sidebar_position' ),
			'classic'  => megaphone_get_option( 'woocommerce_sidebar_standard' ),
			'sticky'   => megaphone_get_option( 'woocommerce_sidebar_sticky' ),
		);

		$args['layout'] = '1';

		return apply_filters( 'megaphone_modify_woocommerce_template_options', $args );
	}
endif;
