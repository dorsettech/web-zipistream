<?php

/**
 * Register menus
 *
 * Callback function theme menus registration and init
 *
 * @since  1.0
 */

add_action( 'init', 'megaphone_register_menus' );

if ( !function_exists( 'megaphone_register_menus' ) ) :
	function megaphone_register_menus() {
		register_nav_menu( 'megaphone_menu_primary', esc_html__( 'Primary Menu' , 'megaphone' ) );
		register_nav_menu( 'megaphone_menu_social', esc_html__( 'Social Menu' , 'megaphone' ) );
		register_nav_menu( 'megaphone_menu_subscribe', esc_html__( 'Subscribe Menu' , 'megaphone' ) );
		register_nav_menu( 'megaphone_menu_donate', esc_html__( 'Donate Menu' , 'megaphone' ) );
		register_nav_menu( 'megaphone_menu_secondary_1', esc_html__( 'Secondary Menu 1' , 'megaphone' ) );
		register_nav_menu( 'megaphone_menu_secondary_2', esc_html__( 'Secondary Menu 2' , 'megaphone' ) );
	}
endif;



/**
 * wp_setup_nav_menu_item callback
 *
 * Get our meta data from nav menu
 *
 * @since  1.0
 */

add_filter( 'wp_setup_nav_menu_item', 'megaphone_get_menu_meta' );

if ( !function_exists( 'megaphone_get_menu_meta' ) ):
	function megaphone_get_menu_meta( $menu_item ) {

		$defaults = array(
			'mega_cat' => 0,
			'mega' => 0
		);

		$meta = get_post_meta( $menu_item->ID, '_megaphone_meta', true );
		$meta = wp_parse_args( $meta, $defaults );
		$menu_item->megaphone_meta = $meta;

		return $menu_item;

	}
endif;


/**
 * wp_update_nav_menu_item callback
 *
 * Store values from custom fields in nav menu
 *
 * @since  1.0
 */

add_action( 'wp_update_nav_menu_item', 'megaphone_update_menu_meta', 10, 3 );


if ( !function_exists( 'megaphone_update_menu_meta' ) ):
	function megaphone_update_menu_meta( $menu_id, $menu_item_db_id, $args ) {

		$meta = array();

		if ( isset( $_REQUEST['menu-item-megaphone-mega-cat'][$menu_item_db_id] ) ) {
			$meta['mega_cat'] = 1;
		}

		if ( isset( $_REQUEST['menu-item-megaphone-mega'][$menu_item_db_id] ) ) {
			$meta['mega'] = 1;
		}

		if ( !empty( $meta ) ) {
			update_post_meta( $menu_item_db_id, '_megaphone_meta', $meta );
		} else {
			delete_post_meta( $menu_item_db_id, '_megaphone_meta' );
		}


	}
endif;




/**
 * wp_edit_nav_menu_walker callback
 *
 * Add custom fields to nav menu form
 *
 * @since  1.0
 */

add_filter( 'wp_edit_nav_menu_walker', 'megaphone_edit_menu_walker', 10, 2 );

if ( !function_exists( 'megaphone_edit_menu_walker' ) ):
	function megaphone_edit_menu_walker( $walker, $menu_id ) {

		if ( !megaphone_get_option( 'mega_menu' ) ) {
			return $walker;
		}

		class Megaphone_Walker_Nav_Menu_Edit extends Walker_Nav_Menu_Edit {

			public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

				parent::start_el( $default_output, $item, $depth, $args, $id );

				$inject_html = '';

				if ( $item->object == 'category' ) {
					$inject_html .= '<p class="description">
		                <label for="menu-item-megaphone-mega-cat['.$item->db_id.']">
		        		<input type="checkbox" id="menu-item-megaphone-mega-cat['.$item->db_id.']" class="widefat" name="menu-item-megaphone-mega-cat['.$item->db_id.']" value="1" '.checked( $item->megaphone_meta['mega_cat'], 1, false ). ' />
		                '.esc_html__( 'Automatically display category posts as "mega menu"', 'megaphone' ).'</label>
		            </p>';
				}

				if ( !$item->menu_item_parent && $item->object != 'category' ) {

					$inject_html .= '<p class="description">
			                <label for="menu-item-megaphone-mega['.$item->db_id.']">
			        		<input type="checkbox" id="menu-item-megaphone-mega['.$item->db_id.']" class="widefat" name="menu-item-megaphone-mega['.$item->db_id.']" value="1" '.checked( $item->megaphone_meta['mega'], 1, false ). ' />
			                '.esc_html__( 'Display submenu items as "mega menu"', 'megaphone' ).'</label>
			            </p>';
				}

				ob_start();
				do_action( 'wp_nav_menu_item_custom_fields', $item->ID, $item, $depth, $args );
				$inject_html .= ob_get_clean();

				$new_output = preg_replace( '/(?=<div.*submitbox)/', $inject_html, $default_output );

				$output .= $new_output;


			}

		}

		return 'Megaphone_Walker_Nav_Menu_Edit';
	}
endif;



/**
 * nav_menu_css_class callback
 *
 * Used to add/modify CSS classes in nav menu
 *
 * @since  1.0
 */

add_filter( 'nav_menu_css_class', 'megaphone_modify_nav_menu_classes', 10, 2 );

if ( !function_exists( 'megaphone_modify_nav_menu_classes' ) ):
	function megaphone_modify_nav_menu_classes( $classes, $item ) {

		if ( !megaphone_get_option( 'mega_menu' ) ) {
			return $classes;
		}

		if ( $item->object == 'category' && isset( $item->megaphone_meta['mega_cat'] ) && $item->megaphone_meta['mega_cat'] ) {
			$classes[] = 'menu-item-has-children megaphone-mega-menu megaphone-category-menu';
		}

		if ( isset( $item->megaphone_meta['mega'] ) && $item->megaphone_meta['mega'] ) {
			$classes[] = 'megaphone-mega-menu';
		}

		return $classes;

	}
endif;


/**
 * Display category posts in mega menu
 *
 * @since  1.0
 */

if ( !function_exists( 'megaphone_get_nav_menu_category_posts' ) ) :

	function megaphone_get_nav_menu_category_posts( $cat_id ) {

		$args = array(
			'post_type'    => 'post',
			'cat'      => $cat_id,
			'posts_per_page' => absint( megaphone_get_option( 'mega_menu_ppp' ) )
		);

		$slider_class = $args['posts_per_page'] > 4 ? 'megaphone-slider has-arrows' : '';

		$output = '<li class="row '.esc_attr( $slider_class ).'">';

		ob_start();

		$args['ignore_sticky_posts'] = 1;

		$menu_posts = new WP_Query( $args );

		if ( $menu_posts->have_posts() ) :

			while ( $menu_posts->have_posts() ) : $menu_posts->the_post(); ?>

				<article <?php post_class( 'col-12 col-lg-3' ); ?>>

		            <?php if ( $fimg = megaphone_get_featured_image( 'megaphone-f-episode' ) ) : ?>

			                <div class="entry-media">
				                <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
				                   	<?php echo megaphone_wp_kses( $fimg ); ?>
								</a>
								<?php if ( megaphone_is_show( $cat_id ) ) : ?>
									<?php megaphone_play_button( 'megaphone-button-play megaphone-button-play-white megaphone-button-play-medium megaphone-mega-menu-play', false ); ?>
								<?php endif; ?>
								
			                </div>

		            <?php endif; ?>

		            <div class="entry-header">

						<?php if ( megaphone_is_show( $cat_id ) ) : ?>
							
							<?php $episodes_ids = megaphone_get( 'episodes_ids' ); ?>
							<?php if ( $episode_number = megaphone_get_episode_number( $episodes_ids, get_the_ID(), true ) ) : ?>
								<div class="megaphone-show-header">
									<div class="entry-episode">
										<?php echo __megaphone( 'episode' ); ?>
										<?php echo absint( $episode_number ); ?>
									</div>
								</div>
							<?php endif; ?>
							
						<?php endif; ?>
				
						<?php $title = megaphone_is_show( $cat_id ) ? megaphone_get_episode_title() : get_the_title(); ?>
						<a href="<?php the_permalink(); ?>" class="entry-title h6"><?php echo wp_kses_post($title); ?></a>
		            </div>

				</article>

			<?php endwhile;

		endif;

		wp_reset_postdata();

		$output .= ob_get_clean();

		$output .= '</li>';

		return $output;

	}

endif;




/**
 * walker_nav_menu_start_el callback
 *
 * Used to display specific data in nav menu on website front-end
 *
 * @since  1.0
 */

add_filter( 'walker_nav_menu_start_el', 'megaphone_walker_nav_menu_start_el', 10, 4 );

function megaphone_walker_nav_menu_start_el( $item_output, $item, $depth, $args ) {

	if ( !megaphone_get_option( 'mega_menu' ) ) {
		return $item_output;
	}

	if ( isset( $item->megaphone_meta['mega_cat'] ) && $item->megaphone_meta['mega_cat'] ) {

		$item_output .= '<ul class="sub-menu megaphone-menu-posts">';
		$item_output .= megaphone_get_nav_menu_category_posts( $item->object_id );
		$item_output .= '</ul>';

	}

	return $item_output;
}