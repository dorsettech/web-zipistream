<?php

/**
 * body_class callback
 *
 * Checks for specific options and applies additional class to body element
 *
 * @since  1.0
 */

add_filter( 'body_class', 'megaphone_body_class' );

if ( !function_exists( 'megaphone_body_class' ) ):
	function megaphone_body_class( $classes ) {

		
		$classes[] = 'megaphone-player-hidden';

		if ( megaphone_has_sidebar( 'right' ) ) {
			$classes[] = 'megaphone-sidebar-right';
		}

		if ( megaphone_has_sidebar( 'left' ) ) {
			$classes[] = 'megaphone-sidebar-left';
		}

		if ( megaphone_has_sidebar( 'none' ) ) {
			$classes[] = 'megaphone-sidebar-none';
		}

		if ( megaphone_get_option( 'header_orientation' ) == 'window' ) {
			$classes[] = 'megaphone-header-window';
		}

		if ( !megaphone_is_color_light( megaphone_get_option( 'color_bg_alt_1' ) ) ) {
			$classes[] = 'megaphone-bg-alt-1-white-text';
		}

		if ( !megaphone_is_color_light( megaphone_get_option( 'color_bg_alt_2' ) ) ) {
			$classes[] = 'megaphone-bg-alt-2-white-text';
		}

		if ( megaphone_get_option( 'overlays' ) != 'dark' ) {
			$classes[] = 'megaphone-overlays-' . megaphone_get_option( 'overlays' );
		}

		if ( megaphone_get( 'cover' ) ) {
			$classes[] = 'megaphone-header-no-margin';
		}

		if ( !megaphone_get_option( 'header_labels' ) ) {
			$classes[] = 'megaphone-header-labels-hidden';
		}

		if( in_array( megaphone_get( 'header', 'layout' ), array( 1,2,3 ) ) ){
			
			if ( megaphone_get_option( 'color_header_middle_bg' ) == megaphone_get_option( 'color_bg' ) ) {
				$classes[] = 'megaphone-header-shadow';
			}


			if ( megaphone_get( 'cover' ) && megaphone_get_option( 'header_indent' ) ) {
				$classes[] = 'megaphone-header-indent';
				megaphone_set('header_indent', true );
			}


		} else {
			if ( megaphone_get_option( 'color_header_bottom_bg' ) == megaphone_get_option( 'color_bg' ) ) {
				$classes[] = 'megaphone-header-shadow';
			}
		}


		$classes[] = 'megaphone-v_' . str_replace( '.', '_', MEGAPHONE_THEME_VERSION );

		if ( is_child_theme() ) {
			$classes[] = 'megaphone-child';
		}

		return $classes;
	}
endif;


/**
 * Content width
 *
 * Checks for specific options and change content width global based on the current layout
 *
 * @since  1.0
 */

add_action( 'template_redirect', 'megaphone_content_width', 0 );

if ( !function_exists( 'megaphone_content_width' ) ):
	function megaphone_content_width() {

		if ( is_page() ) {
			$content_width = megaphone_size_by_col( megaphone_get_option( 'page_width' ) );
		} elseif ( is_single() ) {

			if ( megaphone_is_podcast() ) {
				$content_width = megaphone_size_by_col( megaphone_get_option( 'single_podcast_width' ) );
			} else {
				$content_width = megaphone_size_by_col( megaphone_get_option( 'single_blog_width' ) );
			}


		} else {
			$content_width = megaphone_size_by_col( 12 );
		}

		$GLOBALS['content_width'] = $content_width;
	}
endif;


/**
 * frontpage_template filter callback
 *
 * Use front-page.php template only if a user enabled it in theme options.
 * This provides a possibility for the user to opt-out and use wordpress default reading settings.
 *
 * @since  1.0
 */

add_filter( 'frontpage_template',  'megaphone_front_page_template' );

if ( !function_exists( 'megaphone_front_page_template' ) ):
	function megaphone_front_page_template( $template ) {

		if ( megaphone_get_option( 'front_page_template' ) ) {
			return $template;
		}

		if ( 'posts' == get_option( 'show_on_front' ) ) {
			$template = get_home_template();
		} else {
			$template = get_page_template();
		}

		return $template;
	}

endif;


/**
 * Add span elements to post count number in category widget
 *
 * @since  1.0
 */

add_filter( 'wp_list_categories', 'megaphone_modify_category_widget_post_count', 10, 2 );

if ( !function_exists( 'megaphone_modify_category_widget_post_count' ) ):
	function megaphone_modify_category_widget_post_count( $links, $args ) {

		if ( isset( $args['taxonomy'] ) && $args['taxonomy'] != 'category' ) {
			return $links;
		}

		$links = preg_replace( '/(<a[^>]*>)/', '$1<span class="category-text">', $links );
		$links = str_replace( '</a>', '</span></a>', $links );

		if ( !isset( $args['show_count'] ) ||  !$args['show_count'] ) {
			return $links;
		}

		$links = str_replace( '</a> (', '<span class="count">', $links );
		$links = str_replace( ')', '</span></a>', $links );

		return $links;
	}
endif;

/**
 * Modify Woocommerce Product Category Widget arguments
 *
 * @since  1.0
 */

add_filter( 'woocommerce_product_categories_widget_args', 'megaphone_modify_wc_product_cat_count' );

if ( !function_exists( 'megaphone_modify_wc_product_cat_count' ) ):
	function megaphone_modify_wc_product_cat_count( $args ) {

		$args['walker'] = new Megaphone_Modify_WC_Product_Cat_List();

		return $args;
	}
endif;

/**
 * Modify Woocommerce Product Category widget Class
 *
 * @since  1.0
 */

if ( megaphone_is_woocommerce_active() ) {

	include_once WC()->plugin_path() . '/includes/walkers/class-wc-product-cat-list-walker.php';

	class Megaphone_Modify_WC_Product_Cat_List extends WC_Product_Cat_List_Walker {

		public function start_el( &$output, $cat, $depth = 0, $args = array(), $current_object_id = 0 ) {
			$cat_id = intval( $cat->term_id );

			$output .= '<li class="cat-item cat-item-' . $cat_id;

			if ( $args['current_category'] === $cat_id ) {
				$output .= ' current-cat';
			}

			if ( $args['has_children'] && $args['hierarchical'] && ( empty( $args['max_depth'] ) || $args['max_depth'] > $depth + 1 ) ) {
				$output .= ' cat-parent';
			}

			if ( $args['current_category_ancestors'] && $args['current_category'] && in_array( $cat_id, $args['current_category_ancestors'], true ) ) {
				$output .= ' current-cat-parent';
			}

			$output .= '"><a href="' . get_term_link( $cat_id, $this->tree_type ) . '"><span class="category-text">' . apply_filters( 'list_product_cats', $cat->name, $cat ) . '</span>';

			if ( $args['show_count'] ) {
				$output .= ' <span class="count">' . $cat->count . '</span>';
			}

			$output .= '</span></a>';
		}

	}

}

/**
 * Add css class to parent in category widget so we can have an accordion menu
 *
 * @since  1.0
 */

add_filter( 'category_css_class', 'megaphone_modify_category_widget_css_class', 10, 4 );

if ( !function_exists( 'megaphone_modify_category_widget_css_class' ) ):
	function megaphone_modify_category_widget_css_class( $css_classes, $category, $depth, $args ) {
		if ( isset( $args['hierarchical'] ) && $args['hierarchical'] ) {
			$term = get_queried_object();
			$children = get_terms( $category->taxonomy, array(
					'parent'    => $category->term_id,
					'hide_empty' => false
				) );

			if ( !empty( $children ) ) {
				$css_classes[] = 'cat-parent';
			}

		}
		return $css_classes;
	}
endif;


/**
 * Add span elements to post count number in archives widget
 *
 * @since  1.0
 */

add_filter( 'get_archives_link', 'megaphone_modify_archive_widget_post_count', 10, 6 );

if ( !function_exists( 'megaphone_modify_archive_widget_post_count' ) ):
	function megaphone_modify_archive_widget_post_count( $link_html, $url, $text, $format, $before, $after ) {

		if ( $format == 'html' && !empty( $after ) ) {
			$new_after = str_replace( '(', '<span class="count">', $after );
			$new_after = str_replace( ')', '</span>', $new_after );

			$link_html = str_replace( $after, $new_after, $link_html );
		}

		return $link_html;
	}
endif;


/**
 * Widget display callback
 *
 * Check if background option is selected and add css class to widget
 *
 * @return void
 * @since  1.0
 */

add_filter( 'dynamic_sidebar_params', 'megaphone_modify_widget_display' );

if ( !function_exists( 'megaphone_modify_widget_display' ) ) :
	function megaphone_modify_widget_display( $params ) {

		if ( strpos( $params[0]['id'], 'megaphone_sidebar_footer' ) !== false ) {
			return $params; //do not apply styling for footer widgets
		}

		if ( is_customize_preview() ) {
			$default = megaphone_get_background_css_class( megaphone_get_option( 'widget_bg' ) );

			$params[0]['before_widget'] = str_replace( 'megaphone-bg-alt-1', $default, $params[0]['before_widget'] );
			$params[0]['before_widget'] = str_replace( 'megaphone-bg-alt-2', $default, $params[0]['before_widget'] );
		}

		global $wp_registered_widgets;

		$widget_id              = $params[0]['widget_id'];
		$widget_obj             = $wp_registered_widgets[$widget_id];
		$widget_num             = $widget_obj['params'][0]['number'];
		$widget_opt = get_option( $widget_obj['callback'][0]->option_name );


		if ( isset( $widget_opt[$widget_num]['megaphone-bg'] ) && $widget_opt[$widget_num]['megaphone-bg'] ) {

			$css_class = megaphone_get_background_css_class( $widget_opt[$widget_num]['megaphone-bg'] );
			$default = megaphone_get_background_css_class( megaphone_get_option( 'widget_bg' ) );
			$params[0]['before_widget'] = str_replace( $default, $css_class, $params[0]['before_widget'] );


		}

		return $params;

	}

endif;


/**
 * Modify WooCommerce wrappers
 *
 * Provide support for WooCommerce pages to match theme HTML markup
 *
 * @return HTML output
 * @since  1.0
 */

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
add_action( 'woocommerce_before_main_content', 'megaphone_woocommerce_wrapper_start', 10 );
add_action( 'woocommerce_after_main_content', 'megaphone_woocommerce_wrapper_end', 10 );

if ( !function_exists( 'megaphone_woocommerce_wrapper_start' ) ):
	function megaphone_woocommerce_wrapper_start() {

		echo '<div class="megaphone-section">';
		echo '<div class="container">';
		echo '<div class="section-content row">';

		if ( megaphone_has_sidebar( 'left' ) ) {
			echo '<div class="col-12 col-lg-4 megaphone-order-3">';
			get_sidebar();
			echo '</div>';
		}

		$class =  megaphone_has_sidebar( 'none' ) ? '' : 'col-lg-8';

		echo '<div class="col-12 megaphone-order-1 '.esc_attr( $class ).'">';

	}
endif;

if ( !function_exists( 'megaphone_woocommerce_wrapper_end' ) ):
	function megaphone_woocommerce_wrapper_end() {
		echo '</div>';
		if ( megaphone_has_sidebar( 'right' ) ) {
			echo '<div class="col-12 col-lg-4 megaphone-order-3">';
			get_sidebar();
			echo '</div>';
		}
		echo '</div>';
		echo '</div>';
		echo '</div>';
	}
endif;


/**
 * pre_get_posts filter callback
 *
 * If a user select custom number of posts per specific archive
 * template, override default post per page value
 *
 * @since  1.0
 */

add_action( 'pre_get_posts', 'megaphone_pre_get_posts' );

if ( !function_exists( 'megaphone_pre_get_posts' ) ):
	function megaphone_pre_get_posts( $query ) {

		$ppp = get_option( 'posts_per_page' );

		/* Front page pagination */
		if ( !is_admin() && $query->is_main_query() && !$query->is_feed() && !$query->is_archive() && megaphone_is_front_page( $query )  ) {

			$podcast_args = megaphone_get_podcast_arguments();
			$pag_section = megaphone_frontpage_paginated_section( megaphone_get_option( 'front_page_sections' ) );

			if ( $pag_section = 'episodes' ) {
				
				$ppp = megaphone_get_option('front_page_episodes_ppp') == 'custom' ? megaphone_get_option('front_page_episodes_ppp_num') : $ppp;
				
				$tax_query = array( 
					array(
						'taxonomy' => $podcast_args['podcast']['taxonomy'],
						'terms'    => $podcast_args['podcast']['terms'],
						'operator' => $podcast_args['podcast']['operator']
					) 
				);

				$query->set( 'tax_query', $tax_query );
				$query->set( 'posts_per_page', $ppp );
			}

			if ( $pag_section == 'posts' ) {

				$ppp = megaphone_get_option('front_page_ppp') == 'custom' ? megaphone_get_option('front_page_ppp_num') : $ppp;

				$tax_query = array( 
					array(
						'taxonomy' => $podcast_args['blog']['taxonomy'],
						'terms'    => $podcast_args['blog']['terms'],
						'operator' => $podcast_args['blog']['operator']
					) 
				);

				$query->set( 'post_type', $podcast_args['blog']['post_type'] );
				$query->set( 'tax_query', $tax_query );
				$query->set( 'posts_per_page', $ppp );
			}

		}

		/* Archive page pagination */
		if ( !is_admin() && $query->is_main_query() && ( $query->is_archive() || $query->is_search() || $query->is_posts_page ) && !$query->is_feed() ) {

			if ( $query->is_category() ) {

				$archive_type = megaphone_get_archive_type();
				$id = get_queried_object_id();
				
				$ppp =  $archive_type == 'blog' ? megaphone_get_category_meta( $id, 'ppp_num' ) : megaphone_get_show_meta( $id, 'ppp_num' );
				
				$post_order =  $archive_type == 'blog' ? 'DESC' : megaphone_get_show_meta( $id, 'order' );
				$query->set( 'order', $post_order );
				
			} else {
				
				$ppp = megaphone_get_option( 'archive_ppp' ) == 'custom' ? megaphone_get_option(  'archive_ppp_num' ) : $ppp;
				
			}
			
			$query->set( 'posts_per_page', absint( $ppp ) );

		}

	}
endif;


/**
 * wp_link_pages_link filter callback
 *
 * Used to add css classes to style paginated post links
 *
 * @since  1.0
 */

add_filter( 'wp_link_pages_link', 'megaphone_wp_link_pages_link' );

if ( !function_exists( 'megaphone_wp_link_pages_link' ) ):
	function megaphone_wp_link_pages_link( $link ) {

		if ( stripos( $link, '<a' ) !== false ) {
			$link = str_replace( '<a' , '<a class="megaphone-button"'  , $link );
		} else {
			$link = '<span class="megaphone-button current">'.$link.'</span>';
		}

		return $link;
	}
endif;


/**
 * Woocommerce Ajaxify Cart
 *
 * @return bool
 * @since  1.0
 */


add_filter( 'woocommerce_add_to_cart_fragments', 'megaphone_woocommerce_ajax_fragments' );

if ( !function_exists( 'megaphone_woocommerce_ajax_fragments' ) ):

	function megaphone_woocommerce_ajax_fragments( $fragments ) {
		ob_start();
		get_template_part( 'template-parts/header/elements/cart' );
		$fragments['.megaphone-cart'] = ob_get_clean();
		return $fragments;
	}

endif;


/**
 * Add comment form default fields args filter
 * to replace comment fields labels
 *
 * @since  1.0
 */

add_filter( 'comment_form_default_fields', 'megaphone_comment_fields_labels' );

if ( !function_exists( 'megaphone_comment_fields_labels' ) ):
	function megaphone_comment_fields_labels( $fields ) {

		$replace = array(
			'author' => array(
				'old' => esc_html__( 'Name', 'megaphone' ),
				'new' =>__megaphone( 'comment_name' )
			),
			'email' => array(
				'old' => esc_html__( 'Email', 'megaphone' ),
				'new' =>__megaphone( 'comment_email' )
			),
			'url' => array(
				'old' => esc_html__( 'Website', 'megaphone' ),
				'new' =>__megaphone( 'comment_website' )
			),

			'cookies' => array(
				'old' => esc_html__( 'Save my name, email, and website in this browser for the next time I comment.', 'megaphone' ),
				'new' =>__megaphone( 'comment_cookie_gdpr' )
			)
		);

		foreach ( $fields as $key => $field ) {

			if ( array_key_exists( $key, $replace ) ) {
				$fields[$key] = str_replace( $replace[$key]['old'], $replace[$key]['new'], $fields[$key] );
			}

		}

		return $fields;

	}

endif;



/**
 * Change default arguments of social share plugin
 *
 * @since  1.0
 */

add_filter( 'meks_ess_modify_defaults', 'megaphone_meks_ess_modify_defaults' );

if ( !function_exists( 'megaphone_meks_ess_modify_defaults' ) ):
	function megaphone_meks_ess_modify_defaults( $defaults ) {

		$defaults['platforms'] = array( 'facebook', 'twitter', 'reddit', 'email' );
		$defaults['style'] = '3';
		$defaults['location'] = 'below';
		$defaults['label_share'] = array( 'text' => 'Share this', 'active' => 1 );
		
		return $defaults;
	}
endif;

/**
 * Filter for social share before element
 *
 * @param string  $before
 * @return string
 * @since  1.0
 */
add_filter( 'meks_ess_modify_before', 'megaphone_social_share_modify_before_wrapper' );

if ( !function_exists( 'megaphone_social_share_modify_before_wrapper' ) ):
	function megaphone_social_share_modify_before_wrapper( $before ) {

		$wrapper = '<div class="megaphone-share-wrapper">';

		$before = $wrapper . $before;

		return $before;
	}
endif;

/**
 * Filter for social share after element
 *
 * @param string  $after
 * @return string
 * @since  1.0
 */
add_filter( 'meks_ess_modify_after', 'megaphone_social_share_modify_after_wrapper' );

if ( !function_exists( 'megaphone_social_share_modify_after_wrapper' ) ):
	function megaphone_social_share_modify_after_wrapper( $after ) {

		$wrapper = '</div>';

		$after .= $wrapper;

		return $after;
	}
endif;

/**
 * Remove player loading approach from our plugin and add player through the theme
 *
 * @since  1.0
 */

add_action( 'wp_footer', 'megaphone_player', 0 );

function megaphone_player() {

	if ( !class_exists( 'Meks_AP' ) ) {
		return false;
	}

	$meks_ap = Meks_AP::get_instance();
	remove_action( 'wp_footer', array( $meks_ap, 'display_player' ) );

	if( !is_front_page() && is_singular() && !megaphone_is_podcast() && !is_page_template( 'template-shows.php' ) ){
		return false;
	}

	include_once $meks_ap->get_player_template_path();
}




/**
 * Filter allowed selectors for player so we can match additional content like embeds
 *
 * @return array
 * @since  1.0
 */
add_filter( 'meks_ap_modify_allowed_selectors', 'megaphone_player_modify_selectors' );

if ( !function_exists( 'megaphone_player_modify_selectors' ) ):
	function megaphone_player_modify_selectors( $selectors ) {


		// $selectors['.wp-block-embed-soundcloud'] = array( 'element' => 'iframe', 'type' => 'custom' );
		// $selectors['.wp-block-embed-mixcloud'] = array( 'element' => 'iframe', 'type' => 'custom' );
		// $selectors['.wp-block-embed-spotify'] = array( 'element' => 'iframe', 'type' => 'custom' );
		// $selectors['.wp-block-embed-youtube'] = array( 'element' => 'iframe', 'type' => 'custom' );

		// "iframe[src*='stitcher.com']",
		//   "iframe[src*='podbean.com']",
		//   "iframe[src*='libsyn.com']",
		//   "iframe[src*='spreaker.com']",
		//   "iframe[src*='cadence13.com']",
		//   "iframe[src*='podomatic.com']",
		//   "iframe[src*='blogtalkradio.com']"

		$supports = megaphone_get_option( 'player_supports' );
		
		$iframe_match = array();

		foreach ( $supports as $support ) {

			if ( strpos( $support, 'iframe-' ) !== false ) {
				$iframe_match[] = "iframe[src*='".substr( $support, 7, strlen( $support ) )."']";
			} else {
				$selectors[$support] = array( 'element' => 'iframe', 'type' => 'custom' );
			}
		}

		if ( !empty( $iframe_match ) ) {
			$selectors['iframe'] = array(
				'element' => 'self',
				'type' => 'custom',
				'match' => $iframe_match
			);
		}

		//print_r( $selectors );

		return $selectors;
	}
endif;

/**
 * Player add contents before
 *
 * @since  1.0
 */

add_action( 'meks_ap_player_before', 'megaphone_player_before' );

function megaphone_player_before() {

	echo '<div class="megaphone-loader"><div class="megaphone-ellipsis"><div></div><div></div><div></div><div></div></div></div>';

	echo '<div class="meks-ap-slot-l">';

	if ( is_singular() ) {

		$adjacent = megaphone_get_prev_next_episodes();

		if ( !empty( $adjacent['prev'] ) ) {
			echo '<a class="meks-ap-prev megaphone-play megaphone-play-'.esc_attr( $adjacent['prev']['id'] ).'" data-play-id="'.esc_attr( $adjacent['prev']['id'] ).'" href="'. esc_attr( $adjacent['prev']['permalink'] ) .'"><i class="mf mf-chevron-left"></i></a>';
		} else {
			echo '<span class="meks-ap-prev"></span>';
		}

		if ( !empty( $adjacent['next'] ) ) {
			echo '<a class="meks-ap-next megaphone-play megaphone-play-'.esc_attr( $adjacent['next']['id'] ).'" data-play-id="'.esc_attr( $adjacent['next']['id'] ).'" href="'. esc_attr( $adjacent['next']['permalink'] ).'"><i class="mf mf-chevron-right"></i></a>';
		} else {
			echo '<span class="meks-ap-next"></span>';
		}


		$episodes_ids = megaphone_get( 'episodes_ids' );

		if ( megaphone_get('show_episode_number') && $episode_number = megaphone_get_episode_number( $episodes_ids, get_the_ID(), true ) ) {
			echo '<div class="meks-ap-episode">' . __megaphone( 'episode' ). ' '. absint( $episode_number ) . '</div>';
		}

		echo '<div class="meks-ap-title h6"><a href="'.esc_url( get_the_permalink() ).'">' . megaphone_get_episode_title() . '</a></div>';
	}

	echo '</div>';

	echo '<div class="meks-ap-slot-r">';


}

/**
 * Player add contents after
 *
 * @since  1.0
 */

add_action( 'meks_ap_player_after', 'meks_ap_player_after' );

function meks_ap_player_after() {

	$player_elements = megaphone_get_option( 'player_special_elements' );
	if ( ! empty( $player_elements ) ) {
		foreach ( $player_elements as $element ) {
			get_template_part( 'template-parts/footer/player/elements/' . $element );
		}
	}

	echo '</div>';

}

/**
 * Modify Simple Podcasting Plugin hooks
 *
 * @return void 
 * @since  1.0
 */

global $ssp_admin;

remove_action( 'series_add_form_fields', array( $ssp_admin, 'add_series_term_meta_fields') );
remove_action( 'series_edit_form_fields', array( $ssp_admin, 'edit_series_term_meta_fields') );

add_filter( 'manage_edit-series_columns', 'megaphone_remove_plugin_series_image_meta_field' );
function megaphone_remove_plugin_series_image_meta_field( $columns ) {
	unset( $columns['series_image'] );
	return $columns;
}
