<?php

/* Load frontend scripts and styles */
add_action( 'wp_enqueue_scripts', 'megaphone_load_scripts' );

/**
 * Load scripts and styles on frontend
 *
 * It just wraps two other separate functions for loading css and js files
 *
 * @since  1.0
 */

function megaphone_load_scripts() {
	megaphone_load_css();
	megaphone_load_js();
}

/**
 * Load frontend css files
 *
 * @since  1.0
 */

function megaphone_load_css() {

	//Load fonts
	if ( $fonts_link = megaphone_generate_fonts_link() ) {
		wp_enqueue_style( 'megaphone-fonts', $fonts_link, false, MEGAPHONE_THEME_VERSION );
	}

	//Check if is minified option active and load appropriate files
	if ( megaphone_get_option( 'minify_css' ) ) {
		wp_enqueue_style( 'megaphone-main', get_parent_theme_file_uri( '/assets/css/min.css' ) , false, MEGAPHONE_THEME_VERSION );
	} else {

		$styles = array(
			'iconfont' => 'iconfont.css',
			'photoswipe' => 'photoswipe.css',
			'photoswipe-skin' => 'photoswipe-default-skin.css',
			'owl-carousel' => 'owl-carousel.css',
			'main' => 'main.css'
		);

		foreach ( $styles as $id => $style ) {
			wp_enqueue_style( 'megaphone-' . $id, get_parent_theme_file_uri( '/assets/css/'. $style ) , false, MEGAPHONE_THEME_VERSION );
		}
	}


	//Append dynamic css
	wp_add_inline_style( 'megaphone-main', megaphone_generate_dynamic_css() );

	//Woocomerce styles
	if ( megaphone_is_woocommerce_active() ) {
		wp_enqueue_style( 'megaphone-woocommerce', get_parent_theme_file_uri( '/assets/css/megaphone-woocommerce.css' ), array( 'megaphone-main' ), MEGAPHONE_THEME_VERSION );
		wp_dequeue_style( 'photoswipe-default-skin' );
	}

	//Load RTL css
	if ( megaphone_is_rtl() ) {
		wp_enqueue_style( 'megaphone-rtl', get_parent_theme_file_uri( '/assets/css/rtl.css' ), array( 'megaphone-main' ), MEGAPHONE_THEME_VERSION );
	}
	
	//wp_enqueue_style( 'megaphone-modal-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', array( 'megaphone-modal-css' ), MEGAPHONE_THEME_VERSION );

}


/**
 * Load frontend js files
 *
 * @since  1.0
 */

function megaphone_load_js() {

	//Check if is minified option active and load appropriate files
	if ( megaphone_get_option( 'minify_js' ) ) {

		wp_enqueue_script( 'megaphone-main', get_parent_theme_file_uri( '/assets/js/min.js' ) , array( 'jquery', 'jquery-masonry', 'imagesloaded' ), MEGAPHONE_THEME_VERSION, true );

	} else {

		$scripts = array(
			'photoswipe' => 'photoswipe.js',
			'photoswipe-ui' => 'photoswipe-ui-default.js',
			'owl-carousel' => 'owl-carousel.js',
			'sticky-kit' => 'sticky-kit.js',
			'object-fit' => 'ofi.js',
			'picturefill' => 'picturefill.js',
			'main' => 'main.js'
		);

		foreach ( $scripts as $id => $script ) {
			wp_enqueue_script( 'megaphone-'.$id, get_parent_theme_file_uri( '/assets/js/'. $script ), array( 'jquery', 'jquery-masonry', 'imagesloaded' ), MEGAPHONE_THEME_VERSION, true );
		}
	}

	//Load JS settings object
	wp_localize_script( 'megaphone-main', 'megaphone_js_settings', megaphone_get_js_settings() );

	//Load comment reply js
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	//wp_enqueue_script( 'jquery-modal', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js' , array( 'jquery-modal'), MEGAPHONE_THEME_VERSION, true );

}


/**
 * Load customizer/preview js files
 *
 * @since  1.0
 */

add_action( 'customize_preview_init', 'megaphone_preview_js' );
//add_action( 'customize_controls_print_scripts', 'megaphone_preview_js' );


function megaphone_preview_js() {
	
  	wp_enqueue_script( 'megaphone-customizer', get_parent_theme_file_uri( '/assets/js/admin/customizer.js' ), array( 'customize-preview', 'jquery' ), MEGAPHONE_THEME_VERSION, true );
}


/**
 *  Customizer controls js files
 *
 * @since  1.0
 */
add_action( 'customize_controls_enqueue_scripts', 'megaphone_customizer_controls_js' );

function megaphone_customizer_controls_js() {
	wp_enqueue_script( 'megaphone-customizer-controls', get_parent_theme_file_uri( '/assets/js/admin/customizer-controls.js' ), array( 'jquery', 'customize-preview' ), MEGAPHONE_THEME_VERSION, true );

	//Load JS customizer settings object
	wp_localize_script( 'megaphone-customizer-controls', 'megaphone_customizer_settings', megaphone_get_customize_js_settings() );
}