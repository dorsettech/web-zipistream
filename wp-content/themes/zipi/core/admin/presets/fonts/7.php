<?php

$preset = array(

  'main_font' => array(
    'font-family' => 'Roboto',
    'variant'  => '300',
    'font-weight' => '300'
  ),

  'h_font' => array(
    'font-family' => 'Roboto',
    'variant' => '300',
    'font-weight' => '300'
  ),

  'nav_font' => array(
    'font-family' => 'Roboto',
    'variant' => '300',
    'font-weight' => '300'
  ),

  'button_font' => array(
    'font-family' => 'Roboto',
    'variant' => '400',
    'font-weight' => '400'
  ),

  'font_size_p' => '18',
  'font_size_small' => '16',
  'font_size_nav' => '15',
  'font_size_nav_ico' => '24',
  'font_size_section_title' => '28',
  'font_size_widget_title' => '24',
  'font_size_punchline' => '68',
  'font_size_h1' => '56',
  'font_size_h2' => '48',
  'font_size_h3' => '40',
  'font_size_h4' => '32',
  'font_size_h5' => '26',
  'font_size_h6' => '20',

  'uppercase' => array( 'buttons' ),
);
