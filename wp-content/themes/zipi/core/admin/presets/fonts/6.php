<?php

$preset = array(

  'main_font' => array(
    'font-family' => 'Open Sans',
    'variant'  => '400',
    'font-weight' => '400'
  ),

  'h_font' => array(
    'font-family' => 'Abril Fatface',
    'variant' => '400',
    'font-weight' => '400'
  ),

  'nav_font' => array(
    'font-family' => 'Open Sans',
    'variant' => '400',
    'font-weight' => '400'
  ),

  'button_font' => array(
    'font-family' => 'Open Sans',
    'variant' => '600',
    'font-weight' => '600'
  ),

  'font_size_p' => '18',
  'font_size_small' => '16',
  'font_size_nav' => '15',
  'font_size_nav_ico' => '24',
  'font_size_section_title' => '26',
  'font_size_widget_title' => '24',
  'font_size_punchline' => '64',
  'font_size_h1' => '60',
  'font_size_h2' => '48',
  'font_size_h3' => '40',
  'font_size_h4' => '32',
  'font_size_h5' => '26',
  'font_size_h6' => '20',

   'uppercase' => array( 'buttons' ),
);
