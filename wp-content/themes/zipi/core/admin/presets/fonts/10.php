<?php

$preset = array(

  'main_font' => array(
    'font-family' => 'Open Sans',
    'variant'  => '400',
    'font-weight' => '400'
  ),

  'h_font' => array(
    'font-family' => 'Alegreya',
    'variant' => '700',
    'font-weight' => '700'
  ),

  'nav_font' => array(
    'font-family' => 'Open Sans',
    'variant' => '400',
    'font-weight' => '400'
  ),

  'button_font' => array(
    'font-family' => 'Open Sans',
    'variant' => '600',
    'font-weight' => '600'
  ),

  'font_size_p' => '18',
  'font_size_small' => '16',
  'font_size_nav' => '15',
  'font_size_nav_ico' => '24',
  'font_size_section_title' => '30',
  'font_size_widget_title' => '26',
  'font_size_punchline' => '72',
  'font_size_h1' => '64',
  'font_size_h2' => '52',
  'font_size_h3' => '44',
  'font_size_h4' => '36',
  'font_size_h5' => '30',
  'font_size_h6' => '24',

   'uppercase' => array( 'buttons' ),
);
