<?php

$preset = array(

  'main_font' => array(
    'font-family' => 'Open Sans',
    'variant'  => '400',
    'font-weight' => '400'
  ),

  'h_font' => array(
    'font-family' => 'Roboto Slab',
    'variant' => '700',
    'font-weight' => '700'
  ),

  'nav_font' => array(
    'font-family' => 'Open Sans',
    'variant' => '400',
    'font-weight' => '400'
  ),

  'button_font' => array(
    'font-family' => 'Open Sans',
    'variant' => '600',
    'font-weight' => '600'
  ),

  'font_size_p' => '18',
  'font_size_small' => '16',
  'font_size_nav' => '15',
  'font_size_nav_ico' => '24',
  'font_size_section_title' => '26',
  'font_size_widget_title' => '22',
  'font_size_punchline' => '62',
  'font_size_h1' => '56',
  'font_size_h2' => '44',
  'font_size_h3' => '38',
  'font_size_h4' => '30',
  'font_size_h5' => '24',
  'font_size_h6' => '20',

   'uppercase' => array( 'buttons' ),
);
