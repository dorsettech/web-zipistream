<?php

$preset = array(

  'main_font' => array(
    'font-family' => 'Open Sans',
    'variant'  => '400',
    'font-weight' => '400'
  ),

  'h_font' => array(
    'font-family' => 'PT Serif',
    'variant' => '700',
    'font-weight' => '700'
  ),

  'nav_font' => array(
    'font-family' => 'Open Sans',
    'variant' => '600',
    'font-weight' => '600'
  ),

  'button_font' => array(
    'font-family' => 'Open Sans',
    'variant' => '600',
    'font-weight' => '600'
  ),

  'font_size_p' => '16',
  'font_size_small' => '14',
  'font_size_nav' => '14',
  'font_size_nav_ico' => '24',
  'font_size_section_title' => '32',
  'font_size_widget_title' => '24',
  'font_size_punchline' => '64',
  'font_size_h1' => '58',
  'font_size_h2' => '46',
  'font_size_h3' => '40',
  'font_size_h4' => '32',
  'font_size_h5' => '26',
  'font_size_h6' => '22',

  'uppercase' => array( '.megaphone-header li a', 'buttons' ),
  
);
