<?php

$preset = array(

  //Header
  'color_header_top_bg' => '#343840',
  'color_header_top_txt' => '#ffffff',
  'color_header_top_acc' => '#EB0A0A',

  'color_header_middle_bg' => '#424850',
  'color_header_middle_txt' => '#ffffff',
  'color_header_middle_acc' => '#EB0A0A',

  'color_header_bottom_bg' => '#343840',
  'color_header_bottom_txt' => '#ffffff',
  'color_header_bottom_acc' => '#EB0A0A',

  //Content
  'color_bg' => '#424850',
  'color_h' => '#ffffff',
  'color_txt' => '#ffffff',
  'color_acc' => '#EB0A0A      ',
  'color_meta' => '#e5e5e5',
  'color_bg_alt_1' => '#343840',
  'color_bg_alt_2' => '#343840',


  //Footer
  'color_footer_bg' => '#252a33',
  'color_footer_txt' => '#FFF',
  'color_footer_acc' => '#EB0A0A',
  'color_footer_meta' => '#FFF',


);