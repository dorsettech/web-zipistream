<?php

$preset = array(

  //Header
  'color_header_top_bg' => '#f2f2f2',
  'color_header_top_txt' => '#2B1813',
  'color_header_top_acc' => '#EB0A0A',

  'color_header_middle_bg' => '#ffffff',
  'color_header_middle_txt' => '#2B1813',
  'color_header_middle_acc' => '#EB0A0A',

  'color_header_bottom_bg' => '#f2f2f2',
  'color_header_bottom_txt' => '#2B1813',
  'color_header_bottom_acc' => '#EB0A0A',

  //Content
  'color_bg' => '#ffffff',
  'color_h' => '#2B1813',
  'color_txt' => '#2B1813',
  'color_acc' => '#EB0A0A',
  'color_meta' => '#2B1813',
  'color_bg_alt_1' => '#f2f2f2',
  'color_bg_alt_2' => '#f3f3f3',


  //Footer
  'color_footer_bg' => '#120E0D',
  'color_footer_txt' => '#FFF',
  'color_footer_acc' => '#EB0A0A',
  'color_footer_meta' => '#FFF',


);