<?php

$preset = array(

  //Header
  'color_header_top_bg' => '#f2f2f2',
  'color_header_top_txt' => '#2B1813',
  'color_header_top_acc' => '#7e9d62',

  'color_header_middle_bg' => '#ffffff',
  'color_header_middle_txt' => '#2B1813',
  'color_header_middle_acc' => '#7e9d62',

  'color_header_bottom_bg' => '#f2f2f2',
  'color_header_bottom_txt' => '#2B1813',
  'color_header_bottom_acc' => '#7e9d62',

  //Content
  'color_bg' => '#ffffff',
  'color_h' => '#2B1813',
  'color_txt' => '#2B1813',
  'color_acc' => '#7e9d62',
  'color_meta' => '#2B1813',
  'color_bg_alt_1' => '#f2f2f2',
  'color_bg_alt_2' => '#f3f3f3',


  //Footer
  'color_footer_bg' => '#120E0D',
  'color_footer_txt' => '#FFF',
  'color_footer_acc' => '#7e9d62',
  'color_footer_meta' => '#FFF',


);