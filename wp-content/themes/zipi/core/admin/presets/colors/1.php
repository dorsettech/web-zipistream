<?php

$preset = array(

  //Header
  'color_header_top_bg' => '#f2f2f2',
  'color_header_top_txt' => '#2B1813',
  'color_header_top_acc' => '#FF4F00',

  'color_header_middle_bg' => '#ffffff',
  'color_header_middle_txt' => '#2B1813',
  'color_header_middle_acc' => '#ff4f00',

  'color_header_bottom_bg' => '#f2f2f2',
  'color_header_bottom_txt' => '#2B1813',
  'color_header_bottom_acc' => '#ff4f00',

  //Content
  'color_bg' => '#ffffff',
  'color_h' => '#2B1813',
  'color_txt' => '#2B1813',
  'color_acc' => '#FF4F00',
  'color_meta' => '#2B1813',
  'color_bg_alt_1' => '#f2f2f2',
  'color_bg_alt_2' => '#424850',

  //Footer
  'color_footer_bg' => '#120E0D',
  'color_footer_txt' => '#FFF',
  'color_footer_acc' => '#FF4F00',
  'color_footer_meta' => '#FFF',

);
