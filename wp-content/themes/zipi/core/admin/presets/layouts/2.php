<?php

$preset = array(

  'header_layout' => '1',
  'header_actions' => array( 'donate'),
  'header_actions_r' => array(),
  'header_indent' => '1',

  'front_page_sections' => array( 'welcome', 'episodes', 'posts' ),
  'front_page_pagination' => 'numeric',

  'wa_layout_1_height' => 600,

  //Welcome
  'front_page_wa_display_title' => false,
  'front_page_wa_layout' => '1',
  'front_page_wa_cta' => false,
  'front_page_wa_latest_episode' => true,
  'front_page_wa_subscribe' => false,

  //Episodes
  'front_page_episodes_display_title' => false,
  'front_page_episodes_loop' => '4',
  'front_page_episodes_sidebar_position' => 'right',
  'front_page_episodes_sidebar_standard' => 'megaphone_sidebar_default',
  'front_page_episodes_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
  'front_page_episodes_ppp' => 'custom',
  'front_page_episodes_ppp_num' => 6,
  'front_page_episodes_slider' => false,
  'front_page_episodes_view_all_link' => false,
  'front_page_episodes_bg' => 'none',


  // blog/posts area
  'front_page_posts_display_title' => true,
  'front_page_loop' => '6',
  'front_page_sidebar_position' => 'right',
  'front_page_sidebar_standard' => 'megaphone_sidebar_default',
  'front_page_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
  'front_page_ppp' => 'custom',
  'front_page_ppp_num' =>  get_option( 'posts_per_page' ),
  'front_page_cat' => array(),
  'front_page_tag' => array(),
  'front_page_posts_slider' => false,
  'front_page_posts_view_all_link' => false,
  'front_page_posts_view_all_link_url' => '',
  'front_page_posts_bg' => 'none',


  //Footer
  'footer_subscribe' => true,
  'footer_instagram' => false,
  
);