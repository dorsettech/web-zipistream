<?php

$preset = array(

  'header_layout' => '1',
  'header_actions' => array( 'donate'),
  'header_actions_r' => array(),
  'header_indent' => '1',

  'front_page_sections' => array( 'welcome', 'shows', 'episodes' ),
  'front_page_pagination' => 'numeric',

  //Welcome
  'front_page_wa_display_title' => false,
  'front_page_wa_layout' => '2',
  'front_page_wa_cta' => false,
  'front_page_wa_latest_episode' => true,
  'front_page_wa_subscribe' => true,

  //Shows
  'front_page_shows_display_title' => true,
  'front_page_shows_slider' => true,
  'front_page_shows_view_all_link' => true,
  'front_page_shows_bg' => 'alt-1',

  'show_layout_loop' => '4',
  'show_layout_sidebar_position' => 'right',
  'show_layout_sidebar_standard' => 'megaphone_sidebar_default',
  'show_layout_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
  'show_layout_display_image' => true,
  'show_layout_img_ratio' => '3_2',
  'show_layout_img_custom' => '',
  'show_layout_duotone_overlay' => false,
  'show_layout_meta' => true,
  'show_layout_description' => true,
  'show_layout_display_episodes' => true,
  'show_layout_display_episodes_limit' => '3',
  'show_layout_display_play_btn' => true,
  'show_layout_display_episodes_number' => true,
  'show_layout_display_view_episodes_btn' => true,

  //Episodes
  'front_page_episodes_display_title' => true,
  'front_page_episodes_loop' => '5',
  'front_page_episodes_sidebar_position' => 'right',
  'front_page_episodes_sidebar_standard' => 'megaphone_sidebar_default',
  'front_page_episodes_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
  'front_page_episodes_ppp' => 'custom',
  'front_page_episodes_ppp_num' => 6,
  'front_page_episodes_slider' => false,
  'front_page_episodes_view_all_link' => false,
  'front_page_episodes_bg' => 'none',


  //Footer
  'footer_subscribe' => false,
  'footer_instagram' => true,

);