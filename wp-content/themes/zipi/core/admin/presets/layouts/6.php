<?php

$preset = array(

  'header_layout' => '1',
  'header_actions' => array( 'social-modal', 'hamburger'),
  'header_actions_r' => array( 'donate' ),
  'header_indent' => '0',

  'front_page_sections' => array( 'episodes' ),
  'front_page_pagination' => 'numeric',

//Episodes
  'front_page_episodes_display_title' => false,
  'front_page_episodes_loop' => '3',
  'front_page_episodes_sidebar_position' => 'right',
  'front_page_episodes_sidebar_standard' => 'megaphone_sidebar_default',
  'front_page_episodes_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
  'front_page_episodes_ppp' => 'custom',
  'front_page_episodes_ppp_num' => 6,
  'front_page_episodes_slider' => false,
  'front_page_episodes_view_all_link' => false,
  'front_page_episodes_bg' => 'none',

  //Footer
  'footer_subscribe' => false,
  'footer_instagram' => true,

);