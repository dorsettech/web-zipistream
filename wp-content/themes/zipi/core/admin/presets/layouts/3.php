<?php

$preset = array(

  'header_layout' => '5',
  'header_actions' => array( 'donate'),
  'header_actions_r' => array(),
  'header_indent' => '0',

  'front_page_sections' => array( 'featured', 'shows' ),
  'front_page_pagination' => 'prev-next',

  //Featured
  'front_page_fa_display_title' => false,
  'front_page_fa_loop' => '2',
  'front_page_fa_ppp' => '3',
  'front_page_fa_view_all_link' => false,

  //Shows
  'front_page_shows_display_title' => false,
  'front_page_shows_slider' => false,
  'front_page_shows_view_all_link' => false,
  'front_page_shows_bg' => 'alt-1',

  'show_layout_loop' => '4',
  'show_layout_sidebar_position' => 'right',
  'show_layout_sidebar_standard' => 'megaphone_sidebar_default',
  'show_layout_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
  'show_layout_display_image' => true,
  'show_layout_img_ratio' => '3_2',
  'show_layout_img_custom' => '',
  'show_layout_duotone_overlay' => true,
  'show_layout_meta' => true,
  'show_layout_description' => false,
  'show_layout_display_episodes' => true,
  'show_layout_display_episodes_limit' => '4',
  'show_layout_display_play_btn' => true,
  'show_layout_display_episodes_number' => true,
  'show_layout_display_view_episodes_btn' => true,

  //Footer
  'footer_subscribe' => false,
  'footer_instagram' => true,

);