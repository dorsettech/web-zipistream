<?php

/**
 * Hide update notification and update theme version
 *
 * @since  1.0
 */

add_action( 'wp_ajax_megaphone_update_version', 'megaphone_update_version' );

if ( !function_exists( 'megaphone_update_version' ) ):
	function megaphone_update_version() {
		update_option( 'megaphone_theme_version', MEGAPHONE_THEME_VERSION );
		wp_die();
	}
endif;


/**
 * Hide welcome notification
 *
 * @since  1.0
 */

add_action( 'wp_ajax_megaphone_hide_welcome', 'megaphone_hide_welcome' );

if ( !function_exists( 'megaphone_hide_welcome' ) ):
	function megaphone_hide_welcome() {
		update_option( 'megaphone_welcome_box_displayed', true );
		wp_die();
	}
endif;