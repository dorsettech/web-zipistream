<?php

Kirki::add_section( 'megaphone_performance', array(
    'panel'          => 'megaphone_panel',
    'title'          => esc_attr__( 'Performance', 'megaphone' ),
    'priority'    => 130,
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'minify_css',
    'section'     => 'megaphone_performance',
    'type'        => 'toggle',
    'label'    => esc_html__( 'Use minified CSS', 'megaphone' ),
    'description' => esc_html__( 'Load all theme CSS files combined and minified into a single file.', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'minify_css' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'minify_js',
    'section'     => 'megaphone_performance',
    'type'        => 'toggle',
    'label'    => esc_html__( 'Use minified JS', 'megaphone' ),
    'description' => esc_html__( 'Load all theme JavaScript files combined and minified into a single file.', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'minify_js' ),
) );


$image_sizes = megaphone_get_image_sizes();

foreach ( $image_sizes as $key => $size ) {
$image_sizes[$key] = $size['title'];
}

Kirki::add_field( 'megaphone', array(
    'settings'    => 'disable_img_sizes',
    'section'     => 'megaphone_performance',
    'type'        => 'multicheck',
    'label'    => esc_html__( 'Disable additional image sizes', 'megaphone' ),
    'description' => esc_html__( 'By default, the theme generates additional size for each of the layouts it offers. You can use this option to avoid creating additional sizes if you are not using a particular layout, in order to save your server space.', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'disable_img_sizes' ),
    'choices' => $image_sizes
) );