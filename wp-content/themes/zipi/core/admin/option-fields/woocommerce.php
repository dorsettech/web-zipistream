<?php

Kirki::add_section(
	'megaphone_woocommerce',
	array(
		'panel'    => 'megaphone_panel',
		'title'    => esc_attr__( 'WooCommerce', 'megaphone' ),
		'priority' => 105,
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'woocommerce_sidebar_position',
		'section'  => 'megaphone_woocommerce',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'WooCommerce sidebar layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'woocommerce_sidebar_position' ),
		'choices'  => megaphone_get_sidebar_layouts( false, true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'woocommerce_sidebar_standard',
		'section'  => 'megaphone_woocommerce',
		'type'     => 'select',
		'label'    => esc_html__( 'WooCommerce standard sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'woocommerce_sidebar_standard' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'woocommerce_sidebar_position',
				'operator' => '!=',
				'value'    => 'none',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'woocommerce_sidebar_sticky',
		'section'  => 'megaphone_woocommerce',
		'type'     => 'select',
		'label'    => esc_html__( 'WooCommerce sticky sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'woocommerce_sidebar_sticky' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'woocommerce_sidebar_position',
				'operator' => '!=',
				'value'    => 'none',
			),
		),

	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'woocommerce_cart_force',
		'section'     => 'megaphone_woocommerce',
		'type'        => 'toggle',
		'label'       => esc_html__( 'Always display Cart icon', 'megaphone' ),
		'description' => esc_html__( 'If you check this option, Cart icon will be always visible on WooCommerce pages, even if it is not selected globally in Header options', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'woocommerce_cart_force' ),
	)
);