<?php 

/* Single episode */
Kirki::add_section(
	'megaphone_single_podcast',
	array(
		'panel' => 'megaphone_panel_podcast',
		'title' => esc_attr__( 'Single Episode', 'megaphone' ),
		'priority'    => 40,
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_layout',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_layout' ),
		'choices'  => megaphone_get_single_episode_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_layout_1_img_ratio',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio for layout 1', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_layout_1_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
		'required' => array(
			array(
				'setting'  => 'single_podcast_layout',
				'operator' => '==',
				'value'    => '1',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'single_podcast_layout_1_img_custom',
		'section'     => 'megaphone_single_podcast',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'single_podcast_layout_1_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'single_podcast_layout_1_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
			array(
				'setting'  => 'single_podcast_layout',
				'operator' => '==',
				'value'    => '1',
			),

		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_layout_2_img_ratio',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio for layout 2', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_layout_2_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
		'required' => array(
			array(
				'setting'  => 'single_podcast_layout',
				'operator' => '==',
				'value'    => '2',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'single_podcast_layout_2_img_custom',
		'section'     => 'megaphone_single_podcast',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'single_podcast_layout_2_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'single_podcast_layout_2_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
			array(
				'setting'  => 'single_podcast_layout',
				'operator' => '==',
				'value'    => '2',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_layout_3_height',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'number',
		'label'    => esc_html__( 'Cover area (image) height for layout 3', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_layout_3_height' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'single_podcast_layout',
				'operator' => '==',
				'value'    => '3',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_layout_4_height',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'number',
		'label'    => esc_html__( 'Cover area (image) height for layout 4', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_layout_4_height' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'single_podcast_layout',
				'operator' => '==',
				'value'    => '4',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_sidebar_position',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Sidebar position', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_sidebar_position' ),
		'choices'  => megaphone_get_sidebar_layouts( false, true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_sidebar_standard',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'select',
		'label'    => esc_html__( 'Standard sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_sidebar_standard' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'single_podcast_sidebar_position',
				'operator' => '!=',
				'value'    => 'none',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_sidebar_sticky',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'select',
		'label'    => esc_html__( 'Sticky sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_sidebar_sticky' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'single_podcast_sidebar_position',
				'operator' => '!=',
				'value'    => 'none',
			),
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_width',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'radio-buttonset',
		'label'    => esc_html__( 'Content (text) width', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_width' ),
		'choices'  => array(
			'6' => esc_html__( 'Narrow', 'megaphone' ),
			'7' => esc_html__( 'Medium', 'megaphone' ),
			'8' => esc_html__( 'Wide', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_number',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_number' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_meta_up',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_meta_down',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_fimg',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display featured image', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_fimg' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_fimg_cap',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display featured image caption', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_fimg_cap' ),
		'required' => array(
			array(
				'setting'  => 'single_podcast_fimg',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_subscribe',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display subscribe area', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_subscribe' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_headline',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display headline (episode excerpt)', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_headline' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_author',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'radio',
		'label'    => esc_html__( 'Display author area', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_author' ),
		'choices'  => array(
			'above' => esc_html__( 'Above content', 'megaphone' ),
			'bellow' => esc_html__( 'Bellow content', 'megaphone' ),
			'none' => esc_html__( 'None', 'megaphone' ),
		)
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_tags',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display tags', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_tags' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_show_more',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable show/hide content ', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_show_more' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_show_more_height',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'number',
		'label'    => esc_html__( 'Show/hide content height', 'megaphone' ),
		'description'    => esc_html__( 'Specify after how many pixels the content will be hidden', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_show_more_height' ),
		'required' => array(
			array(
				'setting'  => 'single_podcast_show_more',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_related',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "related episodes" area', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_related' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_related_layout',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Related episodes layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_related_layout' ),
		'choices'  => megaphone_get_episode_layouts( array( 'sidebar' => false ) ),
		'required' => array(
			array(
				'setting'  => 'single_podcast_related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_related_limit',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'number',
		'label'    => esc_html__( 'Number of related episodes', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_related_limit' ),
		'required' => array(
			array(
				'setting'  => 'single_podcast_related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_podcast_related_order',
		'section'  => 'megaphone_single_podcast',
		'type'     => 'radio',
		'label'    => esc_html__( 'Related episodes are ordered by', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_podcast_related_order' ),
		'choices'  => megaphone_get_post_order_opts(),
		'required' => array(
			array(
				'setting'  => 'single_podcast_related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);