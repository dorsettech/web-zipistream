<?php

/* Single blog */
Kirki::add_section(
	'megaphone_single_blog',
	array(
		'panel' => 'megaphone_panel_blog',
		'title' => esc_attr__( 'Single post', 'megaphone' ),
		'priority' => 40,
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_layout',
		'section'  => 'megaphone_single_blog',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_layout' ),
		'choices'  => megaphone_get_single_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_layout_1_img_ratio',
		'section'  => 'megaphone_single_blog',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio for layout 1', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_layout_1_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
		'required' => array(
			array(
				'setting'  => 'single_blog_layout',
				'operator' => '==',
				'value'    => '1',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'single_blog_layout_1_img_custom',
		'section'     => 'megaphone_single_blog',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'single_blog_layout_1_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'single_blog_layout_1_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
			array(
				'setting'  => 'single_blog_layout',
				'operator' => '==',
				'value'    => '1',
			),

		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_layout_2_img_ratio',
		'section'  => 'megaphone_single_blog',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio for layout 2', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_layout_2_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
		'required' => array(
			array(
				'setting'  => 'single_blog_layout',
				'operator' => '==',
				'value'    => '2',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'single_blog_layout_2_img_custom',
		'section'     => 'megaphone_single_blog',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'single_blog_layout_2_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'single_blog_layout_2_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
			array(
				'setting'  => 'single_blog_layout',
				'operator' => '==',
				'value'    => '2',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_layout_3_height',
		'section'  => 'megaphone_single_blog',
		'type'     => 'number',
		'label'    => esc_html__( 'Cover area (image) height for layout 3', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_layout_3_height' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'single_blog_layout',
				'operator' => '==',
				'value'    => '3',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_layout_4_height',
		'section'  => 'megaphone_single_blog',
		'type'     => 'number',
		'label'    => esc_html__( 'Cover area (image) height for layout 4', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_layout_4_height' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'single_blog_layout',
				'operator' => '==',
				'value'    => '4',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_sidebar_position',
		'section'  => 'megaphone_single_blog',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Sidebar position', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_sidebar_position' ),
		'choices'  => megaphone_get_sidebar_layouts( false, true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_sidebar_standard',
		'section'  => 'megaphone_single_blog',
		'type'     => 'select',
		'label'    => esc_html__( 'Standard sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_sidebar_standard' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'single_blog_sidebar_position',
				'operator' => '!=',
				'value'    => 'none',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_sidebar_sticky',
		'section'  => 'megaphone_single_blog',
		'type'     => 'select',
		'label'    => esc_html__( 'Sticky sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_sidebar_sticky' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'single_blog_sidebar_position',
				'operator' => '!=',
				'value'    => 'none',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_width',
		'section'  => 'megaphone_single_blog',
		'type'     => 'radio-buttonset',
		'label'    => esc_html__( 'Content (text) width', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_width' ),
		'choices'  => array(
			'6' => esc_html__( 'Narrow', 'megaphone' ),
			'7' => esc_html__( 'Medium', 'megaphone' ),
			'8' => esc_html__( 'Wide', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_meta_up',
		'section'  => 'megaphone_single_blog',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_meta_up' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_meta_down',
		'section'  => 'megaphone_single_blog',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_fimg',
		'section'  => 'megaphone_single_blog',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display featured image', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_fimg' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_fimg_cap',
		'section'  => 'megaphone_single_blog',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display featured image caption', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_fimg_cap' ),
		'required' => array(
			array(
				'setting'  => 'single_blog_fimg',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_headline',
		'section'  => 'megaphone_single_blog',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display headline (post excerpt)', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_headline' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_tags',
		'section'  => 'megaphone_single_blog',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display tags', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_tags' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_author',
		'section'  => 'megaphone_single_blog',
		'type'     => 'radio',
		'label'    => esc_html__( 'Display author area', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_author' ),
		'choices'  => array(
			'above' => esc_html__( 'Above content', 'megaphone' ),
			'bellow' => esc_html__( 'Bellow content', 'megaphone' ),
			'none' => esc_html__( 'None', 'megaphone' ),
		)
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_related',
		'section'  => 'megaphone_single_blog',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "related posts" area', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_related' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_related_layout',
		'section'  => 'megaphone_single_blog',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Related post layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_related_layout' ),
		'choices'  => megaphone_get_post_layouts( array( 'sidebar' => false ) ),
		'required' => array(
			array(
				'setting'  => 'single_blog_related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_related_limit',
		'section'  => 'megaphone_single_blog',
		'type'     => 'number',
		'label'    => esc_html__( 'Number of related post', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_related_limit' ),
		'required' => array(
			array(
				'setting'  => 'single_blog_related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_related_type',
		'section'  => 'megaphone_single_blog',
		'type'     => 'radio',
		'label'    => esc_html__( 'Related area chooses from posts', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_related_type' ),
		'choices'  => array(
			'cat'         => esc_html__( 'Located in the same category', 'megaphone' ),
			'tag'         => esc_html__( 'Tagged with at least one same tag', 'megaphone' ),
			'cat_or_tag'  => esc_html__( 'Located in the same category OR tagged with a same tag', 'megaphone' ),
			'cat_and_tag' => esc_html__( 'Located in the same category AND tagged with a same tag', 'megaphone' ),
			'author'      => esc_html__( 'By the same author', 'megaphone' ),
			'0'           => esc_html__( 'All posts', 'megaphone' ),
		),
		'required' => array(
			array(
				'setting'  => 'single_blog_related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'single_blog_related_order',
		'section'  => 'megaphone_single_blog',
		'type'     => 'radio',
		'label'    => esc_html__( 'Related posts are ordered by', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'single_blog_related_order' ),
		'choices'  => megaphone_get_post_order_opts(),
		'required' => array(
			array(
				'setting'  => 'single_blog_related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);






