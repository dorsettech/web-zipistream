<?php


Kirki::add_section(
    'megaphone_podcast_general',
    array(
        'panel'    => 'megaphone_panel_podcast',
        'title'    => esc_attr__( 'General', 'megaphone' ),
        'priority' => 70,
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings'    => 'megaphone_podcast_setup',
        'section'     => 'megaphone_podcast_general',
        'type'        => 'radio',
        'label'       => esc_html__( 'For podcast (episodes), I want to use:', 'megaphone' ),
        'default'     => megaphone_get_default_option( 'megaphone_podcast_setup' ),
        'choices'     => megaphone_podcasts_options(),
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings'    => 'blog_terms',
        'section'     => 'megaphone_podcast_general',
        'type'        => 'select',
        'multiple'    => 10,
        'label'       => esc_html__( 'Select categories to separate podcast from regular blog', 'megaphone' ),
        'description' => esc_html__( 'By default, all posts are considered as podcast (episodes). If you want to use regular blog posts as well, please specify categories which are used for regular blog inside this option.', 'megaphone' ),
        'default'     => megaphone_get_default_option( 'blog_terms' ),
        'choices'     => Kirki_Helper::get_terms( 'category' ),
        'required'    => array(
            array(
                'setting'  => 'megaphone_podcast_setup',
                'operator' => '==',
                'value'    => 'theme_organization',
            ),
        ),
    )
);


Kirki::add_field(
    'megaphone',
    array(
        'settings'    => 'podlove_terms',
        'section'     => 'megaphone_podcast_general',
        'type'        => 'select',
        'multiple'    => 10,
        'label'       => esc_html__( 'Select podlove podcast categories', 'megaphone' ),
        'description' => esc_html__( 'If you want to use Shows, please select all categories that you are using as a podcast show to group episodes', 'megaphone' ),
        'default'     => megaphone_get_default_option( 'podlove_terms' ),
        'choices'     => Kirki_Helper::get_terms( 'category' ),
        'required'    => array(
            array(
                'setting'  => 'megaphone_podcast_setup',
                'operator' => '==',
                'value'    => 'podlove',
            ),
        ),
    )
);


Kirki::add_field(
    'megaphone',
    array(
        'settings' => 'episodes_post_order',
        'section'  => 'megaphone_podcast_general',
        'type'     => 'radio',
        'label'    => esc_html__( 'Episodes are listed in', 'megaphone' ),
        'default'  => megaphone_get_default_option( 'episodes_post_order' ),
        'choices'  => array(
            'DESC' => esc_html__( 'Descending order (latest to oldest)', 'megaphone' ),
            'ASC'  => esc_html__( 'Ascending order (oldest to latest)', 'megaphone' ),
        ),
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings'    => 'episode_number_from_title',
        'section'     => 'megaphone_podcast_general',
        'type'        => 'toggle',
        'label'       => esc_html__( 'Autodetect episode number from post title', 'megaphone' ),
        'default'     => megaphone_get_default_option( 'episode_number_from_title' ),
    )
);


Kirki::add_field(
    'megaphone',
    array(
        'settings'    => 'episode_number_format',
        'section'     => 'megaphone_podcast_general',
        'type'        => 'text',
        'label'       => esc_html__( 'Specify episode title pattern', 'megaphone' ),
        'description' => wp_kses_post( __( 'Specify the episode format of your episode titles and make sure to add your episode number inside curly braces {}. <br>Example: <br> Episode {number}: <br> Episode #{number}: <br> [Episode {number}] <br> EP {number}:', 'megaphone' ) ),
        'default'     => megaphone_get_default_option( 'episode_number_format' ),
        'required'    => array(
            array(
                'setting'  => 'episode_number_from_title',
                'operator' => '==',
                'value'    => true,
            ),
        ),
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings'    => 'strip_episode_number_from_title',
        'section'     => 'megaphone_podcast_general',
        'type'        => 'toggle',
        'label'       => esc_html__( 'Strip episodes from title', 'megaphone' ),
        'description' => esc_html__( 'Enable this option to remove episode number and prefix from post titles', 'megaphone' ),
        'default'     => megaphone_get_default_option( 'strip_episode_number_from_title' ),
        'required'    => array(
            array(
                'setting'  => 'episode_number_from_title',
                'operator' => '==',
                'value'    => true,
            ),
        ),
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings'    => 'play_button_is_link',
        'section'     => 'megaphone_podcast_general',
        'type'        => 'toggle',
        'label'       => esc_html__( 'Use play buttons as regular links', 'megaphone' ),
        'description' => esc_html__( 'Enable this option if you want play buttons to always lead to a single post instead of opening the player', 'megaphone' ),
        'default'     => megaphone_get_default_option( 'play_button_is_link' ),
    )
);


Kirki::add_field(
    'megaphone',
    array(
        'settings' => 'podcast_sponsored',
        'section'  => 'megaphone_podcast_general',
        'type'     => 'toggle',
        'label'    => esc_html__( 'Enable sponsored podcast', 'megaphone' ),
        'default'  => megaphone_get_default_option( 'podcast_sponsored' ),
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings' => 'podcast_sponsored_tags',
        'section'  => 'megaphone_podcast_general',
        'type'     => 'select',
        'label'    => esc_html__( 'Define sponsored episodes by tag(s)', 'megaphone' ),
        'description' => esc_html__( 'Posts tagged with these tags will considered as sponsored', 'megaphone' ),
        'default'  => megaphone_get_default_option( 'podcast_sponsored_tags' ),
        'choices'  => Kirki_Helper::get_terms( 'post_tag' ),
        'multiple' => 10,
        'required' => array(
            array(
                'setting'  => 'podcast_sponsored',
                'operator' => '==',
                'value'    => true,
            ),
        ),
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings' => 'podcast_sponsored_manual',
        'section'  => 'megaphone_podcast_general',
        'type'     => 'select',
        'label'    => esc_html__( 'Or select sponsored episodes manually', 'megaphone' ),
        'default'  => megaphone_get_default_option( 'podcast_sponsored_manual' ),
        'choices'  => Kirki_Helper::get_posts(
            array(
                'posts_per_page' => -1,
                'post_type'      => 'post',
                'cat_not__in'    => megaphone_get_option( 'blog_terms' ),
            )
        ),
        'multiple' => 100,
        'required' => array(
            array(
                'setting'  => 'podcast_sponsored',
                'operator' => '==',
                'value'    => true,
            ),
        ),
    )
);