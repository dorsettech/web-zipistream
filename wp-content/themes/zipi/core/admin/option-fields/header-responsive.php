<?php

Kirki::add_section(
	'megaphone_header_responsive',
	array(
		'title' => esc_attr__( 'Responsive/mobile', 'megaphone' ),
		'panel' => 'megaphone_panel_header',
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_elements_responsive',
		'section'  => 'megaphone_header_responsive',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Enable elements for your mobile/responsive header', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_elements_responsive' ),
		'choices'  => megaphone_get_header_main_area_actions( array( 'social', 'search', 'hamburger', 'subscribe' ) ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_menu_elements_responsive',
		'section'  => 'megaphone_header_responsive',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Append elements after the mobile menu', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_menu_elements_responsive' ),
		'choices'  => megaphone_get_header_top_elements( array( 'search-modal', 'social-modal', 'subscribe-modal', 'date', 'hamburger' ) ),
	)
);