<?php

Kirki::add_panel(
	'megaphone_panel_front',
	array(
		'panel'    => 'megaphone_panel',
		'title'    => esc_attr__( 'Front Page', 'megaphone' ),
		'priority' => 30,
	)
);

require_once get_parent_theme_file_path( '/core/admin/option-fields/front-page-general.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/front-page-welcome.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/front-page-featured.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/front-page-shows.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/front-page-episodes.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/front-page-blog.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/front-page-custom-content.php' );

// $custom_sections_numb = megaphone_get_option( 'front_page_general_custom_fields_number' );

for ( $i = 2; $i <= 5; $i++ ) {
	require_once get_parent_theme_file_path( '/core/admin/option-fields/front-page-custom-content-'.$i.'.php' );
}