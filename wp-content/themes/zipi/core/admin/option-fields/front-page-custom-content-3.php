<?php

Kirki::add_section(
	'megaphone_front_custom_content_3',
	array(
		'panel' => 'megaphone_panel_front',
		'title' => esc_attr__( 'Custom Content 3', 'megaphone' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_custom_content_type_3',
		'section'  => 'megaphone_front_custom_content_3',
		'type'     => 'radio',
		'label'    => esc_html__( 'Pull custom content from', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_custom_content_type_3' ),
		'choices'  => array(
			'this' => esc_html__( 'Here (theme options)', 'megaphone' ),
			'page' => esc_html__( 'Page (select any page)', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'        => 'front_page_custom_content_page_3',
		'section'         => 'megaphone_front_custom_content_3',
		'type'            => 'select',
		'label'           => esc_html__( 'Select page', 'megaphone' ),
		'default'         => megaphone_get_default_option( 'front_page_custom_content_page_3' ),
		'choices'         => Kirki_Helper::get_posts(
			array(
				'posts_per_page' => -1,
				'post_type'      => 'page',
			)
		),
		'active_callback' => array(
			array(
				'setting'  => 'front_page_custom_content_type_3',
				'operator' => '==',
				'value'    => 'page',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_custom_content_display_title_3',
		'section'  => 'megaphone_front_custom_content_3',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section/page title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_custom_content_display_title_3' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'        => 'tr_front_page_custom_content_title_3',
		'section'         => 'megaphone_front_custom_content_3',
		'type'            => 'text',
		'label'           => esc_html__( 'Title', 'megaphone' ),
		'default'         => megaphone_get_default_option( 'tr_front_page_custom_content_title_3' ),
		'active_callback' => array(
			array(
				'setting'  => 'front_page_custom_content_type_3',
				'operator' => '==',
				'value'    => 'this',
			),
			array(
				'setting'  => 'front_page_custom_content_display_title_3',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'          => 'tr_front_page_custom_content_3',
		'section'           => 'megaphone_front_custom_content_3',
		'type'              => 'editor',
		'label'             => esc_html__( 'Content', 'megaphone' ),
		'description'       => esc_html__( 'Add your custom content, shortcodes are supported.', 'megaphone' ),
		'default'           => megaphone_get_default_option( 'tr_front_page_custom_content_3' ),
		'sanitize_callback' => 'megaphone_sanitize_ad',
		'active_callback'   => array(
			array(
				'setting'  => 'front_page_custom_content_type_3',
				'operator' => '==',
				'value'    => 'this',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_custom_content_bg_3',
		'section'  => 'megaphone_front_custom_content_3',
		'type'     => 'radio',
		'label'    => esc_html__( 'Background/style options', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_custom_content_bg_3' ),
		'choices'  => megaphone_get_background_opts( true ),
	)
);
