<?php

Kirki::add_section(
	'megaphone_misc',
	array(
		'panel'    => 'megaphone_panel',
		'title'    => esc_attr__( 'Miscellaneous', 'megaphone' ),
		'priority' => 120,
	)
);



Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'rtl_mode',
		'section'     => 'megaphone_misc',
		'type'        => 'toggle',
		'label'       => esc_html__( 'RTL mode (right to left)', 'megaphone' ),
		'description' => esc_html__( 'Enable this option if the website is using right to left writing/reading', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'rtl_mode' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'rtl_lang_skip',
		'section'     => 'megaphone_misc',
		'type'        => 'text',
		'label'       => esc_html__( 'Skip RTL for specific language(s)', 'megaphone' ),
		'description' => esc_html__( 'i.e. If you are using Arabic and English versions on the same WordPress installation you should put "en_US" in this field and its version will not be displayed as RTL. Note: To exclude multiple languages, separate by comma: en_US, de_DE', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'rtl_lang_skip' ),
		'required'    => array(
			array(
				'setting'  => 'rtl_mode',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'breadcrumbs',
		'section'     => 'megaphone_misc',
		'type'        => 'radio',
		'label'       => esc_html__( 'Enable breadcrumbs support', 'megaphone' ),
		'description' => esc_html__( 'Select a plugin you are using for breadcrumbs', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'breadcrumbs' ),
		'choices'     => megaphone_get_breadcrumbs_options(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'popup',
		'section'     => 'megaphone_misc',
		'type'        => 'toggle',
		'label'       => esc_html__( 'Open images in popup', 'megaphone' ),
		'description' => esc_html__( 'If you check this option, images inside galleries and post content will open in popup', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'popup' ),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'go_to_top',
		'section'  => 'megaphone_misc',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable "go to top" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'go_to_top' ),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'more_string',
		'section'     => 'megaphone_misc',
		'type'        => 'text',
		'label'       => esc_html__( 'More string', 'megaphone' ),
		'description' => esc_html__( 'Specify your "more" string to append after the limited post excerpts', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'more_string' ),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'words_read_per_minute',
		'section'     => 'megaphone_misc',
		'type'        => 'number',
		'label'       => esc_html__( 'Words to read per minute', 'megaphone' ),
		'description' => esc_html__( 'Use this option to set the number of words your visitors read per minute, in order to fine-tune the calculation of the post reading time meta data', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'words_read_per_minute' ),
		'choices'     => array(
			'step' => '1',
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'default_fimg',
		'section'     => 'megaphone_misc',
		'type'        => 'image',
		'label'       => esc_html__( 'Default featured image', 'megaphone' ),
		'description' => esc_html__( 'Upload your default featured image/placeholder. It will be displayed for posts that do not have a featured image set', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'default_fimg' ),
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings'    => '404_image',
		'section'     => 'megaphone_misc',
		'type'        => 'image',
		'label'       => esc_html__( '404 image', 'megaphone' ),
		'description' => esc_html__( 'Upload your 404 image. It will be displayed on 404/not found page', 'megaphone' ),
		'default'     => megaphone_get_default_option( '404_image' ),
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);
