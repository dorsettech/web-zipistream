<?php

Kirki::add_panel(
    'megaphone_panel_podcast',
    array(
        'panel'    => 'megaphone_panel',
        'title'    => esc_attr__( 'Podcast', 'megaphone' ),
        'priority' => 10,
    )
);

/* Episodes */
require_once get_parent_theme_file_path( '/core/admin/option-fields/podcast-episode-layouts.php' );

/* Featured */
require_once get_parent_theme_file_path( '/core/admin/option-fields/podcast-featured-layouts.php' );

/* Shows */
require_once get_parent_theme_file_path( '/core/admin/option-fields/podcast-show-layouts.php' );

/* Show */
require_once get_parent_theme_file_path( '/core/admin/option-fields/podcast-show.php' );

/* Single episode */
require_once get_parent_theme_file_path( '/core/admin/option-fields/podcast-single.php' );

/* Player */
require_once get_parent_theme_file_path( '/core/admin/option-fields/podcast-player.php' );

/* General */
require_once get_parent_theme_file_path( '/core/admin/option-fields/podcast-general.php' );