<?php


Kirki::add_section(
	'megaphone_front_general',
	array(
		'panel' => 'megaphone_panel_front',
		'title' => esc_attr__( 'General', 'megaphone' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_template',
		'section'     => 'megaphone_front_general',
		'type'        => 'toggle',
		'label'       => esc_html__( 'Use theme\'s built-in front page', 'megaphone' ),
		'description' => esc_html__( 'Disable this option if you want front page to display "Your latest posts" or "A static page" as specified in Settings -> Reading.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_template' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_sections',
		'section'     => 'megaphone_front_general',
		'type'        => 'sortable',
		'label'       => esc_html__( 'Sections', 'megaphone' ),
		'description' => esc_html__( 'Select (and re-order) front page sections that you want to display', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_sections' ),
		'choices'     => megaphone_front_page_sections_opt(),
		'required'    => array(
			array(
				'setting'  => 'front_page_template',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_general_custom_fields_number',
		'section'     => 'megaphone_front_general',
		'type'        => 'slider',
		'label'       => esc_html__( 'Number of custom content sections', 'megaphone' ),
		'description' => esc_html__( 'Specify how many custom content section you want to use from 1 to 5 (5 is max number of custom content sections).', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_general_custom_fields_number' ),
		'choices'     => array(
			'min'  => 1,
			'max'  => 5,
			'step' => 1,
		)
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_pagination',
		'section'     => 'megaphone_front_general',
		'type'        => 'radio-image',
		'label'       => esc_html__( 'Pagination', 'megaphone' ),
		'description' => esc_html__( 'If enabled, pagination will be applied to the last blog or episodes section on the page', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_pagination' ),
		'choices'     => megaphone_get_pagination_layouts( false, true ),
		'required'    => array(
			array(
				'setting'  => 'front_page_template',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);