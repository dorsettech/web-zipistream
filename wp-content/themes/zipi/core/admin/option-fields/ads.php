<?php

Kirki::add_section( 'megaphone_ads', array(
    'panel'          => 'megaphone_panel',
    'title'          => esc_attr__( 'Ads', 'megaphone' ),
    'description'   => esc_html__( 'Use these options to fill your ad slots. Both HTML/image and JavaScript related ads are allowed. You can also use shortcodes from your favorite Ad plugins.', 'megaphone' ),
    'priority'    => 110,
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'ad_header',
    'section'     => 'megaphone_ads',
    'type'        => 'editor',
    'label'    => esc_html__( 'Inside header', 'megaphone' ),
    'description' => esc_html__( 'This ad will be visible inside header if you choose one of specific header layouts that can display the ad', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'ad_header' ),
    'sanitize_callback' => 'megaphone_sanitize_ad'
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'ad_above_archive',
    'section'     => 'megaphone_ads',
    'type'        => 'editor',
    'label'    => esc_html__( 'Archive top', 'megaphone' ),
    'description' => esc_html__( 'This ad will be displayed above the content of your archive templates (i.e. categories, tags, etc...)', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'ad_above_archive' ),
    'sanitize_callback' => 'megaphone_sanitize_ad'
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'ad_above_singular',
    'section'     => 'megaphone_ads',
    'type'        => 'editor',
    'label'    => esc_html__( 'Single post/page top', 'megaphone' ),
    'description' => esc_html__( 'This ad will be displayed above the content of single posts and pages', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'ad_above_singular' ),
    'sanitize_callback' => 'megaphone_sanitize_ad'
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'ad_above_footer',
    'section'     => 'megaphone_ads',
    'type'        => 'editor',
    'label'    => esc_html__( 'Above footer', 'megaphone' ),
    'description' => esc_html__( 'This ad will be displayed above the footer area on all templates', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'ad_above_footer' ),
    'sanitize_callback' => 'megaphone_sanitize_ad'
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'ad_between_posts',
    'section'     => 'megaphone_ads',
    'type'        => 'editor',
    'label'    => esc_html__( 'Between posts', 'megaphone' ),
    'description' => esc_html__( 'This ad will be displayed between the posts listing on your archive templates', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'ad_between_posts' ),
    'sanitize_callback' => 'megaphone_sanitize_ad'
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'ad_between_position',
    'section'     => 'megaphone_ads',
    'type'        => 'number',
    'label'    => esc_html__( 'Between posts ad position', 'megaphone' ),
    'description' => esc_html__( 'Specify after how many posts you want to display the ad', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'ad_between_position' ),
    'required'    => array(
        array(
            'setting'  => 'ad_between_posts',
            'operator' => '!=',
            'value'    => '',
        ),
    ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'ad_exclude',
    'section'     => 'megaphone_ads',
    'type'        => 'select',
    'multiple'    => 10,
    'label'       => esc_html__( 'Do not show ads on these specific pages', 'megaphone' ),
    'description'       => esc_html__( 'Select pages on which you don\'t want to display ads', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'ad_exclude' ),
    'choices'     => Kirki_Helper::get_posts( array( 'post_type' => 'page', 'posts_per_page' => '-1' ) )
) );