<?php

Kirki::add_section(
	'megaphone_archives_general',
	array(
		'panel' => 'megaphone_panel_blog',
		'title' => esc_attr__( 'Archives Template', 'megaphone' ),
		'priority' => 20,
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_layout',
		'section'  => 'megaphone_archives_general',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_layout' ),
		'choices'  => megaphone_get_archive_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'archive_layout_height',
		'section'     => 'megaphone_archives_general',
		'type'        => 'number',
		'label'       => esc_html__( 'Cover area (image) height', 'megaphone' ),
		'description' => esc_html__( 'This option will be applied if archive has image', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'archive_layout_height' ),
		'choices'     => array(
			'step' => '1',
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_description',
		'section'  => 'megaphone_archives_general',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display archive description', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_description' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_meta',
		'section'  => 'megaphone_archives_general',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display number of posts label', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_meta' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_loop',
		'section'  => 'megaphone_archives_general',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Posts layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_loop' ),
		'choices'  => megaphone_get_post_layouts( array(), array( 10, 11 ) ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_sidebar_position',
		'section'  => 'megaphone_archives_general',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Sidebar position', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_sidebar_position' ),
		'choices'  => megaphone_get_sidebar_layouts(),
		'required' => array(
			array(
				'setting'  => 'archive_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_post_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_sidebar_standard',
		'section'  => 'megaphone_archives_general',
		'type'     => 'select',
		'label'    => esc_html__( 'Standard sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_sidebar_standard' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'archive_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_post_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_sidebar_sticky',
		'section'  => 'megaphone_archives_general',
		'type'     => 'select',
		'label'    => esc_html__( 'Sticky sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_sidebar_sticky' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'archive_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_post_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_ppp',
		'section'  => 'megaphone_archives_general',
		'type'     => 'radio',
		'label'    => esc_html__( 'Number of posts per page', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_ppp' ),
		'choices'  => array(
			'inherit' => esc_html__( 'Inherit from global option set in Settings / Reading', 'megaphone' ),
			'custom'  => esc_html__( 'Custom number', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_ppp_num',
		'section'  => 'megaphone_archives_general',
		'type'     => 'number',
		'label'    => esc_html__( 'Specify number of posts', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_ppp_num' ),
		'required' => array(
			array(
				'setting'  => 'archive_ppp',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'archive_pagination',
		'section'  => 'megaphone_archives_general',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Pagination', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'archive_pagination' ),
		'choices'  => megaphone_get_pagination_layouts(),
	)
);