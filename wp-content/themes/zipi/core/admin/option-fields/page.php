<?php


Kirki::add_section( 'megaphone_page', array(
    'panel'          => 'megaphone_panel',
    'title'          => esc_attr__( 'Page', 'megaphone' ),
    'priority'    => 80,
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_layout',
    'section'     => 'megaphone_page',
    'type'        => 'radio-image',
    'label'       => esc_html__( 'Layout', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_layout' ),
    'choices'     => megaphone_get_page_layouts()
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_layout_1_img_ratio',
    'section'     => 'megaphone_page',
    'type'        => 'select',
    'label'       => esc_html__( 'Image ratio for layout 1', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_layout_1_img_ratio' ),
    'choices'   => megaphone_get_image_ratio_opts(),
    'required'    => array(
        array(
            'setting'  => 'page_layout',
            'operator' => '==',
            'value'    => '1'
        ),
    ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_layout_1_img_custom',
    'section'     => 'megaphone_page',
    'type'        => 'text',
    'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
    'description'      => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_layout_1_img_custom' ),
    'required'    => array(
        array(
            'setting'  => 'page_layout_1_img_ratio',
            'operator' => '==',
            'value'    => 'custom'
        ),
        array(
            'setting'  => 'page_layout',
            'operator' => '==',
            'value'    => '1'
        ),

    ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_layout_2_img_ratio',
    'section'     => 'megaphone_page',
    'type'        => 'select',
    'label'       => esc_html__( 'Image ratio for layout 2', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_layout_2_img_ratio' ),
    'choices'   => megaphone_get_image_ratio_opts(),
    'required'    => array(
        array(
            'setting'  => 'page_layout',
            'operator' => '==',
            'value'    => '2'
        ),
    ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_layout_2_img_custom',
    'section'     => 'megaphone_page',
    'type'        => 'text',
    'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
    'description'      => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_layout_2_img_custom' ),
    'required'    => array(
        array(
            'setting'  => 'page_layout_2_img_ratio',
            'operator' => '==',
            'value'    => 'custom'
        ),
        array(
            'setting'  => 'page_layout',
            'operator' => '==',
            'value'    => '2'
        ),
    ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_layout_3_height',
    'section'     => 'megaphone_page',
    'type'        => 'number',
    'label'       => esc_html__( 'Cover area (image) height for layout 3', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_layout_3_height' ),
    'choices'     => array(
        'step' => '1'
    ),
    'required'    => array(
        array(
            'setting'  => 'page_layout',
            'operator' => '==',
            'value'    => '3'
        ),
    ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_layout_4_height',
    'section'     => 'megaphone_page',
    'type'        => 'number',
    'label'       => esc_html__( 'Cover area (image) height for layout 4', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_layout_4_height' ),
    'choices'     => array(
        'step' => '1'
    ),
    'required'    => array(
        array(
            'setting'  => 'page_layout',
            'operator' => '==',
            'value'    => '4'
        ),
    ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_sidebar_position',
    'section'     => 'megaphone_page',
    'type'        => 'radio-image',
    'label'       => esc_html__( 'Sidebar position', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_sidebar_position' ),
    'choices'     => megaphone_get_sidebar_layouts( false, true )
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_sidebar_standard',
    'section'     => 'megaphone_page',
    'type'        => 'select',
    'label'       => esc_html__( 'Standard sidebar', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_sidebar_standard' ),
    'choices'     => megaphone_get_sidebars_list(),
    'required'    => array(
        array(
            'setting'  => 'page_sidebar_position',
            'operator' => '!=',
            'value'    =>  'none'
        )
    )
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_sidebar_sticky',
    'section'     => 'megaphone_page',
    'type'        => 'select',
    'label'       => esc_html__( 'Sticky sidebar', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_sidebar_sticky' ),
    'choices'     => megaphone_get_sidebars_list(),
    'required'    => array(
        array(
            'setting'  => 'page_sidebar_position',
            'operator' => '!=',
            'value'    =>  'none'
        )
    )
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_width',
    'section'     => 'megaphone_page',
    'type'        => 'radio-buttonset',
    'label'       => esc_html__( 'Content (text) width', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_width' ),
    'choices'     => array(
        '6'   => esc_html__( 'Narrow', 'megaphone' ),
        '7' => esc_html__( 'Medium', 'megaphone' ),
        '8'  => esc_html__( 'Wide', 'megaphone' ),
    ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_fimg',
    'section'     => 'megaphone_page',
    'type'        => 'toggle',
    'label'       => esc_html__( 'Display featured image', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_fimg' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'page_fimg_cap',
    'section'     => 'megaphone_page',
    'type'        => 'toggle',
    'label'       => esc_html__( 'Display featured image caption', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'page_fimg_cap' ),
    'required'    => array(
        array(
            'setting'  => 'page_fimg',
            'operator' => '==',
            'value'    =>  true
        )
    )
) );