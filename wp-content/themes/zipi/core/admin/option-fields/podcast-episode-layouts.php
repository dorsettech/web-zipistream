<?php

Kirki::add_panel(
	'megaphone_panel_episode_layouts',
	array(
		'panel'    => 'megaphone_panel_podcast',
		'title'    => esc_attr__( 'Episode Listing Layouts', 'megaphone' ),
		'priority' => 10,
	)
);

/* Layout A */
Kirki::add_section(
	'megaphone_layout_a_episode',
	array(
		'panel'       => 'megaphone_panel_episode_layouts',
		'title'       => esc_attr__( 'Layout A', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_a.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_number',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_number' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_img_type',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Featured element', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_img_type' ),
		'choices'  => array(
			'image' => esc_html__( 'Image', 'megaphone' ),
			'box'   => esc_html__( 'Episode box', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_img_ratio',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_a_episode_img_custom',
		'section'     => 'megaphone_layout_a_episode',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_a_episode_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_a_episode_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_meta_up',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_meta_down',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_excerpt',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_excerpt_type',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Excerpt type', 'megaphone' ),
		'choices'  => array(
			'auto'   => esc_html__( 'Automatic excerpt (with characters limit)', 'megaphone' ),
			'manual' => esc_html__( 'Full content (manually split with read-more tag)', 'megaphone' ),
		),
		'default'  => megaphone_get_default_option( 'layout_a_episode_excerpt_type' ),
		'required' => array(
			array(
				'setting'  => 'layout_a_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_excerpt_limit',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_a_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
			array(
				'setting'  => 'layout_a_episode_excerpt_type',
				'operator' => '==',
				'value'    => 'auto',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_width',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'radio-buttonset',
		'label'    => esc_html__( 'Content (text) width', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_width' ),
		'choices'  => array(
			'6' => esc_html__( 'Narrow', 'megaphone' ),
			'7' => esc_html__( 'Medium', 'megaphone' ),
			'8' => esc_html__( 'Wide', 'megaphone' ),
		),
		'required' => array(
			array(
				'setting'  => 'layout_a_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_play_btn',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_play_btn' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_episode_play_icon',
		'section'  => 'megaphone_layout_a_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" icon', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_episode_play_icon' ),
	)
);



/* Layout B */
Kirki::add_section(
	'megaphone_layout_b_episode',
	array(
		'panel'       => 'megaphone_panel_episode_layouts',
		'title'       => esc_attr__( 'Layout B', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_b.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_number',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_number' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_img_type',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Featured element', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_img_type' ),
		'choices'  => array(
			'image' => esc_html__( 'Image', 'megaphone' ),
			'box'   => esc_html__( 'Episode box', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_img_ratio',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_b_episode_img_custom',
		'section'     => 'megaphone_layout_b_episode',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_b_episode_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_b_episode_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_meta_up',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_meta_down',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_excerpt',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_excerpt_type',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Excerpt type', 'megaphone' ),
		'choices'  => array(
			'auto'   => esc_html__( 'Automatic excerpt (with characters limit)', 'megaphone' ),
			'manual' => esc_html__( 'Full content (manually split with read-more tag)', 'megaphone' ),
		),
		'default'  => megaphone_get_default_option( 'layout_b_episode_excerpt_type' ),
		'required' => array(
			array(
				'setting'  => 'layout_b_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_excerpt_limit',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_b_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
			array(
				'setting'  => 'layout_b_episode_excerpt_type',
				'operator' => '==',
				'value'    => 'auto',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_width',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'radio-buttonset',
		'label'    => esc_html__( 'Content (text) width', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_width' ),
		'choices'  => array(
			'9'  => esc_html__( 'Narrow', 'megaphone' ),
			'10' => esc_html__( 'Medium', 'megaphone' ),
			'12' => esc_html__( 'Wide', 'megaphone' ),
		),
		'required' => array(
			array(
				'setting'  => 'layout_b_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_play_btn',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_play_btn' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_episode_play_icon',
		'section'  => 'megaphone_layout_b_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" icon', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_episode_play_icon' ),
	)
);


/* Layout C */
Kirki::add_section(
	'megaphone_layout_c_episode',
	array(
		'panel'       => 'megaphone_panel_episode_layouts',
		'title'       => esc_attr__( 'Layout C', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_c.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_episode_number',
		'section'  => 'megaphone_layout_c_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_episode_number' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_episode_img_type',
		'section'  => 'megaphone_layout_c_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Featured element', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_episode_img_type' ),
		'choices'  => array(
			'image' => esc_html__( 'Image', 'megaphone' ),
			'box'   => esc_html__( 'Episode box', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_episode_img_ratio',
		'section'  => 'megaphone_layout_c_episode',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_episode_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_c_episode_img_custom',
		'section'     => 'megaphone_layout_c_episode',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_c_episode_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_c_episode_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_episode_meta_up',
		'section'  => 'megaphone_layout_c_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_episode_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_episode_meta_down',
		'section'  => 'megaphone_layout_c_episode',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_episode_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_episode_excerpt',
		'section'  => 'megaphone_layout_c_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_episode_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_episode_excerpt_limit',
		'section'  => 'megaphone_layout_c_episode',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_episode_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_c_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_episode_play_btn',
		'section'  => 'megaphone_layout_c_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_episode_play_btn' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_episode_play_icon',
		'section'  => 'megaphone_layout_c_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" icon', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_episode_play_icon' ),
	)
);



/* Layout D */
Kirki::add_section(
	'megaphone_layout_d_episode',
	array(
		'panel'       => 'megaphone_panel_episode_layouts',
		'title'       => esc_attr__( 'Layout D', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_d.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_episode_number',
		'section'  => 'megaphone_layout_d_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_episode_number' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_episode_img_type',
		'section'  => 'megaphone_layout_d_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Featured element', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_episode_img_type' ),
		'choices'  => array(
			'image' => esc_html__( 'Image', 'megaphone' ),
			'box'   => esc_html__( 'Episode box', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_episode_img_ratio',
		'section'  => 'megaphone_layout_d_episode',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_episode_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_d_episode_img_custom',
		'section'     => 'megaphone_layout_d_episode',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_d_episode_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_d_episode_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_episode_meta_up',
		'section'  => 'megaphone_layout_d_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_episode_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_episode_meta_down',
		'section'  => 'megaphone_layout_d_episode',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_episode_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_episode_excerpt',
		'section'  => 'megaphone_layout_d_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_episode_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_episode_excerpt_limit',
		'section'  => 'megaphone_layout_d_episode',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_episode_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_d_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_episode_play_btn',
		'section'  => 'megaphone_layout_d_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_episode_play_btn' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_episode_play_icon',
		'section'  => 'megaphone_layout_d_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" icon', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_episode_play_icon' ),
	)
);




/* Layout E */
Kirki::add_section(
	'megaphone_layout_e_episode',
	array(
		'panel'       => 'megaphone_panel_episode_layouts',
		'title'       => esc_attr__( 'Layout E', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_e.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_episode_number',
		'section'  => 'megaphone_layout_e_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_episode_number' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_episode_img_type',
		'section'  => 'megaphone_layout_e_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Featured element', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_episode_img_type' ),
		'choices'  => array(
			'image' => esc_html__( 'Image', 'megaphone' ),
			'box'   => esc_html__( 'Episode box', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_episode_img_ratio',
		'section'  => 'megaphone_layout_e_episode',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_episode_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_e_episode_img_custom',
		'section'     => 'megaphone_layout_e_episode',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_e_episode_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_e_episode_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_episode_meta_up',
		'section'  => 'megaphone_layout_e_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_episode_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_episode_meta_down',
		'section'  => 'megaphone_layout_e_episode',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_episode_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_episode_excerpt',
		'section'  => 'megaphone_layout_e_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_episode_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_episode_excerpt_limit',
		'section'  => 'megaphone_layout_e_episode',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_episode_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_e_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_episode_play_btn',
		'section'  => 'megaphone_layout_e_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_episode_play_btn' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_episode_play_icon',
		'section'  => 'megaphone_layout_e_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" icon', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_episode_play_icon' ),
	)
);




/* Layout F */
Kirki::add_section(
	'megaphone_layout_f_episode',
	array(
		'panel'       => 'megaphone_panel_episode_layouts',
		'title'       => esc_attr__( 'Layout F', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_f.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_episode_number',
		'section'  => 'megaphone_layout_f_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_episode_number' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_episode_img_type',
		'section'  => 'megaphone_layout_f_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Featured element', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_episode_img_type' ),
		'choices'  => array(
			'image' => esc_html__( 'Image', 'megaphone' ),
			'box'   => esc_html__( 'Episode box', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_episode_img_ratio',
		'section'  => 'megaphone_layout_f_episode',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_episode_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_f_episode_img_custom',
		'section'     => 'megaphone_layout_f_episode',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_f_episode_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_f_episode_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_episode_meta_up',
		'section'  => 'megaphone_layout_f_episode',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_episode_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_episode_meta_down',
		'section'  => 'megaphone_layout_f_episode',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_episode_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_episode_play_btn',
		'section'  => 'megaphone_layout_f_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_episode_play_btn' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_episode_play_icon',
		'section'  => 'megaphone_layout_f_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" icon', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_episode_play_icon' ),
	)
);



/* Layout G */
Kirki::add_section(
	'megaphone_layout_g_episode',
	array(
		'panel'       => 'megaphone_panel_episode_layouts',
		'title'       => esc_attr__( 'Layout G', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_g.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_g_episode_number',
		'section'  => 'megaphone_layout_g_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_g_episode_number' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_g_episode_play_btn',
		'section'  => 'megaphone_layout_g_episode',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "play" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_g_episode_play_btn' ),
	)
);