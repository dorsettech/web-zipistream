<?php

Kirki::add_section(
	'megaphone_header_top',
	array(
		'title' => esc_attr__( 'Top Bar', 'megaphone' ),
		'panel' => 'megaphone_panel_header',
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_top',
		'section'  => 'megaphone_header_top',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable top bar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_top' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_top_l',
		'section'  => 'megaphone_header_top',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Top bar left slot', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_top_l' ),
		'choices'  => megaphone_get_header_top_elements(),
		'required' => array(
			array(
				'setting'  => 'header_top',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_top_c',
		'section'  => 'megaphone_header_top',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Top bar center slot', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_top_c' ),
		'choices'  => megaphone_get_header_top_elements(),
		'required' => array(
			array(
				'setting'  => 'header_top',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_top_r',
		'section'  => 'megaphone_header_top',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Top bar right slot', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_top_r' ),
		'choices'  => megaphone_get_header_top_elements(),
		'required' => array(
			array(
				'setting'  => 'header_top',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'  => 'header_top_height',
		'section'   => 'megaphone_header_top',
		'type'      => 'slider',
		'label'     => esc_html__( 'Header top height', 'megaphone' ),
		'default'   => megaphone_get_default_option( 'header_top_height' ),
		'choices'   => array(
			'min'  => '20',
			'max'  => '100',
			'step' => '1',
		),
		'transport' => 'postMessage',
		'required' => array(
			array(
				'setting'  => 'header_top',
				'operator' => '==',
				'value'    => true,
			),
		)
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_header_top_bg',
		'section'  => 'megaphone_header_top',
		'type'     => 'color',
		'label'    => esc_html__( 'Top bar background color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_header_top_bg' ),
		'required' => array(
			array(
				'setting'  => 'header_top',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_header_top_txt',
		'section'  => 'megaphone_header_top',
		'type'     => 'color',
		'label'    => esc_html__( 'Top bar text color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_header_top_txt' ),
		'required' => array(
			array(
				'setting'  => 'header_top',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_header_top_acc',
		'section'  => 'megaphone_header_top',
		'type'     => 'color',
		'label'    => esc_html__( 'Top bar accent color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_header_top_acc' ),
		'required' => array(
			array(
				'setting'  => 'header_top',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);