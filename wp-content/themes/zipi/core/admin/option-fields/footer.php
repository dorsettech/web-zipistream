<?php


Kirki::add_section(
	'megaphone_footer',
	array(
		'panel' => 'megaphone_panel',
        'title' => esc_attr__( 'Footer', 'megaphone' ),
        'priority'    => 40,
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'footer_subscribe',
		'section'  => 'megaphone_footer',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display subscribe area', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'footer_subscribe' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'footer_instagram',
		'section'  => 'megaphone_footer',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display instagram area', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'footer_instagram' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'footer_instagram_front',
		'section'  => 'megaphone_footer',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Use instagram area on front (home) page only', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'footer_instagram_front' ),
		'required' => array(
			array(
				'setting'  => 'footer_instagram',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'footer_instagram_username',
		'section'     => 'megaphone_footer',
		'type'        => 'text',
		'label'       => esc_html__( 'Instagram username or hashtag', 'megaphone' ),
		'description' => esc_html__( 'Example 1: @natgeo Example 2: #flowers', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'footer_instagram_username' ),
		'required'    => array(
			array(
				'setting'  => 'footer_instagram',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);



Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'footer_widgets',
		'section'  => 'megaphone_footer',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display footer widgets', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'footer_widgets' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'footer_widgets_layout',
		'section'  => 'megaphone_footer',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Footer widgets layout', 'megaphone' ),
		'description'     => wp_kses_post( sprintf( __( 'Note: Each column represents one Footer Sidebar in <a href="%s">Apperance -> Widgets</a> settings.', 'megaphone' ), admin_url( 'widgets.php' ) ) ),
		'default'  => megaphone_get_default_option( 'footer_widgets_layout' ),
		'choices'  => megaphone_get_footer_layouts(),
		'required' => array(
			array(
				'setting'  => 'footer_widgets',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'footer_copyright',
		'section'  => 'megaphone_footer',
		'type'     => 'textarea',
		'label'    => esc_html__( 'Footer copyright text', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'footer_copyright' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_footer_bg',
		'section'  => 'megaphone_footer',
		'type'     => 'color',
		'label'    => esc_html__( 'Background color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_footer_bg' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_footer_txt',
		'section'  => 'megaphone_footer',
		'type'     => 'color',
		'label'    => esc_html__( 'Text color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_footer_txt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_footer_acc',
		'section'  => 'megaphone_footer',
		'type'     => 'color',
		'label'    => esc_html__( 'Accent color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_footer_acc' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_footer_meta',
		'section'  => 'megaphone_footer',
		'type'     => 'color',
		'label'    => esc_html__( 'Meta color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_footer_meta' ),
	)
);
