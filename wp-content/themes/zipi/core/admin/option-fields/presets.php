<?php

Kirki::add_section( 'megaphone_presets', array(
    'panel'          => 'megaphone_panel',
    'title'          => esc_attr__( 'Presets', 'megaphone' ),
    'description'   => esc_html__( 'Use these settings to set options in bulk with our handpicked design presets. Of course, you can fine-tune everything in the theme options.', 'megaphone' ),
    'priority'    => 150
) );


$all_presets = megaphone_get_option_presets();

$preset_layouts_choices = array();
$preset_layouts_settings = array();

foreach( $all_presets['layouts'] as $preset_id => $preset ){
$preset_layouts_choices[$preset_id]['alt'] = $preset['alt'];
$preset_layouts_choices[$preset_id]['src'] = $preset['src'];
$preset_layouts_settings[$preset_id] = array( 'settings' => $preset['settings'] );
}

$preset_colors_choices = array();
$preset_colors_settings = array();

foreach( $all_presets['colors'] as $preset_id => $preset ){
$preset_colors_choices[$preset_id]['alt'] = $preset['alt'];
$preset_colors_choices[$preset_id]['src'] = $preset['src'];
$preset_colors_settings[$preset_id] = array( 'settings' => $preset['settings'] );
}

$preset_fonts_choices = array();
$preset_fonts_settings = array();

foreach( $all_presets['fonts'] as $preset_id => $preset ){
$preset_fonts_choices[$preset_id]['alt'] = $preset['alt'];
$preset_fonts_choices[$preset_id]['src'] = $preset['src'];
$preset_fonts_settings[$preset_id] = array( 'settings' => $preset['settings'] );
}


Kirki::add_field( 'megaphone', array(
    'settings'    => 'preset_layouts',
    'section'     => 'megaphone_presets',
    'type'        => 'radio-image',
    'label'       => esc_html__( 'Layouts', 'megaphone' ),
    'default'     => 0,
    'choices'     => $preset_layouts_choices,
    'preset'      => $preset_layouts_settings
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'preset_colors',
    'section'     => 'megaphone_presets',
    'type'        => 'radio-image',
    'label'       => esc_html__( 'Color', 'megaphone' ),
    'default'     => 0,
    'choices'     => $preset_colors_choices,
    'preset'      => $preset_colors_settings
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'preset_fonts',
    'section'     => 'megaphone_presets',
    'type'        => 'radio-image',
    'label'       => esc_html__( 'Font', 'megaphone' ),
    'default'     => 0,
    'choices'     => $preset_fonts_choices,
    'preset'      => $preset_fonts_settings
) );