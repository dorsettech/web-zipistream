<?php



Kirki::add_section(
	'megaphone_front_featured',
	array(
		'panel' => 'megaphone_panel_front',
		'title' => esc_attr__( 'Featured Area', 'megaphone' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_fa_display_title',
		'section'  => 'megaphone_front_featured',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_fa_display_title' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_featured_title',
		'section'  => 'megaphone_front_featured',
		'type'     => 'text',
		'label'    => esc_html__( 'Title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_featured_title' ),
		'required' => array(
			array(
				'setting'  => 'front_page_fa_display_title',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_fa_loop',
		'section'  => 'megaphone_front_featured',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_fa_loop' ),
		'choices'  => megaphone_get_featured_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_fa_ppp',
		'section'  => 'megaphone_front_featured',
		'type'     => 'number',
		'label'    => esc_html__( 'Number of episodes', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_fa_ppp' ),
		'required' => array(
			array(
				'setting'  => 'front_page_fa_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_featured_layouts( array( 'slider' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_fa_orderby',
		'section'  => 'megaphone_front_featured',
		'type'     => 'radio',
		'label'    => esc_html__( 'Order episodes by', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_fa_orderby' ),
		'choices'  => megaphone_get_post_order_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_fa_cat',
		'section'     => 'megaphone_front_featured',
		'type'        => 'select',
		'multiple'    => 10,
		'label'       => esc_html__( 'From category', 'megaphone' ),
		'description' => esc_html__( 'Select one or more categories to pull the episodes from, or leave empty for all categories', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_fa_cat' ),
		'choices'     => Kirki_Helper::get_terms(
			array(
				'taxonomy' => 'category',
				'exclude'  => megaphone_get_option( 'blog_terms' ),
			)
		),
		'required' => array(
			array(
				'setting'  => 'megaphone_podcast_setup',
				'operator' => '!=',
				'value'    => 'simple',
			),
		),
	)
);


if ( megaphone_is_simple_podcast_active() ) {
	Kirki::add_field(
		'megaphone',
		array(
			'settings'    => 'front_page_fa_cat',
			'section'     => 'megaphone_front_featured',
			'type'        => 'select',
			'multiple'    => 10,
			'label'       => esc_html__( 'From series', 'megaphone' ),
			'description' => esc_html__( 'Select one or more series to pull the episodes from, or leave empty for all series', 'megaphone' ),
			'default'     => megaphone_get_default_option( 'front_page_fa_cat' ),
			'choices'     => Kirki_Helper::get_terms(
				array(
					'taxonomy' => 'series'
				)
			),
			'required' => array(
				array(
					'setting'  => 'megaphone_podcast_setup',
					'operator' => '==',
					'value'    => 'simple',
				),
			),
		)
	);
}

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_fa_tag',
		'section'     => 'megaphone_front_featured',
		'type'        => 'select',
		'multiple'    => 10,
		'label'       => esc_html__( 'Tagged with', 'megaphone' ),
		'description' => esc_html__( 'Select one or more tags to pull the episodes from, or leave empty for all tags', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_fa_tag' ),
		'choices'     => Kirki_Helper::get_terms( 'post_tag' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_fa_unique',
		'section'     => 'megaphone_front_featured',
		'type'        => 'toggle',
		'label'       => esc_html__( 'Make featured posts unique', 'megaphone' ),
		'description' => esc_html__( 'If you check this option, featured episodes will be automatically excluded from the latest episodes area', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_fa_unique' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_fa_view_all_link',
		'section'  => 'megaphone_front_featured',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section link', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_fa_view_all_link' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_featured_view_all_link_label',
		'section'  => 'megaphone_front_featured',
		'type'     => 'text',
		'label'    => esc_html__( 'Section link label', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_featured_view_all_link_label' ),
		'required' => array(
			array(
				'setting'  => 'front_page_fa_view_all_link',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_fa_view_all_link_url',
		'section'  => 'megaphone_front_featured',
		'type'     => 'link',
		'label'    => esc_html__( 'Section link (URL)', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_fa_view_all_link_url' ),
		'required' => array(
			array(
				'setting'  => 'front_page_fa_view_all_link',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_fa_subscribe',
		'section'  => 'megaphone_front_featured',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display subscribe menu', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_fa_subscribe' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_fa_bg',
		'section'  => 'megaphone_front_featured',
		'type'     => 'radio',
		'label'    => esc_html__( 'Background/style options', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_fa_bg' ),
		'choices'  => megaphone_get_background_opts( true ),
		'required' => array(
			array(
				'setting'  => 'front_page_fa_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array( 3, 4, 5 ) ) ,
			),
		),
	)
);