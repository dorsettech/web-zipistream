<?php

Kirki::add_section(
	'megaphone_header_general',
	array(
		'panel' => 'megaphone_panel_header',
		'title' => esc_attr__( 'General', 'megaphone' ),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_layout',
		'section'  => 'megaphone_header_general',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_layout' ),
		'choices'  => megaphone_get_header_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'  => 'header_height',
		'section'   => 'megaphone_header_general',
		'type'      => 'slider',
		'label'     => esc_html__( 'Header height', 'megaphone' ),
		'default'   => megaphone_get_default_option( 'header_height' ),
		'choices'   => array(
			'min'  => '40',
			'max'  => '300',
			'step' => '1',
		),
		'transport' => 'postMessage',
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'  => 'header_bottom_height',
		'section'   => 'megaphone_header_general',
		'type'      => 'slider',
		'label'     => esc_html__( 'Header bottom height', 'megaphone' ),
		'default'   => megaphone_get_default_option( 'header_bottom_height' ),
		'choices'   => array(
			'min'  => '40',
			'max'  => '300',
			'step' => '1',
		),
		'transport' => 'postMessage',
		'required' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => 'in',
				'value'    => array( '4', '5', '6', '7', '8', '9' ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_orientation',
		'section'  => 'megaphone_header_general',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Elements orientation', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_orientation' ),
		'choices'  => array(
			'content' => array(
				'alt' => esc_html__( 'Site content', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/header_orientation_content.svg' ),
			),
			'window'  => array(
				'alt' => esc_html__( 'Browser (screen)', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/header_orientation_window.svg' ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_main_nav',
		'section'  => 'megaphone_header_general',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable main navigation', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_main_nav' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_site_desc',
		'section'  => 'megaphone_header_general',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable site desciprition', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_site_desc' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_actions',
		'section'  => 'megaphone_header_general',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Enable special elements', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_actions' ),
		'choices'  => megaphone_get_header_main_area_actions(),
		'required' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => 'in',
				'value'    => array( '1', '2', '3', '4', '6', '7', '9' ),
			),
		),

	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_actions_l',
		'section'  => 'megaphone_header_general',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Enable special elements on the left', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_actions_l' ),
		'choices'  => megaphone_get_header_main_area_actions(),
		'required' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => 'in',
				'value'    => array( '5', '8' ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_actions_r',
		'section'  => 'megaphone_header_general',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Enable special elements on the right', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_actions_r' ),
		'choices'  => megaphone_get_header_main_area_actions(),
		'required' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => 'in',
				'value'    => array( '5', '8' ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'logo',
		'section'     => 'megaphone_header_general',
		'type'        => 'image',
		'label'       => esc_html__( 'Logo', 'megaphone' ),
		'description' => esc_html__( 'This is your default logo image. If it is not uploaded, theme will display the website title instead.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'logo' ),
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'logo_retina',
		'section'     => 'megaphone_header_general',
		'type'        => 'image',
		'label'       => esc_html__( 'Retina logo (2x)', 'megaphone' ),
		'description' => esc_html__( 'Optionally upload another logo for devices with retina displays. It should be double the size of your standard logo.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'logo_retina' ),
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'logo_mini',
		'section'     => 'megaphone_header_general',
		'type'        => 'image',
		'label'       => esc_html__( 'Mobile logo', 'megaphone' ),
		'description' => esc_html__( 'Optionally upload another logo which will be used as mobile/tablet logo.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'logo_mini' ),
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'logo_mini_retina',
		'section'     => 'megaphone_header_general',
		'type'        => 'image',
		'label'       => esc_html__( 'Mobile retina logo (2x)', 'megaphone' ),
		'description' => esc_html__( 'Upload double sized mobile logo for devices with retina displays.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'logo_mini_retina' ),
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);


Kirki::add_field( 'megaphone', 
	array(
		'settings'    => 'header_indent',
		'section'     => 'megaphone_header_general',
		'type'        => 'radio-image',
		'label'       => esc_html__( 'Indent cover into header', 'megaphone' ),
		'description'       => esc_html__( 'If the current page has a cover area, it will be indented into header. Header will get transparent background and will use the set of alternative logos.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'header_indent' ),
		'choices'  => array(
			'1' => array( 'alt' => esc_html__( 'On', 'megaphone' ), 'src' => get_parent_theme_file_uri( '/assets/img/admin/header_indent_on.svg' )  ),
			'0'    => array( 'alt' => esc_html__( 'Off', 'megaphone' ), 'src' => get_parent_theme_file_uri( '/assets/img/admin/header_indent_off.svg' ) ),
		),
		array(
				'setting'  => 'header_layout',
				'operator' => 'in',
				'value'    => array( '1', '2', '3' ),
			)
	) );

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'logo_alt',
		'section'     => 'megaphone_header_general',
		'type'        => 'image',
		'label'       => esc_html__( 'Alternative Logo', 'megaphone' ),
		'description' => esc_html__( 'This is your alternative logo image. If it is not uploaded, theme will display the website title instead.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'logo_alt' ),
		'choices'     => array(
			'save_as' => 'array',
		),
		'required' => array(
			array(
				'setting'  => 'header_indent',
				'operator' => '==',
				'value'    => '1',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'logo_alt_retina',
		'section'     => 'megaphone_header_general',
		'type'        => 'image',
		'label'       => esc_html__( 'Alternative Retina logo (2x)', 'megaphone' ),
		'description' => esc_html__( 'Optionally upload another logo for devices with retina displays. It should be double the size of your standard logo.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'logo_alt_retina' ),
		'choices'     => array(
			'save_as' => 'array',
		),
		'required' => array(
			array(
				'setting'  => 'header_indent',
				'operator' => '==',
				'value'    => '1',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'logo_alt_mini',
		'section'     => 'megaphone_header_general',
		'type'        => 'image',
		'label'       => esc_html__( 'Alternative Mobile logo', 'megaphone' ),
		'description' => esc_html__( 'Optionally upload another logo which will be used as mobile/tablet logo.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'logo_alt_mini' ),
		'choices'     => array(
			'save_as' => 'array',
		),
		'required' => array(
			array(
				'setting'  => 'header_indent',
				'operator' => '==',
				'value'    => '1',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'logo_alt_mini_retina',
		'section'     => 'megaphone_header_general',
		'type'        => 'image',
		'label'       => esc_html__( 'Alternative Mobile retina logo (2x)', 'megaphone' ),
		'description' => esc_html__( 'Upload double sized mobile logo for devices with retina displays.', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'logo_alt_mini_retina' ),
		'choices'     => array(
			'save_as' => 'array',
		),
		'required' => array(
			array(
				'setting'  => 'header_indent',
				'operator' => '==',
				'value'    => '1',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_header_middle_bg',
		'section'  => 'megaphone_header_general',
		'type'     => 'color',
		'label'    => esc_html__( 'Header background color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_header_middle_bg' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_header_middle_txt',
		'section'  => 'megaphone_header_general',
		'type'     => 'color',
		'label'    => esc_html__( 'Header text color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_header_middle_txt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_header_middle_acc',
		'section'  => 'megaphone_header_general',
		'type'     => 'color',
		'label'    => esc_html__( 'Header accent color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_header_middle_acc' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_header_bottom_bg',
		'section'  => 'megaphone_header_general',
		'type'     => 'color',
		'label'    => esc_html__( 'Bottom bar background color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_header_bottom_bg' ),
		'required' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => 'in',
				'value'    => array( '4', '5', '6', '7', '8', '9', '10' ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_header_bottom_txt',
		'section'  => 'megaphone_header_general',
		'type'     => 'color',
		'label'    => esc_html__( 'Bottom bar text color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_header_bottom_txt' ),
		'required' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => 'in',
				'value'    => array( '5', '6', '7', '8', '9', '10' ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_header_bottom_acc',
		'section'  => 'megaphone_header_general',
		'type'     => 'color',
		'label'    => esc_html__( 'Bottom bar accent color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_header_bottom_acc' ),
		'required' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => 'in',
				'value'    => array( '5', '6', '7', '8', '9', '10' ),
			),
		),
	)
);