<?php

Kirki::add_panel(
	'megaphone_panel_post_layouts',
	array(
		'panel'    => 'megaphone_panel_blog',
		'title'    => esc_attr__( 'Post Listing Layouts', 'megaphone' ),
		'priority' => 10,
	)
);

/* Layout A */
Kirki::add_section(
	'megaphone_layout_a',
	array(
		'panel'       => 'megaphone_panel_post_layouts',
		'title'       => esc_attr__( 'Layout A', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_a.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_img_ratio',
		'section'  => 'megaphone_layout_a',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_a_img_custom',
		'section'     => 'megaphone_layout_a',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_a_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_a_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_meta_up',
		'section'  => 'megaphone_layout_a',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_meta_down',
		'section'  => 'megaphone_layout_a',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_excerpt',
		'section'  => 'megaphone_layout_a',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display post text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_excerpt_type',
		'section'  => 'megaphone_layout_a',
		'type'     => 'radio',
		'label'    => esc_html__( 'Excerpt type', 'megaphone' ),
		'choices'  => array(
			'auto'   => esc_html__( 'Automatic excerpt (with characters limit)', 'megaphone' ),
			'manual' => esc_html__( 'Full content (manually split with read-more tag)', 'megaphone' ),
		),
		'default'  => megaphone_get_default_option( 'layout_a_excerpt_type' ),
		'required' => array(
			array(
				'setting'  => 'layout_a_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_excerpt_limit',
		'section'  => 'megaphone_layout_a',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_a_excerpt',
				'operator' => '==',
				'value'    => true,
			),
			array(
				'setting'  => 'layout_a_excerpt_type',
				'operator' => '==',
				'value'    => 'auto',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_width',
		'section'  => 'megaphone_layout_a',
		'type'     => 'radio-buttonset',
		'label'    => esc_html__( 'Content (text) width', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_width' ),
		'choices'  => array(
			'6' => esc_html__( 'Narrow', 'megaphone' ),
			'7' => esc_html__( 'Medium', 'megaphone' ),
			'8' => esc_html__( 'Wide', 'megaphone' ),
		),
		'required' => array(
			array(
				'setting'  => 'layout_a_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_a_rm',
		'section'  => 'megaphone_layout_a',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "read more" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_a_rm' ),
	)
);



/* Layout B */
Kirki::add_section(
	'megaphone_layout_b',
	array(
		'panel'       => 'megaphone_panel_post_layouts',
		'title'       => esc_attr__( 'Layout B', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_b.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_img_ratio',
		'section'  => 'megaphone_layout_b',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_b_img_custom',
		'section'     => 'megaphone_layout_b',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_b_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_b_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_meta_up',
		'section'  => 'megaphone_layout_b',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_meta_down',
		'section'  => 'megaphone_layout_b',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_excerpt',
		'section'  => 'megaphone_layout_b',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display post text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_excerpt_type',
		'section'  => 'megaphone_layout_b',
		'type'     => 'radio',
		'label'    => esc_html__( 'Excerpt type', 'megaphone' ),
		'choices'  => array(
			'auto'   => esc_html__( 'Automatic excerpt (with characters limit)', 'megaphone' ),
			'manual' => esc_html__( 'Full content (manually split with read-more tag)', 'megaphone' ),
		),
		'default'  => megaphone_get_default_option( 'layout_b_excerpt_type' ),
		'required' => array(
			array(
				'setting'  => 'layout_b_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_excerpt_limit',
		'section'  => 'megaphone_layout_b',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_b_excerpt',
				'operator' => '==',
				'value'    => true,
			),
			array(
				'setting'  => 'layout_b_excerpt_type',
				'operator' => '==',
				'value'    => 'auto',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_width',
		'section'  => 'megaphone_layout_b',
		'type'     => 'radio-buttonset',
		'label'    => esc_html__( 'Content (text) width', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_width' ),
		'choices'  => array(
			'9'  => esc_html__( 'Narrow', 'megaphone' ),
			'10' => esc_html__( 'Medium', 'megaphone' ),
			'12' => esc_html__( 'Wide', 'megaphone' ),
		),
		'required' => array(
			array(
				'setting'  => 'layout_b_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_b_rm',
		'section'  => 'megaphone_layout_b',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "read more" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_b_rm' ),
	)
);



/* Layout C */
Kirki::add_section(
	'megaphone_layout_c',
	array(
		'panel'       => 'megaphone_panel_post_layouts',
		'title'       => esc_attr__( 'Layout C', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_c.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_img_ratio',
		'section'  => 'megaphone_layout_c',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_c_img_custom',
		'section'     => 'megaphone_layout_c',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_c_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_c_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_meta_up',
		'section'  => 'megaphone_layout_c',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_meta_down',
		'section'  => 'megaphone_layout_c',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_excerpt',
		'section'  => 'megaphone_layout_c',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display post text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_excerpt_limit',
		'section'  => 'megaphone_layout_c',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_c_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_c_rm',
		'section'  => 'megaphone_layout_c',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "read more" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_c_rm' ),
	)
);



/* Layout D */
Kirki::add_section(
	'megaphone_layout_d',
	array(
		'panel'       => 'megaphone_panel_post_layouts',
		'title'       => esc_attr__( 'Layout D', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_d.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_img_ratio',
		'section'  => 'megaphone_layout_d',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_d_img_custom',
		'section'     => 'megaphone_layout_d',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_d_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_d_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_meta_up',
		'section'  => 'megaphone_layout_d',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_meta_down',
		'section'  => 'megaphone_layout_d',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_excerpt',
		'section'  => 'megaphone_layout_d',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display post text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_excerpt_limit',
		'section'  => 'megaphone_layout_d',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_d_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_d_rm',
		'section'  => 'megaphone_layout_d',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "read more" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_d_rm' ),
	)
);



/* Layout E */
Kirki::add_section(
	'megaphone_layout_e',
	array(
		'panel'       => 'megaphone_panel_post_layouts',
		'title'       => esc_attr__( 'Layout E', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_e.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_img_ratio',
		'section'  => 'megaphone_layout_e',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_e_img_custom',
		'section'     => 'megaphone_layout_e',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_e_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_e_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_meta_up',
		'section'  => 'megaphone_layout_e',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_meta_down',
		'section'  => 'megaphone_layout_e',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_excerpt',
		'section'  => 'megaphone_layout_e',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display post text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_excerpt_limit',
		'section'  => 'megaphone_layout_e',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_e_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_e_rm',
		'section'  => 'megaphone_layout_e',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "read more" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_e_rm' ),
	)
);



/* Layout F */
Kirki::add_section(
	'megaphone_layout_f',
	array(
		'panel'       => 'megaphone_panel_post_layouts',
		'title'       => esc_attr__( 'Layout F', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_f.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_img_ratio',
		'section'  => 'megaphone_layout_f',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'layout_f_img_custom',
		'section'     => 'megaphone_layout_f',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'layout_f_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'layout_f_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_meta_up',
		'section'  => 'megaphone_layout_f',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_meta_down',
		'section'  => 'megaphone_layout_f',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_f_rm',
		'section'  => 'megaphone_layout_f',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display "read more" button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_f_rm' ),
	)
);