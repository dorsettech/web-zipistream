<?php


Kirki::add_section( 'megaphone_typography', array(
    'panel'          => 'megaphone_panel',
    'title'          => esc_attr__( 'Typography', 'megaphone' ),
    'priority'    => 100,
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'main_font',
    'section'     => 'megaphone_typography',
    'type'        => 'typography',
    'label'       => esc_html__( 'Main text font', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'main_font' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'h_font',
    'section'     => 'megaphone_typography',
    'type'        => 'typography',
    'label'       => esc_html__( 'Headings font', 'megaphone' ),
    'description'    => esc_html__( 'This is the font used for titles and headings', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'h_font' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'nav_font',
    'section'     => 'megaphone_typography',
    'type'        => 'typography',
    'label'       => esc_html__( 'Navigation font', 'megaphone' ),
    'description'    => esc_html__( 'This is the font used for main website navigation', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'nav_font' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'nav_font',
    'section'     => 'megaphone_typography',
    'type'        => 'typography',
    'label'       => esc_html__( 'Navigation font', 'megaphone' ),
    'description'    => esc_html__( 'This is the font used for main website navigation', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'nav_font' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'button_font',
    'section'     => 'megaphone_typography',
    'type'        => 'typography',
    'label'       => esc_html__( 'Button font', 'megaphone' ),
    'description'    => esc_html__( 'This is the font used for buttons and special labels', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'button_font' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_p',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'Regular text font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_p' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_small',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'Small text font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_small' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_nav',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'Main website navigation font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_nav' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_nav_ico',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'Navigation icons (hamburger, search...) font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_nav_ico' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_section_title',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'Section title font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_section_title' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_widget_title',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'Widget title font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_widget_title' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_punchline',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'Punchline font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_punchline' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_h1',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'H1 font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_h1' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_h2',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'H2 font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_h2' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_h3',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'H3 font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_h3' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_h4',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'H4 font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_h4' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_h5',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'H5 font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_h5' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'font_size_h6',
    'section'     => 'megaphone_typography',
    'type'        => 'number',
    'label'       => esc_html__( 'H6 font size', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'font_size_h6' ),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'uppercase',
    'section'     => 'megaphone_typography',
    'type'        => 'multicheck',
    'label'       => esc_html__( 'Uppercase text', 'megaphone' ),
    'description' => esc_html__( 'Select elements that you want to display with all CAPITAL LETTERS', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'uppercase' ),
    'choices'  => megaphone_get_typography_uppercase_options(),
) );