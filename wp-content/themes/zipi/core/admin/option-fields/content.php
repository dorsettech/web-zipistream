<?php

Kirki::add_section(
	'megaphone_content',
	array(
        'panel'    => 'megaphone_panel',
		'title'    => esc_attr__( 'Content Styling', 'megaphone' ),
		'priority' => 20,
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_bg',
		'section'  => 'megaphone_content',
		'type'     => 'color',
		'label'    => esc_html__( 'Background color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_bg' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_txt',
		'section'  => 'megaphone_content',
		'type'     => 'color',
		'label'    => esc_html__( 'Text color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_txt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_h',
		'section'  => 'megaphone_content',
		'type'     => 'color',
		'label'    => esc_html__( 'Heading/title color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_h' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_acc',
		'section'  => 'megaphone_content',
		'type'     => 'color',
		'label'    => esc_html__( 'Accent color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_acc' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_meta',
		'section'  => 'megaphone_content',
		'type'     => 'color',
		'label'    => esc_html__( 'Meta color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_meta' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_bg_alt_1',
		'section'  => 'megaphone_content',
		'type'     => 'color',
		'label'    => esc_html__( 'Alternative background color 1', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_bg_alt_1' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'color_bg_alt_2',
		'section'  => 'megaphone_content',
		'type'     => 'color',
		'label'    => esc_html__( 'Alternative background color 2', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'color_bg_alt_2' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'overlays',
		'section'     => 'megaphone_content',
		'type'        => 'radio-buttonset',
		'label'       => esc_html__( 'Image overlays', 'megaphone' ),
		'description' => esc_html__( 'Use this option to control overlay opacity for covers and specific layouts with text over images', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'overlays' ),
		'choices'     => array(
			'dark' => esc_html__( 'Dark', 'megaphone' ),
			'soft' => esc_html__( 'Soft', 'megaphone' ),
			'none' => esc_html__( 'None', 'megaphone' ),
		),
	)
);
