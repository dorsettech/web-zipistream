<?php


Kirki::add_section(
	'megaphone_front_blog',
	array(
		'panel' => 'megaphone_panel_front',
		'title' => esc_attr__( 'Blog Area', 'megaphone' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_posts_display_title',
		'section'  => 'megaphone_front_blog',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_posts_display_title' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_posts_title',
		'section'  => 'megaphone_front_blog',
		'type'     => 'text',
		'label'    => esc_html__( 'Title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_posts_title' ),
		'required' => array(
			array(
				'setting'  => 'front_page_posts_display_title',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_loop',
		'section'  => 'megaphone_front_blog',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_loop' ),
		'choices'  => megaphone_get_post_layouts( array(), array( 10, 11 ) ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_sidebar_position',
		'section'  => 'megaphone_front_blog',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Sidebar position', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_sidebar_position' ),
		'choices'  => megaphone_get_sidebar_layouts(),
		'required' => array(
			array(
				'setting'  => 'front_page_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_post_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_sidebar_standard',
		'section'  => 'megaphone_front_blog',
		'type'     => 'select',
		'label'    => esc_html__( 'Standard sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_sidebar_standard' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'front_page_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_post_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_sidebar_sticky',
		'section'  => 'megaphone_front_blog',
		'type'     => 'select',
		'label'    => esc_html__( 'Sticky sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_sidebar_sticky' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'front_page_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_post_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_ppp',
		'section'  => 'megaphone_front_blog',
		'type'     => 'radio',
		'label'    => esc_html__( 'Number of posts per page', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_ppp' ),
		'choices'  => array(
			'inherit' => esc_html__( 'Inherit from global option set in Settings / Reading', 'megaphone' ),
			'custom'  => esc_html__( 'Custom number', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_ppp_num',
		'section'  => 'megaphone_front_blog',
		'type'     => 'number',
		'label'    => esc_html__( 'Specify number of posts', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_ppp_num' ),
		'required' => array(
			array(
				'setting'  => 'front_page_ppp',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_tag',
		'section'     => 'megaphone_front_blog',
		'type'        => 'select',
		'multiple'    => 10,
		'label'       => esc_html__( 'Tagged with', 'megaphone' ),
		'description' => esc_html__( 'Select one or more tags to pull the posts from, or leave empty for all tags', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_tag' ),
		'choices'     => Kirki_Helper::get_terms( 'post_tag' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_posts_slider',
		'section'  => 'megaphone_front_blog',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable section slider', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_posts_slider' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_posts_view_all_link',
		'section'  => 'megaphone_front_blog',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section link', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_posts_view_all_link' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_posts_view_all_link_label',
		'section'  => 'megaphone_front_blog',
		'type'     => 'text',
		'label'    => esc_html__( 'Section link label', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_posts_view_all_link_label' ),
		'required' => array(
			array(
				'setting'  => 'front_page_posts_view_all_link',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_posts_view_all_link_url',
		'section'  => 'megaphone_front_blog',
		'type'     => 'link',
		'label'    => esc_html__( 'Section link (URL)', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_posts_view_all_link_url' ),
		'required' => array(
			array(
				'setting'  => 'front_page_posts_view_all_link',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_posts_bg',
		'section'  => 'megaphone_front_blog',
		'type'     => 'radio',
		'label'    => esc_html__( 'Background/style options', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_posts_bg' ),
		'choices'  => megaphone_get_background_opts( true ),
	)
);