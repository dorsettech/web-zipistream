<?php

Kirki::add_section(
	'megaphone_front_shows',
	array(
		'panel' => 'megaphone_panel_front',
		'title' => esc_attr__( 'Shows Area', 'megaphone' ),
		'description'   => esc_html__( 'This section lists your Shows (podcast categories).', 'megaphone' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_shows_display_title',
		'section'  => 'megaphone_front_shows',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_shows_display_title' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_shows_title',
		'section'  => 'megaphone_front_shows',
		'type'     => 'text',
		'label'    => esc_html__( 'Title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_shows_title' ),
		'required' => array(
			array(
				'setting'  => 'front_page_shows_display_title',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

$taxonomy = megaphone_is_simple_podcast_active() && megaphone_get_option('megaphone_podcast_setup') == 'simple' ? 'series' : 'category';

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_shows_categories',
		'section'     => 'megaphone_front_shows',
		'type'        => 'sortable',
		'multiple'    => 10,
		'label'       => esc_html__( 'Shows to display', 'megaphone' ),
		'description' => esc_html__( 'Enable and reorder shows (eye and cross icon) or leave all disabled to display all shows and ordered by title', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_shows_categories' ),
		'choices'     => Kirki_Helper::get_terms( 
			array(
			'taxonomy' => $taxonomy,
			'hide_empty' => true,
			'exclude' => megaphone_get_option( 'blog_terms' )
			) 
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_shows_slider',
		'section'  => 'megaphone_front_shows',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable section slider', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_shows_slider' ),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_shows_view_all_link',
		'section'  => 'megaphone_front_shows',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section link', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_shows_view_all_link' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_shows_view_all_link_label',
		'section'  => 'megaphone_front_shows',
		'type'     => 'text',
		'label'    => esc_html__( 'Section link label', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_shows_view_all_link_label' ),
		'required' => array(
			array(
				'setting'  => 'front_page_shows_view_all_link',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_shows_view_all_link_url',
		'section'  => 'megaphone_front_shows',
		'type'     => 'link',
		'label'    => esc_html__( 'Section link (URL)', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_shows_view_all_link_url' ),
		'required' => array(
			array(
				'setting'  => 'front_page_shows_view_all_link',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_shows_bg',
		'section'  => 'megaphone_front_shows',
		'type'     => 'radio',
		'label'    => esc_html__( 'Background/style options', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_shows_bg' ),
		'choices'  => megaphone_get_background_opts(),
	)
);