<?php


Kirki::add_section(
	'megaphone_archives_show',
	array(
		'panel' => 'megaphone_panel_podcast',
		'title' => esc_attr__( 'Single Show (podcast category)', 'megaphone' ),
		'priority'    => 50,
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout',
		'section'  => 'megaphone_archives_show',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout' ),
		'choices'  => megaphone_get_archive_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'show_layout_height',
		'section'     => 'megaphone_archives_show',
		'type'        => 'number',
		'label'       => esc_html__( 'Cover area (image) height', 'megaphone' ),
		'description' => esc_html__( 'This option will be applied if show has image', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'show_layout_height' ),
		'choices'     => array(
			'step' => '1',
		),

	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_description',
		'section'  => 'megaphone_archives_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display show description', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_description' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_meta',
		'section'  => 'megaphone_archives_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display number of episodes label', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_meta' ),

	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_loop',
		'section'  => 'megaphone_archives_show',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Episodes layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_loop' ),
		'choices'  => megaphone_get_episode_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_sidebar_position',
		'section'  => 'megaphone_archives_show',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Sidebar position', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_sidebar_position' ),
		'choices'  => megaphone_get_sidebar_layouts(),
		'required' => array(
			array(
				'setting'  => 'show_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_episode_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_sidebar_standard',
		'section'  => 'megaphone_archives_show',
		'type'     => 'select',
		'label'    => esc_html__( 'Standard sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_sidebar_standard' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'show_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_episode_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_sidebar_sticky',
		'section'  => 'megaphone_archives_show',
		'type'     => 'select',
		'label'    => esc_html__( 'Sticky sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_sidebar_sticky' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'show_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_episode_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_ppp',
		'section'  => 'megaphone_archives_show',
		'type'     => 'radio',
		'label'    => esc_html__( 'Number of episodes per page', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_ppp' ),
		'choices'  => array(
			'inherit' => esc_html__( 'Inherit from global option set in Settings / Reading', 'megaphone' ),
			'custom'  => esc_html__( 'Custom number', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_ppp_num',
		'section'  => 'megaphone_archives_show',
		'type'     => 'number',
		'label'    => esc_html__( 'Specify number of posts', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_ppp_num' ),
		'required' => array(
			array(
				'setting'  => 'show_ppp',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_pagination',
		'section'  => 'megaphone_archives_show',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Pagination', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_pagination' ),
		'choices'  => megaphone_get_pagination_layouts(),
	)
);