<?php

Kirki::add_section(
	'megaphone_header_misc',
	array(
		'title' => esc_attr__( 'Misc.', 'megaphone' ),
		'panel' => 'megaphone_panel_header',
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_labels',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display labels for special elements', 'megaphone' ),
		'section'  => 'megaphone_header_misc',
		'default'  => megaphone_get_default_option( 'header_labels' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'mega_menu',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable mega menu functionality', 'megaphone' ),
		'section'  => 'megaphone_header_misc',
		'default'  => megaphone_get_default_option( 'mega_menu' ),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'mega_menu_ppp',
		'type'     => 'number',
		'label'    => esc_html__( 'Mega menu posts (in category menu item) limit', 'megaphone' ),
		'section'  => 'megaphone_header_misc',
		'default'  => megaphone_get_default_option( 'mega_menu_ppp' ),
		'choices'  => array(
			'min'  => '3',
			'max'  => '10',
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'mega_menu',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);