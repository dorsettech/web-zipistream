<?php

Kirki::add_panel( 'megaphone_panel_all_layouts', array(
    'panel'          => 'megaphone_panel',
    'title'          => esc_attr__( 'Layouts', 'megaphone' ),
    'priority'    => 40,
) );


/* Blog */
require_once get_parent_theme_file_path( '/core/admin/option-fields/post-layouts.php' );

/* Podcast */
require_once get_parent_theme_file_path( '/core/admin/option-fields/episode-layouts.php' );

/* Show */
require_once get_parent_theme_file_path( '/core/admin/option-fields/show-layouts.php' );

/* Featured */
require_once get_parent_theme_file_path( '/core/admin/option-fields/featured-layouts.php' );