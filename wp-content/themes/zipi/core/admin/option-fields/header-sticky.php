<?php

Kirki::add_section(
	'megaphone_header_sticky',
	array(
		'title' => esc_attr__( 'Sticky Header', 'megaphone' ),
		'panel' => 'megaphone_panel_header',
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_sticky',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable sticky header', 'megaphone' ),
		'section'  => 'megaphone_header_sticky',
		'default'  => megaphone_get_default_option( 'header_sticky' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_sticky_layout',
		'section'  => 'megaphone_header_sticky',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'header_sticky_layout' ),
		'choices'  => megaphone_get_header_layouts( array( '4', '5', '6', '7', '8', '9' ) ),
		'required' => array(
			array(
				'setting'  => 'header_sticky',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'header_sticky_offset',
		'type'        => 'number',
		'label'       => esc_html__( 'Sticky header offset', 'megaphone' ),
		'description' => esc_html__( 'Specify after how many px of scrolling the sticky header appears', 'megaphone' ),
		'section'     => 'megaphone_header_sticky',
		'default'     => megaphone_get_default_option( 'header_sticky_offset' ),
		'choices'     => array(
			'min'  => '50',
			'max'  => '1000',
			'step' => '50',
		),
		'required'    => array(
			array(
				'setting'  => 'header_sticky',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'header_sticky_up',
		'type'        => 'toggle',
		'label'       => esc_html__( 'Smart sticky', 'megaphone' ),
		'description' => esc_html__( 'Sticky header appears only if you scroll up', 'megaphone' ),
		'section'     => 'megaphone_header_sticky',
		'default'     => megaphone_get_default_option( 'header_sticky_up' ),
		'required'    => array(
			array(
				'setting'  => 'header_sticky',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'header_sticky_logo',
		'type'     => 'radio',
		'label'    => esc_html__( 'Sticky header logo', 'megaphone' ),
		'section'  => 'megaphone_header_sticky',
		'default'  => megaphone_get_default_option( 'header_sticky_logo' ),
		'choices'  => array(
			'regular' => esc_html__( 'Regular logo', 'megaphone' ),
			'mini'    => esc_html__( 'Mini (mobile) logo', 'megaphone' ),
		),
		'required' => array(
			array(
				'setting'  => 'header_sticky',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'  => 'header_sticky_height',
		'type'      => 'slider',
		'label'     => esc_html__( 'Sticky header height', 'megaphone' ),
		'section'   => 'megaphone_header_sticky',
		'default'   => megaphone_get_default_option( 'header_sticky_height' ),
		'choices'   => array(
			'min'  => '40',
			'max'  => '200',
			'step' => '1',
		),
		'transport' => 'postMessage',
		'required'  => array(
			array(
				'setting'  => 'header_sticky',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);
