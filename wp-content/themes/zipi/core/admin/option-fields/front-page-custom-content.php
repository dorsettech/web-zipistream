<?php

Kirki::add_section(
	'megaphone_front_custom_content',
	array(
		'panel' => 'megaphone_panel_front',
		'title' => esc_attr__( 'Custom Content', 'megaphone' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_custom_content_type',
		'section'  => 'megaphone_front_custom_content',
		'type'     => 'radio',
		'label'    => esc_html__( 'Pull custom content from', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_custom_content_type' ),
		'choices'  => array(
			'this' => esc_html__( 'Here (theme options)', 'megaphone' ),
			'page' => esc_html__( 'Page (select any page)', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'        => 'front_page_custom_content_page',
		'section'         => 'megaphone_front_custom_content',
		'type'            => 'select',
		'label'           => esc_html__( 'Select page', 'megaphone' ),
		'default'         => megaphone_get_default_option( 'front_page_custom_content_page' ),
		'choices'         => Kirki_Helper::get_posts(
			array(
				'posts_per_page' => -1,
				'post_type'      => 'page',
			)
		),
		'active_callback' => array(
			array(
				'setting'  => 'front_page_custom_content_type',
				'operator' => '==',
				'value'    => 'page',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_custom_content_display_title',
		'section'  => 'megaphone_front_custom_content',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section/page title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_custom_content_display_title' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'        => 'tr_front_page_custom_content_title',
		'section'         => 'megaphone_front_custom_content',
		'type'            => 'text',
		'label'           => esc_html__( 'Title', 'megaphone' ),
		'default'         => megaphone_get_default_option( 'tr_front_page_custom_content_title' ),
		'active_callback' => array(
			array(
				'setting'  => 'front_page_custom_content_type',
				'operator' => '==',
				'value'    => 'this',
			),
			array(
				'setting'  => 'front_page_custom_content_display_title',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'          => 'tr_front_page_custom_content',
		'section'           => 'megaphone_front_custom_content',
		'type'              => 'editor',
		'label'             => esc_html__( 'Content', 'megaphone' ),
		'description'       => esc_html__( 'Add your custom content, shortcodes are supported.', 'megaphone' ),
		'default'           => megaphone_get_default_option( 'tr_front_page_custom_content' ),
		'sanitize_callback' => 'megaphone_sanitize_ad',
		'active_callback'   => array(
			array(
				'setting'  => 'front_page_custom_content_type',
				'operator' => '==',
				'value'    => 'this',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_custom_content_bg',
		'section'  => 'megaphone_front_custom_content',
		'type'     => 'radio',
		'label'    => esc_html__( 'Background/style options', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_custom_content_bg' ),
		'choices'  => megaphone_get_background_opts( true ),
	)
);
