<?php

Kirki::add_section(
	'megaphone_panel_podcast_show',
	array(
		'panel'       => 'megaphone_panel_podcast',
		'title'       => esc_attr__( 'Shows (podcast categories) Layout', 'megaphone' ),
		'description'   => esc_html__( 'These options will be applied to pages which list Shows. For example, the Shows section on front page or Shows page template which lists all Shows.', 'megaphone' ),
		'priority'    => 30,
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_loop',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_loop' ),
		'choices'  => megaphone_get_show_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_sidebar_position',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Sidebar position', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_sidebar_position' ),
		'choices'  => megaphone_get_sidebar_layouts(),
		'required' => array(
			array(
				'setting'  => 'show_layout_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_show_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_sidebar_standard',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'select',
		'label'    => esc_html__( 'Standard sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_sidebar_standard' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'show_layout_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_show_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_sidebar_sticky',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'select',
		'label'    => esc_html__( 'Sticky sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_sidebar_sticky' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'show_layout_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_show_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_display_image',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display image', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_display_image' ),
		'required' => array(
			array(
				'setting'  => 'show_layout_loop',
				'operator' => 'in',
				'value'    => array( '4', '5','6' ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_img_ratio',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'select',
		'label'    => esc_html__( 'Image ratio', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_img_ratio' ),
		'choices'  => megaphone_get_image_ratio_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'show_layout_img_custom',
		'section'     => 'megaphone_panel_podcast_show',
		'type'        => 'text',
		'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
		'description' => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'show_layout_img_custom' ),
		'required'    => array(
			array(
				'setting'  => 'show_layout_img_ratio',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);




Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_duotone_overlay',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Use duotone effect over the image', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_duotone_overlay' ),
		'required' => array(
			array(
				'setting'  => 'show_layout_display_image',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);



Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_meta',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display meta (number of episodes in the show)', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_meta' ),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_description',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display description', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_description' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_description_excerpt',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable description excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_description_excerpt' ),
		'required' => array(
			array(
				'setting'  => 'show_layout_description',
				'operator' => '==',
				'value'    => true,
			),
		)
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_description_excerpt_limit',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'number',
		'label'    => esc_html__( 'Description excerpt limit (number of characters)', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_description_excerpt_limit' ),
		'required' => array(
			array(
				'setting'  => 'show_layout_description',
				'operator' => '==',
				'value'    => true,
			),
			array(
				'setting'  => 'show_layout_description_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		)
	)
);



Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_display_episodes',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episodes', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_display_episodes' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_display_episodes_limit',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'number',
		'label'    => esc_html__( 'Choose number of episodes to display', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_display_episodes_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'show_layout_display_episodes',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_display_episodes_number',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number label', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_display_episodes_number' ),
		'required' => array(
			array(
				'setting'  => 'show_layout_display_episodes',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_display_play_btn',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display play episode button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_display_play_btn' ),
		'required' => array(
			array(
				'setting'  => 'show_layout_display_episodes',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'show_layout_display_view_episodes_btn',
		'section'  => 'megaphone_panel_podcast_show',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display view all episodes button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'show_layout_display_view_episodes_btn' ),
	)
);


$taxonomy = megaphone_is_simple_podcast_active() && megaphone_get_option('megaphone_podcast_setup') == 'simple' ? 'series' : 'category';

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'show_layout_categories',
		'section'     => 'megaphone_panel_podcast_show',
		'type'        => 'sortable',
		'multiple'    => 10,
		'label'       => esc_html__( 'Shows to display', 'megaphone' ),
		'description' => esc_html__( 'Enable and reorder shows (eye and cross icon) or leave all disabled to display all shows and ordered by title', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'show_layout_categories' ),
		'choices'     => Kirki_Helper::get_terms( 
			array(
			'taxonomy' => $taxonomy,
			'hide_empty' => true,
			'exclude' => megaphone_get_option( 'blog_terms' )
			) 
		),
	)
);
