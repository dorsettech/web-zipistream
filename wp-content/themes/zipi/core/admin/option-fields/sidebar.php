<?php

// Kirki::add_panel( 'megaphone_sidebar_panel', array(
// 'panel'          => 'megaphone_panel',
// 'title'          => esc_attr__( 'Sidebar & Widgets', 'megaphone' ),
// 'priority'    => 21,
// ) );

Kirki::add_section(
	'megaphone_sidebar_section',
	array(
		'panel' => 'megaphone_panel',
        'title' => esc_attr__( 'Sidebar & Widgets', 'megaphone' ),
        'priority'    => 30,
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'     => 'sidebars',
		'section'      => 'megaphone_sidebar_section',
		'type'         => 'repeater',
		'label'        => esc_html__( 'Sidebars', 'megaphone' ),
		'description'  => wp_kses_post( sprintf( __( 'Use this option to create additional sidebars for your website. Afterwards, you can manage sidebars content in the <a href="%s">Apperance -> Widgets</a> settings.', 'megaphone' ), admin_url( 'widgets.php' ) ) ),
		'row_label'    => array(
			'type'  => 'text',
			'value' => esc_html__( 'Custom sidebar', 'megaphone' ),
		),

		'button_label' => esc_html__( 'Add new', 'megaphone' ),

		'default'      => megaphone_get_default_option( 'sidebars' ),
		'fields'       => array(
			'name' => array(
				'type'    => 'text',
				'label'   => esc_html__( 'Sidebar name', 'megaphone' ),
				'default' => '',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'widget_bg',
		'section'  => 'megaphone_sidebar_section',
		'type'     => 'radio',
		'label'    => esc_html__( 'Widget default background color', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'widget_bg' ),
		'choices'  => megaphone_get_background_opts( false ),
	)
);
