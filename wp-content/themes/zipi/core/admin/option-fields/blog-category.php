<?php

Kirki::add_section(
	'megaphone_archives_category',
	array(
		'panel' => 'megaphone_panel_blog',
		'title' => esc_attr__( 'Category Template', 'megaphone' ),
		'priority' => 30,
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_settings',
		'section'  => 'megaphone_archives_category',
		'type'     => 'radio',
		'label'    => esc_html__( 'Category settings', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_settings' ),
		'choices'  => array(
			'inherit' => esc_html__( 'Inherit from general Archive settings', 'megaphone' ),
			'custom'  => esc_html__( 'Customize', 'megaphone' ),
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_layout',
		'section'  => 'megaphone_archives_category',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_layout' ),
		'choices'  => megaphone_get_archive_layouts(),
		'required' => array(
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'category_layout_height',
		'section'     => 'megaphone_archives_category',
		'type'        => 'number',
		'label'       => esc_html__( 'Cover area (image) height', 'megaphone' ),
		'description' => esc_html__( 'This option will be applied if category has image', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'category_layout_height' ),
		'choices'     => array(
			'step' => '1',
		),
		'required'    => array(
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_description',
		'section'  => 'megaphone_archives_category',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display category description', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_description' ),
		'required' => array(
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_meta',
		'section'  => 'megaphone_archives_category',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display number of posts label', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_meta' ),
		'required' => array(
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_loop',
		'section'  => 'megaphone_archives_category',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Posts layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_loop' ),
		'choices'  => megaphone_get_post_layouts(),
		'required' => array(
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_sidebar_position',
		'section'  => 'megaphone_archives_category',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Sidebar position', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_sidebar_position' ),
		'choices'  => megaphone_get_sidebar_layouts(),
		'required' => array(
			array(
				'setting'  => 'category_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_post_layouts( array( 'sidebar' => true ) ) ) ),
			),
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_sidebar_standard',
		'section'  => 'megaphone_archives_category',
		'type'     => 'select',
		'label'    => esc_html__( 'Standard sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_sidebar_standard' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'category_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_post_layouts( array( 'sidebar' => true ) ) ) ),
			),
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_sidebar_sticky',
		'section'  => 'megaphone_archives_category',
		'type'     => 'select',
		'label'    => esc_html__( 'Sticky sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_sidebar_sticky' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'category_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_post_layouts( array( 'sidebar' => true ) ) ) ),
			),
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_ppp',
		'section'  => 'megaphone_archives_category',
		'type'     => 'radio',
		'label'    => esc_html__( 'Number of posts per page', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_ppp' ),
		'choices'  => array(
			'inherit' => esc_html__( 'Inherit from global option set in Settings / Reading', 'megaphone' ),
			'custom'  => esc_html__( 'Custom number', 'megaphone' ),
		),
		'required' => array(
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_ppp_num',
		'section'  => 'megaphone_archives_category',
		'type'     => 'number',
		'label'    => esc_html__( 'Specify number of posts', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_ppp_num' ),
		'required' => array(
			array(
				'setting'  => 'category_ppp',
				'operator' => '==',
				'value'    => 'custom',
			),
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'category_pagination',
		'section'  => 'megaphone_archives_category',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Pagination', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'category_pagination' ),
		'choices'  => megaphone_get_pagination_layouts(),
		'required' => array(
			array(
				'setting'  => 'category_settings',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);