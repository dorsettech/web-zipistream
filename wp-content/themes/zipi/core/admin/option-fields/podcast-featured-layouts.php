<?php

Kirki::add_panel(
	'megaphone_panel_featured_layouts',
	array(
		'panel'    => 'megaphone_panel_podcast',
		'title'    => esc_attr__( 'Featured Episode Layouts', 'megaphone' ),
		'priority' => 20,
	)
);


/* Layout Featured A */

Kirki::add_section(
	'megaphone_layout_fa_1',
	array(
		'panel'       => 'megaphone_panel_featured_layouts',
		'title'       => esc_attr__( 'Featured Layout A', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_fa_a.svg' ) . '"/>' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_1_episode_height',
		'section'  => 'megaphone_layout_fa_1',
		'type'     => 'number',
		'label'    => esc_html__( 'Image height for layout 1', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_1_episode_height' ),
		'choices'  => array(
			'step' => '1',
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_1_episode_number',
		'section'  => 'megaphone_layout_fa_1',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_1_episode_number' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_1_episode_meta_up',
		'section'  => 'megaphone_layout_fa_1',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_1_episode_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_1_episode_meta_down',
		'section'  => 'megaphone_layout_fa_1',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_1_episode_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_1_episode_excerpt',
		'section'  => 'megaphone_layout_fa_1',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_1_episode_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_1_episode_excerpt_limit',
		'section'  => 'megaphone_layout_fa_1',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_1_episode_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_fa_1_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_1_episode_play_btn',
		'section'  => 'megaphone_layout_fa_1',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display play button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_1_episode_play_btn' ),
	)
);



/* Layout Featured B */

Kirki::add_section(
	'megaphone_layout_fa_2',
	array(
		'panel'       => 'megaphone_panel_featured_layouts',
		'title'       => esc_attr__( 'Featured Layout B', 'megaphone' ),
		'description' => wp_kses_post( '<img src="' . get_parent_theme_file_uri( '/assets/img/admin/layout_fa_b.svg' ) . '"/>' ),
	)
);

Kirki::add_field( 'megaphone', array(
    'settings'    => 'layout_fa_2_episode_img_ratio',
    'section'     => 'megaphone_layout_fa_2',
    'type'        => 'select',
    'label'       => esc_html__( 'Image ratio', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'layout_fa_2_episode_img_ratio' ),
    'choices'   => megaphone_get_image_ratio_opts(),
) );

Kirki::add_field( 'megaphone', array(
    'settings'    => 'layout_fa_2_episode_img_custom',
    'section'     => 'megaphone_layout_fa_2',
    'type'        => 'text',
    'label'       => esc_html__( 'Your custom ratio', 'megaphone' ),
    'description'      => esc_html__( 'i.e. Put 2:1', 'megaphone' ),
    'default'     => megaphone_get_default_option( 'layout_fa_2_episode_img_custom' ),
    'required'    => array(
        array(
            'setting'  => 'layout_fa_2_episode_img_ratio',
            'operator' => '==',
            'value'    => 'custom'
        ),
    ),
) );

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_2_episode_number',
		'section'  => 'megaphone_layout_fa_2',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display episode number', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_2_episode_number' ),
	)
);


Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_2_episode_meta_up',
		'section'  => 'megaphone_layout_fa_2',
		'type'     => 'radio',
		'label'    => esc_html__( 'Choose top meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_2_episode_meta_up' ),
		'choices'  => megaphone_get_meta_opts( true ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_2_episode_meta_down',
		'section'  => 'megaphone_layout_fa_2',
		'type'     => 'sortable',
		'label'    => esc_html__( 'Choose bottom meta', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_2_episode_meta_down' ),
		'choices'  => megaphone_get_meta_opts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_2_episode_excerpt',
		'section'  => 'megaphone_layout_fa_2',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display text excerpt', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_2_episode_excerpt' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_2_episode_excerpt_limit',
		'section'  => 'megaphone_layout_fa_2',
		'type'     => 'number',
		'label'    => esc_html__( 'Excerpt characters limit', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_2_episode_excerpt_limit' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'layout_fa_2_episode_excerpt',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'layout_fa_2_episode_play_btn',
		'section'  => 'megaphone_layout_fa_2',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display play button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'layout_fa_2_episode_play_btn' ),
	)
);