<?php


Kirki::add_section(
    'megaphone_podcast_player',
    array(
        'panel'    => 'megaphone_panel_podcast',
        'title'    => esc_attr__( 'Player', 'megaphone' ),
        'priority' => 60,
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings'  => 'player_height',
        'section'   => 'megaphone_podcast_player',
        'type'      => 'slider',
        'label'     => esc_html__( 'Player height', 'megaphone' ),
        'default'   => megaphone_get_default_option( 'player_height' ),
        'choices'   => array(
            'min'  => '80',
            'max'  => '200',
            'step' => '1',
        ),
        'transport' => 'postMessage',
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings' => 'player_supports',
        'section'  => 'megaphone_podcast_player',
        'type'     => 'multicheck',
        'label'    => esc_html__( 'Player supported sources', 'megaphone' ),
        'description' => esc_html__( 'Besides the default WordPress audio block, select additional sources that you want to detect inside the content and automatically append into the player.', 'megaphone' ),
        'default'  => megaphone_get_default_option( 'player_supports' ),
        'choices'  => megaphone_get_player_support_opts()
    )
);



Kirki::add_field(
    'megaphone',
    array(
        'settings' => 'player_special_elements',
        'section'  => 'megaphone_podcast_player',
        'type'     => 'sortable',
        'label'    => esc_html__( 'Player special elements', 'megaphone' ),
        'description' => esc_html__( 'Select additional elements to display inside the audio player', 'megaphone' ),
        'default'  => megaphone_get_default_option( 'player_special_elements' ),
        'choices'  => array(
            'subscribe' => esc_html__( 'Subscribe', 'megaphone' ),
            'share' => esc_html__( 'Share', 'megaphone' ),
            'download' => esc_html__( 'Download', 'megaphone' ),
        ),
    )
);

Kirki::add_field(
    'megaphone',
    array(
        'settings' => 'player_prev_next',
        'section'  => 'megaphone_podcast_player',
        'type'     => 'radio',
        'label'    => esc_html__( 'Player "next" button plays', 'megaphone' ),
        'default'  => megaphone_get_default_option( 'player_prev_next' ),
        'choices'  => array(
            'asc' => esc_html__( 'Newer episode', 'megaphone' ),
            'desc'  => esc_html__( 'Older episode', 'megaphone' ),
        ),
    )
);