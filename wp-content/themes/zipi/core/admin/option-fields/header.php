<?php

Kirki::add_panel(
	'megaphone_panel_header',
	array(
		'priority' => 40,
		'panel'    => 'megaphone_panel',
		'title'    => esc_attr__( 'Header', 'megaphone' ),
	)
);



require_once get_parent_theme_file_path( '/core/admin/option-fields/header-general.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/header-top.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/header-sticky.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/header-misc.php' );
require_once get_parent_theme_file_path( '/core/admin/option-fields/header-responsive.php' );