<?php

Kirki::add_section(
	'megaphone_front_episodes',
	array(
		'panel' => 'megaphone_panel_front',
		'title' => esc_attr__( 'Episodes Area', 'megaphone' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_display_title',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_display_title' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_episodes_title',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'text',
		'label'    => esc_html__( 'Title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_episodes_title' ),
		'required' => array(
			array(
				'setting'  => 'front_page_episodes_display_title',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_loop',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_loop' ),
		'choices'  => megaphone_get_episode_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_sidebar_position',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Sidebar position', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_sidebar_position' ),
		'choices'  => megaphone_get_sidebar_layouts(),
		'required' => array(
			array(
				'setting'  => 'front_page_episodes_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_episode_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_sidebar_standard',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'select',
		'label'    => esc_html__( 'Standard sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_sidebar_standard' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'front_page_episodes_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_episode_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_sidebar_sticky',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'select',
		'label'    => esc_html__( 'Sticky sidebar', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_sidebar_sticky' ),
		'choices'  => megaphone_get_sidebars_list(),
		'required' => array(
			array(
				'setting'  => 'front_page_episodes_loop',
				'operator' => 'in',
				'value'    => array_map( 'strval', array_keys( megaphone_get_episode_layouts( array( 'sidebar' => true ) ) ) ),
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_ppp',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'radio',
		'label'    => esc_html__( 'Number of episodes per page', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_ppp' ),
		'choices'  => array(
			'inherit' => esc_html__( 'Inherit from global option set in Settings / Reading', 'megaphone' ),
			'custom'  => esc_html__( 'Custom number', 'megaphone' ),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_ppp_num',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'number',
		'label'    => esc_html__( 'Specify number of episodes', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_ppp_num' ),
		'required' => array(
			array(
				'setting'  => 'front_page_episodes_ppp',
				'operator' => '==',
				'value'    => 'custom',
			),
		),
	)
);

if ( !megaphone_is_simple_podcast_active() ) {
	Kirki::add_field(
		'megaphone',
		array(
			'settings'    => 'front_page_episodes_cat',
			'section'     => 'megaphone_front_episodes',
			'type'        => 'select',
			'multiple'    => 10,
			'label'       => esc_html__( 'From category', 'megaphone' ),
			'description' => esc_html__( 'Select one or more categories to pull the episodes from, or leave empty for all categories', 'megaphone' ),
			'default'     => megaphone_get_default_option( 'front_page_episodes_cat' ),
			'choices'     => Kirki_Helper::get_terms(
				array(
					'taxonomy' => 'category',
					'exclude'  => megaphone_get_option( 'blog_terms' ),
				)
			),
			'required' => array(
				array(
					'setting'  => 'megaphone_podcast_setup',
					'operator' => '!=',
					'value'    => 'simple',
				),
			),
		)
	);
}


if ( megaphone_is_simple_podcast_active() ) {
	Kirki::add_field(
		'megaphone',
		array(
			'settings'    => 'front_page_episodes_cat',
			'section'     => 'megaphone_front_episodes',
			'type'        => 'select',
			'multiple'    => 10,
			'label'       => esc_html__( 'From series', 'megaphone' ),
			'description' => esc_html__( 'Select one or more series to pull the episodes from, or leave empty for all series', 'megaphone' ),
			'default'     => megaphone_get_default_option( 'front_page_episodes_cat' ),
			'choices'     => Kirki_Helper::get_terms(
				array(
					'taxonomy' => 'series'
				)
			),
			'required' => array(
				array(
					'setting'  => 'megaphone_podcast_setup',
					'operator' => '==',
					'value'    => 'simple',
				),
			),
		)
	);
}

Kirki::add_field(
	'megaphone',
	array(
		'settings'    => 'front_page_episodes_tag',
		'section'     => 'megaphone_front_episodes',
		'type'        => 'select',
		'multiple'    => 10,
		'label'       => esc_html__( 'Tagged with', 'megaphone' ),
		'description' => esc_html__( 'Select one or more tags to pull the episodes from, or leave empty for all tags', 'megaphone' ),
		'default'     => megaphone_get_default_option( 'front_page_episodes_tag' ),
		'choices'     => Kirki_Helper::get_terms( 'post_tag' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_slider',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Enable section slider', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_slider' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_view_all_link',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section link', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_view_all_link' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_episodes_view_all_link_label',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'text',
		'label'    => esc_html__( 'Section link label', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_episodes_view_all_link_label' ),
		'required' => array(
			array(
				'setting'  => 'front_page_episodes_view_all_link',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_view_all_link_url',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'link',
		'label'    => esc_html__( 'Section link (URL)', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_view_all_link_url' ),
		'required' => array(
			array(
				'setting'  => 'front_page_episodes_view_all_link',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_episodes_bg',
		'section'  => 'megaphone_front_episodes',
		'type'     => 'radio',
		'label'    => esc_html__( 'Background/style options', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_episodes_bg' ),
		'choices'  => megaphone_get_background_opts( true ),
	)
);