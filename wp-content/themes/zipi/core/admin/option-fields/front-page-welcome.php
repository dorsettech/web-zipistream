<?php


Kirki::add_section(
	'megaphone_front_welcome',
	array(
		'panel' => 'megaphone_panel_front',
		'title' => esc_attr__( 'Welcome Area', 'megaphone' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_wa_display_title',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display section title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_wa_display_title' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_wa_title',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'text',
		'label'    => esc_html__( 'Title', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_wa_title' ),
		'required' => array(
			array(
				'setting'  => 'front_page_wa_display_title',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_wa_layout',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'radio-image',
		'label'    => esc_html__( 'Layout', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_wa_layout' ),
		'choices'  => megaphone_get_welcome_layouts(),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_wa_img',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'image',
		'label'    => esc_html__( 'Image', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_wa_img' ),
		'choices'  => array(
			'save_as' => 'array',
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'wa_layout_1_height',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'number',
		'label'    => esc_html__( 'Cover area (image) height for Layout 1', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'wa_layout_1_height' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'front_page_wa_layout',
				'operator' => '==',
				'value'    => '1',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'wa_layout_2_height',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'number',
		'label'    => esc_html__( 'Cover area (image) height for Layout 2', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'wa_layout_2_height' ),
		'choices'  => array(
			'step' => '1',
		),
		'required' => array(
			array(
				'setting'  => 'front_page_wa_layout',
				'operator' => '==',
				'value'    => '2',
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_wa_punchline',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'text',
		'label'    => esc_html__( 'Punchline', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_wa_punchline' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_wa_text',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'editor',
		'label'    => esc_html__( 'Intro text', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_wa_text' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_wa_cta',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display CTA button', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_wa_cta' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'tr_front_page_wa_cta_label',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'text',
		'label'    => esc_html__( 'Button label', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'tr_front_page_wa_cta_label' ),
		'required' => array(
			array(
				'setting'  => 'front_page_wa_cta',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_wa_cta_url',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'link',
		'label'    => esc_html__( 'Button link (URL)', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_wa_cta_url' ),
		'required' => array(
			array(
				'setting'  => 'front_page_wa_cta',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_wa_latest_episode',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display latest episode', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_wa_latest_episode' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_wa_subscribe',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'toggle',
		'label'    => esc_html__( 'Display subscribe menu', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_wa_subscribe' ),
	)
);

Kirki::add_field(
	'megaphone',
	array(
		'settings' => 'front_page_wa_bg',
		'section'  => 'megaphone_front_welcome',
		'type'     => 'radio',
		'label'    => esc_html__( 'Background/style options', 'megaphone' ),
		'default'  => megaphone_get_default_option( 'front_page_wa_bg' ),
		'choices'  => megaphone_get_background_opts( true ),
		'required' => array(
			array(
				'setting'  => 'front_page_wa_layout',
				'operator' => '==',
				'value'    => '3',
			),
		),
	)
);