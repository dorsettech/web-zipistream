<?php

/**
 * Display theme admin notices
 *
 * @since  1.0
 */

add_action( 'admin_init', 'megaphone_check_installation' );

if ( !function_exists( 'megaphone_check_installation' ) ):
	function megaphone_check_installation() {
		add_action( 'admin_notices', 'megaphone_welcome_msg', 1 );
		add_action( 'admin_notices', 'megaphone_update_msg', 1 );
		add_action( 'admin_notices', 'megaphone_required_plugins_msg', 30 );
	}
endif;


/**
 * Display welcome message and quick tips after theme activation
 *
 * @since  1.0
 */

if ( !function_exists( 'megaphone_welcome_msg' ) ):
	function megaphone_welcome_msg() {

		if ( get_option( 'megaphone_welcome_box_displayed' ) ||  get_option( 'merlin_megaphone_completed' ) ) {
			return false;
		}

		update_option( 'megaphone_theme_version', MEGAPHONE_THEME_VERSION );
		include_once get_parent_theme_file_path( '/core/admin/welcome-panel.php' );
	}
endif;


/**
 * Display message when new version of the theme is installed/updated
 *
 * @since  1.0
 */

if ( !function_exists( 'megaphone_update_msg' ) ):
	function megaphone_update_msg() {

		if ( !get_option( 'megaphone_welcome_box_displayed' ) && !get_option( 'merlin_megaphone_completed' ) ) {
			return false;
		}

		$prev_version = get_option( 'megaphone_theme_version', '0.0.0' );

		if ( version_compare( MEGAPHONE_THEME_VERSION, $prev_version, '>' ) ) {
			include_once get_parent_theme_file_path( '/core/admin/update-panel.php' );
		}

	}
endif;

/**
 * Display message if required plugins are not installed and activated
 *
 * @since  1.0
 */

if ( !function_exists( 'megaphone_required_plugins_msg' ) ):
	function megaphone_required_plugins_msg() {

		if ( !get_option( 'megaphone_welcome_box_displayed' ) && !get_option( 'merlin_megaphone_completed' ) ) {
			return false;
		}

		if ( !megaphone_is_kirki_active() ) {
			$class = 'notice notice-error';
			$message = wp_kses_post( sprintf( __( 'Important: Kirki Toolkit plugin is required to run your theme options customizer panel. Please visit <a href="%s">recommended plugins page</a> to install it.', 'megaphone' ), admin_url( 'admin.php?page=megaphone-plugins' ) ) );
			printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
		}

	}
endif;


/**
 * Add widget form options
 *
 * Add custom options to each widget
 *
 * @return void
 * @since  1.0
 */

add_action( 'in_widget_form', 'megaphone_add_widget_form_options', 10, 3 );

if ( !function_exists( 'megaphone_add_widget_form_options' ) ) :

	function megaphone_add_widget_form_options(  $widget, $return, $instance ) {

		if ( !isset( $instance['megaphone-bg'] ) ) {
			$instance['megaphone-bg'] = 0;
		}

		$backgrounds = megaphone_get_background_opts();

?>
		<p class="megaphone-opt-bg">
			<label><?php esc_html_e( 'Background/style', 'megaphone' ); ?>:</label><br/>
			<label><input type="radio" id="<?php echo esc_attr( $widget->get_field_id( 'megaphone-bg' ) ); ?>" name="<?php echo esc_attr( $widget->get_field_name( 'megaphone-bg' ) ); ?>" value="0" <?php checked( $instance['megaphone-bg'], 0 ); ?> />
				<?php esc_html_e( 'Inherit (from global widget color option)', 'megaphone' ); ?>
				</label><br/>
			<?php foreach ( $backgrounds as $id => $title ): ?>
				<label><input type="radio" id="<?php echo esc_attr( $widget->get_field_id( 'megaphone-bg' ) ); ?>" name="<?php echo esc_attr( $widget->get_field_name( 'megaphone-bg' ) ); ?>" value="<?php echo esc_attr( $id ); ?>" <?php checked( $instance['megaphone-bg'], $id ); ?> />
				<?php echo esc_html( $title ); ?>
				</label><br/>
			<?php endforeach; ?>
			<small class="howto"><?php  esc_html_e( 'Optionally apply specific background to this widget', 'megaphone' ); ?></small>
		</p>

	<?php

	}

endif;


/**
 * Save widget form options
 *
 * Save custom options to each widget
 *
 * @return void
 * @since  1.0
 */

add_filter( 'widget_update_callback', 'megaphone_save_widget_form_options', 20, 4 );

if ( !function_exists( 'megaphone_save_widget_form_options' ) ) :

	function megaphone_save_widget_form_options( $instance, $new_instance, $old_instance, $object ) {

		$instance['megaphone-bg'] = isset( $new_instance['megaphone-bg'] ) ? $new_instance['megaphone-bg'] : 0;

		return $instance;

	}

endif;


/**
 * Store registered sidebars and menus so we can use them inside theme options
 * before wp_registered_sidebars global is initialized
 *
 * @since  1.0
 */

add_action( 'admin_init', 'megaphone_check_sidebars_and_menus' );

if ( !function_exists( 'megaphone_check_sidebars_and_menus' ) ):
	function megaphone_check_sidebars_and_menus() {
		global $wp_registered_sidebars;
		if ( !empty( $wp_registered_sidebars ) ) {
			update_option( 'megaphone_registered_sidebars', $wp_registered_sidebars );
		}

		$registered_menus = get_registered_nav_menus();
		if ( !empty( $registered_menus ) ) {
			update_option( 'megaphone_registered_menus', $registered_menus );
		}

	}
endif;


/**
 * Change default arguments of author widget plugin
 *
 * @since  1.0
 */

add_filter( 'mks_author_widget_modify_defaults', 'megaphone_author_widget_defaults' );

if ( !function_exists( 'megaphone_author_widget_defaults' ) ):
	function megaphone_author_widget_defaults( $defaults ) {
		$defaults['title'] = '';
		$defaults['avatar_size'] = 100;
		return $defaults;
	}
endif;


/**
 * Change default arguments of flickr widget plugin
 *
 * @since  1.0
 */

add_filter( 'mks_flickr_widget_modify_defaults', 'megaphone_flickr_widget_defaults' );

if ( !function_exists( 'megaphone_flickr_widget_defaults' ) ):
	function megaphone_flickr_widget_defaults( $defaults ) {

		$defaults['count'] = 9;
		$defaults['t_width'] = 76;
		$defaults['t_height'] = 76;

		return $defaults;
	}
endif;



/**
 * Remove audio player fields ffrom its settings page
 *
 * @since  1.0
 */

add_filter( 'meks_ap_modify_options_fields', 'megaphone_ap_modify_options_fields' );

if ( !function_exists( 'megaphone_ap_modify_options_fields' ) ):
	function megaphone_ap_modify_options_fields( $fields ) {

		unset( $fields['post_type'] );

		return $fields;
	}
endif;


/**
 * Add Meks dashboard widget
 *
 * @since  1.0
 */

add_action( 'wp_dashboard_setup', 'megaphone_add_dashboard_widgets' );

if ( !function_exists( 'megaphone_add_dashboard_widgets' ) ):
	function megaphone_add_dashboard_widgets() {
		add_meta_box( 'megaphone_dashboard_widget', 'Meks - WordPress Themes & Plugins', 'megaphone_dashboard_widget_cb', 'dashboard', 'side', 'high' );
	}
endif;


/**
 * Meks dashboard widget
 *
 * @since  1.0
 */
if ( !function_exists( 'megaphone_dashboard_widget_cb' ) ):
	function megaphone_dashboard_widget_cb() {

		$transient = 'megaphone_mksaw';
		$hide = '<style>#megaphone_dashboard_widget{display:none;}</style>';

		$data = get_transient( $transient );
	
		if ( $data == 'error' ) {
			echo $hide;
			return;
		}

		if ( !empty( $data ) ) {
			echo $data;
			return;
		}

		$url = 'https://demo.mekshq.com/mksaw.php';
		$args = array( 'body' => array( 'key' => md5( 'meks' ), 'theme' => 'megaphone' ) );
		$response = wp_remote_post( $url, $args );

		if ( is_wp_error( $response ) ) {
			set_transient( $transient, 'error', DAY_IN_SECONDS );
			echo $hide;
			return;
		}

		$json = wp_remote_retrieve_body( $response );

		if ( empty( $json ) ) {
			set_transient( $transient, 'error', DAY_IN_SECONDS );
			echo $hide;
			return;
		}

		$json = json_decode( $json );

		if ( !isset( $json->data ) ) {
			set_transient( $transient, 'error', DAY_IN_SECONDS );
			echo $hide;
			return;
		} 

		set_transient( $transient, $json->data, DAY_IN_SECONDS );
		echo $json->data;
		
	}
endif;
