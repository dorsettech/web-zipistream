<?php

//delete_option('megaphone_settings');

/**
 * Load Kirki Framework
 */

if ( ! class_exists( 'Kirki' ) ) {
	return;
}

add_filter( 'kirki_config', 'megaphone_modify_kirki_config' );

function megaphone_modify_kirki_config( $config ) {
	return wp_parse_args( array(
			'disable_loader' => true
		), $config );
}

/**
 * Theme Options initialization
 */
add_action( 'init', 'megaphone_options_init', 100 );

function megaphone_options_init() {

	/**
	 * Kirki params
	 */

	Kirki::add_config( 'megaphone', array(
			'capability'    => 'edit_theme_options',
			'option_type'   => 'option',
			'option_name'   => 'megaphone_settings',
		) );

	/* Root */

	Kirki::add_panel( 'megaphone_panel', array(
			'priority'    => 10,
			'title'       => esc_html__( 'Theme Options', 'megaphone' )
		) );


	/* Podcast */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/podcast.php' );

	/* Blog */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/blog.php' );

	/* Front page */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/front-page.php' );

	/* Header */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/header.php' );

	/* Content */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/content.php' );

	/* Sidebar */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/sidebar.php' );

	/* Footer */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/footer.php' );

	/* Page */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/page.php' );

	/* Typography */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/typography.php' );

	/* WooCommerce */
	if ( megaphone_is_woocommerce_active() ) {
		require_once get_parent_theme_file_path( '/core/admin/option-fields/woocommerce.php' );
	}

	/* Ads */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/ads.php' );

	/* Miscellaneous */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/miscellaneous.php' );

	/* Performance */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/performance.php' );

	/* Translate */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/translate.php' );

	/* Presets */
	require_once get_parent_theme_file_path( '/core/admin/option-fields/presets.php' );

}