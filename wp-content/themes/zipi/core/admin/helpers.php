<?php

/**
 * Get the list of available post listing layouts
 *
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_post_layouts' ) ) :
	function megaphone_get_post_layouts( $filter = array(), $exclude = array() ) {

		$layouts = megaphone_get_post_layouts_map();

		if ( ! empty( $filter ) ) {
			foreach ( $layouts as $id => $layout ) {
				foreach ( $filter as $what => $value ) {
					if ( ( isset( $layout[ $what ] ) && $layout[ $what ] == $value ) || ( ! isset( $layout[ $what ] ) && $value == false ) ) {
						continue;
					}

					unset( $layouts[ $id ] );
				}
			}
		}

		if ( ! empty( $exclude ) ) {
			foreach ( $layouts as $id => $layout ) {
				foreach ( $exclude as $exclude_id ) {
					if ( $id == $exclude_id ) {
						unset( $layouts[ $id ] );
					}
				}
			}
		}

		$layouts = apply_filters( 'megaphone_modify_post_layouts', $layouts );

		return $layouts;

	}
endif;

/**
 * Get the list of available episode listing layouts
 *
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_episode_layouts' ) ) :
	function megaphone_get_episode_layouts( $filter = array(), $exclude = array() ) {

		$layouts = megaphone_get_episode_layouts_map();

		if ( ! empty( $filter ) ) {
			foreach ( $layouts as $id => $layout ) {
				foreach ( $filter as $what => $value ) {
					if ( ( isset( $layout[ $what ] ) && $layout[ $what ] == $value ) || ( ! isset( $layout[ $what ] ) && $value == false ) ) {
						continue;
					}

					unset( $layouts[ $id ] );
				}
			}
		}

		if ( ! empty( $exclude ) ) {
			foreach ( $layouts as $id => $layout ) {
				foreach ( $exclude as $exclude_id ) {
					if ( $id == $exclude_id ) {
						unset( $layouts[ $id ] );
					}
				}
			}
		}

		$layouts = apply_filters( 'megaphone_modify_episode_layouts', $layouts );

		return $layouts;

	}
endif;



/**
 * Get the list of available featured post layouts
 *
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_featured_layouts' ) ) :
	function megaphone_get_featured_layouts( $filter = array() ) {

		$layouts = megaphone_get_featured_layouts_map();

		if ( ! empty( $filter ) ) {
			foreach ( $layouts as $id => $layout ) {

				foreach ( $filter as $what => $value ) {

					if ( ( isset( $layout[ $what ] ) && $layout[ $what ] == $value ) || ( ! isset( $layout[ $what ] ) && $value == false ) ) {
						continue;
					}

					unset( $layouts[ $id ] );

				}
			}
		}

		$layouts = apply_filters( 'megaphone_modify_featured_layouts', $layouts );

		return $layouts;

	}
endif;

/**
 * Get the list of available post listing layouts
 *
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_show_layouts' ) ) :
	function megaphone_get_show_layouts( $filter = array(), $exclude = array() ) {

		$layouts = megaphone_get_show_layouts_map();

		if ( ! empty( $filter ) ) {
			foreach ( $layouts as $id => $layout ) {
				foreach ( $filter as $what => $value ) {
					if ( ( isset( $layout[ $what ] ) && $layout[ $what ] == $value ) || ( ! isset( $layout[ $what ] ) && $value == false ) ) {
						continue;
					}

					unset( $layouts[ $id ] );
				}
			}
		}

		if ( ! empty( $exclude ) ) {
			foreach ( $layouts as $id => $layout ) {
				foreach ( $exclude as $exclude_id ) {
					if ( $id == $exclude_id ) {
						unset( $layouts[ $id ] );
					}
				}
			}
		}

		$layouts = apply_filters( 'megaphone_modify_show_layouts', $layouts );

		return $layouts;

	}
endif;


/**
 * Get the list of welcome area layouts
 *
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_welcome_layouts' ) ) :
	function megaphone_get_welcome_layouts() {

		$layouts = array();

		$layouts['1'] = array(
			'alt' => esc_html__( 'Layout 1', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/welcome_layout_1.svg' ),
		);
		$layouts['2'] = array(
			'alt' => esc_html__( 'Layout 2', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/welcome_layout_2.svg' ),
		);
		$layouts['3'] = array(
			'alt' => esc_html__( 'Layout 3', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/welcome_layout_3.svg' ),
		);

		$layouts = apply_filters( 'megaphone_modify_welcome_layouts', $layouts );

		return $layouts;

	}
endif;



/**
 * Get the list of header layouts
 *
 * @param bool    $inherit Wheter to display "inherit" option
 * @param bool    $none    Wheter to display "none" option
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_header_layouts' ) ) :
	function megaphone_get_header_layouts( $exclude = array() ) {

		$layouts = array();

		$layouts['1'] = array(
			'alt' => esc_html__( 'Layout 1', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/header_layout_1.svg' ),
		);
		$layouts['2'] = array(
			'alt' => esc_html__( 'Layout 2', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/header_layout_2.svg' ),
		);
		$layouts['3'] = array(
			'alt' => esc_html__( 'Layout 3', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/header_layout_3.svg' ),
		);
		$layouts['4'] = array(
			'alt' => esc_html__( 'Layout 4', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/header_layout_4.svg' ),
		);
		$layouts['5'] = array(
			'alt' => esc_html__( 'Layout 5', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/header_layout_5.svg' ),
		);
		$layouts['6'] = array(
			'alt' => esc_html__( 'Layout 6', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/header_layout_6.svg' ),
		);
		$layouts['7'] = array(
			'alt' => esc_html__( 'Layout 7', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/header_layout_7.svg' ),
		);
		$layouts['8'] = array(
			'alt' => esc_html__( 'Layout 8', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/header_layout_8.svg' ),
		);
		$layouts['9'] = array(
			'alt' => esc_html__( 'Layout 9', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/header_layout_9.svg' ),
		);

		if ( ! empty( $exclude ) ) {
			foreach ( $exclude as $element ) {
				if ( isset( $layouts[ $element ] ) ) {
					unset( $layouts[ $element ] );
				}
			}
		}

		$layouts = apply_filters( 'megaphone_modify_header_layouts', $layouts );

		return $layouts;

	}
endif;


/**
 * Get meta options
 *
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_meta_opts' ) ) :
	function megaphone_get_meta_opts( $disable = false ) {

		$options             = array();
		$options['date']     = esc_html__( 'Date', 'megaphone' );
		$options['author']   = esc_html__( 'Author', 'megaphone' );
		$options['rtime']    = esc_html__( 'Reading time', 'megaphone' );
		$options['comments'] = esc_html__( 'Comments', 'megaphone' );
		$options['category'] = esc_html__( 'Category', 'megaphone' );

		if ( $disable ) {
			$options['none'] = esc_html__( 'None', 'megaphone' );
		}

		$options = apply_filters( 'megaphone_modify_meta_opts', $options );

		return $options;
	}
endif;



/**
 * Get header actions options
 *
 * @return array List of available options
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_header_main_area_actions' ) ) :
	function megaphone_get_header_main_area_actions( $exclude = array() ) {
		$actions = array(
			'search'          => esc_html__( 'Search form', 'megaphone' ),
			'search-modal'    => esc_html__( 'Search button/modal', 'megaphone' ),
			'social'          => esc_html__( 'Social menu', 'megaphone' ),
			'social-modal'    => esc_html__( 'Social button/modal', 'megaphone' ),
			'donate'          => esc_html__( 'Donate menu', 'megaphone' ),
			'subscribe'       => esc_html__( 'Subscribe menu', 'megaphone' ),
			'subscribe-modal' => esc_html__( 'Subscribe button/modal', 'megaphone' ),
		);

		if ( megaphone_is_woocommerce_active() ) {
			$actions['cart'] = esc_html__( 'WooCommerce cart', 'megaphone' );
		}

		$actions['hamburger'] = esc_html__( 'Hamburger menu (hidden sidebar)', 'megaphone' );

		if ( ! empty( $exclude ) ) {
			foreach ( $exclude as $element ) {
				if ( isset( $actions[ $element ] ) ) {
					unset( $actions[ $element ] );
				}
			}
		}

		$actions = apply_filters( 'megaphone_modify_header_main_area_actions', $actions );

		return $actions;
	}
endif;


/**
 * Get header top elements options
 *
 * @return array List of available options
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_header_top_elements' ) ) :
	function megaphone_get_header_top_elements( $exclude = array() ) {
		$actions = array(
			'menu-secondary-1' => esc_html__( 'Secondary menu 1', 'megaphone' ),
			'menu-secondary-2' => esc_html__( 'Secondary menu 2', 'megaphone' ),
			'date'             => esc_html__( 'Date', 'megaphone' ),
			'search'           => esc_html__( 'Search form', 'megaphone' ),
			'search-modal'     => esc_html__( 'Search button/modal', 'megaphone' ),
			'social'           => esc_html__( 'Social menu', 'megaphone' ),
			'social-modal'     => esc_html__( 'Social button/modal', 'megaphone' ),
			'donate'           => esc_html__( 'Donate menu', 'megaphone' ),
			'subscribe'        => esc_html__( 'Subscribe menu', 'megaphone' ),
			'subscribe-modal'  => esc_html__( 'Subscribe button/modal', 'megaphone' ),
		);

		if ( megaphone_is_woocommerce_active() ) {
			$actions['cart'] = esc_html__( 'WooCommerce cart', 'megaphone' );
		}

		$actions['hamburger'] = esc_html__( 'Hamburger menu (hidden sidebar)', 'megaphone' );

		if ( ! empty( $exclude ) ) {
			foreach ( $exclude as $element ) {
				if ( isset( $actions[ $element ] ) ) {
					unset( $actions[ $element ] );
				}
			}
		}

		$actions = apply_filters( 'megaphone_modify_header_top_elements', $actions );

		return $actions;
	}
endif;



/**
 * Get the list of available pagination types
 *
 * @param bool    $ihnerit Whether you want to include "inherit" option in the list
 * @param bool    $none    Whether you want to add "none" option ( to set layout to "off")
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_pagination_layouts' ) ) :
	function megaphone_get_pagination_layouts( $inherit = false, $none = false ) {

		$layouts = array();

		if ( $inherit ) {
			$layouts['inherit'] = array(
				'alt' => esc_html__( 'Inherit', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/inherit.svg' ),
			);
		}

		if ( $none ) {
			$layouts['none'] = array(
				'alt' => esc_html__( 'None', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/pagination_none.svg' ),
			);
		}

		$layouts['numeric']         = array(
			'alt' => esc_html__( 'Numeric pagination links', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/pagination_numeric.svg' ),
		);
		$layouts['prev-next']       = array(
			'alt' => esc_html__( 'Prev/Next page links', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/pagination_prevnext.svg' ),
		);
		$layouts['load-more']       = array(
			'alt' => esc_html__( 'Load more button', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/pagination_loadmore.svg' ),
		);
		$layouts['infinite-scroll'] = array(
			'alt' => esc_html__( 'Infinite scroll', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/pagination_infinite.svg' ),
		);

		$layouts = apply_filters( 'megaphone_modify_pagination_layouts', $layouts );

		return $layouts;
	}
endif;


/**
 * Get footer layouts options
 *
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_footer_layouts' ) ) :
	function megaphone_get_footer_layouts() {
		$layouts = array(
			'12'      => array(
				'alt' => '12',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_12.svg' ),
			),
			'4'       => array(
				'alt' => '4',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_4.svg' ),
			),
			'6-6'     => array(
				'alt' => '6-6',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_6_6.svg' ),
			),
			'4-4-4'   => array(
				'alt' => '4-4-4',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_4_4_4.svg' ),
			),
			'3-3-3-3' => array(
				'alt' => '3-3-3-3',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_3_3_3_3.svg' ),
			),
			'8-4'     => array(
				'alt' => '8-4',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_8_4.svg' ),
			),
			'4-8'     => array(
				'alt' => '4-8',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_4_8.svg' ),
			),
			'6-3-3'   => array(
				'alt' => '6-3-3',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_6_3_3.svg' ),
			),
			'3-3-6'   => array(
				'alt' => '3-3-6',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_3_3_6.svg' ),
			),
			'3-6-3'   => array(
				'alt' => '3-6-3',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_3_6_3.svg' ),
			),
			'3-4-5'   => array(
				'alt' => '3-4-5',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_3_4_5.svg' ),
			),
			'5-4-3'   => array(
				'alt' => '5-4-3',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_5_4_3.svg' ),
			),
			'3-5-4'   => array(
				'alt' => '3-5-4',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_3_5_4.svg' ),
			),
			'4-5-3'   => array(
				'alt' => '4-5-3',
				'src' => get_parent_theme_file_uri( '/assets/img/admin/footer_4_5_3.svg' ),
			),
		);

		$layouts = apply_filters( 'megaphone_modify_footer_layouts', $layouts );

		return $layouts;
	}
endif;


/**
 * Get image ratio options
 *
 * @param bool    $megaphone Wheter to include "megaphone (not cropped)" ratio option
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_image_ratio_opts' ) ) :
	function megaphone_get_image_ratio_opts( $original = false ) {

		$options = array();

		if ( $original ) {
			$options['original'] = esc_html__( 'Original (ratio as uploaded - do not crop)', 'megaphone' );
		}

		$options['21_9']   = esc_html__( '21:9', 'megaphone' );
		$options['16_9']   = esc_html__( '16:9', 'megaphone' );
		$options['3_2']    = esc_html__( '3:2', 'megaphone' );
		$options['4_3']    = esc_html__( '4:3', 'megaphone' );
		$options['1_1']    = esc_html__( '1:1 (square)', 'megaphone' );
		$options['3_4']    = esc_html__( '3:4', 'megaphone' );
		$options['custom'] = esc_html__( 'Custom ratio', 'megaphone' );

		$options = apply_filters( 'megaphone_modify_ratio_opts', $options );
		return $options;
	}
endif;

/**
 * Get the list of available single post layouts
 *
 * @param bool    $ihnerit Whether you want to add "inherit" option
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_single_layouts' ) ) :
	function megaphone_get_single_layouts( $inherit = false ) {

		$layouts = array();

		if ( $inherit ) {
			$layouts['inherit'] = array(
				'alt' => esc_html__( 'Inherit', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/inherit.png' ),
			);
		}

		$layouts['1'] = array(
			'alt' => esc_html__( 'Layout 1', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/single_layout_1.svg' ),
		);
		$layouts['2'] = array(
			'alt' => esc_html__( 'Layout 2', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/single_layout_2.svg' ),
		);
		$layouts['3'] = array(
			'alt' => esc_html__( 'Layout 3', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/single_layout_3.svg' ),
		);
		$layouts['4'] = array(
			'alt' => esc_html__( 'Layout 4', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/single_layout_4.svg' ),
		);
		$layouts['5'] = array(
			'alt' => esc_html__( 'Layout 5', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/single_layout_5.svg' ),
		);

		$layouts = apply_filters( 'megaphone_modify_single_layouts', $layouts );

		return $layouts;
	}
endif;


/**
 * Get the list of available single episode layouts
 *
 * @param bool    $ihnerit Whether you want to add "inherit" option
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_single_episode_layouts' ) ) :
	function megaphone_get_single_episode_layouts( $inherit = false ) {

		$layouts = array();

		if ( $inherit ) {
			$layouts['inherit'] = array(
				'alt' => esc_html__( 'Inherit', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/inherit.png' ),
			);
		}

		$layouts['1'] = array(
			'alt' => esc_html__( 'Layout 1', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/episode_layout_1.svg' ),
		);
		$layouts['2'] = array(
			'alt' => esc_html__( 'Layout 2', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/episode_layout_2.svg' ),
		);
		$layouts['3'] = array(
			'alt' => esc_html__( 'Layout 3', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/episode_layout_3.svg' ),
		);
		$layouts['4'] = array(
			'alt' => esc_html__( 'Layout 4', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/episode_layout_4.svg' ),
		);
		$layouts['5'] = array(
			'alt' => esc_html__( 'Layout 5', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/episode_layout_5.svg' ),
		);

		$layouts = apply_filters( 'megaphone_modify_single_episode_layouts', $layouts );

		return $layouts;
	}
endif;

/**
 * Get the list of available page layouts
 *
 * @param bool    $ihnerit Whether you want to add "inherit" option
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_page_layouts' ) ) :
	function megaphone_get_page_layouts( $inherit = false ) {

		$layouts = array();

		if ( $inherit ) {
			$layouts['inherit'] = array(
				'alt' => esc_html__( 'Inherit', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/inherit.png' ),
			);
		}

		$layouts['1'] = array(
			'alt' => esc_html__( 'Layout 1', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/page_layout_1.svg' ),
		);
		$layouts['2'] = array(
			'alt' => esc_html__( 'Layout 2', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/page_layout_2.svg' ),
		);
		$layouts['3'] = array(
			'alt' => esc_html__( 'Layout 3', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/page_layout_3.svg' ),
		);

		$layouts = apply_filters( 'megaphone_modify_page_layouts', $layouts );

		return $layouts;

	}
endif;


/**
 * Get the list of available archive layouts
 *
 * @param bool    $ihnerit Whether you want to add "inherit" option
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_archive_layouts' ) ) :
	function megaphone_get_archive_layouts( $inherit = false ) {

		$layouts = array();

		if ( $inherit ) {
			$layouts['inherit'] = array(
				'alt' => esc_html__( 'Inherit', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/inherit.png' ),
			);
		}

		$layouts['1'] = array(
			'alt' => esc_html__( 'Layout 1', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/archive_layout_1.svg' ),
		);
		$layouts['2'] = array(
			'alt' => esc_html__( 'Layout 2', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/archive_layout_2.svg' ),
		);
		$layouts['3'] = array(
			'alt' => esc_html__( 'Layout 3', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/archive_layout_3.svg' ),
		);

		$layouts = apply_filters( 'megaphone_modify_archive_layouts', $layouts );

		return $layouts;

	}
endif;


/**
 * Get the list of available sidebar layouts
 *
 * You may have left sidebar, right sidebar or no sidebar
 *
 * @param bool    $ihnerit Whether you want to include "inherit" option in the list
 * @return array List of available sidebar layouts
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_sidebar_layouts' ) ) :
	function megaphone_get_sidebar_layouts( $inherit = false, $none = false ) {

		$layouts = array();

		if ( $inherit ) {
			$layouts['inherit'] = array(
				'alt' => esc_html__( 'Inherit', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/inherit.svg' ),
			);
		}

		if ( $none ) {
			$layouts['none'] = array(
				'alt' => esc_html__( 'None', 'megaphone' ),
				'src' => get_parent_theme_file_uri( '/assets/img/admin/sidebar_none.svg' ),
			);
		}

		$layouts['left']  = array(
			'alt' => esc_html__( 'Left sidebar', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/sidebar_left.svg' ),
		);
		$layouts['right'] = array(
			'alt' => esc_html__( 'Right sidebar', 'megaphone' ),
			'src' => get_parent_theme_file_uri( '/assets/img/admin/sidebar_right.svg' ),
		);

		$layouts = apply_filters( 'megaphone_modify_sidebar_layouts', $layouts );

		return $layouts;
	}
endif;


/**
 * Get the list of registered sidebars
 *
 * @param bool    $ihnerit Whether you want to include "inherit" option in the list
 * @return array Returns list of available sidebars
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_sidebars_list' ) ) :
	function megaphone_get_sidebars_list( $inherit = false ) {

		$sidebars = array();

		if ( $inherit ) {
			$sidebars['inherit'] = esc_html__( 'Inherit', 'megaphone' );
		}

		$sidebars['none'] = esc_html__( 'None', 'megaphone' );

		global $wp_registered_sidebars;

		if ( ! empty( $wp_registered_sidebars ) ) {

			foreach ( $wp_registered_sidebars as $sidebar ) {
				$sidebars[ $sidebar['id'] ] = $sidebar['name'];
			}
		}
		// Get sidebars from wp_options if global var is not loaded yet
		$fallback_sidebars = get_option( 'megaphone_registered_sidebars' );
		if ( ! empty( $fallback_sidebars ) ) {
			foreach ( $fallback_sidebars as $sidebar ) {
				if ( ! array_key_exists( $sidebar['id'], $sidebars ) ) {
					$sidebars[ $sidebar['id'] ] = $sidebar['name'];
				}
			}
		}

		// Check for theme additional sidebars
		$custom_sidebars = megaphone_get_option( 'sidebars' );

		if ( $custom_sidebars ) {
			foreach ( $custom_sidebars as $k => $sidebar ) {
				if ( is_numeric( $k ) && ! array_key_exists( 'megaphone_sidebar_' . $k, $sidebars ) ) {
					$sidebars[ 'megaphone_sidebar_' . $k ] = $sidebar['name'];
				}
			}
		}

		// Do not display footer sidebars for selection
		unset( $sidebars['megaphone_sidebar_footer_1'] );
		unset( $sidebars['megaphone_sidebar_footer_2'] );
		unset( $sidebars['megaphone_sidebar_footer_3'] );
		unset( $sidebars['megaphone_sidebar_footer_4'] );

		// Do not display hidden sidebar for selection
		unset( $sidebars['megaphone_sidebar_hidden'] );

		$sidebars = apply_filters( 'megaphone_modify_sidebars_list', $sidebars );

		return $sidebars;
	}
endif;


/**
 * Get the list of registered sidebars
 *
 * @param bool    $ihnerit Whether you want to include "inherit" option in the list
 * @return array Returns list of available sidebars
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_menus_list' ) ) :
	function megaphone_get_menus_list() {

		$menus = array();

		$menus['none'] = esc_html__( 'None', 'megaphone' );

		$registered_menus = get_registered_nav_menus();

		if ( ! empty( $registered_menus ) ) {
			foreach ( $registered_menus as $id => $menu ) {
				$menus[ $id ] = $menu;
			}
		}

		// Get menus from wp_options if global var is not loaded yet
		$fallback_menus = get_option( 'megaphone_registered_menus' );

		// print_r( $fallback_menus );

		if ( ! empty( $fallback_menus ) ) {
			foreach ( $fallback_menus as $id => $menu ) {
				if ( ! array_key_exists( $id, $menus ) ) {
					$menus[ $id ] = $menu;
				}
			}
		}

		$menus = apply_filters( 'megaphone_modify_menus_list', $menus );

		return $menus;
	}
endif;

/**
 * Get the list of available options for post ordering
 *
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_post_order_opts' ) ) :
	function megaphone_get_post_order_opts() {

		$options = array(
			'date'          => esc_html__( 'Date', 'megaphone' ),
			'comment_count' => esc_html__( 'Number of comments', 'megaphone' ),
			'title'         => esc_html__( 'Title (alphabetically)', 'megaphone' ),
		);

		$options = apply_filters( 'megaphone_modify_post_order_opts', $options );

		return $options;
	}
endif;


/**
 * Get the list of available options for background of an element
 *
 * @param bool    $none wheter to include "none" option
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_background_opts' ) ) :
	function megaphone_get_background_opts( $none = false ) {
		$options = array();

		if ( $none ) {
			$options['none'] = esc_html__( 'None (transparent)', 'megaphone' );
		}

		$options['alt-1'] = esc_html__( 'Alt background 1', 'megaphone' );
		$options['alt-2'] = esc_html__( 'Alt background 2', 'megaphone' );

		$options = apply_filters( 'megaphone_modify_background_opts', $options );

		return $options;
	}
endif;




/**
 * Get the list of available user roles
 *
 * @return array List of available user roles
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_user_role_opts' ) ) :
	function megaphone_get_user_role_opts() {

		$options = array();

		$data = wp_roles();

		foreach ( $data->roles as $id => $role ) {
			$options[ $id ] = $role['name'];
		}

		$options = apply_filters( 'megaphone_modify_user_role_opts', $options );
		return $options;
	}
endif;


/**
 * Get related plugins
 *
 * Check if Yet Another Related Posts Plugin (YARPP) or Contextual Related Posts or WordPress Related Posts or Jetpack by WordPress.com is active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_related_posts_plugins' ) ) :
	function megaphone_get_related_posts_plugins() {
		$related_plugins['default'] = esc_html__( 'Built-in (megaphone) related posts', 'megaphone' );
		$related_plugins['yarpp']   = esc_html__( 'Yet Another Related Posts Plugin (YARPP)', 'megaphone' );
		$related_plugins['crp']     = esc_html__( 'Contextual Related Posts', 'megaphone' );
		$related_plugins['wrpr']    = esc_html__( 'WordPress Related Posts', 'megaphone' );
		$related_plugins['jetpack'] = esc_html__( 'Jetpack by WordPress.com', 'megaphone' );

		return $related_plugins;
	}
endif;


/**
 * Get breadcrumbs by options
 *
 * Check breadcrumbs support depending on witch plugins are active
 *
 * @return bool
 * @since  1.0
 */
if ( ! function_exists( 'megaphone_get_breadcrumbs_options' ) ) :
	function megaphone_get_breadcrumbs_options() {

		$options['none']  = esc_html__( 'None', 'megaphone' );
		$options['yoast'] = esc_html__( 'Yoast SEO (or Yoast Breadcrumbs)', 'megaphone' );
		$options['bcn']   = esc_html__( 'Breadcrumb NavXT', 'megaphone' );

		$options = apply_filters( 'megaphone_modify_breadcrumbs_options', $options );

		return $options;
	}
endif;


/**
 * Get player support options
 *
 * @return array List of available options
 * @since  1.0
 */

if ( ! function_exists( 'megaphone_get_player_support_opts' ) ) :
	function megaphone_get_player_support_opts() {

		$options = array();

		$options['.wp-block-embed-soundcloud'] = esc_html__( 'SoundCloud block', 'megaphone' );
		$options['.wp-block-embed-mixcloud']   = esc_html__( 'Mixcloud block', 'megaphone' );
		$options['.wp-block-embed-spotify']    = esc_html__( 'Spotify block', 'megaphone' );
		$options['.wp-block-embed-youtube']    = esc_html__( 'YouTube block', 'megaphone' );
		$options['iframe-stitcher.com']        = esc_html__( 'Stitcher embed (iframe)', 'megaphone' );
		$options['iframe-podbean.com']         = esc_html__( 'Podbean embed (iframe)', 'megaphone' );
		$options['iframe-libsyn.com']          = esc_html__( 'Libsyn embed (iframe)', 'megaphone' );
		$options['iframe-spreaker.com']        = esc_html__( 'Spreaker embed (iframe)', 'megaphone' );
		$options['iframe-podomatic.com']       = esc_html__( 'Podomatic embed (iframe)', 'megaphone' );
		$options['iframe-blogtalkradio.com']   = esc_html__( 'BlogTalkRadio embed (iframe)', 'megaphone' );
		$options['iframe-cadence13.com']       = esc_html__( 'Cadence 13 embed (iframe)', 'megaphone' );
		$options['iframe-megaphone.fm']       = esc_html__( 'Megaphone.fm (iframe)', 'megaphone' );
		$options['iframe-ivoox.com']       = esc_html__( 'Ivoox (iframe)', 'megaphone' );
		$options['iframe-buzzsprout.com']       = esc_html__( 'Buzzsprout (iframe)', 'megaphone' );
		$options['iframe-anchor.fm']       = esc_html__( 'Anchor (iframe)', 'megaphone' );
		$options['iframe-simplecast.com']       = esc_html__( 'Simplecast (iframe)', 'megaphone' );
		$options['iframe-iheart.com']       = esc_html__( 'iHeart (iframe)', 'megaphone' );
		$options['iframe-tunein.com']       = esc_html__( 'TuneIn (iframe)', 'megaphone' );
		$options['iframe-ausha.co']       = esc_html__( 'Ausha (iframe)', 'megaphone' );

		$options = apply_filters( 'megaphone_modify_player_support_opts', $options );

		return $options;
	}
endif;


/**
 * Get Admin JS localized variables
 *
 * Function creates list of variables from theme to pass
 * them to global JS variable so we can use it in JS files
 *
 * @since  1.0
 *
 * @return array List of JS settings
 */
if ( ! function_exists( 'megaphone_get_admin_js_settings' ) ) :
	function megaphone_get_admin_js_settings() {

		$js_settings             = array();
		$js_settings['ajax_url'] = admin_url( 'admin-ajax.php' );
		return $js_settings;
	}
endif;

/**
 * All podcasts options
 *
 * Function for listing all podcasts options (including podcast plugins)
 *
 * @since  1.0
 *
 * @return array List of options
 */
if ( ! function_exists( 'megaphone_podcasts_options' ) ) :
	function megaphone_podcasts_options() {

		return array(
			'theme_organization' => esc_html__( 'Megaphone theme logic', 'megaphone' ),
			'podlove'            => esc_html__( 'Podlove podcasting plugin', 'megaphone' ),
			'simple'             => esc_html__( 'Seriously simple podcasting plugin', 'megaphone' ),
		);

	}
endif;

/**
 * Get frontpage sections options
 *
 * @since  1.0
 *
 * @return array List of options
 */
if ( ! function_exists( 'megaphone_front_page_sections_opt' ) ) :
	function megaphone_front_page_sections_opt() {

		$options = array(
			'welcome'   => esc_html__( 'Welcome', 'megaphone' ),
			'featured'  => esc_html__( 'Featured episodes', 'megaphone' ),
			'shows'     => esc_html__( 'Shows (podcast categories)', 'megaphone' ),
			'episodes'  => esc_html__( 'Episodes', 'megaphone' ),
			'posts'     => esc_html__( 'Blog (latest posts)', 'megaphone' ),
			'custom-content' => esc_html__( 'Custom content', 'megaphone' ),
		);

		// $custom_sections_numb = megaphone_get_option( 'front_page_general_custom_fields_number' );

		for ( $i = 2; $i <= 5; $i++ ) {
			$options['custom-content-'.$i] = esc_html__( 'Custom content '. $i, 'megaphone' );
		}

		return $options;

	}
endif;
