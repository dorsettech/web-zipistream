<?php
/**
 * Load post metaboxes
 *
 * Callback function for post metaboxes load
 *
 * @since  1.0
 */

if ( !function_exists( 'megaphone_load_post_metaboxes' ) ) :
	function megaphone_load_post_metaboxes() {

		add_meta_box(
			'megaphone_post_display',
			esc_html__( 'Display Settings', 'megaphone' ),
			'megaphone_post_display_metabox',
			array( 'post', 'podcast' ),
			'side',
			'default'
		);

	}
endif;


/**
 * Save post meta
 *
 * Callback function to save post meta data
 *
 * @since  1.0
 */

if ( !function_exists( 'megaphone_save_post_metaboxes' ) ) :
	function megaphone_save_post_metaboxes( $post_id, $post ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;

		if ( !isset( $_POST['megaphone_post_metabox_nonce'] ) || !wp_verify_nonce( $_POST['megaphone_post_metabox_nonce'], 'megaphone_post_metabox_save' ) ) {
			return;
		}

		if ( ( $post->post_type == 'post' ||  $post->post_type == 'podcast' ) && isset( $_POST['megaphone'] ) ) {

			$post_type = get_post_type_object( $post->post_type );
			
			if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
				return $post_id;

			$meta = array();

			if ( isset( $_POST['megaphone']['settings'] ) ) {

				$meta['settings'] = $_POST['megaphone']['settings'];

				if ( $_POST['megaphone']['settings'] == 'custom' ) {

					if ( isset( $_POST['megaphone']['layout'] ) ) {
						$meta['layout'] = $_POST['megaphone']['layout'];
					}

					if ( isset( $_POST['megaphone']['sidebar'] ) ) {
						$meta['sidebar'] = $_POST['megaphone']['sidebar'];
					}
				}
			}

			if ( !empty( $meta ) ) {
				update_post_meta( $post_id, '_megaphone_meta', $meta );
			} else {
				delete_post_meta( $post_id, '_megaphone_meta' );
			}

		}
	}
endif;

/**
 * Display metabox
 *
 * Callback function to create layout metabox
 *
 * @since  1.0
 */

if ( !function_exists( 'megaphone_post_display_metabox' ) ) :
	function megaphone_post_display_metabox( $object, $box ) {

		wp_nonce_field( 'megaphone_post_metabox_save', 'megaphone_post_metabox_nonce' );

		$meta = megaphone_get_post_meta( $object->ID );
		$layouts = megaphone_get_single_layouts( );
		$sidebar_layouts = megaphone_get_sidebar_layouts( false, true );
		$sidebars = megaphone_get_sidebars_list( );
		//todo detect if post is podcast

?>
        <div class="megaphone-opt-display">
			<label>
				<input type="radio" name="megaphone[settings]" value="inherit" <?php checked( $meta['settings'], 'inherit' ); ?>>
				<?php esc_html_e( 'Inherit from theme options', 'megaphone' ); ?>
			</label>
	        <br/>
			<label>
				<input type="radio" name="megaphone[settings]" value="custom" <?php checked( $meta['settings'], 'custom' ); ?>>
				<?php esc_html_e( 'Customize', 'megaphone' ); ?>
			</label>
		</div>

		<?php $class = $meta['settings'] == 'inherit' ? 'megaphone-hidden' : ''; ?>
		<div class="megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">
	        <h4><?php esc_html_e( 'Layout', 'megaphone' ); ?></h4>
	        <ul class="megaphone-img-select-wrap">
	            <?php foreach ( $layouts as $id => $layout ): ?>
	                <li>
	                    <img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['layout'], 'selected' ) ); ?>">
	                    <span><?php echo esc_html( $layout['alt'] ); ?></span>
	                    <input type="radio" class="megaphone-hidden" name="megaphone[layout]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['layout'] );?>/> </label>
	                </li>
	            <?php endforeach; ?>
	        </ul>

	        <h4><?php esc_html_e( 'Sidebar', 'megaphone' ); ?></h4>

	        <ul class="megaphone-img-select-wrap">
	            <?php foreach ( $sidebar_layouts as $id => $layout ): ?>
	                <li>
	                    <img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['sidebar']['position'], 'selected' ) ); ?>">
	                    <span><?php echo esc_html( $layout['alt'] ); ?></span>
	                    <input type="radio" class="megaphone-hidden" name="megaphone[sidebar][position]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['sidebar']['position'] );?>/> </label>
	                </li>
	            <?php endforeach; ?>
	        </ul>

	        <p>
	        	<select name="megaphone[sidebar][classic]" class="widefat">
	                <?php foreach ( $sidebars as $id => $name ): ?>
	                    <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $id, $meta['sidebar']['classic'] );?>><?php echo esc_html( $name ); ?></option>
	                <?php endforeach; ?>
	            </select>
	        </p>
	        <small class="howto"><?php esc_html_e( 'Choose standard sidebar to display', 'megaphone' ); ?></small>

	        <p>
	        	<select name="megaphone[sidebar][sticky]" class="widefat">
	                <?php foreach ( $sidebars as $id => $name ): ?>
	                    <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $id, $meta['sidebar']['sticky'] );?>><?php echo esc_html( $name ); ?></option>
	                <?php endforeach; ?>
	            </select>
	        </p>
	        <small class="howto"><?php esc_html_e( 'Choose sticky sidebar to display', 'megaphone' ); ?></small>

    	</div>

		<?php
	}
endif;