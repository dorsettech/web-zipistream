<?php

/**
 * Save category meta
 *
 * Callback function to save category meta data
 *
 * @since  1.0
 */

add_action( 'edited_category', 'megaphone_save_category_meta_fields', 10, 2 );
add_action( 'create_category', 'megaphone_save_category_meta_fields', 10, 2 );
add_action( 'edited_series', 'megaphone_save_category_meta_fields', 10, 2 );
add_action( 'create_series', 'megaphone_save_category_meta_fields', 10, 2 );

if ( !function_exists( 'megaphone_save_category_meta_fields' ) ) :
	function megaphone_save_category_meta_fields( $term_id ) {

		if ( isset( $_POST['megaphone'] ) ) {

			$meta = array();

			if ( isset( $_POST['megaphone']['image'] ) ) {
				$meta['image'] = $_POST['megaphone']['image'];
			}

			if ( isset( $_POST['megaphone']['settings'] ) ) {
				$meta['settings'] = $_POST['megaphone']['settings'];

				if ( $_POST['megaphone']['settings'] == 'custom' ) {

					if ( isset( $_POST['megaphone']['layout'] ) ) {
						$meta['layout'] = $_POST['megaphone']['layout'];
					}

					if ( isset( $_POST['megaphone']['loop'] ) ) {
						$meta['loop'] = $_POST['megaphone']['loop'];
					}

					if ( isset( $_POST['megaphone']['pagination'] ) ) {
						$meta['pagination'] = $_POST['megaphone']['pagination'];
					}

					if ( isset( $_POST['megaphone']['ppp_num'] ) ) {
						$meta['ppp_num'] = absint( $_POST['megaphone']['ppp_num'] );
					}

					if ( isset( $_POST['megaphone']['sidebar'] ) ) {
						$meta['sidebar'] = $_POST['megaphone']['sidebar'];
					}
				}

			}


			if ( !empty( $meta ) ) {
				update_term_meta( $term_id, '_megaphone_meta', $meta );
			} else {
				delete_term_meta( $term_id, '_megaphone_meta' );
			}

		}

	}
endif;


/**
 * Add category meta
 *
 * Callback function to load category meta fields on "new category" screen
 *
 * @since  1.0
 */

add_action( 'category_add_form_fields', 'megaphone_category_add_meta_fields', 10, 2 );
add_action( 'series_add_form_fields', 'megaphone_category_add_meta_fields', 100, 2 );

if ( !function_exists( 'megaphone_category_add_meta_fields' ) ) :
	function megaphone_category_add_meta_fields() {
		$meta = megaphone_get_category_meta();
		$loops = megaphone_get_post_layouts( array(), array(10, 11) );
		//todo check if is podcast category
		$layouts = megaphone_get_archive_layouts();
		$paginations = megaphone_get_pagination_layouts();
		$sidebar_layouts = megaphone_get_sidebar_layouts();
		$sidebars = megaphone_get_sidebars_list();
?>
        <div class="form-field megaphone-opt-display">
            <label><?php esc_html_e( 'Display settings', 'megaphone' ); ?></label>
            <label>
                <input type="radio" class="megaphone-settings-type" name="megaphone[settings]" value="inherit" <?php checked( $meta['settings'], 'inherit' ); ?>>
				<?php esc_html_e( 'Inherit from Category theme options', 'megaphone' ); ?>
            </label>
            <label>
                <input type="radio" name="megaphone[settings]" value="custom" <?php checked( $meta['settings'], 'custom' ); ?>>
				<?php esc_html_e( 'Customize', 'megaphone' ); ?>
            </label>

        </div>

        <?php $class = $meta['settings'] == 'custom' ? '' : 'megaphone-hidden'; ?>

        <div class="form-field megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">

			<label><?php esc_html_e( 'Template layout', 'megaphone' ); ?></label>

		    <p>
		    	<ul class="megaphone-img-select-wrap">
				  	<?php foreach ( $layouts as $id => $layout ): ?>
				  		<li>
				  			<img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['layout'], 'selected' ) ); ?>">
				  			<br/><span><?php echo esc_attr( $layout['alt'] ); ?></span>
				  			<input type="radio" class="megaphone-hidden megaphone-count-me" name="megaphone[layout]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['layout'] );?>/>
				  		</li>
				  	<?php endforeach; ?>
			    </ul>
		    	<small class="howto"><?php esc_html_e( 'Choose a layout', 'megaphone' ); ?></small>
		    </p>

	    </div>

        <div class="form-field megaphone-opt-layouts megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">

			<label><?php esc_html_e( 'Posts layout', 'megaphone' ); ?></label>

		    <p>
		    	<ul class="megaphone-img-select-wrap">
				  	<?php foreach ( $loops as $id => $layout ): ?>
				  		<li>
				  			<img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['loop'], 'selected' ) ); ?>" data-sidebar="<?php echo absint( megaphone_loop_has_sidebar($id) ); ?>">
				  			<br/><span><?php echo esc_attr( $layout['alt'] ); ?></span>
				  			<input type="radio" class="megaphone-hidden megaphone-count-me" name="megaphone[loop]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['loop'] );?>/>
				  		</li>
				  	<?php endforeach; ?>
			    </ul>
		    	<small class="howto"><?php esc_html_e( 'Choose a layout', 'megaphone' ); ?></small>
		    </p>

	    </div>

	    <div class="form-field megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">
        	<label><?php esc_html_e( 'Post per page', 'megaphone' ); ?></label>
		    <p>
		  		<input type="number" class="megaphone-count-me small-text" name="megaphone[ppp_num]" value="<?php echo absint( $meta['ppp_num'] ); ?>"/>
		    </p>
        </div>

        <?php $sidebar_class = megaphone_loop_has_sidebar( $meta['loop'] ) ? '' : 'megaphone-opt-disabled'; ?>
        <div class="form-field megaphone-opt-sidebar <?php echo esc_attr( $sidebar_class ); ?> megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">
        	<label><?php esc_html_e( 'Sidebar', 'megaphone' ); ?></label>
			    <p>
			    	<ul class="megaphone-img-select-wrap">
					  	<?php foreach ( $sidebar_layouts as $id => $layout ): ?>
					  		<li>
					  			<img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['sidebar']['position'], 'selected' ) ); ?>">
					  			<br/><span><?php echo esc_attr( $layout['alt'] ); ?></span>
					  			<input type="radio" class="megaphone-hidden megaphone-count-me" name="megaphone[sidebar][position]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['sidebar']['position'] );?>/>
					  		</li>
					  	<?php endforeach; ?>
			    	</ul>
			    	<small class="howto"><?php esc_html_e( 'Choose sidebar position', 'megaphone' ); ?></small>
			    	<br/>
			    </p>

			    <p>
				    <select name="megaphone[sidebar][classic]" class="megaphone-count-me">
					  	<?php foreach ( $sidebars as $id => $sidebar ): ?>
					  		<option class="megaphone-count-me" value="<?php echo esc_attr( $id ); ?>" <?php selected( $id, $meta['sidebar']['classic'] );?>><?php echo esc_html( $sidebar ); ?></option>
					  	<?php endforeach; ?>
				  	</select>
			    	<small class="howto"><?php esc_html_e( 'Choose a regular sidebar to display', 'megaphone' ); ?></small>
			    	<br/>
		    	</p>

		    	<p>
			    	<select name="megaphone[sidebar][sticky]" class="megaphone-count-me">
					  	<?php foreach ( $sidebars as $id => $sidebar ): ?>
					  		<option class="megaphone-count-me" value="<?php echo esc_attr( $id ); ?>" <?php selected( $id, $meta['sidebar']['sticky'] );?>><?php echo esc_html( $sidebar ); ?></option>
					  	<?php endforeach; ?>
				  	</select>
			    	<small class="howto"><?php esc_html_e( 'Choose a sticky sidebar to display', 'megaphone' ); ?></small>
			    	<br/>
		    	</p>
        </div>

       
        <div class="form-field megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">
        	<label><?php esc_html_e( 'Pagination', 'megaphone' ); ?></label>
		    <p>
		    	<ul class="megaphone-img-select-wrap">
				  	<?php foreach ( $paginations as $id => $layout ): ?>
				  		<li>
				  			<img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['pagination'], 'selected' ) ); ?>">
				  			<br/><span><?php echo esc_attr( $layout['alt'] ); ?></span>
				  			<input type="radio" class="megaphone-hidden megaphone-count-me" name="megaphone[pagination]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['pagination'] );?>/>
				  		</li>
				  	<?php endforeach; ?>
			    </ul>
		    	<small class="howto"><?php esc_html_e( 'Choose a layout', 'megaphone' ); ?></small>
		    </p>
        </div>

        <div class="form-field">
            <label><?php esc_html_e( 'Image', 'megaphone' ); ?></label>
			<?php $display = $meta['image'] ? 'initial' : 'none'; ?>
            <p>
                <img id="megaphone-image-preview" src="<?php echo esc_url( $meta['image'] ); ?>" style="display:<?php echo esc_attr( $display ); ?>;">
            </p>

            <p>
                <input type="hidden" name="megaphone[image]" id="megaphone-image-url" value="<?php echo esc_attr( $meta['image'] ); ?>"/>
                <input type="button" id="megaphone-image-upload" class="button-secondary" value="<?php esc_html_e( 'Upload', 'megaphone' ); ?>"/>
                <input type="button" id="megaphone-image-clear" class="button-secondary" value="<?php esc_html_e( 'Clear', 'megaphone' ); ?>" style="display:<?php echo esc_attr( $display ); ?>"/>
            </p>

            <p class="description"><?php esc_html_e( 'Upload an image for this category', 'megaphone' ); ?></p>
        </div>

		<?php
	}
endif;


/**
 * Edit category meta
 *
 * Callback function to load category meta fields on edit screen
 *
 * @since  1.0
 */

add_action( 'category_edit_form_fields', 'megaphone_category_edit_meta_fields', 10, 2 );
add_action( 'series_edit_form_fields', 'megaphone_category_edit_meta_fields', 10, 2 );

if ( !function_exists( 'megaphone_category_edit_meta_fields' ) ) :
	function megaphone_category_edit_meta_fields( $term ) {
		$meta = megaphone_get_category_meta( $term->term_id );
		$layouts = megaphone_get_archive_layouts();
		$paginations = megaphone_get_pagination_layouts();
		$sidebar_layouts = megaphone_get_sidebar_layouts();
		$sidebars = megaphone_get_sidebars_list();
		
		$exclude = megaphone_get_archive_type( false, $term->term_id ) == 'blog' ? array(10, 11) : array();
		$loops = megaphone_get_post_layouts(array(), $exclude);
		//todo check if is podcast category
?>
        <tr class="form-field megaphone-opt-display">
            <th scope="row" valign="top">
                <?php esc_html_e( 'Display settings', 'megaphone' ); ?>
            </th>
            <td>
                <label>
                    <input type="radio" name="megaphone[settings]" value="inherit" <?php checked( $meta['settings'], 'inherit' ); ?>>
					<?php esc_html_e( 'Inherit from Category theme options', 'megaphone' ); ?>
                </label>
                <br/>
                <label>
                    <input type="radio" name="megaphone[settings]" value="custom" <?php checked( $meta['settings'], 'custom' ); ?>>
					<?php esc_html_e( 'Customize', 'megaphone' ); ?>
                </label>
            </td>
        </tr>
        
        <?php $class = $meta['settings'] == 'custom' ? '' : 'megaphone-hidden'; ?>

        <tr class="form-field megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">
        	<th scope="row" valign="top">
                <?php esc_html_e( 'Template layout', 'megaphone' ); ?>
            </th>
            <td>
			    <p>
			    	<ul class="megaphone-img-select-wrap">
					  	<?php foreach ( $layouts as $id => $layout ): ?>
					  		<li>
					  			<img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['layout'], 'selected' ) ); ?>">
					  			<br/><span><?php echo esc_attr( $layout['alt'] ); ?></span>
					  			<input type="radio" class="megaphone-hidden megaphone-count-me" name="megaphone[layout]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['layout'] );?>/>
					  		</li>
					  	<?php endforeach; ?>
				    </ul>
			    	<small class="howto"><?php esc_html_e( 'Choose a layout', 'megaphone' ); ?></small>
			    </p>
		    </td>
        </tr>

        <tr class="form-field megaphone-opt-layouts megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">
        	<th scope="row" valign="top">
                <?php esc_html_e( 'Posts layout', 'megaphone' ); ?>
            </th>
            <td>
			    <p>
			    	<ul class="megaphone-img-select-wrap">
					  	<?php foreach ( $loops as $id => $layout ): ?>
					  		<li>
					  			<img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['loop'], 'selected' ) ); ?>" data-sidebar="<?php echo absint( megaphone_loop_has_sidebar($id) ); ?>">
					  			<br/><span><?php echo esc_attr( $layout['alt'] ); ?></span>
					  			<input type="radio" class="megaphone-hidden megaphone-count-me" name="megaphone[loop]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['loop'] );?>/>
					  		</li>
					  	<?php endforeach; ?>
				    </ul>
			    	<small class="howto"><?php esc_html_e( 'Choose a layout', 'megaphone' ); ?></small>
			    </p>
		    </td>
        </tr>

        <tr class="form-field megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">
        	<th scope="row" valign="top">
                <?php esc_html_e( 'Post per page', 'megaphone' ); ?>
            </th>
            <td>
			    <p>
			  		<input type="number" class="megaphone-count-me small-text" name="megaphone[ppp_num]" value="<?php echo absint( $meta['ppp_num'] ); ?>"/>
			    </p>
		    </td>
        </tr>

        <?php $sidebar_class = megaphone_loop_has_sidebar( $meta['loop'] ) ? '' : 'megaphone-opt-disabled'; ?>

        <tr class="form-field megaphone-opt-sidebar <?php echo esc_attr( $sidebar_class ); ?> megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">
        	<th scope="row" valign="top">
                <?php esc_html_e( 'Sidebar', 'megaphone' ); ?>
            </th>
            <td>
			    <p>
			    	<ul class="megaphone-img-select-wrap">
					  	<?php foreach ( $sidebar_layouts as $id => $layout ): ?>
					  		<li>
					  			<img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['sidebar']['position'], 'selected' ) ); ?>">
					  			<br/><span><?php echo esc_attr( $layout['alt'] ); ?></span>
					  			<input type="radio" class="megaphone-hidden megaphone-count-me" name="megaphone[sidebar][position]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['sidebar']['position'] );?>/>
					  		</li>
					  	<?php endforeach; ?>
			    	</ul>
			    	<small class="howto"><?php esc_html_e( 'Choose sidebar position', 'megaphone' ); ?></small>
			    	<br/>
			    </p>

			    <p>
				    <select name="megaphone[sidebar][classic]" class="megaphone-count-me">
					  	<?php foreach ( $sidebars as $id => $sidebar ): ?>
					  		<option class="megaphone-count-me" value="<?php echo esc_attr( $id ); ?>" <?php selected( $id, $meta['sidebar']['classic'] );?>><?php echo esc_html( $sidebar ); ?></option>
					  	<?php endforeach; ?>
				  	</select>
			    	<small class="howto"><?php esc_html_e( 'Choose a regular sidebar to display', 'megaphone' ); ?></small>
			    	<br/>
		    	</p>

		    	<p>
			    	<select name="megaphone[sidebar][sticky]" class="megaphone-count-me">
					  	<?php foreach ( $sidebars as $id => $sidebar ): ?>
					  		<option class="megaphone-count-me" value="<?php echo esc_attr( $id ); ?>" <?php selected( $id, $meta['sidebar']['sticky'] );?>><?php echo esc_html( $sidebar ); ?></option>
					  	<?php endforeach; ?>
				  	</select>
			    	<small class="howto"><?php esc_html_e( 'Choose a sticky sidebar to display', 'megaphone' ); ?></small>
			    	<br/>
		    	</p>
		    </td>
        </tr>

        

        <tr class="form-field megaphone-opt-display-custom <?php echo esc_attr( $class ); ?>">
        	<th scope="row" valign="top">
                <?php esc_html_e( 'Pagination', 'megaphone' ); ?>
            </th>
            <td>
			    <p>
			    	<ul class="megaphone-img-select-wrap">
					  	<?php foreach ( $paginations as $id => $layout ): ?>
					  		<li>
					  			<img src="<?php echo esc_url( $layout['src'] ); ?>" title="<?php echo esc_attr( $layout['alt'] ); ?>" class="megaphone-img-select <?php echo esc_attr( megaphone_selected( $id, $meta['pagination'], 'selected' ) ); ?>">
					  			<br/><span><?php echo esc_attr( $layout['alt'] ); ?></span>
					  			<input type="radio" class="megaphone-hidden megaphone-count-me" name="megaphone[pagination]" value="<?php echo esc_attr( $id ); ?>" <?php checked( $id, $meta['pagination'] );?>/>
					  		</li>
					  	<?php endforeach; ?>
				    </ul>
			    	<small class="howto"><?php esc_html_e( 'Choose a layout', 'megaphone' ); ?></small>
			    </p>
		    </td>
        </tr>

        <tr class="form-field">
            <th scope="row" valign="top">
                <?php esc_html_e( 'Image', 'megaphone' ); ?>
            </th>
            <td>
				<?php $display = $meta['image'] ? 'initial' : 'none'; ?>
                <p>
                    <img id="megaphone-image-preview" src="<?php echo esc_url( $meta['image'] ); ?>" style="display:<?php echo esc_attr( $display ); ?>;">
                </p>

                <p>
                    <input type="hidden" name="megaphone[image]" id="megaphone-image-url" value="<?php echo esc_url( $meta['image'] ); ?>"/>
                    <input type="button" id="megaphone-image-upload" class="button-secondary" value="<?php esc_html_e( 'Upload', 'megaphone' ); ?>"/>
                    <input type="button" id="megaphone-image-clear" class="button-secondary" value="<?php esc_html_e( 'Clear', 'megaphone' ); ?>" style="display:<?php echo esc_attr( $display ); ?>"/>
                </p>

                <p class="description"><?php esc_html_e( 'Upload an image for this category', 'megaphone' ); ?></p>
            </td>
        </tr>
		<?php
	}
endif;