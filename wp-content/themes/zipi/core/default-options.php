<?php
/**
 * Get default option by passing option id or don't pass anything to function and get all options
 *
 * @param string  $option
 * @return array|mixed|false
 * @param since   1.0
 */

if ( !function_exists( 'megaphone_get_default_option' ) ):
	function megaphone_get_default_option( $option = null ) {

		$translate = megaphone_get_translate_options();

		$defaults = array(

			// Header - General
			'header_layout' => '1',
			'header_height' => 100,
			'header_bottom_height' => 60,
			'header_orientation' => 'content', // content, window
			'header_main_nav' => true,
			'header_site_desc' => false,
			'header_actions' => array( 'donate' ),
			'header_actions_l' => array( 'search-modal' ),
			'header_actions_r' => array( 'donate' ),
			'logo' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_logo.png' ) ) ),
			'logo_retina' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_logo@2x.png' ) ) ),
			'logo_mini' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_logo_mini.png' ) ) ),
			'logo_mini_retina' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_logo_mini@2x.png' ) ) ),

			'header_indent' => '1',

			'logo_alt' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_logo_invert.png' ) ) ),
			'logo_alt_retina' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_logo_invert@2x.png' ) ) ),
			'logo_alt_mini' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_logo_invert_mini.png' ) ) ),
			'logo_alt_mini_retina' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_logo_invert_mini@2x.png' ) ) ),

			// Header - Top bar
			'header_top' => false,
			'header_top_l' => array( 'menu-secondary-1' ),
			'header_top_c' => array( ),
			'header_top_r' => array( 'social-modal', 'search-modal', 'subscribe-modal', 'hamburger' ),
			'header_top_height' => 50,

			// Header - Sticky
			'header_sticky' => true,
			'header_sticky_offset' => 300,
			'header_sticky_up' => false,
			'header_sticky_logo' => 'mini',
			'header_sticky_height' => 80,
			'header_sticky_layout' => '1',

			// Header - Misc
			'header_labels' => false,
			//Mega menu
			'mega_menu' => true,
			'mega_menu_ppp' => 6,

			// Header - Responsive
			'header_elements_responsive' => array( 'donate' ), // donate, search-modal, social-modal, subscribe-modal, cart
			'header_menu_elements_responsive' => array( 'social' ),


			//Header colors
			'color_header_top_bg' => '#f2f2f2',
			'color_header_top_txt' => '#2B1813',
			'color_header_top_acc' => '#FF4F00',

			'color_header_middle_bg' => '#ffffff',
			'color_header_middle_txt' => '#2B1813',
			'color_header_middle_acc' => '#ff4f00',

			'color_header_bottom_bg' => '#f6f6f6',
			'color_header_bottom_txt' => '#2B1813',
			'color_header_bottom_acc' => '#ff4f00',

			'color_header_sticky_bg' => '#ffffff',
			'color_header_sticky_txt' => '#2B1813',
			'color_header_sticky_acc' => '#ff4f00',

			// Content
			'color_bg' => '#ffffff',
			'color_h' => '#2B1813',
			'color_txt' => '#2B1813',
			'color_acc' => '#FF4F00',
			'color_meta' => '#2B1813',
			'color_bg_alt_1' => '#f2f2f2',
			'color_bg_alt_2' => '#424850',
			'overlays' => 'dark',

			// Sidebars
			'sidebars' => array(),
			'widget_bg' => 'alt-1',

			// Footer
			'footer_subscribe' => false,
			'footer_instagram' => false,
			'footer_instagram_front' => true,
			'footer_instagram_username' => 'unsplash',
			'footer_widgets' => true,
			'footer_widgets_layout' => '4',
			'footer_copyright' => wp_kses_post( sprintf( __( 'Created by %s &middot; Copyright {current_year} &middot; All rights reserved', 'megaphone' ), '<a href="https://mekshq.com" target="_blank">Meks</a>' ) ),

			'color_footer_bg' => '#42484F',
			'color_footer_txt' => '#FFF',
			'color_footer_acc' => '#FF4F00',
			'color_footer_meta' => '#FFF',


			// blog layouts
			'layout_a_meta_up' => 'category',
			'layout_a_meta_down' => array( 'author', 'date' ),
			'layout_a_excerpt' => true,
			'layout_a_excerpt_limit' => 250,
			'layout_a_excerpt_type' => 'auto',
			'layout_a_width' => '6',
			'layout_a_rm' => false,
			'layout_a_img_ratio' => '21_9',
			'layout_a_img_custom' => '',

			'layout_b_meta_up' => 'category',
			'layout_b_meta_down' => array( 'author', 'date' ),
			'layout_b_excerpt' => true,
			'layout_b_excerpt_limit' => 250,
			'layout_b_excerpt_type' => 'auto',
			'layout_b_width' => '9',
			'layout_b_rm' => false,
			'layout_b_img_ratio' => '4_3',
			'layout_b_img_custom' => '',

			'layout_c_meta_up' => 'category',
			'layout_c_meta_down' => array( 'author' ),
			'layout_c_excerpt' => true,
			'layout_c_excerpt_limit' => 250,
			'layout_c_rm' => false,
			'layout_c_img_ratio' => '16_9',
			'layout_c_img_custom' => '',

			'layout_d_meta_up' => 'category',
			'layout_d_meta_down' => array( 'date' ),
			'layout_d_excerpt' => true,
			'layout_d_excerpt_limit' => 150,
			'layout_d_rm' => false,
			'layout_d_img_ratio' => '4_3',
			'layout_d_img_custom' => '',

			'layout_e_meta_up' => 'category',
			'layout_e_meta_down' => array( 'author', 'date' ),
			'layout_e_excerpt' => true,
			'layout_e_excerpt_limit' => 250,
			'layout_e_rm' => true,
			'layout_e_img_ratio' => '16_9',
			'layout_e_img_custom' => '',

			'layout_f_meta_up' => 'category',
			'layout_f_meta_down' => array( 'author' ),
			'layout_f_rm' => true,
			'layout_f_img_ratio' => '1_1',
			'layout_f_img_custom' => '',

			// featured layouts
			'layout_fa_1_episode_number' => false,
			'layout_fa_1_episode_meta_up' => 'category',
			'layout_fa_1_episode_meta_down' => array( 'author', 'date' ),
			'layout_fa_1_episode_excerpt' => false,
			'layout_fa_1_episode_excerpt_limit' => '150',
			'layout_fa_1_episode_play_btn' => true,
			'layout_fa_1_episode_height' => 500,

			'layout_fa_2_episode_number' => true,
			'layout_fa_2_episode_meta_up' => 'category',
			'layout_fa_2_episode_meta_down' => array( 'author', 'date' ),
			'layout_fa_2_episode_excerpt' => false,
			'layout_fa_2_episode_excerpt_limit' => '150',
			'layout_fa_2_episode_play_btn' => true,
			'layout_fa_2_episode_img_ratio' => '16_9',
			'layout_fa_2_episode_img_custom' => '',

			// episode layouts
			'layout_a_episode_number' => true,
			'layout_a_episode_meta_up' => 'category',
			'layout_a_episode_meta_down' => array( 'author', 'date' ),
			'layout_a_episode_excerpt' => true,
			'layout_a_episode_excerpt_limit' => 250,
			'layout_a_episode_excerpt_type' => 'auto',
			'layout_a_episode_width' => '6',
			'layout_a_episode_play_btn' => true,
			'layout_a_episode_play_icon' => false,
			'layout_a_episode_img_type' => 'image', // image or box
			'layout_a_episode_img_ratio' => '21_9',
			'layout_a_episode_img_custom' => '',

			'layout_b_episode_number' => true,
			'layout_b_episode_meta_up' => 'category',
			'layout_b_episode_meta_down' => array( 'author', 'date' ),
			'layout_b_episode_excerpt' => true,
			'layout_b_episode_excerpt_limit' => 250,
			'layout_b_episode_excerpt_type' => 'auto',
			'layout_b_episode_width' => '9',
			'layout_b_episode_play_btn' => true,
			'layout_b_episode_play_icon' => false,
			'layout_b_episode_img_type' => 'image', // image or box
			'layout_b_episode_img_ratio' => '3_2',
			'layout_b_episode_img_custom' => '',

			'layout_c_episode_number' => true,
			'layout_c_episode_meta_up' => 'category',
			'layout_c_episode_meta_down' => array( 'author', 'date' ),
			'layout_c_episode_excerpt' => true,
			'layout_c_episode_excerpt_limit' => 250,
			'layout_c_episode_play_btn' => true,
			'layout_c_episode_play_icon' => false,
			'layout_c_episode_img_type' => 'image', // image or box
			'layout_c_episode_img_ratio' => '16_9',
			'layout_c_episode_img_custom' => '',

			'layout_d_episode_number' => true,
			'layout_d_episode_meta_up' => 'category',
			'layout_d_episode_meta_down' => array( ),
			'layout_d_episode_excerpt' => true,
			'layout_d_episode_excerpt_limit' => 140,
			'layout_d_episode_play_btn' => true,
			'layout_d_episode_play_icon' => false,
			'layout_d_episode_img_type' => 'image', // image or box
			'layout_d_episode_img_ratio' => '4_3',
			'layout_d_episode_img_custom' => '',

			'layout_e_episode_number' => true,
			'layout_e_episode_meta_up' => 'category',
			'layout_e_episode_meta_down' => array( 'author', 'date' ),
			'layout_e_episode_excerpt' => true,
			'layout_e_episode_excerpt_limit' => 100,
			'layout_e_episode_play_btn' => true,
			'layout_e_episode_play_icon' => false,
			'layout_e_episode_img_type' => 'image', // image or box
			'layout_e_episode_img_ratio' => '16_9',
			'layout_e_episode_img_custom' => '',

			'layout_f_episode_number' => true,
			'layout_f_episode_meta_up' => 'category',
			'layout_f_episode_meta_down' => array( 'author' ),
			'layout_f_episode_play_btn' => true,
			'layout_f_episode_play_icon' => false,
			'layout_f_episode_img_type' => 'image', // image or box
			'layout_f_episode_img_ratio' => '1_1',
			'layout_f_episode_img_custom' => '',

			'layout_g_episode_number' => true,
			'layout_g_episode_play_btn' => true,

			// show layout
			'show_layout_loop' => '4',
			'show_layout_sidebar_position' => 'right',
			'show_layout_sidebar_standard' => 'megaphone_sidebar_default',
			'show_layout_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
			'show_layout_display_image' => true,
			'show_layout_img_ratio' => '3_2',
			'show_layout_img_custom' => '',
			'show_layout_duotone_overlay' => false,
			'show_layout_meta' => true,
			'show_layout_description' => true,
			'show_layout_description_excerpt' => false,
			'show_layout_description_excerpt_limit' => '250',
			'show_layout_display_episodes' => true,
			'show_layout_display_episodes_limit' => '3',
			'show_layout_display_play_btn' => true,
			'show_layout_display_episodes_number' => true,
			'show_layout_display_view_episodes_btn' => true,
			'show_layout_categories' => array(),


			// Front page

			// general
			'front_page_template' => true,
			'front_page_sections' => array( 'episodes' ), // 'episodes', 'welcome', 'posts', featured 'shows',
			'front_page_general_custom_fields_number' => 1,
			'front_page_pagination' => 'numeric',

			// welcome/about area
			'front_page_wa_display_title' => false,
			'tr_front_page_wa_title' => $translate['front_page_wa_title']['text'],
			'front_page_wa_layout' => '2',
			'front_page_wa_img' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_default.jpg' ) ) ),
			'wa_layout_1_height' => 700,
			'wa_layout_2_height' => 700,
			'wa_layout_3_img_ratio' => '1_1', // hard coded
			'tr_front_page_wa_punchline' => $translate['front_page_wa_punchline']['text'],
			'tr_front_page_wa_text' => $translate['front_page_wa_text']['text'],
			'front_page_wa_cta' => false,
			'tr_front_page_wa_cta_label' => $translate['front_page_wa_cta_label']['text'],
			'front_page_wa_cta_url' => home_url( '/' ),
			'front_page_wa_latest_episode' => true,
			'front_page_wa_subscribe' => true,
			'front_page_wa_bg' => 'none',

			// subscribe on area

			// featured area
			'front_page_fa_display_title' => true,
			'tr_front_page_featured_title' => $translate['front_page_featured_title']['text'],
			'front_page_fa_loop' => '4',
			'front_page_fa_ppp' => '3',
			'front_page_fa_orderby' => 'date',
			'front_page_fa_cat' => array(),
			'front_page_fa_tag' => array(),
			'front_page_fa_unique' => true,
			'front_page_fa_view_all_link' => false,
			'tr_front_page_featured_view_all_link_label' => $translate['front_page_featured_view_all_link_label']['text'],
			'front_page_fa_view_all_link_url' => '',
			'front_page_fa_subscribe' => false,
			'front_page_fa_bg' => 'none',

			// blog/posts area
			'front_page_posts_display_title' => true,
			'tr_front_page_posts_title' => $translate['front_page_posts_title']['text'],
			'front_page_loop' => '6',
			'front_page_sidebar_position' => 'right',
			'front_page_sidebar_standard' => 'megaphone_sidebar_default',
			'front_page_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
			'front_page_ppp' => 'custom',
			'front_page_ppp_num' =>  get_option( 'posts_per_page' ),
			'front_page_cat' => array(),
			'front_page_tag' => array(),
			'front_page_posts_slider' => false,
			'front_page_posts_view_all_link' => false,
			'tr_front_page_posts_view_all_link_label' => $translate['front_page_posts_view_all_link_label']['text'],
			'front_page_posts_view_all_link_url' => '',
			'front_page_posts_bg' => 'none',

			// episodes area
			'front_page_episodes_display_title' => true,
			'tr_front_page_episodes_title' => $translate['front_page_episodes_title']['text'],
			'front_page_episodes_loop' => '5',
			'front_page_episodes_sidebar_position' => 'right',
			'front_page_episodes_sidebar_standard' => 'megaphone_sidebar_default',
			'front_page_episodes_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
			'front_page_episodes_ppp' => 'inherit',
			'front_page_episodes_ppp_num' => get_option( 'posts_per_page' ),
			'front_page_episodes_cat' => array(),
			'front_page_episodes_tag' => array(),
			'front_page_episodes_slider' => false,
			'front_page_episodes_view_all_link' => false,
			'tr_front_page_episodes_view_all_link_label' => $translate['front_page_episodes_view_all_link_label']['text'],
			'front_page_episodes_view_all_link_url' => '',
			'front_page_episodes_bg' => 'none',

			// shows area
			'front_page_shows_display_title' => true,
			'tr_front_page_shows_title' => $translate['front_page_shows_title']['text'],
			'front_page_shows_categories' => array(),
			'front_page_shows_slider' => true,
			'front_page_shows_view_all_link' => true,
			'tr_front_page_shows_view_all_link_label' => $translate['front_page_shows_view_all_link_label']['text'],
			'front_page_shows_view_all_link_url' => '',
			'front_page_shows_bg' => 'alt-1',

			// custom content area
			'front_page_custom_content_type' => 'this',
			'front_page_custom_content_page' => '',
			'front_page_custom_content_display_title' => true,
			'tr_front_page_custom_content_title' => $translate['front_page_custom_content_title']['text'],
			'tr_front_page_custom_content' => $translate['front_page_custom_content']['text'],
			'front_page_custom_content_bg' => 'none',
			
			// custom content area 2
			'front_page_custom_content_type_2' => 'this',
			'front_page_custom_content_page_2' => '',
			'front_page_custom_content_display_title_2' => true,
			'tr_front_page_custom_content_title_2' => $translate['front_page_custom_content_title_2']['text'],
			'tr_front_page_custom_content_2' => $translate['front_page_custom_content_2']['text'],
			'front_page_custom_content_bg_2' => 'none',

			// custom content area 3
			'front_page_custom_content_type_3' => 'this',
			'front_page_custom_content_page_3' => '',
			'front_page_custom_content_display_title_3' => true,
			'tr_front_page_custom_content_title_3' => $translate['front_page_custom_content_title_3']['text'],
			'tr_front_page_custom_content_3' => $translate['front_page_custom_content_3']['text'],
			'front_page_custom_content_bg_3' => 'none',
			
			// custom content area 4
			'front_page_custom_content_type_4' => 'this',
			'front_page_custom_content_page_4' => '',
			'front_page_custom_content_display_title_4' => true,
			'tr_front_page_custom_content_title_4' => $translate['front_page_custom_content_title_4']['text'],
			'tr_front_page_custom_content_4' => $translate['front_page_custom_content_4']['text'],
			'front_page_custom_content_bg_4' => 'none',

			// custom content area 5
			'front_page_custom_content_type_5' => 'this',
			'front_page_custom_content_page_5' => '',
			'front_page_custom_content_display_title_5' => true,
			'tr_front_page_custom_content_title_5' => $translate['front_page_custom_content_title_5']['text'],
			'tr_front_page_custom_content_5' => $translate['front_page_custom_content_5']['text'],
			'front_page_custom_content_bg_5' => 'none',


			// Single Blog
			'single_blog_layout' => '3',
			'single_blog_layout_1_img_ratio' => '21_9',
			'single_blog_layout_1_img_custom' => '',
			'single_blog_layout_2_img_ratio' => '21_9',
			'single_blog_layout_2_img_custom' => '',
			'single_blog_layout_3_height' => 600,
			'single_blog_layout_4_height' => 600,
			'single_blog_layout_5_img_ratio' => '1_1',
			'single_blog_sidebar_position' => 'none',
			'single_blog_sidebar_standard' => 'megaphone_sidebar_default',
			'single_blog_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
			'single_blog_width' => '6',
			'single_blog_meta_up' => 'category',
			'single_blog_meta_down' => array( 'author', 'date', 'rtime' ),
			'single_blog_fimg' => true,
			'single_blog_fimg_cap' => false,
			'single_blog_headline' => false,
			'single_blog_tags' => true,
			'single_blog_author' => 'bellow',
			'single_blog_related' => true,
			'single_blog_related_layout' => '5',
			'single_blog_related_limit' => 6,
			'single_blog_related_type' => 'cat',
			'single_blog_related_order' => 'date',

			// Single Podcast
			'single_podcast_layout' => '3',
			'single_podcast_layout_1_img_ratio' => '21_9',
			'single_podcast_layout_1_img_custom' => '',
			'single_podcast_layout_2_img_ratio' => '21_9',
			'single_podcast_layout_2_img_custom' => '',
			'single_podcast_layout_3_height' => 600,
			'single_podcast_layout_4_height' => 600,
			'single_podcast_layout_5_img_ratio' => '1_1',
			'single_podcast_sidebar_position' => 'none',
			'single_podcast_sidebar_standard' => 'megaphone_sidebar_default',
			'single_podcast_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
			'single_podcast_width' => '6',
			'single_podcast_subscribe' => false,
			'single_podcast_number' => true,
			'single_podcast_meta_up' => 'category',
			'single_podcast_meta_down' => array( 'author', 'date' ),
			'single_podcast_fimg' => true,
			'single_podcast_fimg_cap' => false,
			'single_podcast_headline' => false,
			'single_podcast_tags' => true,
			'single_podcast_author' => 'above',
			'single_podcast_show_more' => false,
			'single_podcast_show_more_height' => '700',
			'single_podcast_related' => true,
			'single_podcast_related_layout' => '5',
			'single_podcast_related_limit' => 6,
			'single_podcast_related_order' => 'date',

			/* Sponsored podcast */
			'podcast_sponsored' => true,
			'podcast_sponsored_tags' => array(),
			'podcast_sponsored_manual' => '',
			'podcast_sponsored_manual_ids' => '',

			// Page
			'page_layout' => '1',
			'page_layout_1_img_ratio' => '21_9',
			'page_layout_1_img_custom' => '',
			'page_layout_2_img_ratio' => '21_9',
			'page_layout_2_img_custom' => '',
			'page_layout_3_height' => 400,
			'page_layout_4_height' => 400,
			'page_sidebar_position' => 'right',
			'page_sidebar_standard' => 'megaphone_sidebar_default',
			'page_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
			'page_width' => '6',
			'page_fimg' => true,
			'page_fimg_cap' => false,

			// Archive
			'archive_layout' => '2',
			'archive_layout_height' => 400,
			'archive_loop' => '4',
			'archive_description' => true,
			'archive_meta' => true,
			'archive_ppp' => 'inherit',
			'archive_ppp_num' => get_option( 'posts_per_page' ),
			'archive_pagination' => 'load-more',
			'archive_sidebar_position' => 'right',
			'archive_sidebar_standard' => 'megaphone_sidebar_default',
			'archive_sidebar_sticky' => 'megaphone_sidebar_default_sticky',

			// Category
			'category_settings' => 'inherit',
			'category_layout' => '1',
			'category_layout_height' => 400,
			'category_loop' => '4',
			'category_description' => true,
			'category_meta' => true,
			'category_ppp' => 'inherit',
			'category_ppp_num' =>  get_option( 'posts_per_page' ),
			'category_pagination' => 'infinite-scroll',
			'category_sidebar_position' => 'right',
			'category_sidebar_standard' => 'megaphone_sidebar_default',
			'category_sidebar_sticky' => 'megaphone_sidebar_default_sticky',

			// Show
			'show_settings' => 'custom',
			'show_layout' => '2',
			'show_layout_height' => 500,
			'show_loop' => '4',
			'show_description' => true,
			'show_meta' => true,
			'show_ppp' => 'inherit',
			'show_ppp_num' => get_option( 'posts_per_page' ),
			'show_pagination' => 'infinite-scroll',
			'show_sidebar_position' => 'right',
			'show_sidebar_standard' => 'megaphone_sidebar_default',
			'show_sidebar_sticky' => 'megaphone_sidebar_default_sticky',

			// Typography
			'main_font' => array(
				'font-family' => 'Open Sans',
				'variant'  => '400',
				'font-weight' => '400'
			),

			'h_font' => array(
				'font-family' => 'PT Serif',
				'variant' => '700',
				'font-weight' => '700'
			),

			'nav_font' => array(
				'font-family' => 'Open Sans',
				'variant' => '600',
				'font-weight' => '600'
			),

			'button_font' => array(
				'font-family' => 'Open Sans',
				'variant' => '600',
				'font-weight' => '600'
			),

			'font_size_p' => '16',
			'font_size_small' => '14',
			'font_size_nav' => '14',
			'font_size_nav_ico' => '24',
			'font_size_section_title' => '32',
			'font_size_widget_title' => '24',
			'font_size_punchline' => '64',
			'font_size_h1' => '58',
			'font_size_h2' => '46',
			'font_size_h3' => '40',
			'font_size_h4' => '32',
			'font_size_h5' => '26',
			'font_size_h6' => '22',

			'uppercase' => array( '.megaphone-header li a', 'buttons' ),

			// Misc.
			'megaphone_podcast_setup' => 'theme_organization', // global option to set podcasts
			'blog_terms' => array(), // global option to enable standard posts in specific terms
			'podlove_terms' => array(), // podlove plugin categories
			'simple_terms' => array(), // simple podcasting plugin series
			'episodes_post_order' =>  'DESC',
			'episode_number_from_title' => false,
			'episode_number_format' => '', // preg match : Episode number:, Episode #number:
			'strip_episode_number_from_title' => false,
			'play_button_is_link' => false,
			'player_special_elements' => array( 'subscribe', 'share' ),
			'player_prev_next' => 'asc',
			'player_supports' => array( 
				'.wp-block-embed-soundcloud', 
				'.wp-block-embed-mixcloud', 
				'.wp-block-embed-spotify', 
				'.wp-block-embed-youtube', 
				'iframe-stitcher.com', 
				'iframe-podbean.com', 
				'iframe-libsyn.com', 
				'iframe-spreaker.com', 
				'iframe-podomatic.com', 
				'iframe-blogtalkradio.com', 
				'iframe-cadence13.com',
				'iframe-megaphone.fm',
				'iframe-ivoox.com',
				'iframe-buzzsprout.com',
				'iframe-anchor.fm',
				'iframe-simplecast.com',
				'iframe-iheart.com',
				'iframe-tunein.com',
				'iframe-ausha.co'
			),
			'player_height' => 120,
			'default_fimg' => array( 'url' => esc_url( get_parent_theme_file_uri( '/assets/img/megaphone_default.jpg' ) ) ),
			'404_image' => '',
			'breadcrumbs' => 'none',
			'rtl_mode' => false,
			'rtl_lang_skip' => '',
			'more_string' => '...',
			'words_read_per_minute' => 180,
			'popup' => true,
			'go_to_top' => false,
			//'primary_category' => true,

			// Ads
			'ad_header' => '',
			'ad_above_archive' => '',
			'ad_above_singular' => '',
			'ad_above_footer' => '',
			'ad_between_posts' => '',
			'ad_between_position' => 6,
			'ad_exclude' => array(),

			// WooCommerce
			'woocommerce_sidebar_position' => 'right',
			'woocommerce_sidebar_standard' => 'megaphone_sidebar_default',
			'woocommerce_sidebar_sticky' => 'megaphone_sidebar_default_sticky',
			'woocommerce_cart_force' => true,

			// Translation Options
			'enable_translate' => true,

			// Performance
			'minify_css' => true,
			'minify_js' => true,
			'disable_img_sizes' => array(),
		);

		foreach ( $translate as $string_key => $item ) {

			if ( isset( $item['hidden'] ) ) {
				continue;
			}

			if ( isset( $item['default'] ) ) {
				$defaults['tr_' . $string_key] = $item['default'];
			}
		}


		$defaults = apply_filters( 'megaphone_modify_default_options', $defaults );

		if ( empty( $option ) ) {
			return $defaults;
		}

		if ( isset( $defaults[$option] ) ) {
			return $defaults[$option];
		}


		return false;
	}
endif;
