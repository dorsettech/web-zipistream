<?php get_header(); ?>



<div class="megaphone-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-6">
                <div class="megaphone-content-alt text-center m-mc mb-40 mb-sm-15 md-md-20">
                    <h1><?php echo megaphone_get( 'title' ); ?></h1>
                </div>

                <?php if (megaphone_get( '404_image' ) ) : ?>
                    <div class="entry-media mb-40 mb-md-30 mb-sm-15 text-center">
                        <img src="<?php echo esc_attr( megaphone_get( '404_image' ) ); ?>" alt="404-image" > 
                    </div>
                <?php endif; ?>

                <p><?php echo megaphone_get( 'text' ); ?></p>
                
                <div class="search-alt">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>

    </div>
</div>




<?php get_footer(); ?>