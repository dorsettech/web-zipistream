(function($) {

    "use strict";

    var megaphone_app = {

        settings: {

            admin_bar: {
                height: 0,
                position: ''
            },

            pushes: {
                url: [],
                up: 0,
                down: 0
            },

            window_last_top: 0,
            infinite_allow: true,
            play_allow: true
        },

        init: function() {
            this.align_full_fix();
            this.align_wide_fix();
            this.admin_bar_check();
            this.sticky_header();
            this.sliders();
            this.pagination();
            this.check_history();
            this.overlays();
            this.sidebar_margin_top_fix();
            this.sidebar();
            this.go_to_top();
            this.popup();
            this.player();
            this.reverse_menu();
            this.accordion_widget();
            this.mega_menu_width();
            this.object_fit_fix();
        },

        resize: function() {
            this.admin_bar_check();
            this.align_full_fix();
            this.align_wide_fix();
            this.sidebar_margin_top_fix();
            this.sidebar();
            this.mega_menu_width();
        },

        scroll: function() {
            this.sticky_header();
        },

        admin_bar_check: function() {

            if ($('#wpadminbar').length && $('#wpadminbar').is(':visible')) {
                this.settings.admin_bar.height = $('#wpadminbar').height();
                this.settings.admin_bar.position = $('#wpadminbar').css('position');
            }

        },

        sticky_header: function() {

            if (!megaphone_js_settings.header_sticky) {
                return false;
            }

            var sticky_top = this.settings.admin_bar.position == 'fixed' ? this.settings.admin_bar.height : 0;
            var top = $(window).scrollTop();
            var last_top = this.settings.window_last_top;

            $('.header-sticky').css('top', sticky_top);

            if (megaphone_js_settings.header_sticky_up) {

                if (last_top > top && top >= megaphone_js_settings.header_sticky_offset) {
                    if (!$("body").hasClass('megaphone-header-sticky-active')) {
                        $("body").addClass("megaphone-header-sticky-active");
                    }
                } else {
                    if ($("body").hasClass('megaphone-header-sticky-active')) {
                        $("body").removeClass("megaphone-header-sticky-active");
                    }
                }

            } else {

                if (top >= megaphone_js_settings.header_sticky_offset) {
                    if (!$("body").hasClass('megaphone-header-sticky-active')) {
                        $("body").addClass("megaphone-header-sticky-active");
                    }

                } else {
                    if ($("body").hasClass('megaphone-header-sticky-active')) {
                        $("body").removeClass("megaphone-header-sticky-active");
                    }
                }
            }

            this.settings.window_last_top = top;

        },

        align_full_fix: function() {

            if (!$('body').hasClass('megaphone-sidebar-none')) {
                return;
            }

            var style = '.alignfull { width: ' + $(window).width() + 'px; margin-left: -' + $(window).width() / 2 + 'px; margin-right: -' + $(window).width() / 2 + 'px; left: 50%; right:50%; position: relative; max-width: unset; }';

            if ($('#megaphone-align-fix').length) {
                $('#megaphone-align-fix').html(style);
            } else {
                $('head').append('<style id="megaphone-align-fix" type="text/css">' + style + '</style>');
            }

        },

        align_wide_fix: function() {
            if (!$('body').hasClass('megaphone-sidebar-none')) {
                return;
            }

            var style = '.alignwide { max-width: ' + $('.megaphone-section .container').width() + 'px; width: 100vw; position: relative; left: 50%; -webkit-transform: translateX(-50%); -ms-transform: translateX(-50%); transform: translateX(-50%);}';

            if ($('#megaphone-alignwide-fix').length) {
                $('#megaphone-alignwide-fix').html(style);
            } else {
                $('head').append('<style id="megaphone-alignwide-fix" type="text/css">' + style + '</style>');
            }
        },

        accordion_widget: function() {

            $('.widget').find('.menu-item-has-children > a, .page_item_has_children > a, .cat-parent > a').after('<span class="megaphone-accordion-nav"><i class="mf mf-chevron-down"></i></span>');

            $('.widget').on('click', '.megaphone-accordion-nav', function() {
                $(this).closest('li').find('ul.sub-menu:first, ul.children:first').slideToggle('fast').parent().toggleClass('active');
            });

        },

        sliders: function() {

            if (!$('.megaphone-slider').length) {
                return;
            }

            $('body').imagesLoaded(function() {

                $('.megaphone-slider').each(function() {

                    var owl = $(this).addClass('owl-carousel');
                    var has_nav = $(this).hasClass('has-arrows') ? true : false;
                    var has_center = $(this).hasClass('slider-center') ? true : false;

                    var col_element_classes = owl.children().first().attr('class');
                    var items = col_element_classes.match(/col-(\d+)/);
                    items = items === null ? 1 : 12 / items[1];
                    var items_sm = col_element_classes.match(/col-sm-(\d+)/);
                    items_sm = items_sm === null ? items : 12 / items_sm[1];
                    var items_md = col_element_classes.match(/col-md-(\d+)/);
                    items_md = items_md === null ? items_sm : 12 / items_md[1];
                    var items_lg = col_element_classes.match(/col-lg-(\d+)/);
                    items_lg = items_lg === null ? items_md : 12 / items_lg[1];

                    var responsive_opts = {};

                    if (!$(this).closest('.widget').length) {
                        responsive_opts[megaphone_js_settings.grid.breakpoint.sm] = { items: items_sm, margin: megaphone_js_settings.grid.gutter.sm };
                        responsive_opts[megaphone_js_settings.grid.breakpoint.md] = { items: items_md, margin: megaphone_js_settings.grid.gutter.md };
                        responsive_opts[megaphone_js_settings.grid.breakpoint.lg] = { items: items_lg, margin: megaphone_js_settings.grid.gutter.lg };
                        responsive_opts[megaphone_js_settings.grid.breakpoint.xl] = { items: items_lg, margin: megaphone_js_settings.grid.gutter.xl };
                    }


                    owl.owlCarousel({
                        rtl: megaphone_js_settings.rtl_mode ? true : false,
                        loop: true,
                        items: items,
                        margin: megaphone_js_settings.grid.gutter.sm,
                        stagePadding: 0,
                        nav: has_nav,
                        center: has_center,
                        navSpeed: 150,
                        navText: ['<a href="javascript:void(0);" class="megaphone-button-circle prev"><i class="mf mf-chevron-left"></i></a>', '<a href="javascript:void(0);" class="megaphone-button-circle next"><i class="mf mf-chevron-right"></i></a>'],
                        onInitialized: function(event) {
                            var target = $(event.currentTarget);
                            target.removeClass('row');
                            target.find('.owl-item').each(function() {
                                $(this).children().removeClass();
                            });
                        },
                        responsive: responsive_opts
                    });

                    if (!has_nav) {
                        $(this).closest('.container').find('.megaphone-slider-nav a').on('click', function() {
                            console.log('click');
                            if ($(this).hasClass('prev')) {
                                owl.trigger('prev.owl.carousel');
                            } else {
                                owl.trigger('next.owl.carousel');
                            }
                        });
                    }

                });
            });

        },

        pagination: function() {

            $('body').on('click', '.megaphone-pagination.load-more > a', function(e) {
                e.preventDefault();

                megaphone_app.load_more_items({
                    opener: $(this),
                    url: $(this).attr('href'),
                    next_url_selector: '.megaphone-pagination.load-more > a'
                }, function() {});

            });

            if (!$('.megaphone-pagination.megaphone-infinite-scroll').length) {
                return false;
            }

            $(window).scroll(function() {

                if (megaphone_app.settings.infinite_allow && $('.megaphone-pagination').length) {


                    var pagination = $('.megaphone-pagination');
                    var opener = pagination.find('a');

                    if ($(this).scrollTop() > (pagination.offset().top - $(this).height() - 200)) {

                        megaphone_app.settings.infinite_allow = false;

                        megaphone_app.load_more_items({
                            opener: opener,
                            url: opener.attr('href'),
                            next_url_selector: '.megaphone-pagination.megaphone-infinite-scroll a'
                        }, function() {
                            megaphone_app.settings.infinite_allow = true;
                        });

                    }
                }
            });

        },

        load_more_items: function(args, callback) {

            $('.megaphone-pagination').toggleClass('megaphone-loader-active');

            var defaults = {
                opener: '',
                url: '',
                next_url_selector: '.load-more > a'
            };

            var options = $.extend({}, defaults, args);

            $("<div>").load(options.url, function() {

                var next_url = $(this).find(options.next_url_selector).attr('href');
                var next_title = $(this).find('title').text();
                var new_items = $(this).find('.megaphone-items').children();
                var container = options.opener.closest('.section-content').find('.megaphone-items');

                new_items.imagesLoaded(function() {

                    new_items.hide().appendTo(container).fadeIn();

                    if (next_url !== undefined) {
                        $(options.next_url_selector).attr('href', next_url);

                    } else {
                        $(options.next_url_selector).closest('.megaphone-pagination').parent().fadeOut('fast').remove();
                    }

                    var push_obj = {
                        prev: window.location.href,
                        next: options.url,
                        offset: $(window).scrollTop(),
                        prev_title: window.document.title,
                        next_title: next_title
                    };

                    megaphone_app.push_state(push_obj);

                    $('.megaphone-pagination').toggleClass('megaphone-loader-active');
                    megaphone_app.sidebar();

                    callback();

                });
            });
        },

        push_state: function(args) {
            var defaults = {
                    prev: window.location.href,
                    next: '',
                    offset: $(window).scrollTop(),
                    prev_title: window.document.title,
                    next_title: window.document.title,
                    increase_counter: true
                },
                push_object = $.extend({}, defaults, args);

            if (push_object.increase_counter) {
                megaphone_app.settings.pushes.up++;
                megaphone_app.settings.pushes.down++;
            }
            delete(push_object.increase_counter);

            megaphone_app.settings.pushes.url.push(push_object);
            window.document.title = push_object.next_title;
            window.history.pushState(push_object, '', push_object.next);
        },

        check_history: function() {

            if (!$('.megaphone-pagination.load-more').length && !$('.megaphone-pagination.megaphone-infinite-scroll').length) {
                return false;
            }

            megaphone_app.push_state({
                increase_counter: false
            });

            var last_up, last_down = 0;

            $(window).scroll(function() {

                if (megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.up].offset !== last_up && $(window).scrollTop() < megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.up].offset) {
                    last_up = megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.up].offset;
                    last_down = 0;
                    window.document.title = megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.up].prev_title;
                    window.history.replaceState(megaphone_app.settings.pushes.url, '', megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.up].prev); //1

                    megaphone_app.settings.pushes.down = megaphone_app.settings.pushes.up;
                    if (megaphone_app.settings.pushes.up !== 0) {
                        megaphone_app.settings.pushes.up--;
                    }
                }

                if (megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.down].offset !== last_down && $(window).scrollTop() > megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.down].offset) {
                    last_down = megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.down].offset;
                    last_up = 0;

                    window.document.title = megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.down].next_title;
                    window.history.replaceState(megaphone_app.settings.pushes.url, '', megaphone_app.settings.pushes.url[megaphone_app.settings.pushes.down].next);

                    megaphone_app.settings.pushes.up = megaphone_app.settings.pushes.down;
                    if (megaphone_app.settings.pushes.down < megaphone_app.settings.pushes.url.length - 1) {
                        megaphone_app.settings.pushes.down++;
                    }

                }
            });

        },

        sidebar_margin_top_fix: function() {

            $('body').imagesLoaded(function() {

                if($('.megaphone-sidebar').length){

                    $('.megaphone-section .megaphone-sidebar').each(function() {

                        if (window.matchMedia('(min-width: ' + megaphone_js_settings.grid.breakpoint.lg + 'px)').matches) {

                            var section_header_height = $(this).parent().parent().find('.section-head').outerHeight(true);
                            $(this).css('margin-top', section_header_height + 'px');
                        }
                    });
                    
                }
            });
        },

        sidebar: function() {

            if ($('.megaphone-sticky').length || $('.widget-sticky').length) {

                if (window.matchMedia('(min-width: ' + megaphone_js_settings.grid.breakpoint.lg + 'px)').matches && $('.widget-sticky').length && !$('.megaphone-sticky').length) {

                    $('.megaphone-sidebar').each(function() {
                        if ($(this).find('.widget-sticky').length) {
                            $(this).find('.widget-sticky').wrapAll('<div class="megaphone-sticky"></div>');
                        }
                    });

                }

                $('body').imagesLoaded(function() {


                    var sticky_sidebar = $('.megaphone-sticky');
                    var top_padding = window.matchMedia('(min-width: ' + megaphone_js_settings.grid.breakpoint.xl + 'px)').matches ? megaphone_js_settings.grid.gutter.xl : megaphone_js_settings.grid.gutter.lg;

                    sticky_sidebar.each(function() {

                        var content = $(this).closest('.section-content').find('.megaphone-content-height');
                        var parent = $(this).parent();

                        var sticky_offset = $('.megaphone-header.header-sticky').length && !megaphone_js_settings.header_sticky_up ? $('.megaphone-header.header-sticky').outerHeight() : 0;

                        var admin_bar_offset = megaphone_app.settings.admin_bar.position == 'fixed' ? megaphone_app.settings.admin_bar.height : 0;
                        var offset_top = sticky_offset + admin_bar_offset + top_padding;

                        var widgets = $(this).children().addClass('widget-sticky');

                        if (window.matchMedia('(min-width: ' + megaphone_js_settings.grid.breakpoint.lg + 'px)').matches) {

                            if(content.height() <= parent.height() ){
                                
                                parent.css('height', 'auto');
                                parent.css('min-height', '1px');

                            } else{

                                parent.height( content.height() );

                                $(this).stick_in_parent({
                                    offset_top: offset_top
                                });
                            }

                            megaphone_app.masonry_widgets();

                        } else {
                            parent.css('height', 'auto');
                            parent.css('min-height', '1px');
                            widgets.unwrap();
                            megaphone_app.masonry_widgets();
                        }

                    });
                });

            } else {
                megaphone_app.masonry_widgets();
            }

        },

        mega_menu_width: function() {
            var container_width = $('.header-middle > .container').width();
            $('.megaphone-header .megaphone-mega-menu > ul').innerWidth(container_width);
        },

        masonry_widgets: function() {

            if (!$('.megaphone-sidebar').not('.megaphone-sidebar-hidden').length) {
                return false;
            }

            $('body').imagesLoaded(function() {

                var sidebar = $('.megaphone-sidebar').not('.megaphone-sidebar-hidden');

                sidebar.each(function() {
                    if (window.matchMedia('(min-width: ' + megaphone_js_settings.grid.breakpoint.lg + 'px)').matches) {
                        if ($(this).hasClass('has-masonry')) {
                            $(this).removeClass('has-masonry').masonry('destroy');
                        }
                    } else {
                        $(this).addClass('has-masonry').masonry({
                            columnWidth: '.col-md-6',
                            percentPosition: true
                        });
                    }
                });
            });

        },

        overlays: function() {

            //Sidebar
            $('body').on('click', '.megaphone-hamburger a', function(e) {
                e.preventDefault();
                var top = megaphone_app.settings.admin_bar.position == 'fixed' || $(window).scrollTop() == 0 ? megaphone_app.settings.admin_bar.height : 0;
                $('.megaphone-sidebar-hidden').css('top', top);
                $('body').addClass('overlay-lock overlay-sidebar-open');

            });

            $('body').on('click', '.megaphone-action-close', function() {
                $('body').removeClass('overlay-lock overlay-sidebar-open');

            });

            //Popup header action
            $('body').on('click', '.megaphone-action-close', function() {
                $('body').removeClass('overlay-lock overlay-sidebar-open');
            });

            $('body').on('click', '.megaphone-action-overlay', function(e) {
                e.preventDefault();
                if (e.target === this) {
                    $('body').removeClass('overlay-lock overlay-sidebar-open');
                }
            });


            //Modal open
            $('body').on('click', '.megaphone-modal-opener a', function(e) {
                e.preventDefault();

                $(this).closest('.megaphone-modal-opener').next().addClass('modal-open');
                $('.megaphone-header-sticky-active .header-sticky').addClass('header-sticky-modal');
                $('body').addClass('overlay-lock');

            });


            //Modal close
            $('body').on('click', '.megaphone-modal-close', function(e) {
                e.preventDefault();
                $(this).closest('.megaphone-modal').removeClass('modal-open');
                $('body').removeClass('overlay-lock');
                $('.header-sticky').removeClass('header-sticky-modal');
            });

            $(document).keyup(function(e) {
                if (e.keyCode == 27 && $('body').hasClass('overlay-lock')) {
                    $('body').removeClass('overlay-lock');
                    $('.megaphone-modal').removeClass('modal-open');
                }
            });

            $(document).keyup(function(e) {
                if (e.keyCode == 27 && $('body').hasClass('overlay-sidebar-open')) {
                    $('body').removeClass('overlay-lock overlay-sidebar-open');
                }
            });

            //Modal close
            $('body').on('click', '.megaphone-show-more-link', function(e) {
                e.preventDefault();
                $('.megaphone-hidden-content').toggleClass('megaphone-show-hidden-content');
                $('.megaphone-show-more-link i').toggleClass('mf-chevron-up');

            });


        },

        player: function() {


            $('body').on('click', 'a.megaphone-play', function(e) {

                e.preventDefault();

                if( !megaphone_app.settings.play_allow ) {         
                    return;
                }

                var current = $(this);
                var current_id = current.attr('data-play-id');
                var current_buttons = $('.megaphone-play-' + current_id);
                var link = current.attr('href');
                var player_status = megaphone_app.get_player_status();

                if( current.hasClass('button-disabled') ){
                    return;
                }

                if (current.hasClass('megaphone-is-playing')) {
                    current_buttons.removeClass('megaphone-is-playing');
                    current_buttons.find('.mf').toggleClass('mf-play mf-pause');

                    if (player_status == 'audio') {
                        window.meks_ap_player.pause();
                    }

                    return;
                }

                $('.megaphone-is-playing').find('.mf').toggleClass('mf-play mf-pause');
                $('.megaphone-is-playing').removeClass(' button-disabled megaphone-is-playing');

                current_buttons.addClass('megaphone-is-playing');
                current_buttons.find('.mf').toggleClass('mf-play mf-pause');

                //Check if we just need to continue playing current video
                var player_data_id = $('#meks-ap-player').attr('data-playing-id');

                if (player_data_id == current_id && player_status == 'audio') {

                    window.meks_ap_player.play();

                    return;
                }


                //Adding preloader
                $('.meks-ap .megaphone-loader').remove();
                $('.meks-ap').prepend('<div class="megaphone-loader"><div class="megaphone-ellipsis"><div></div><div></div><div></div><div></div></div></div>');

                $('body').removeClass('megaphone-player-hidden').addClass('megaphone-player-loader');

                //New play

                if (player_status == 'audio') {
                    window.meks_ap_player.pause();
                    window.meks_ap_player.remove();
                }

                $('.meks-ap-slot-l').html('');
                $('#meks-ap-player').html('');
                $('#meks-ap-player').attr('data-playing-id', current_id);

                $('.meks-ap').removeClass('meks-ap-collapsed megaphone-player-has-embed');

                
                megaphone_app.settings.play_allow = false;

                $('<div>').load(link, function() {

                    var new_content = $(this);

                    // BuzzSprout fix
                    if ( new_content.html().match(/buzzsprout-player-/g) ) {
                        
                        var buzzsprout = new_content.find('div[id^="buzzsprout-player-"]');
                        var buzzsprout_div = buzzsprout[0];
                        var buzzsprout_script = buzzsprout.next().attr('src');

                        $('body').append(buzzsprout_div);

                        $.ajax({
                            method: "GET",
                            url: buzzsprout_script,
                            dataType: "script"
                        }).done(function(response){

                            window.meks_ap_detect_audio($('body'));
                            player_status = megaphone_app.get_player_status();

                            if ( player_status == 'audio') {
                                window.meks_ap_player.play();
                            } else {
                                current_buttons.addClass('button-disabled');
                                if(!player_status){
                                    $('.meks-ap-player').html( '<p>' + megaphone_js_settings.audio_error_msg + '</p>');
                                }
                            }

                        });

                    } else {

                        window.meks_ap_detect_audio(new_content);
                        player_status = megaphone_app.get_player_status();

                        if ( player_status == 'audio') {
                            window.meks_ap_player.play();
                        } else {
                            current_buttons.addClass('button-disabled');
                            if(!player_status){
                                $('.meks-ap-player').html( '<p>' + megaphone_js_settings.audio_error_msg + '</p>');
                            }
                        }
    
                    }
                    
                    $('.meks-ap-slot-l').html( new_content.find('.meks-ap-slot-l').html() );
                    $('body').removeClass('megaphone-player-loader');
                    megaphone_app.settings.play_allow = true;

                });


            });


            /* Sync player play/pause with other buttons on the page */

            $('body').on('click', '#meks-ap-player .mejs-playpause-button', function(e) {

                var player_data_id = $('#meks-ap-player').attr('data-playing-id');

                $('.megaphone-play-' + player_data_id).first().click();

            });


            /* Check what we should display depending on audio format */

             $(document).on('meks_ap_player_loaded', function() {

                var status = megaphone_app.get_player_status();

                if ($('.megaphone-play-current').length && status == 'audio') {
                    $('.megaphone-play-current').addClass('megaphone-play-visible');
                } 

                if( status != 'audio'){
                    $('.meks-ap').addClass('megaphone-player-has-embed');
                    $('.meks-ap .download-button').removeAttr('herf').addClass('disabled');
                } else {
                    $('.meks-ap .download-button').attr('href',  $('.meks-ap-player audio').attr('src') ).removeClass('disabled');
                }

                $('body').removeClass('megaphone-player-hidden');

            });



            /* Player ended - we need to reset some stuff */
            $(document).on('meks_ap_player_ended', function() {

                var player_data_id = $('#meks-ap-player').attr('data-playing-id');
                var current_buttons = $('.megaphone-play-' + player_data_id);
             
                current_buttons.removeClass('megaphone-is-playing');
                current_buttons.find('.mf').toggleClass('mf-play mf-pause');

            });



        },

        get_player_status: function() {

            if (typeof window.meks_ap_player_status === 'undefined') {
                return false;
            }

            return window.meks_ap_player_status;

        },

        go_to_top: function() {

            if (!megaphone_js_settings.go_to_top || !$('#megaphone-goto-top').length) {
                return;
            }

            $('body').imagesLoaded(function() {

                var visible = $(window).scrollTop() >= 400 ? true : false;

                if (visible) {
                    $('#megaphone-goto-top').fadeTo(0, 0.8);
                } else {
                    $('#megaphone-goto-top').fadeOut(100);
                }

                $(window).scroll(function() {

                    setTimeout(function() {

                        if (!visible && $(this).scrollTop() >= 400) {
                            visible = true;
                            $('#megaphone-goto-top').fadeTo(0, 0.8);
                        }

                        if (visible && $(this).scrollTop() < 400) {
                            visible = false;
                            $('#megaphone-goto-top').fadeOut(100);
                        }
                    }, 700);

                });

                $('body').on('click', '#megaphone-goto-top', function() {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });

            });

        },

        popup: function() {

            if (!megaphone_js_settings.popup) {
                return false;
            }

            $('body').on('click', '.wp-block-gallery .blocks-gallery-item a, .wp-block-image a', function(e) {

                if (!/\.(?:jpg|jpeg|gif|png|webp)$/i.test($(this).attr('href'))) {
                    return;
                }

                e.preventDefault();

                var pswpElement = document.querySelectorAll('.pswp')[0];
                var items = [];
                var index = 0;
                var opener = $(this);
                var is_gallery = opener.closest('.wp-block-gallery').length;
                var links = is_gallery ? $(this).closest('.wp-block-gallery').find('.blocks-gallery-item a') : $('.wp-block-image a');

                $.each(links, function(ind) {

                    if (opener.attr('href') == $(this).attr('href')) {
                        index = ind;
                    }

                    var link = $(this);
                    var item = {};
                    var image = new Image();
                    image.onload = function() {

                        item = {
                            src: link.attr('href'),
                            w: image.naturalWidth,
                            h: image.naturalHeight,
                            title: is_gallery ? link.closest('.blocks-gallery-item').find('figcaption').html() : link.closest('.wp-block-image').find('figcaption').html()
                        };

                        items.push(item);

                        if (ind == (links.length - 1)) {

                            var options = {
                                history: false,
                                index: index,
                                preload: [2, 2],
                                captionEl: true,
                                fullscreenEl: false,
                                zoomEl: false,
                                shareEl: false,
                                preloaderEl: true,
                                closeOnScroll: false
                            };

                            var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                            gallery.init();
                        }

                    };

                    if (/\.(?:jpg|jpeg|gif|png|webp)$/i.test(link.attr('href'))) {
                        image.src = link.attr('href');
                    }


                });


            });



        },

        reverse_menu: function() {

            $('.megaphone-header ul li').on('mouseenter', 'ul li', function(e) {

                if ($(this).find('ul').length) {

                    var rt = $(window).width() - ($(this).find('ul').offset().left + $(this).find('ul').outerWidth());
                    if (rt < 0) {
                        $(this).find('ul').addClass('megaphone-rev');
                    }

                }
            });

        },

        object_fit_fix: function() {

            $('body').imagesLoaded(function() {
                objectFitImages('.owl-item img, .entry-media img');
            });

        }

    };

    $(document).ready(function() {
        megaphone_app.init();

    });

    $(window).resize(function() {
        megaphone_app.resize();
    });

    $(window).scroll(function() {
        megaphone_app.scroll();
    });

})(jQuery);