(function($) {

    "use strict";

    $(document).ready(function() {

        /* Enable/disable slider and sidebar basend on current layout */
        $('body').on('click', '.megaphone-opt-layouts img.megaphone-img-select', function(e) {
            e.preventDefault();
            var sidebar = parseInt($(this).data('sidebar'));

            if( sidebar ){
                $('.megaphone-opt-sidebar').removeClass('megaphone-opt-disabled');
            } else {
                $('.megaphone-opt-sidebar').addClass('megaphone-opt-disabled');
            }

        });


        /* Image upload */
        var meta_image_frame;

        $('body').on('click', '#megaphone-image-upload', function(e) {

            e.preventDefault();

            if (meta_image_frame) {
                meta_image_frame.open();
                return;
            }

            meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
                title: 'Choose your image',
                button: {
                    text: 'Set Category image'
                },
                library: {
                    type: 'image'
                }
            });

            meta_image_frame.on('select', function() {

                var media_attachment = meta_image_frame.state().get('selection').first().toJSON();
                $('#megaphone-image-url').val(media_attachment.url);
                $('#megaphone-image-preview').attr('src', media_attachment.url);
                $('#megaphone-image-preview').show();
                $('#megaphone-image-clear').show();

            });

            meta_image_frame.open();
        });


        $('body').on('click', '#megaphone-image-clear', function(e) {
            $('#megaphone-image-preview').hide();
            $('#megaphone-image-url').val('');
            $(this).hide();
        });

    });

})(jQuery);