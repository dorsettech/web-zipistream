(function($) {

    "use strict";

    $(document).ready(function() {

        /* Metabox switch - do not show every metabox for every template */

        var megaphone_is_gutenberg = typeof wp.apiFetch !== 'undefined';
        var megaphone_template_selector = megaphone_is_gutenberg ? '.editor-page-attributes__template select' : '#page_template';

        if (megaphone_is_gutenberg) {

            var post_id = $('#post_ID').val();
            wp.apiFetch({ path: '/wp/v2/pages/' + post_id, method: 'GET' }).then(function(obj) {
                megaphone_template_metaboxes(obj.template);
            });

        } else {
            megaphone_template_metaboxes(false);
        }

        $('body').on('change', megaphone_template_selector, function(e) {
            megaphone_template_metaboxes(false);
        });

        function megaphone_template_metaboxes(t) {

            var template = t ? t : $(megaphone_template_selector).val();

            if (template == 'template-blank.php' || template == 'template-shows.php' ) {
                $('#megaphone_page_display').fadeOut(300);
                $('#megaphone_page_authors').fadeOut(300);
            } else if ( template == 'template-authors.php' ) {
                $('#megaphone_page_display').fadeIn(300);
                $('#megaphone_page_authors').fadeIn(300);
            } else {
                $('#megaphone_page_display').fadeIn(300);
                $('#megaphone_page_authors').fadeOut(300);
            }

        }

    });
})(jQuery);