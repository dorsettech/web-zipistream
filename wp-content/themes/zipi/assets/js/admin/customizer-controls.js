(function($) {

    "use strict";

    wp.customize.bind( 'ready', function() { 

        var customize = this; // WordPress customize object alias.

        // customize.previewer.save()
        // customize.previewer.send('refresh');

        /* FRONT PAGE GENERAL OPTIONS */
        var front_page_sections  = $('#customize-control-megaphone_settings-front_page_sections').find('li');
        var custom_section_input = $('#customize-control-megaphone_settings-front_page_general_custom_fields_number').find('input[type="text"]');

        if ( custom_section_input.length == 0 ) {
            
            $('#accordion-section-megaphone_front_general').on('click', function(){

                // Handle all sections
                custom_section_input     = $('#customize-control-megaphone_settings-front_page_general_custom_fields_number').find('input[type="text"]');
                var custom_section_count = parseInt( custom_section_input.val() );
                var ids                  = [ 'custom-content-2', 'custom-content-3', 'custom-content-4', 'custom-content-5' ];
                var filtered_ids         = ids.slice( custom_section_count - 1,  ids.length );
        
                front_page_sections.each( function() {
                    var $this = $(this);
                    if ( filtered_ids.indexOf( $this.attr('data-value') ) != -1 ) {
                        $this.hide().addClass('megaphone-force-hide');
                    } 
                });

                $('body').on('change', '#customize-control-megaphone_settings-front_page_general_custom_fields_number input', function(){
                    var $self = $(this);
                    var contents_minus = ids.slice( parseInt($self.val()) - 1,  ids.length );

                    megaphone_update_options_panel();
                    
                    front_page_sections.each( function() {
                        var $this = $(this);
                        if ( contents_minus.indexOf( $this.attr('data-value') ) != -1 ) {
                            $this.hide().addClass('megaphone-force-hide');
                            if ( ! $this.hasClass('invisible') ){
                                $this.find('i.visibility').click();
                            }
                        } else {
                            $this.show().removeClass('megaphone-force-hide');
                        }
                    });
                   
                });

            });
        }

        /* FRONT PAGE SECTIONS */
        var check_database_options = true;
        var refresh_interval = '';
        var is_published = false;
        var refresh_force = false;
        var refresh_force_interval = '';

        $('#accordion-panel-megaphone_panel_front').on('click', megaphone_update_options_panel);
        
        $('#customize-save-button-wrapper #save').on('click', function() {
            var check_input_val = $('#customize-control-megaphone_settings-front_page_general_custom_fields_number').find('input[type="text"]');
            if ( check_input_val.length > 0 ) {
                refresh_interval = setInterval(function(){
                    megaphone_update_options_panel();
                    //console.log('publish');
                },500);
                is_published = true;
                refresh_force = true;
            }
        });

        
        $('body').on('click', '#sub-accordion-panel-megaphone_panel_front .customize-panel-back, .accordion-section.control-panel', function() {
            clearInterval(refresh_interval);
            clearInterval(refresh_force_interval);
            is_published = false;
        });


        function megaphone_update_options_panel() {

            $('#accordion-section-megaphone_front_custom_content_2').hide().addClass('megaphone-force-hide');
            $('#accordion-section-megaphone_front_custom_content_3').hide().addClass('megaphone-force-hide');
            $('#accordion-section-megaphone_front_custom_content_4').hide().addClass('megaphone-force-hide');
            $('#accordion-section-megaphone_front_custom_content_5').hide().addClass('megaphone-force-hide');

            if ( !check_database_options ) {
                var custom_section_fields_count = $('#customize-control-megaphone_settings-front_page_general_custom_fields_number').find('input[type="text"]').val();
                for (var index = 2; index <= custom_section_fields_count; index++) {
                    $('#accordion-section-megaphone_front_custom_content_'+index).show().removeClass('megaphone-force-hide');
                }

                if ( refresh_force && !is_published ) {
                    refresh_force_interval = setInterval(function(){
                        megaphone_update_options_panel();
                        //console.log('force');
                    },500);
                    refresh_force = false;
                }
            }

            if ( check_database_options ) {

                for (var index = 2; index <= megaphone_customizer_settings.custom_fields_count; index++) {
                    $('#accordion-section-megaphone_front_custom_content_'+index).show().removeClass('megaphone-force-hide');
                }
                check_database_options = false;
            }
        }


        // var synced = true;
        // wp.customize.previewer.bind('synced', function() { 
        //     if ( synced ) {
        //         setTimeout( function(){
        //             megaphone_update_options_panel();
        //         },200);
        //         check_database_options = true;
        //         synced = false;
        //     }
        // });

    });
    
})(jQuery);