(function($) {

    "use strict";

    wp.customize('megaphone_settings[header_height]', function(value) {
        value.bind(function(newvalue) {
            $('.header-middle > .container').height(newvalue);
        });
    });

    wp.customize('megaphone_settings[header_bottom_height]', function(value) {
        value.bind(function(newvalue) {
            $('.header-bottom > .container').height(newvalue);
        });
    });

    wp.customize('megaphone_settings[header_top_height]', function(value) {
        value.bind(function(newvalue) {
            $('.header-top > .container').height(newvalue);
        });
    });

    wp.customize('megaphone_settings[header_sticky_height]', function(value) {
        value.bind(function(newvalue) {
            $('.header-sticky-main > .container').height(newvalue);
        });
    });

    wp.customize('megaphone_settings[player_height]', function(value) {
        value.bind(function(newvalue) {
            $('.meks-ap').height(newvalue);
            $('.meks-ap-collapsed, .megaphone-player-hidden .meks-ap').css('bottom', -(newvalue));
            
        });
    });

})(jQuery);