<?php

//Fonts
$main_font = megaphone_get_option( 'main_font', 'font' );
$h_font = megaphone_get_option( 'h_font', 'font' );
$nav_font = megaphone_get_option( 'nav_font', 'font' );
$button_font = megaphone_get_option( 'button_font', 'font' );

//Font sizes
$font_size_p = absint( megaphone_get_option( 'font_size_p' ));
$font_size_small = absint( megaphone_get_option( 'font_size_small' ));
$font_size_nav = absint( megaphone_get_option( 'font_size_nav' ));
$font_size_punchline = absint( megaphone_get_option( 'font_size_punchline' ));

$font_size_h1 = absint( megaphone_get_option( 'font_size_h1' ));
$font_size_h2 = absint( megaphone_get_option( 'font_size_h2' ));
$font_size_h3 = absint( megaphone_get_option( 'font_size_h3' ));
$font_size_h4 = absint( megaphone_get_option( 'font_size_h4' ));
$font_size_h5 = absint( megaphone_get_option( 'font_size_h5' ));
$font_size_h6 = absint( megaphone_get_option( 'font_size_h6' ));

//Colors

$color_bg = megaphone_get_option('color_bg');
$color_h = megaphone_get_option('color_h');
$color_txt = megaphone_get_option('color_txt');
$color_acc = megaphone_get_option('color_acc');
$color_meta = megaphone_get_option('color_meta');

$color_bg_alt_1 = megaphone_get_option('color_bg_alt_1');
$color_bg_alt_2 = megaphone_get_option('color_bg_alt_2');

$color_dark_txt = megaphone_is_color_light( $color_bg ) ? $color_txt : $color_bg;
$color_button_txt = megaphone_is_color_light( $color_acc ) ? $color_dark_txt : '#fff';
$color_button_hover = megaphone_is_color_light( $color_txt ) ? $color_dark_txt : '#fff';

$color_dark_alt_txt_2 = megaphone_is_color_light( $color_bg_alt_2 ) ? $color_dark_txt : '#fff';
$color_dark_alt_txt_1 = megaphone_is_color_light( $color_bg_alt_1 ) ? $color_dark_txt : '#fff';
$blog_content_width = megaphone_get_option('single_blog_width');


//Grid vars
$grid = megaphone_grid_vars();

?>

.edit-post-visual-editor.editor-styles-wrapper {
	line-height: 1.625;
	color: <?php echo esc_attr( $color_txt ); ?>;
	background: <?php echo esc_attr( $color_bg ); ?>;
}
.edit-post-visual-editor.editor-styles-wrapper{
	font-family: <?php echo wp_kses_post( $main_font['font-family'] ); ?>, Arial, sans-serif;
	font-weight: <?php echo esc_attr( $main_font['font-weight'] ); ?>;
	<?php if ( isset( $main_font['font-style'] ) && !empty( $main_font['font-style'] ) ):?>
	font-style: <?php echo esc_attr( $main_font['font-style'] ); ?>;
	<?php endif; ?>
}

.edit-post-visual-editor.editor-styles-wrapper p{
  line-height: 1.625;
}

.editor-styles-wrapper h1,
.editor-styles-wrapper.edit-post-visual-editor .editor-post-title__block .editor-post-title__input,
.editor-styles-wrapper h2,
.editor-styles-wrapper h3,
.editor-styles-wrapper h4,
.editor-styles-wrapper h5,
.editor-styles-wrapper h6,
.wp-block-cover .wp-block-cover-image-text, 
.wp-block-cover .wp-block-cover-text, 
.wp-block-cover h2, 
.wp-block-cover-image .wp-block-cover-image-text, 
.wp-block-cover-image .wp-block-cover-text, 
.wp-block-cover-image h2,
.entry-category a,
.entry-summary,
p.has-drop-cap:not(:focus)::first-letter,
.wp-block-heading h1,
.wp-block-heading h2,
.wp-block-heading h3,
.wp-block-heading h4,
.wp-block-heading h5,
.wp-block-heading h6 {
	font-family: <?php echo wp_kses_post( $h_font['font-family'] ); ?>, Arial, sans-serif;
	font-weight: <?php echo esc_attr( $h_font['font-weight'] ); ?>;
	<?php if ( isset( $h_font['font-style'] ) && !empty( $h_font['font-style'] ) ):?>
	font-style: <?php echo esc_attr( $h_font['font-style'] ); ?>;
	<?php endif; ?>
}
b,
strong{
	font-weight: <?php echo esc_attr( $h_font['font-weight'] ); ?>; 
}

.editor-styles-wrapper h1,
.editor-styles-wrapper.edit-post-visual-editor .editor-post-title__block .editor-post-title__input,
.editor-styles-wrapper h2,
.editor-styles-wrapper h3,
.editor-styles-wrapper h4,
.editor-styles-wrapper h5,
.editor-styles-wrapper h6,
.has-large-font-size,
.wp-block-heading h1,
.wp-block-heading h2,
.wp-block-heading h3,
.wp-block-heading h4,
.wp-block-heading h5,
.wp-block-heading h6 {
	color: <?php echo esc_attr( $color_h ); ?>;	
}


.editor-rich-text__tinymce a,
.editor-writing-flow a,
.wp-block-freeform.block-library-rich-text__tinymce a{
	color: <?php echo esc_attr( $color_txt ); ?>;	
	border-bottom:1px solid <?php echo esc_attr( $color_acc ); ?>;
}


.wp-block-button .wp-block-button__link,
.wp-block-search__button{
	font-family: <?php echo wp_kses_post( $button_font['font-family'] ); ?>, Arial, sans-serif;
	font-weight: <?php echo esc_attr( $button_font['font-weight'] ); ?>;
	<?php if ( isset( $button_font['font-style'] ) && !empty( $button_font['font-style'] ) ):?>
	font-style: <?php echo esc_attr( $button_font['font-style'] ); ?>;
	<?php endif; ?>
	font-size:<?php echo esc_attr( $font_size_small ); ?>px;
}

.editor-styles-wrapper.edit-post-visual-editor .editor-post-title__block .editor-post-title__input::-webkit-input-placeholder,
.block-editor-block-list__layout .block-editor-default-block-appender>.block-editor-default-block-appender__content{
	color: <?php echo esc_attr( $color_txt ); ?>;		
}
.editor-styles-wrapper .wp-block button, 
.editor-styles-wrapper .wp-block input,
.editor-styles-wrapper .wp-block optgroup, 
.editor-styles-wrapper .wp-block select, 
.editor-styles-wrapper .wp-block textarea{
	color:initial;
}



/* Blocks */

tr {
	border-bottom: 1px solid <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;
}
.wp-block-table.is-style-stripes tr:nth-child(odd){
	background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;
}
.wp-block-button .wp-block-button__link{
    background-color: <?php echo esc_attr( $color_acc ); ?>; 
    color: <?php echo esc_attr( $color_bg ); ?>;
}

.wp-block .wp-block-button.is-style-outline .wp-block-button__link{
    border: 1px solid <?php echo esc_attr( $color_txt); ?>;
    color: <?php echo esc_attr( $color_txt); ?>;    
}
.wp-block-button.is-style-outline .wp-block-button__link:hover{
    border: 1px solid <?php echo esc_attr( $color_acc ); ?>; 
    color: <?php echo esc_attr( $color_acc ); ?>; 
    background: 0 0;   
}

.is-style-outline .wp-block-button__link {
    background: 0 0;
    color:<?php echo esc_attr( $color_acc ); ?>;
    border: 2px solid currentcolor;
}
.wp-block-quote:before{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.07 ) ); ?>;
}
.wp-block-pullquote:not(.is-style-solid-color){
	color: <?php echo esc_attr( $color_txt); ?>;
	border-color: <?php echo esc_attr( $color_acc ); ?>;  
}
.wp-block-pullquote{
	background-color: <?php echo esc_attr( $color_acc ); ?>;  
	color: <?php echo esc_attr( $color_bg); ?>; 	
}
.editor-styles-wrapper .wp-block-pullquote.alignfull.is-style-solid-color{
	box-shadow: -526px 0 0 <?php echo esc_attr( $color_acc); ?>, -1052px 0 0 <?php echo esc_attr( $color_acc); ?>,
	526px 0 0 <?php echo esc_attr( $color_acc); ?>, 1052px 0 0 <?php echo esc_attr( $color_acc ); ?>; 
}
.editor-styles-wrapper .wp-block  pre,
.editor-styles-wrapper .wp-block code{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.05 ) ); ?>;  
}
.editor-styles-wrapper .wp-block-separator{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.05 ) ); ?>;
}
.editor-styles-wrapper .wp-block-tag-cloud a{
    background-color: <?php echo esc_attr( $color_bg_alt_1); ?>;
    color: <?php echo esc_attr( $color_dark_txt); ?>;    
}
.wp-block-rss__item-author, .wp-block-rss__item-publish-date{
    color: <?php echo esc_attr( $color_bg); ?>; 
}
.wp-block-calendar tfoot a{
    color: <?php echo esc_attr( $color_meta); ?>; 
}

.editor-styles-wrapper .wp-block-quote:before{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.07 ) ); ?>;
}

.editor-styles-wrapper .wp-block .wp-block-quote:before {
	-webkit-mask: url( <?php echo esc_attr(get_parent_theme_file_uri('/assets/img/razor_shape_big.svg')); ?>) no-repeat;
    mask: url( <?php echo esc_attr(get_parent_theme_file_uri('/assets/img/razor_shape_big.svg')); ?>) no-repeat;
}

.editor-styles-wrapper .wp-block-search__input{
    border: 1px solid <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;
}
.editor-styles-wrapper .wp-block-search .wp-block-search__button{
	background-color: <?php echo esc_attr( $color_acc); ?>;	
	color: <?php echo esc_attr( $color_bg); ?>;	
}
.editor-styles-wrapper .wp-block-latest-comments__comment-meta,
.editor-styles-wrapper .wp-block-latest-posts__post-date{
    color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.5 ) ); ?>; 
}

.editor-styles-wrapper .wp-block-archives .count,
.editor-styles-wrapper .wp-block-categories .count{
    background-color: <?php echo esc_attr( $color_acc ); ?>;
    color: <?php echo esc_attr( $color_button_txt ); ?>; 
}


.editor-styles-wrapper .wp-block-pullquote,
.editor-styles-wrapper .wp-block-pullquote:not(.is-style-solid-color){
	border-top: 4px solid <?php echo esc_attr( $color_acc ); ?>; 
	border-bottom: 4px solid <?php echo esc_attr( $color_acc ); ?>; 
}

.editor-styles-wrapper code, 
.editor-styles-wrapper kbd, 
.editor-styles-wrapper pre, 
.editor-styles-wrapper samp,
.editor-styles-wrapper .wp-block-code,
.editor-styles-wrapper code,
.editor-styles-wrapper pre{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.05 ) ); ?>;
    font-size: 14px; 	
}
.wp-block-code .editor-plain-text{
	background-color: transparent;
}
.editor-styles-wrapper .wp-block-code,
.editor-styles-wrapper code,
.editor-styles-wrapper pre,
.editor-styles-wrapper pre h2{
	color: <?php echo esc_attr( $color_txt ); ?>;
}
.wp-block-rss__item-author, .wp-block-rss__item-publish-date{
	color: <?php echo esc_attr( $color_meta); ?>; 	
}


/* Mobile Font sizes */

.edit-post-visual-editor.editor-styles-wrapper {
	font-size:<?php echo esc_attr( $font_size_p ); ?>px;
}

.editor-styles-wrapper h1,
.wp-block-heading h1{
	font-size:26px;
}
.editor-styles-wrapper h2,
.wp-block-heading h2,
.editor-styles-wrapper.edit-post-visual-editor .editor-post-title__block .editor-post-title__input{
	font-size:24px;
}
.editor-styles-wrapper h3,
.wp-block-heading h3{
	font-size:22px;
}
.editor-styles-wrapper h4,
.wp-block-cover .wp-block-cover-image-text,
.wp-block-cover .wp-block-cover-text,
.wp-block-cover h2,
.wp-block-cover-image .wp-block-cover-image-text,
.wp-block-cover-image .wp-block-cover-text,
.wp-block-cover-image h2,
.wp-block-heading h4{
	font-size:20px;
}
.editor-styles-wrapper h5,
.wp-block-heading h5{
	font-size:18px;
}
.editor-styles-wrapper h6,
.wp-block-heading h6  {
	font-size:16px;
}
.wp-block-pullquote blockquote > .block-editor-rich-text p{
    font-size: 24px;
    line-height: 1.67;
}
.wp-block-quote.is-large p, 
.wp-block-quote.is-style-large p{
	font-size:22px;
}

@media (min-width: <?php echo esc_attr($grid['breakpoint']['md']); ?>px) and (max-width: <?php echo esc_attr($grid['breakpoint']['lg']); ?>px){ 

.editor-styles-wrapper h1,
.wp-block-heading h1{
	font-size:40px;
}
.editor-styles-wrapper h2,
.wp-block-heading h2,
.editor-styles-wrapper.edit-post-visual-editor .editor-post-title__block .editor-post-title__input{
	font-size:32px;
}
.editor-styles-wrapper h3,
.wp-block-heading h3{
	font-size:28px;
}
.editor-styles-wrapper h4,
.wp-block-cover .wp-block-cover-image-text,
.wp-block-cover .wp-block-cover-text,
.wp-block-cover h2,
.wp-block-cover-image .wp-block-cover-image-text,
.wp-block-cover-image .wp-block-cover-text,
.wp-block-cover-image h2,
.wp-block-heading h4{
	font-size:24px;
}
.editor-styles-wrapper h5,
.wp-block-heading h5{
	font-size:20px;
}
.editor-styles-wrapper h6,
.wp-block-heading h6  {
	font-size:18px;
}
}


/* Desktop Font sizes */
@media (min-width: <?php echo esc_attr($grid['breakpoint']['lg']); ?>px){ 

.edit-post-visual-editor.editor-styles-wrapper{
	font-size:<?php echo esc_attr( $font_size_p ); ?>px;
}
.editor-styles-wrapper h1,
.wp-block-heading h1 {
	font-size:<?php echo esc_attr( $font_size_h1 ); ?>px;
}

.editor-styles-wrapper h2,
.editor-styles-wrapper.edit-post-visual-editor .editor-post-title__block .editor-post-title__input,
.wp-block-heading h2 {
	font-size:<?php echo esc_attr( $font_size_h2 ); ?>px;
}
.editor-styles-wrapper h3,
.wp-block-heading h3 {
	font-size:<?php echo esc_attr( $font_size_h3 ); ?>px;
}
.editor-styles-wrapper h4,
.wp-block-cover .wp-block-cover-image-text,
.wp-block-cover .wp-block-cover-text,
.wp-block-cover h2,
.wp-block-cover-image .wp-block-cover-image-text,
.wp-block-cover-image .wp-block-cover-text,
.wp-block-cover-image h2,
.wp-block-heading h4 {
	font-size:<?php echo esc_attr( $font_size_h4 ); ?>px;
}
.editor-styles-wrapper h5,
.wp-block-heading h5 {
	font-size:<?php echo esc_attr( $font_size_h5 ); ?>px;
}

.editor-styles-wrapper h6,
.wp-block-heading h6 {
	font-size:<?php echo esc_attr( $font_size_h6 ); ?>px;
}
.wp-block-quote.is-large p, 
.wp-block-quote.is-style-large p{
	font-size:26px;
}
}

.wp-block{
	max-width: <?php echo esc_attr(megaphone_size_by_col( $blog_content_width ) + 28); ?>px;
}

<?php if( in_array( megaphone_get_option('single_blog_layout'), array(1,3) ) ):  ?>
.wp-block.editor-post-title__block{
	max-width: 746px;
}
.wp-block.editor-post-title__block .editor-post-title__input{
	text-align: center;
}

@media (min-width: 600px){
.block-library-list.editor-rich-text.block-editor-rich-text{
	margin-left: -20px;
}
}

<?php endif; ?>

<?php
/* Uppercase option */
$uppercase_defaults = megaphone_get_typography_uppercase_options();
$uppercase = megaphone_get_option('uppercase');

foreach ( $uppercase_defaults as $key => $value ) {
    if ( in_array( $key, $uppercase ) ) {
        echo '.editor-styles-wrapper '.esc_attr( $key ) .'{ text-transform: uppercase;}';
    } else {
        echo '.editor-styles-wrapper '.esc_attr( $key ) .'{ text-transform: none;}';
    }
}
?>
