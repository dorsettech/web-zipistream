<?php

//Fonts
$main_font = megaphone_get_option( 'main_font', 'font' );
$h_font = megaphone_get_option( 'h_font', 'font' );
$nav_font = megaphone_get_option( 'nav_font', 'font' );
$button_font = megaphone_get_option( 'button_font', 'font' );

//Font sizes
$font_size_p = number_format( absint( megaphone_get_option( 'font_size_p' ) ) / 10,  1 );
$font_size_small = number_format( absint( megaphone_get_option( 'font_size_small' ) ) / 10,  1 );
$font_size_nav = number_format( absint( megaphone_get_option( 'font_size_nav' ) ) / 10,  1 );
$font_size_nav_ico = number_format( absint( megaphone_get_option( 'font_size_nav_ico' ) ) / 10,  1 );
$font_size_section_title = number_format( absint( megaphone_get_option( 'font_size_section_title' ) ) / 10,  1 );
$font_size_widget_title = number_format( absint( megaphone_get_option( 'font_size_widget_title' ) ) / 10,  1 );
$font_size_punchline = number_format( absint( megaphone_get_option( 'font_size_punchline' ) ) / 10,  1 );

$font_size_h1 = number_format( absint( megaphone_get_option( 'font_size_h1' ) ) / 10,  1 );
$font_size_h2 = number_format( absint( megaphone_get_option( 'font_size_h2' ) ) / 10,  1 );
$font_size_h3 = number_format( absint( megaphone_get_option( 'font_size_h3' ) ) / 10,  1 );
$font_size_h4 = number_format( absint( megaphone_get_option( 'font_size_h4' ) ) / 10,  1 );
$font_size_h5 = number_format( absint( megaphone_get_option( 'font_size_h5' ) ) / 10,  1 );
$font_size_h6 = number_format( absint( megaphone_get_option( 'font_size_h6' ) ) / 10,  1 );

//Colors
$color_header_top_bg = megaphone_get_option('color_header_top_bg');
$color_header_top_txt = megaphone_get_option('color_header_top_txt');
$color_header_top_acc = megaphone_get_option('color_header_top_acc');
$header_top_height = megaphone_get_option('header_top_height');

$color_header_middle_bg = megaphone_get_option('color_header_middle_bg');
$color_header_middle_txt = megaphone_get_option('color_header_middle_txt');
$color_header_middle_acc = megaphone_get_option('color_header_middle_acc');


$color_header_bottom_bg = megaphone_get_option('color_header_bottom_bg');
$color_header_bottom_txt = megaphone_get_option('color_header_bottom_txt');
$color_header_bottom_acc = megaphone_get_option('color_header_bottom_acc');

$color_header_sticky_bg = megaphone_get_option('color_header_middle_bg');
$color_header_sticky_txt = megaphone_get_option('color_header_middle_txt');
$color_header_sticky_acc = megaphone_get_option('color_header_middle_acc');

$color_bg = megaphone_get_option('color_bg');
$color_h = megaphone_get_option('color_h');
$color_txt = megaphone_get_option('color_txt');
$color_acc = megaphone_get_option('color_acc');
$color_meta = megaphone_get_option('color_meta');

$color_bg_alt_1 = megaphone_get_option('color_bg_alt_1');
$color_bg_alt_2 = megaphone_get_option('color_bg_alt_2');

$color_footer_bg = megaphone_get_option('color_footer_bg');
$color_footer_txt = megaphone_get_option('color_footer_txt');
$color_footer_acc = megaphone_get_option('color_footer_acc');
$color_footer_meta = megaphone_get_option('color_footer_meta');

$color_dark_txt = megaphone_is_color_light( $color_bg ) ? $color_txt : $color_bg;
$color_button_txt = megaphone_is_color_light( $color_acc ) ? $color_dark_txt : '#fff';
$color_button_hover = megaphone_is_color_light( $color_txt ) ? $color_dark_txt : '#fff';

$color_dark_alt_txt_2 = megaphone_is_color_light( $color_bg_alt_2 ) ? $color_dark_txt : '#fff';
$color_dark_alt_txt_1 = megaphone_is_color_light( $color_bg_alt_1 ) ? $color_dark_txt : '#fff';
//Other
$header_height = absint( megaphone_get_option('header_height') );
$header_bottom_height = absint( megaphone_get_option('header_bottom_height') );
$single_show_more_height = absint( megaphone_get_option('single_podcast_show_more_height') );
$header_sticky_height = absint(megaphone_get_option('header_sticky_height'));

//Grid vars
$grid = megaphone_grid_vars();

?>

body,
#cancel-comment-reply-link{
	color: <?php echo esc_attr( $color_txt ); ?>;
	background: <?php echo esc_attr( $color_bg ); ?>;
}

body,
#cancel-comment-reply-link,
.meks-ap{
	font-family: <?php echo wp_kses_post( $main_font['font-family'] ); ?>, Arial, sans-serif;
	font-weight: <?php echo esc_attr( $main_font['font-weight'] ); ?>;
	<?php if ( isset( $main_font['font-style'] ) && !empty( $main_font['font-style'] ) ):?>
	font-style: <?php echo esc_attr( $main_font['font-style'] ); ?>;
	<?php endif; ?>
}

/* Typography */
h1,h2,h3,h4,h5,h6,
.h1,.h2,.h3,.h4,.h5,.h6,.h7,.h8,
.fn,
p.has-drop-cap:not(:focus)::first-letter{
	font-family: <?php echo wp_kses_post( $h_font['font-family'] ); ?>, Arial, sans-serif;
	font-weight: <?php echo esc_attr( $h_font['font-weight'] ); ?>;
	<?php if ( isset( $h_font['font-style'] ) && !empty( $h_font['font-style'] ) ):?>
	font-style: <?php echo esc_attr( $h_font['font-style'] ); ?>;
	<?php endif; ?>
}
h1,h2,h3,h4,h5,h6,
.h1,.h2,.h3,.h4,.h5,.h6,
.has-large-font-size{
    color: <?php echo esc_attr( $color_h ); ?>;
}

.entry-title a,
.show-title a{
	color: <?php echo esc_attr( $color_txt ); ?>;
}
.meta-sponsored span{
    color: <?php echo esc_attr( $color_acc ); ?>;   
}
.megaphone-overlay .meta-sponsored span{
    color: #FFF;       
}

/* Mobile font sizes */

body{
	font-size:1.6rem;
}
.megaphone-header{
	font-size:<?php echo esc_attr( $font_size_nav ); ?>rem;
}


h1, .h1{
	font-size:2.6rem;
}
h2, .h2,
.has-large-font-size,
.has-huge-font-size, 
.has-larger-font-size{
	font-size:2.4rem;
}
h3, .h3{
	font-size:2.2rem;
}
h4, .h4{
	font-size:2rem;
}
h5, .h5{
	font-size:1.8rem;
}
h6, .h6,
.megaphone-show .megaphone-shows-list-small .entry-title {
	font-size:1.6rem;
}
.megaphone-items .megaphone-shows-list-medium .entry-title{
    font-size:1.8rem;
}
.h7{
    font-size: 2rem;
}
.h8,
.section-content .megaphone-menu-subscribe .header-el-label{
    font-size: 1.6rem;
}

.section-title{
    font-size: 2rem;
}
.h0{
	font-size:3rem;    
}
.author-header .text-small,
.text-small {
    font-size: 1.2rem;
}

.widget-title,
.author-header .h8,
.mks_author_widget h3{
    font-size:<?php echo esc_attr( $font_size_widget_title - 0.2 ); ?>rem; 
}
.archive-label{
    font-size: 1.6rem;
}
.entry-content .meks_ess_share_label h5{
    font-size:<?php echo esc_attr( $font_size_widget_title - 0.6 ); ?>rem; 
}
.widget{
    font-size:<?php echo esc_attr( $font_size_small ); ?>rem;
}
.header-mobile>.container{
    height: 60px;
}
.megaphone-player-paceholder-big .megaphone-placeholder-title,
.megaphone-player-paceholder-medium .megaphone-placeholder-title{
    font-size: 1.8rem;
}
.megaphone-placeholder-label{
    color: <?php echo esc_attr( $color_txt ); ?>;  
}
.megaphone-button,
input[type="submit"],
input[type="button"],
button[type="submit"],
.megaphone-pagination a,
ul.page-numbers a,
.meks-instagram-follow-link .meks-widget-cta,
.mks_autor_link_wrap a,
.mks_read_more a,
.paginated-post-wrapper a,
.entry-content .megaphone-button,
.megaphone-pagination a, 
.page-numbers.current{
    font-size: 1.2rem;
}
.header-sticky-main > .container{
    height: 60px;
}
.megaphone-menu-action .mf{
    font-size:<?php echo esc_attr( $font_size_nav_ico ); ?>rem;
}
@media (min-width: <?php echo esc_attr($grid['breakpoint']['sm']); ?>px){ 

}

@media (min-width: <?php echo esc_attr($grid['breakpoint']['md']); ?>px){ 

.header-mobile>.container{
    height: 80px;
}
body.megaphone-header-indent .megaphone-header + .megaphone-section{
    margin-top: -80px;
}
body.megaphone-header-indent .megaphone-header + .megaphone-section .overlay-container{
    padding-top: 50px;
}
.header-sticky-main > .container{
    height: 70px;
}
.megaphone-placeholder-label{
    color: <?php echo esc_attr( $color_acc ); ?>;     
}
.megaphone-button,
input[type="submit"],
input[type="button"],
button[type="submit"],
.megaphone-pagination a,
ul.page-numbers a,
.meks-instagram-follow-link .meks-widget-cta,
.mks_autor_link_wrap a,
.mks_read_more a,
.paginated-post-wrapper a,
.entry-content .megaphone-button,
.megaphone-pagination a, 
.page-numbers.current{
    font-size: <?php echo esc_attr( $font_size_small - 0.2 ); ?>rem;
}

.widget-title,
.author-header .h8,
.mks_author_widget h3{
    font-size:<?php echo esc_attr( $font_size_widget_title ); ?>rem; 
}
.archive-label{
    font-size: 1.6rem;
}

}

/* Specific Font sizes on mobile devices */
@media (max-width: <?php echo esc_attr($grid['breakpoint']['sm']); ?>px){
	.megaphone-overlay .h1,
	.megaphone-overlay .h2,
	.megaphone-overlay .h3,
	.megaphone-overlay .h4,
	.megaphone-overlay .h5{
		font-size: 2.2rem;
	}
}

@media (max-width: <?php echo esc_attr($grid['breakpoint']['md']); ?>px){ 
    .megaphone-layout-c .h4,
    .megaphone-layout-d .h5,
    .megaphone-layout-e .h4{
        font-size: 2.2rem;
    }
    .megaphone-layout-f .h4{
        font-size:1.6rem;
    }
}


@media (min-width: <?php echo esc_attr($grid['breakpoint']['md']); ?>px) and (max-width: <?php echo esc_attr($grid['breakpoint']['lg']); ?>px){ 
.h0{
	font-size:4.6rem;
}
h1, .h1{
	font-size:4rem;
}
h2, .h2{
	font-size:3.2rem;
}
h3, .h3{
	font-size:2.8rem;
}
h4, .h4,
.wp-block-cover .wp-block-cover-image-text,
.wp-block-cover .wp-block-cover-text,
.wp-block-cover h2,
.wp-block-cover-image .wp-block-cover-image-text,
.wp-block-cover-image .wp-block-cover-text,
.wp-block-cover-image h2{
	font-size:2.4rem;
}
h5, .h5{
	font-size:2rem;
}
h6, .h6,
.megaphone-show .megaphone-shows-list-small .entry-title{
	font-size:1.8rem;
}

.section-title {
	font-size:2.8rem;
}


.megaphone-site-branding .site-title.logo-img-none{
    font-size: 3rem;
}

.megaphone-layout-c .h4,
.megaphone-layout-d .h5,
.megaphone-layout-e .h4{
       font-size: 2.8rem;
}
.megaphone-layout-f .h4{
    font-size:2.4rem;
}
}

/* Desktop font sizes */
@media (min-width: <?php echo esc_attr($grid['breakpoint']['lg']); ?>px){ 
    body{
	    font-size:<?php echo esc_attr( $font_size_p ); ?>rem;
    }
    .h0{
        font-size:<?php echo esc_attr( $font_size_punchline ); ?>rem;
    }   
    h1, .h1{
        font-size:<?php echo esc_attr( $font_size_h1 ); ?>rem;
    }
    h2, .h2{
        font-size:<?php echo esc_attr( $font_size_h2 ); ?>rem;
    }
    h3, .h3{
        font-size:<?php echo esc_attr( $font_size_h3 ); ?>rem;
    }
    h4, .h4{
        font-size:<?php echo esc_attr( $font_size_h4 ); ?>rem;
    }
    h5, .h5,
    .header-sticky-main .h4{
        font-size:<?php echo esc_attr( $font_size_h5 ); ?>rem;
    }
    h6, .h6,
    .megaphone-show .megaphone-shows-list-medium .entry-title{
        font-size: <?php echo esc_attr( $font_size_h6 ); ?>rem;
    }

    .section-title{
        font-size:<?php echo esc_attr( $font_size_section_title ); ?>rem;
    }
    .paragraph-small,
    .text-small {
        font-size:<?php echo esc_attr( $font_size_small ); ?>rem;
    }

    .megaphone-shows-list-small .entry-title{
        font-size: <?php echo esc_attr( $font_size_h6 - 0.6 ); ?>rem;
    }
    .megaphone-shows-list-medium .entry-title{
        font-size: <?php echo esc_attr( $font_size_h6 - 0.2 ); ?>rem;
    }

    .header-sticky-main > .container{
        height: <?php echo esc_attr( $header_sticky_height ); ?>px;
    }

    .megaphone-show .megaphone-shows-list-small .entry-title{
        font-size:1.8rem;
    }

    .megaphone-show-d.megaphone-show .megaphone-shows-list-small .entry-title{
        font-size:1.6rem;
    }

    body.megaphone-header-indent .megaphone-header + .megaphone-section{
    margin-top: -<?php echo esc_attr( $header_height ); ?>px;
    }
    body.megaphone-header-indent .megaphone-header + .megaphone-section .overlay-container{
        padding-top: <?php echo esc_attr( $header_height / 2 ); ?>px;
    }
}

@media (max-width: 599px){ 
.megaphone-layout-a .entry-content,
.megaphone-layout-b .entry-content,
.megaphone-layout-c .entry-content,
.megaphone-layout-e .entry-content{
    font-size:<?php echo esc_attr( $font_size_small ); ?>rem;
}
}

.megaphone-header{
	font-family: <?php echo wp_kses_post( $nav_font['font-family'] ); ?>, Arial, sans-serif;
	font-weight: <?php echo esc_attr( $nav_font['font-weight'] ); ?>;
	<?php if ( isset( $nav_font['font-style'] ) && !empty( $nav_font['font-style'] ) ):?>
	font-style: <?php echo esc_attr( $nav_font['font-style'] ); ?>;
	<?php endif; ?>
}
/* Header top */

.header-top{
	background-color: <?php echo esc_attr( $color_header_top_bg ); ?>;
	color: <?php echo esc_attr( $color_header_top_txt ); ?>;
    font-size: 1.4rem;
}
.header-top .sub-menu{
	background-color: <?php echo esc_attr( $color_header_top_bg ); ?>;
}
.header-top a{
	color: <?php echo esc_attr( $color_header_top_txt ); ?>;
}
.header-top li:hover > a{
	color: <?php echo esc_attr( $color_header_top_acc ); ?>;
}
.header-top .container {
	height: <?php echo esc_attr( $header_top_height ); ?>px;
}


.megaphone-menu-action .search-form button[type=submit],
.widget_search button[type=submit] {
    background: 0 0;
    border: none;
    color: <?php echo esc_attr( $color_txt ); ?>; 
}
.megaphone-menu-action .search-form button[type=submit]:hover{
    border: none; 
    color: <?php echo esc_attr( $color_acc ); ?>;     
}
.widget_search button[type=submit]:hover{
    color: <?php echo esc_attr( $color_acc ); ?>; 
}


/* Header main */
.header-middle,
.header-middle .sub-menu{
    background-color: <?php echo esc_attr( $color_header_middle_bg ); ?>;
}
.megaphone-header ul.megaphone-menu .megaphone-mega-menu .megaphone-menu-posts>li a{
    color: <?php echo esc_attr( $color_header_middle_txt ); ?>;  
}
.megaphone-header ul.megaphone-menu .megaphone-mega-menu .megaphone-menu-posts>li a:hover{
    color: <?php echo esc_attr( $color_header_middle_acc ); ?>;      
}
.header-middle,
.header-middle a,
.header-mobile .megaphone-menu-action a,
.header-middle ul.megaphone-menu .megaphone-mega-menu .menu-item-has-children:hover > a{
    color: <?php echo esc_attr( $color_header_middle_txt ); ?>;
}

.header-middle a:hover,
.header-mobile .megaphone-menu-action a:hover,
.megaphone-modal-close:hover{
    color: <?php echo esc_attr( $color_header_middle_acc ); ?>;
}


.header-middle li:hover > a,
.header-middle .current-menu-item > a,
.header-middle .current-menu-parent > a {
    color: <?php echo esc_attr( $color_header_middle_acc ); ?>;
}
.header-middle > .container {
	height: <?php echo esc_attr( $header_height ); ?>px;
}

.header-middle .megaphone-menu-donate li a,
.header-middle .megaphone-menu-donate .empty-list a{
    border-color: <?php echo esc_attr( $color_header_middle_txt ); ?>;
}
.header-middle .megaphone-menu-donate li a:hover,
.header-middle .megaphone-menu-donate .empty-list a:hover,
.megaphone-header-indent .header-middle .megaphone-menu-donate li a:hover{
    border-color: <?php echo esc_attr( $color_header_middle_acc ); ?>; 
    color: <?php echo esc_attr( $color_header_middle_acc ); ?>;
}


/* Header bottom */
.header-bottom,
.header-bottom .sub-menu{
    background-color: <?php echo esc_attr( $color_header_bottom_bg ); ?>;
}
.header-bottom,
.header-bottom a{
    color: <?php echo esc_attr( $color_header_bottom_txt ); ?>; 
}
.header-bottom li:hover > a,
.header-bottom .current-menu-item > a,
.header-bottom .current-menu-parent > a{
    color: <?php echo esc_attr( $color_header_bottom_acc ); ?>; 
}
.header-bottom > .container{
    height: <?php echo esc_attr( $header_bottom_height ); ?>px;
}

.header-bottom .megaphone-menu-donate li a,
.header-bottom .megaphone-menu-donate .empty-list a{
    border-color: <?php echo esc_attr( $color_header_bottom_txt ); ?>;
}
.header-bottom .megaphone-menu-donate li a:hover,
.header-bottom .megaphone-menu-donate .empty-list a:hover{
    border-color: <?php echo esc_attr( $color_header_bottom_acc ); ?>; 
    color: <?php echo esc_attr( $color_header_bottom_acc ); ?>;
}



/* Header sticky */
.header-sticky,
.header-sticky .sub-menu{
    background-color: <?php echo esc_attr( $color_header_sticky_bg ); ?>;
}
.header-sticky,
.header-sticky a{
    color: <?php echo esc_attr( $color_header_sticky_txt ); ?>; 
}
.header-sticky li:hover > a,
.header-sticky .current-menu-item > a,
.header-sticky .current-menu-parent > a{
    color: <?php echo esc_attr( $color_header_sticky_acc ); ?>; 
}

.header-sticky .megaphone-menu-donate li a,
.header-sticky .megaphone-menu-donate .empty-list a{
    border-color: <?php echo esc_attr( $color_header_sticky_txt ); ?>;
}
.header-sticky .megaphone-menu-donate li a:hover,
.header-sticky .megaphone-menu-donate .empty-list a:hover{
    border-color: <?php echo esc_attr( $color_header_sticky_acc ); ?>; 
    color: <?php echo esc_attr( $color_header_sticky_acc ); ?>;
}

.widget .current-menu-item > a,
.widget .current-menu-parent > a{
    color: <?php echo esc_attr( $color_acc ); ?>;
}



/* Header responsive sizes */
@media (min-width: 900px) and (max-width: 1050px){
	.header-middle > .container {
    	height: 100px;
	}
	.header-bottom > .container,
	.header-bottom-slots {
    	height: 50px;
	}
}

/* Content */
a,
.placeholder-slot-r .entry-episode{
	color: <?php echo esc_attr( $color_acc ); ?>;
}
.megaphone-archive-no-image .overlay-container .archive-label,
.archive-label{
	color: <?php echo esc_attr( $color_txt ); ?>;    
}
.entry-content a{
	color: <?php echo esc_attr( $color_txt ); ?>;
    border-color: <?php echo esc_attr( $color_acc ); ?>;
}
.entry-content a:hover,
.entry-summary > span{
    color: <?php echo esc_attr( $color_acc ); ?>;
}
.megaphone-hidden-content{
    max-height: <?php echo absint( $single_show_more_height ); ?>px;
}


/* Actions */
.megaphone-menu-donate li a,
.megaphone-menu-donate .empty-list a{
    border-color: <?php echo esc_attr( $color_txt ); ?>;
}
.megaphone-menu-donate li a:hover,
.megaphone-menu-donate .empty-list a:hover{
    border-color: <?php echo esc_attr( $color_acc ); ?>; 
    color: <?php echo esc_attr( $color_acc ); ?>;
}
.overlay-container .archive-label{
    color: #FFF;
}

.megaphone-bg-alt-1{
    background-color: <?php echo esc_attr( $color_bg_alt_1 ); ?>;
}
.megaphone-bg-alt-2 {
	background-color: <?php echo esc_attr( $color_bg_alt_2 ); ?>
}
.megaphone-modal{
    background:#FFF;    
}
.megaphone-overlay-fallback{
    background-color: <?php echo esc_attr( $color_txt ); ?>;   
}

.megaphone-modal .megaphone-menu-social li a:hover, 
.meks_ess a:hover {
    background: <?php echo esc_attr( $color_txt); ?>;
}


/* Button */

.megaphone-button,
input[type="submit"],
input[type="button"],
button[type="submit"],
.megaphone-pagination a,
ul.page-numbers a,
.meks-instagram-follow-link .meks-widget-cta,
.widget .mks_autor_link_wrap a,
.widget .mks_read_more a,
.paginated-post-wrapper a,
.entry-content .megaphone-button,
#cancel-comment-reply-link,
.comment-reply-link,
.megaphone-buttons .megaphone-menu-subscribe a,
.megaphone-menu-donate a,
.entry-category a,
.entry-category .meta-item,
.episode-item,
.header-el-label,
.megaphone-link-special,
.entry-episode,
.link-uppercase,
.archive-label,
.wp-block-button .wp-block-button__link,
.placeholder-slot-r .megaphone-placeholder-title{
	font-family: <?php echo wp_kses_post( $button_font['font-family'] ); ?>, Arial, sans-serif;
	font-weight: <?php echo esc_attr( $button_font['font-weight'] ); ?>;
	<?php if ( isset( $button_font['font-style'] ) && !empty( $button_font['font-style'] ) ):?>
	font-style: <?php echo esc_attr( $button_font['font-style'] ); ?>;
    <?php endif; ?>   
}

.megaphone-button,
input[type="submit"],
input[type="button"],
button[type="submit"],
.megaphone-pagination a,
ul.page-numbers a,
.meks-instagram-follow-link .meks-widget-cta,
.widget .mks_autor_link_wrap a,
.widget .mks_read_more a,
.paginated-post-wrapper a,
.entry-content .megaphone-button,
#cancel-comment-reply-link,
.comment-reply-link{
    background: <?php echo esc_attr( $color_acc ); ?>;
    color: <?php echo esc_attr( $color_button_txt ); ?>;
}
.megaphone-button:hover,
input[type="submit"]:hover,
input[type="button"]:hover,
button[type="submit"]:hover,
.megaphone-pagination a:hover,
.meks-instagram-follow-link .meks-widget-cta:hover,
.widget .mks_autor_link_wrap a:hover,
.widget .mks_read_more a:hover,
.paginated-post-wrapper a:hover,
.entry-content .megaphone-button:hover,
ul.page-numbers a:hover {
    background: <?php echo esc_attr( $color_txt ); ?>;
    color: <?php echo esc_attr( $color_button_hover ); ?>;
}
.megaphone-button-hollow:hover,
.comment-reply-link:hover,
.megaphone-button-circle:hover,
#cancel-comment-reply-link:hover,
button.megaphone-button-hollow:hover,
.megaphone-modal.search-alt button[type=submit]:hover,
.search-alt button[type="submit"]:hover{
    background: 0 0;
    border-color: <?php echo esc_attr( $color_acc ); ?>;
    color: <?php echo esc_attr( $color_acc ); ?>;
}
.megaphone-button-circle{
    border: 1px solid <?php echo esc_attr( $color_txt ); ?>;
    color: <?php echo esc_attr( $color_txt ); ?>;
}
.megaphone-button-hollow,
.comment-reply-link,
#cancel-comment-reply-link,
button.megaphone-button-hollow,
.megaphone-modal.search-alt button[type=submit],
.search-alt button[type="submit"]{
    background: transparent;
    border: 1px solid <?php echo esc_attr( $color_txt ); ?>;
    color: <?php echo esc_attr( $color_txt ); ?>;
}
.megaphone-buttons .megaphone-menu-subscribe li a{
    color: <?php echo esc_attr( $color_txt ); ?>;    
}
.megaphone-buttons .megaphone-menu-subscribe li a:hover{
    color: <?php echo esc_attr( $color_acc ); ?>;    
}
.megaphone-overlay .megaphone-button{
    background: #FFF;
    color: #333;
    border: none;
}
.megaphone-overlay .megaphone-button-hollow,
.megaphone-subscribe-indented .megaphone-buttons .megaphone-menu-subscribe a{
    background: transparent;
    color: #FFF;
}
.megaphone-overlay .megaphone-button:hover{
    color: #FFF;
    background: <?php echo esc_attr( $color_acc ); ?>;
}
.megaphone-overlay .entry-category a:hover,
.megaphone-placeholder-title a:hover{
    color: <?php echo esc_attr( $color_acc ); ?>;   
}


.megaphone-show-more:before{
    background: -webkit-gradient(linear,left top,left bottom,from(rgba(255,255,255,0)),to(<?php echo esc_attr( $color_bg ); ?>));
    background: linear-gradient(to bottom,rgba(255,255,255,0) 0,<?php echo esc_attr( $color_bg ); ?> 100%);    
}


.section-title + a,
.section-subnav a,
.megaphone-link-special,
.section-title + a:before{
    color: <?php echo esc_attr( $color_txt ); ?>;  
}
.section-title + a:hover,
.megaphone-link-special:hover{
    color: <?php echo esc_attr( $color_acc ); ?>; 
}
.section-title:before{
    background: <?php echo esc_attr( $color_acc ); ?>;    
}
.section-title-gray:before{
    background: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.07 ) ); ?>; 
}

.entry-category a,
.entry-meta a:hover,
.megaphone-show-header .entry-episode,
.entry-title a:hover,
.show-title a:hover,
.author-header a:hover,
.author-header a:hover,
.fn a:hover,
.section-subnav a:hover,
.entry-tags a:hover,
.tagcloud a:hover,
.megaphone-show .shows-title a:hover,
.megaphone-overlay .megaphone-breadcrumbs a:hover,
.megaphone-breadcrumbs a{
    color: <?php echo esc_attr( $color_acc ); ?>;  
}
.entry-category a:hover,
.megaphone-show .shows-title a,
.megaphone-breadcrumbs a:hover,
.megaphone-sidebar-branding a{
    color: <?php echo esc_attr( $color_txt ); ?>;  
}
.entry-tags a,
.tagcloud a{
    background: #F3F3F3;
    color: <?php echo esc_attr( $color_txt ); ?>;  
}

.tagcloud a {
    background: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.07 ) ); ?>; 
}

.entry-category a:before{
    color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_meta, 0.5 ) ); ?>;    
}

.entry-meta .meta-item,
.comment-metadata a,
.entry-category .meta-item,
.megaphone-breadcrumbs{
    color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_meta, 0.5 ) ); ?>;
}
.entry-meta .meta-item::before,
.entry-category a:before, 
.entry-category .meta-sponsored + .meta-item:before,
.megaphone-show-header .meta-sponsored + .entry-episode:before{
    color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_meta, 0.5 ) ); ?>;    
}

.megaphone-overlay .entry-meta .meta-item::before,
.megaphone-overlay .entry-category a:before, 
.megaphone-overlay .entry-category .meta-sponsored + .meta-item:before,
.megaphone-overlay .megaphone-show-header .meta-sponsored + .entry-episode:before{
    color: rgba(255,255,255,0.5);    
}

.entry-meta a,
.author-header a,
.author-header a,
.fn a{
    color: <?php echo esc_attr( $color_meta ); ?>; 
}

.entry-media .entry-episode,
.episode-item{
    color: #FFF;  
}
.megaphone-triangle{
    border-color: transparent transparent <?php echo esc_attr( $color_acc ); ?> transparent;
}
.megaphone-triangle-after{
    border-color: <?php echo esc_attr( $color_acc ); ?> transparent transparent transparent;    
}
.megaphone-triangle:before{
    background-color: <?php echo esc_attr( $color_acc ); ?>;       
}

/* Pagination */
.double-bounce1, .double-bounce2{
	background-color: <?php echo esc_attr( $color_acc ); ?>;    	
}
.megaphone-pagination .page-numbers.current,
.paginated-post-wrapper .current,
ul.page-numbers .current{
	background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;   
	color: <?php echo esc_attr( $color_txt ); ?>;  
}
.megaphone-button.disabled,
.megaphone-button.disabled:hover{	
	background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>; 
	color: <?php echo esc_attr( $color_txt ); ?>;  
    pointer-events: none;
}
.megaphone-ellipsis div{
	background-color: <?php echo esc_attr( $color_acc ); ?>; 
}
.megaphone-no-image .entry-media{
	background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;     
}
.megaphone-show-episode-number{
    background-color: <?php echo esc_attr( $color_bg ); ?>; 
	color: <?php echo esc_attr( $color_txt ); ?>;      
}
.megaphone-show-episode-number strong{
    color: <?php echo esc_attr( $color_acc ); ?>;    
}
.megaphone-show .entry-media{
    background-color: <?php echo esc_attr( $color_acc ); ?>;     
}

.player-paceholder .megaphone-placeholder-title{
    color: <?php echo esc_attr( $color_dark_txt ); ?>;       
}
.wa-layout-3 .player-paceholder .megaphone-button-play,
.wa-layout-3 .player-paceholder .megaphone-placeholder-title a,
.single-layout-5 .player-paceholder .megaphone-button-play,
.single-layout-5 .player-paceholder .megaphone-placeholder-title{
    color: <?php echo esc_attr( $color_txt ); ?>;    
}
.player-paceholder .megaphone-button-play,
.player-paceholder .megaphone-placeholder-title a{
    color: <?php echo esc_attr( $color_dark_txt ); ?>;     
}
.megaphone-button-play:hover,
.player-paceholder .megaphone-placeholder-title a:hover,
.wa-layout-3 .player-paceholder.player-paceholder-medium a:hover{
    color: <?php echo esc_attr( $color_acc ); ?>;      
}
.megaphone-play-current:hover a,
.megaphone-play-current:hover .megaphone-placeholder-title,
.megaphone-play-current:hover .megaphone-button-play-medium{
    color: <?php echo esc_attr( $color_acc ); ?>;
}
.megaphone-shows-list .megaphone-button-play{
    color: <?php echo esc_attr( $color_txt ); ?>;
}
.megaphone-shows-list .megaphone-button-play:hover{
    color: <?php echo esc_attr( $color_acc ); ?>;
}


/* Player */


body .meks-ap,
body .meks-ap-player,
body .meks-ap-player iframe {
    height: <?php echo esc_attr( megaphone_get_option('player_height') ); ?>px;   
}

body .meks-ap-collapsed,
.megaphone-player-hidden .meks-ap{
    bottom: -<?php echo esc_attr( megaphone_get_option('player_height') ); ?>px;   
}

.meks-ap-slot-l{
    background-color: <?php echo esc_attr( $color_acc ); ?>;  
    color:<?php echo esc_attr( $color_button_txt ); ?>;   
}
.meks-ap-title{
    color:<?php echo esc_attr( $color_button_txt ); ?>;   
}
.megaphone-player-action .megaphone-menu-subscribe a{
    color: <?php echo esc_attr( $color_dark_txt ); ?>;  
}
.meks-ap-player .mejs-volume-current{
    background-color: <?php echo esc_attr( $color_acc ); ?>; 
}
.meks-ap-player .mejs-volume-handle{
    background: 0 0;
}
.meks-ap-toggle.meks-ap-bg{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.5 ) ); ?>;
}
.meks-ap-player .mejs__speed-selected, 
.meks-ap-player .mejs-speed-selected,
.meks-ap .mejs-speed-selector-label:hover,
.megaphone-player-action .megaphone-menu-subscribe a:hover,
.megaphone-player-action .megaphone-share-wrapper .meks_ess a:hover{
    color: <?php echo esc_attr( $color_acc ); ?>;    
}
.megaphone-player-action .mejs-button:hover > button{
    color: <?php echo esc_attr( $color_txt ); ?>;    
}

/* Widget */
.widget-inside,
.widget-inside.megaphone-bg-alt-1{
    background-color: <?php echo esc_attr( $color_bg_alt_1 ); ?>;
    color: <?php echo esc_attr( $color_dark_alt_txt_1 ); ?>; 
}
.widget a{
    color: <?php echo esc_attr( $color_txt ); ?>;        
}
.widget li{
    color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.5 ) ); ?>;   
}
.widget a:hover{
    color: <?php echo esc_attr( $color_acc ); ?>;        
}
.widget-inside.megaphone-bg-alt-2{
    background-color: <?php echo esc_attr( $color_bg_alt_2 ); ?>;  
    color: <?php echo esc_attr( $color_dark_alt_txt_2 ); ?>;   
}
.widget-inside.megaphone-bg-alt-2 p,
.widget-inside.megaphone-bg-alt-2 .widget-title,
.widget-inside.megaphone-bg-alt-2 a{
    color: <?php echo esc_attr( $color_dark_alt_txt_2 ); ?>;    
}

.megaphone-bg-alt-2 .widget-inside.megaphone-bg-alt-2 p,
.megaphone-bg-alt-2 .widget-inside.megaphone-bg-alt-2 .widget-title,
.megaphone-bg-alt-2 .widget-inside.megaphone-bg-alt-2 a {
    color: <?php echo esc_attr( $color_dark_alt_txt_2 ); ?>;   
}
.megaphone-bg-alt-1 .widget-inside.megaphone-bg-alt-1:after{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_dark_alt_txt_1, 0.05 ) ); ?>;   
}
.megaphone-bg-alt-2 .widget-inside.megaphone-bg-alt-2:after{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_dark_alt_txt_2, 0.05 ) ); ?>;  
}
.megaphone-bg-alt-2 input[type="text"], 
.megaphone-bg-alt-2 input[type="email"], 
.megaphone-bg-alt-2 input[type="url"], 
.megaphone-bg-alt-2 input[type="password"], 
.megaphone-bg-alt-2 input[type="search"], 
.megaphone-bg-alt-2 input[type="number"], 
.megaphone-bg-alt-2 select, 
.megaphone-bg-alt-2 textarea{
    border-color:  <?php echo esc_attr( megaphone_hex_to_rgba( $color_dark_alt_txt_2, 0.2 ) ); ?>;
}

ul.mks_social_widget_ul li a:hover{
    background-color: <?php echo esc_attr( $color_txt ); ?>;
}

.widget_calendar #today a{
	color: #fff;
}

.widget_calendar #today a{

}

.rssSummary,
.widget-title .rsswidget{
	color: #120E0D;	
}
.widget .count,
.wp-block-archives .count,
.wp-block-categories .count{
    background-color: <?php echo esc_attr( $color_acc ); ?>;
    color: <?php echo esc_attr( $color_button_txt ); ?>;   
}
.widget_categories ul li a,
.widget_archive ul li a{
    color: <?php echo esc_attr( $color_txt ); ?>; 
}
.widget_categories ul li a:hover,
.widget_archive ul li a:hover{
    color: <?php echo esc_attr( $color_acc ); ?>;   
}
.megaphone-duotone-overlay .entry-media::before{
    background-color: <?php echo esc_attr( $color_acc ); ?>;    
}

.section-content.alignfull + .megaphone-bg-alt-1{
	box-shadow: -526px 0 0 <?php echo esc_attr( $color_bg_alt_1); ?>, -1052px 0 0 <?php echo esc_attr( $color_bg_alt_1); ?>,
	526px 0 0 <?php echo esc_attr( $color_bg_alt_1); ?>, 1052px 0 0 <?php echo esc_attr( $color_bg_alt_1 ); ?>; 
}


/* Forms */

input[type="text"],
input[type="email"],
input[type="url"],
input[type="password"],
input[type="search"],
input[type="number"],
input[type="tel"],
input[type="range"],
input[type="date"],
input[type="month"],
input[type="week"],
input[type="time"],
input[type="datetime"],
input[type="datetime-local"],
input[type="color"],
select,
textarea{
    border: 1px solid <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;
}

.megaphone-footer .container + .container .megaphone-copyright {
    border-top: 1px solid <?php echo esc_attr( megaphone_hex_to_rgba( $color_footer_txt, 0.1 ) ); ?>;
}

.megaphone-goto-top,
.megaphone-goto-top:hover{
	background-color: <?php echo esc_attr( $color_txt ); ?>; 	
	color: <?php echo esc_attr( $color_bg ); ?>; 
}


<?php

/* Uppercase option */

$uppercase_defaults = megaphone_get_typography_uppercase_options();
$uppercase = megaphone_get_option('uppercase');

foreach ( $uppercase_defaults as $key => $value ) {

    if( $key == 'buttons'){
        continue;
    }

    if ( in_array( $key, $uppercase ) ) {
        echo html_entity_decode( $key ) .'{ text-transform: uppercase;}';
    } else {
        echo html_entity_decode( $key ) .'{ text-transform: none;}';
    }
}

?>

<?php if(in_array('buttons', $uppercase) ): ?>
    
.megaphone-menu-donate li a,
.megaphone-buttons .megaphone-menu-subscribe li a,
.megaphone-button, input[type="submit"], 
input[type="button"], button[type="submit"], 
.megaphone-pagination a, 
ul.page-numbers a, ul.page-numbers .current, 
.comment-reply-link, #cancel-comment-reply-link, 
.meks-instagram-follow-link .meks-widget-cta, 
.mks_autor_link_wrap a, 
.mks_read_more a, 
.paginated-post-wrapper a, 
.entry-content .megaphone-button, 
.megaphone-subscribe .empty-list a, 
.megaphone-menu-donate .empty-list a, 
.link-uppercase, 
.megaphone-link-special, 
.entry-tags a, 
.entry-category a, 
.entry-category .meta-item,
.megaphone-buttons .megaphone-menu-subscribe .header-el-label,
.placeholder-slot-r .megaphone-placeholder-title{
    
    text-transform: uppercase;
}

<?php endif; ?>

<?php


/* Editor font sizes */
$font_sizes = megaphone_get_editor_font_sizes();

if ( !empty( $font_sizes ) ) {
	echo '@media(min-width: '.esc_attr( $grid['breakpoint']['lg']).'px){'; 
    foreach ( $font_sizes as $id => $item ) {  
        	echo '.has-'. $item['slug'] .'-font-size{ font-size: '.number_format( $item['size'] / 10,  1 ) .'rem;}';
    }
    echo '}';
}

/* Editor colors */
$colors = megaphone_get_editor_colors();

if ( !empty( $colors ) ) {
    foreach ( $colors as $id => $item ) {  
        	echo '.has-'. $item['slug'] .'-background-color{ background-color: ' . esc_attr($item['color']) .';}';
        	echo '.has-'. $item['slug'] .'-color{ color: ' . esc_attr($item['color']) .';}';
    }
}
?>

/* Footer */
.megaphone-footer{
    background-color: <?php echo esc_attr( $color_footer_bg ); ?>;
    color: <?php echo esc_attr( $color_footer_txt ); ?>;
    font-size: 1.6rem;
}
.megaphone-footer a{
    color: <?php echo esc_attr( $color_footer_txt ); ?>;
}
.megaphone-footer a:hover{
    color: <?php echo esc_attr( $color_footer_acc ); ?>;
}
.megaphone-footer .widget-title{
    color: <?php echo esc_attr( $color_footer_txt ); ?>;
}
.megaphone-footer .tagcloud a {
    background: <?php echo esc_attr( megaphone_hex_to_rgba( $color_footer_txt, 0.2 ) ); ?>;
    color: <?php echo esc_attr( $color_footer_txt ); ?>;
}

.megaphone-footer-widgets + .megaphone-copyright{
  border-top: 1px solid <?php echo esc_attr( megaphone_hex_to_rgba( $color_footer_txt, 0.1 ) ); ?>;
}

.megaphone-bg-alt-1 + .megaphone-bg-alt-1 .megaphone-section-separator{
    border-top: 1px solid <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;    
}

.megaphone-footer .widget .count,
.megaphone-footer .widget_categories li a,
.megaphone-footer .widget_archive li a,
.megaphone-footer .widget .megaphone-accordion-nav,
.megaphone-footer table,
.megaphone-footer .widget-title .rsswidget,
.megaphone-footer .widget li{
	color: <?php echo esc_attr( $color_footer_txt ); ?>;
}
.megaphone-footer select{
    color: <?php echo esc_attr( $color_footer_bg ); ?>;    
}
.footer-divider{
	border-top: 1px solid <?php echo esc_attr( megaphone_hex_to_rgba( $color_footer_txt, 0.1 ) ); ?>;
}
.megaphone-footer .rssSummary,
.megaphone-footer .widget p{
	color: <?php echo esc_attr( $color_footer_txt ); ?>;
}

.megaphone-empty-message{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;   
}

.megaphone-footer input[type="text"], 
.megaphone-footer input[type="email"], 
.megaphone-footer input[type="url"], 
.megaphone-footer input[type="password"], 
.megaphone-footer input[type="search"], 
.megaphone-footer input[type="number"], 
.megaphone-footer input[type="tel"], 
.megaphone-footer input[type="range"], 
.megaphone-footer input[type="date"], 
.megaphone-footer input[type="month"], 
.megaphone-footer input[type="week"],
.megaphone-footer input[type="time"], 
.megaphone-footer input[type="datetime"], 
.megaphone-footer input[type="datetime-local"], 
.megaphone-footer input[type="color"], 
.megaphone-footer textarea{
    border-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_footer_txt, 0.8 ) ); ?>;    
}

.megaphone-footer .megaphone-button-hollow, 
.megaphone-footer .megaphone-subscribe-menu li a, 
.megaphone-footer .megaphone-modal.search-alt button[type=submit], 
.megaphone-footer .search-alt button[type="submit"]{
    border-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_footer_txt, 0.8 ) ); ?>;  
    color: <?php echo esc_attr( $color_footer_txt ); ?>; 
}
.megaphone-footer .megaphone-button-hollow:hover, 
.megaphone-footer .megaphone-subscribe-menu li a:hover, 
.megaphone-footer .megaphone-modal.search-alt button[type=submit]:hover, 
.megaphone-footer .search-alt button[type="submit"]:hover{
    border-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_footer_acc, 0.8 ) ); ?>;  
    color: <?php echo esc_attr( $color_footer_acc ); ?>; 
}
.megaphone-footer .megaphone-button:hover, 
.megaphone-footer input[type="submit"]:hover, 
.megaphone-footer input[type="button"]:hover, 
.megaphone-footer button[type="submit"]:hover, 
.megaphone-footer .meks-instagram-follow-link .meks-widget-cta:hover, 
.megaphone-footer .widget .mks_autor_link_wrap a:hover, 
.megaphone-footer .widget .mks_read_more a:hover,
.megaphone-footer ul.mks_social_widget_ul li a:hover{
    background-color: <?php echo esc_attr( $color_footer_txt ); ?>;
	color: <?php echo esc_attr( $color_footer_bg ); ?>;
}
.megaphone-footer ul.mks_social_widget_ul li a:hover:before{
	color: <?php echo esc_attr( $color_footer_bg ); ?>;
}
.megaphone-footer .widget li,
.megaphone-footer .rss-date{
    color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_footer_txt, 0.5 ) ); ?>;   
}
.megaphone-footer .tagcloud a:hover{
	color: <?php echo esc_attr( $color_footer_acc ); ?>;
}

/* Blocks */

tr {
	border-bottom: 1px solid <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;
}
.wp-block-table.is-style-stripes tr:nth-child(odd){
	background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.1 ) ); ?>;
}
.wp-block-button .wp-block-button__link{
    background-color: <?php echo esc_attr( $color_acc ); ?>; 
    color: <?php echo esc_attr( $color_bg ); ?>;
}
.wp-block-button .wp-block-button__link:hover{
    background-color: <?php echo esc_attr( $color_txt); ?>;  
    color: <?php echo esc_attr( $color_bg ); ?>;  
}
body .wp-block-button .wp-block-button__link.has-background:hover{
    background-color: <?php echo esc_attr( $color_txt); ?> !important;  
    color: <?php echo esc_attr( $color_bg ); ?>;     
}
.wp-block-button.is-style-outline .wp-block-button__link{
    border: 1px solid <?php echo esc_attr( $color_txt); ?>;
    color: <?php echo esc_attr( $color_txt); ?>;    
}
.wp-block-button.is-style-outline .wp-block-button__link:hover{
    border: 1px solid <?php echo esc_attr( $color_acc ); ?>; 
    color: <?php echo esc_attr( $color_acc ); ?>; 
    background: 0 0;   
}

.is-style-outline .wp-block-button__link {
    background: 0 0;
    color:<?php echo esc_attr( $color_acc ); ?>;
    border: 2px solid currentcolor;
}
.wp-block-quote:before{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.07 ) ); ?>;
}
.wp-block-pullquote:not(.is-style-solid-color){
	color: <?php echo esc_attr( $color_txt); ?>;
	border-color: <?php echo esc_attr( $color_acc ); ?>;  
}
.wp-block-pullquote{
	background-color: <?php echo esc_attr( $color_acc ); ?>;  
	color: <?php echo esc_attr( $color_bg); ?>; 	
}
.megaphone-sidebar-none .wp-block-pullquote.alignfull.is-style-solid-color{
	box-shadow: -526px 0 0 <?php echo esc_attr( $color_acc); ?>, -1052px 0 0 <?php echo esc_attr( $color_acc); ?>,
	526px 0 0 <?php echo esc_attr( $color_acc); ?>, 1052px 0 0 <?php echo esc_attr( $color_acc ); ?>; 
}
.entry-content > pre,
.entry-content > code,
.entry-content > p code,
.comment-content > pre,
.comment-content > code,
.comment-content > p code{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.05 ) ); ?>;
    font-size: 1.4rem;    
}
.wp-block-separator{
    background-color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.05 ) ); ?>;
}
.wp-block-tag-cloud a{
    background-color: <?php echo esc_attr( $color_bg_alt_1); ?>;
    color: <?php echo esc_attr( $color_dark_txt); ?>;    
}
.wp-block-rss__item-author, 
.wp-block-rss__item-publish-date{
    color: <?php echo esc_attr( $color_meta); ?>; 
}
.wp-block-calendar tfoot a{
    color: <?php echo esc_attr( $color_meta); ?>; 
}
.wp-block-latest-comments__comment-meta,
.wp-block-latest-posts__post-date{
    color: <?php echo esc_attr( megaphone_hex_to_rgba( $color_txt, 0.5 ) ); ?>; 
}


.megaphone-header-indent .megaphone-header + .megaphone-section{
    margin-top: -60px;
}
.megaphone-header-indent .megaphone-header + .megaphone-section .overlay-container{
    padding-top: 40px;
}

.megaphone-header-indent .header-middle, 
.megaphone-header-indent .header-middle nav > ul > li > a, 
.megaphone-header-indent .header-mobile .megaphone-menu-action a,
.megaphone-header-indent .header-middle .megaphone-menu-action a,
.megaphone-header-indent .header-middle .megaphone-menu-social a,
.megaphone-header-indent .header-middle .megaphone-menu-subscribe a,
.megaphone-header-indent .header-middle .list-reset a,
.megaphone-header-indent .header-middle .site-title a{
    color: #FFF;
}
.megaphone-header-indent .header-middle .megaphone-menu-donate li a, 
.megaphone-header-indent .header-middle .megaphone-menu-donate .empty-list a,
.megaphone-header-indent .header-middle .megaphone-search li .search-alt input[type=text],
.megaphone-header-indent .header-mobile .megaphone-menu-action a{
    color: #FFF;
    border-color: #FFF;
}
.megaphone-header-indent .header-middle nav > ul > li:hover > a,
.megaphone-header-indent .header-middle .site-title a:hover{
    color: <?php echo esc_attr( $color_header_middle_acc); ?>; 
}
.megaphone-menu-action .search-form button[type=submit]{
    color: #FFF;
}

.megaphone-show{
    background-color: <?php echo esc_attr( $color_bg); ?>; 
}
.separator-before:before{
    background-color: <?php echo esc_attr( $color_txt); ?>;    
}
.megaphone-overlay .separator-before:before{
    background-color:#FFF;
}



.megaphone-content-post{
    max-width: <?php echo megaphone_size_by_col( megaphone_get_option('single_blog_width') ) + $grid['gutter']['lg']; ?>px;
}

.megaphone-content-episode{
    max-width: <?php echo megaphone_size_by_col( megaphone_get_option('single_podcast_width') ) + $grid['gutter']['lg']; ?>px;
}

.megaphone-content-page{
    max-width: <?php echo megaphone_size_by_col( megaphone_get_option('page_width') ) + $grid['gutter']['lg']; ?>px;
}

@media (min-width: <?php echo esc_attr($grid['breakpoint']['xl']); ?>px){ 
    .megaphone-content-post{
        max-width: <?php echo megaphone_size_by_col( megaphone_get_option('single_blog_width') ) + $grid['gutter']['xl']; ?>px;
    }

    .megaphone-content-episode{
        max-width: <?php echo megaphone_size_by_col( megaphone_get_option('single_podcast_width') ) + $grid['gutter']['xl']; ?>px;
    }

    .megaphone-content-page{
        max-width: <?php echo megaphone_size_by_col( megaphone_get_option('page_width') ) + $grid['gutter']['xl']; ?>px;
    }

}
    

<?php


/* Apply image size options */
$image_sizes = megaphone_get_image_sizes();

if ( !empty( $image_sizes ) ) {

	echo '@media(min-width: '.esc_attr( $grid['breakpoint']['md']).'px){'; 
    foreach ( $image_sizes as $id => $size ) {
    	if( isset($size['cover']) )  {      
        	echo '.size-'.$id .'{ height: '.absint($size['h'] * 0.9).'px !important;}';
    	}
    }
    echo '}';

	echo '@media(min-width: '.esc_attr( $grid['breakpoint']['lg']).'px){'; 
    foreach ( $image_sizes as $id => $size ) {
    	if( $size['h'] && $size['h'] < 5000)  {      
        	echo '.'.$id .' .entry-media, .size-'.$id .'{ height: '.esc_attr($size['h']).'px !important;}';
    	}
    }
    echo '}';

}


            
?>

/* Woocommerce styles */
<?php if ( megaphone_is_woocommerce_active() ): ?>
.megaphone-header .megaphone-cart-wrap a:hover{
	color: <?php echo esc_attr($color_header_middle_acc); ?>;	
}
.megaphone-cart-count {
    background-color: <?php echo esc_attr($color_header_middle_acc); ?>;
    color: <?php echo esc_attr($color_header_middle_bg); ?>;
}
.woocommerce ul.products li.product .button, 
.woocommerce ul.products li.product .added_to_cart{
  background: <?php echo esc_attr( $color_acc ); ?>;	
  color: <?php echo esc_attr( $color_button_txt ); ?>;
}
.woocommerce ul.products li.product .amount{
	color: <?php echo megaphone_hex_to_rgba($color_h, 0.8); ?>;
}
.woocommerce ul.products li.product .button:hover{
    background-color: <?php echo esc_attr( $color_txt ); ?>;
  color: <?php echo esc_attr( $color_button_hover ); ?>;	
}
.woocommerce ul.products .woocommerce-loop-product__link{
  color: <?php echo esc_attr($color_txt); ?>;
}
.woocommerce ul.products .woocommerce-loop-product__link:hover{
  color: <?php echo esc_attr($color_acc); ?>;  
}
.woocommerce ul.products li.product .woocommerce-loop-category__title, 
.woocommerce ul.products li.product .woocommerce-loop-product__title,
.woocommerce ul.products li.product h3{
  font-size: <?php echo esc_attr($font_size_p); ?>rem;
}
.woocommerce div.product form.cart .button,
.woocommerce #respond input#submit, 
.woocommerce a.button, 
.woocommerce button.button, 
.woocommerce input.button,
.woocommerce #respond input#submit.alt, 
.woocommerce a.button.alt, 
.woocommerce button.button.alt, 
.woocommerce input.button.alt,
.woocommerce ul.products li.product .added_to_cart{
  background: <?php echo esc_attr( $color_acc ); ?>;	
  color: <?php echo esc_attr( $color_button_txt ); ?>;
	font-family: <?php echo wp_kses_post( $button_font['font-family'] ); ?>, Arial, sans-serif;
	font-weight: <?php echo esc_attr( $button_font['font-weight'] ); ?>;
	<?php if ( isset( $button_font['font-style'] ) && !empty( $button_font['font-style'] ) ):?>
	font-style: <?php echo esc_attr( $button_font['font-style'] ); ?>;
	<?php endif; ?>
}

.woocommerce .button.wc-backward{
  background: <?php echo megaphone_hex_to_hsla($color_acc, -15); ?>;
  color: <?php echo esc_attr($color_button_txt); ?>;  
}
.wc-tab,
.woocommerce div.product .woocommerce-tabs ul.tabs li{
	font-size: <?php echo esc_attr( $font_size_p ); ?>rem;
}
.woocommerce button.disabled,
.woocommerce button.alt:disabled{
	background-color: <?php echo esc_attr( $color_bg_alt_1 ); ?>	
}

.price,
.amount,
.woocommerce div.product p.price {
  color: <?php echo esc_attr( $color_meta ); ?>;
}

.woocommerce div.product form.cart .button:hover,
.woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover,
.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover{
    background: <?php echo esc_attr( $color_txt ); ?>;	
    color: <?php echo esc_attr( $color_button_txt ); ?>;
}
.woocommerce #respond input#submit, 
.woocommerce a.button, 
.woocommerce button.button, 
.woocommerce input.button, 
.woocommerce ul.products li.product .added_to_cart{
  color: <?php echo esc_attr($color_button_txt); ?>;  
}
.woocommerce .woocommerce-breadcrumb a:hover{
  color: <?php echo esc_attr($color_acc); ?>;
}
.woocommerce div.product .woocommerce-tabs ul.tabs li.active a {
    border-bottom: 3px solid <?php echo esc_attr($color_acc); ?>;
}
.woocommerce .woocommerce-breadcrumb,
.woocommerce .woocommerce-breadcrumb a{
  color: <?php echo esc_attr($color_meta); ?>;
}
body.woocommerce .megaphone-entry ul.products li.product, body.woocommerce-page ul.products li.product{
  box-shadow:inset 0px 0px 0px 1px <?php echo megaphone_hex_to_rgba($color_txt, 0.3); ?>;
}

.woocommerce div.product .woocommerce-tabs ul.tabs li.active a {
  border-bottom: 3px solid <?php echo esc_attr($color_acc); ?>;
}

body.woocommerce .megaphone-entry ul.products li.product, body.woocommerce-page ul.products li.product{
  box-shadow:inset 0px 0px 0px 1px <?php echo megaphone_hex_to_rgba($color_txt, 0.3); ?>;
}


body .woocommerce .woocommerce-error,
body .woocommerce .woocommerce-info, 
body .woocommerce .woocommerce-message{
   background-color: <?php echo esc_attr($color_bg_alt_1); ?>;
   color: <?php echo esc_attr($color_txt); ?>;
}
body .woocommerce-checkout #payment ul.payment_methods, 
body .woocommerce table.shop_table,
body .woocommerce table.shop_table td, 
body .woocommerce-cart .cart-collaterals .cart_totals tr td, 
body .woocommerce-cart .cart-collaterals .cart_totals tr th, 
body .woocommerce table.shop_table tbody th, 
body .woocommerce table.shop_table tfoot td, 
body .woocommerce table.shop_table tfoot th, 
body .woocommerce .order_details, 
body .woocommerce .cart-collaterals 
body .cross-sells, .woocommerce-page .cart-collaterals .cross-sells, 
body .woocommerce .cart-collaterals .cart_totals, 
body .woocommerce ul.order_details, 
body .woocommerce .shop_table.order_details tfoot th, 
body .woocommerce .shop_table.customer_details th, 
body .woocommerce-checkout #payment ul.payment_methods, 
body .woocommerce .col2-set.addresses .col-1, 
body .woocommerce .col2-set.addresses .col-2, 
body.woocommerce-cart table.cart td.actions .coupon .input-text,
body .woocommerce table.shop_table tbody:first-child tr:first-child th, 
body .woocommerce table.shop_table tbody:first-child tr:first-child td,
body .woocommerce ul.products,
body .woocommerce-product-search input[type=search]{
   border-color: <?php echo megaphone_hex_to_rgba($color_txt, 0.1); ?>;
}
body .select2-container .select2-choice,
body .select2-container--default .select2-selection--single, 
body .select2-dropdown{
	border-color: <?php echo megaphone_hex_to_rgba($color_txt, 0.3); ?>;	
}
body .select2-dropdown{
  background: <?php echo esc_attr($color_bg); ?>;
}
.select2-container--default .select2-results__option[aria-selected=true], 
.select2-container--default .select2-results__option[data-selected=true]{
	background-color: <?php echo esc_attr($color_acc); ?>;
  color: <?php echo esc_attr($color_bg); ?>; 
}

body.woocommerce div.product .woocommerce-tabs ul.tabs li a,
body.woocommerce-cart .cart-collaterals .cart_totals table th{
  color: <?php echo esc_attr($color_txt); ?>; 
}
body.woocommerce div.product .woocommerce-tabs ul.tabs li a:hover{
  color: <?php echo esc_attr($color_acc); ?>; 
}

.woocommerce nav.woocommerce-pagination ul li a,
.woocommerce nav.woocommerce-pagination ul li span{ 
  background: <?php echo esc_attr( $color_acc ); ?>;	
  color: <?php echo esc_attr( $color_bg ); ?>;	
}
.woocommerce nav.woocommerce-pagination ul li a:hover{
	background: <?php echo esc_attr( $color_txt ); ?>;	
  color: <?php echo esc_attr( $color_button_hover ); ?>;	
}
.woocommerce nav.woocommerce-pagination ul li span.current{
	background: <?php echo esc_attr( $color_bg_alt_1 ); ?>;
	color: <?php echo esc_attr( $color_txt ); ?>;
}
.woocommerce .widget_price_filter .ui-slider .ui-slider-range{
	background:<?php echo megaphone_hex_to_rgba($color_acc, 0.5); ?>;  	
}
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle{
  background: <?php echo esc_attr($color_acc); ?>;
}


.woocommerce ul.product_list_widget li,
.woocommerce .widget_shopping_cart .cart_list li,
.woocommerce.widget_shopping_cart .cart_list li{
	border-bottom:1px solid <?php echo megaphone_hex_to_rgba($color_bg, 0.1); ?>;  
}

.woocommerce-MyAccount-navigation ul{
	background: <?php echo esc_attr($color_bg_alt_1); ?>;	
}
body.woocommerce .widget_text .megaphone-inverted .button:hover{
	background: <?php echo esc_attr($color_bg); ?>;	
}
.woocommerce-checkout #payment,
.woocommerce .col2-set.addresses .col-1,
.woocommerce .col2-set.addresses .col-2{
	background: <?php echo esc_attr($color_bg_alt_1); ?>;		
}

<?php endif; ?>