<?php

	    /* Template Name: signup */

		

get_header();
?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>

.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: #4b2287;
	font-size: 15px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #59B2E0;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #59B2E6;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #53A3CD;
	border-color: #53A3CD;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}
.login_form{
	padding-top:40px;
}
.login_form .login.active {
    background-color: #4b2287;
    color: #ffffff;
    padding: 10px;
    width: 100%;
	padding: 12px 20px;
    flex-grow: 1;
    flex-basis: auto;
    text-align: center;
    cursor: pointer;
    margin: 0;
    list-style: none;
}
.login_form .login.active a{
	color:#fff
}
.login_form .login{
	background-color: #eeeeee;
    color: #4b2287;
	padding: 10px;
    width: 100%;
	padding: 12px 20px;
    flex-grow: 1;
    flex-basis: auto;
    text-align: center;
    cursor: pointer;
    margin: 0;
    list-style: none;
}
.login_form .login a{
	color: #4b2287;
}
.login_form .panel-heading .col-xs-6{
	padding-left:0;
	padding-right:0;
}
.login_form .panel-heading {
    padding: 0;
    border-bottom: 0px solid transparent;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
}
.login_form .panel-body { 
    padding: 60px;
    border-top: 0px;
}
#pms_login label {
    font-size: 15px;
    margin-bottom: 10px;
    letter-spacing: 0.4px;
}
#pms_login input#rememberme {
    margin-top: -7px;
    width: 20px;
}
#pms_register-form input[type=checkbox], #pms_register-form input[type=radio] {
    margin-top: -7px;
}
#pms_login input#wp-submit {
    border: 2px solid transparent!important;
    -moz-border-image: -moz-linear-gradient(left, #fe004e 0%, #8700cd 100%)!important;
    -webkit-border-image: -webkit-linear-gradient(left, #fe004e 0%, #8700cd 100%)!important;
    border-image: linear-gradient(to left, #fe004e 0%, #8700cd 100%)!important;
    border-image-slice: 1!important;
    font-family: 'avertape-bolduploaded_file';
    color: #3d3b44;
    text-transform: uppercase;
    padding-top: 7px;
    padding-bottom: 9px;
    background-color: #fff;
    font-size: 11px;
    background-image: none!important;
    font-size: 12px!important;
    padding-top: 10px!important;
    padding-bottom: 10px!important;
    padding-left: 30px;
    padding-right: 30px;
    letter-spacing: -0.5px;
    width: 100%;
	margin-top:10px;
}
#pms_login input {
    border-radius: 0px;
    border: 1px solid #d6d6d6;
}
#pms_login a {
    color: #615c5c;
    text-align: center;
    letter-spacing: 0.5px;
}

#pms_register-form label {
    font-size: 15px;
    margin-bottom: 10px;
    letter-spacing: 0.4px;
} 
#pms_register-form input {
    border-radius: 0px;
    border: 1px solid #d6d6d6;
}
#pms_register-form li.pms-field.pms-user-login-field {
   /* margin-top: 30px;*/
}
#pms_register-form input[type="submit"]{
	  border: 2px solid transparent!important;
    -moz-border-image: -moz-linear-gradient(left, #fe004e 0%, #8700cd 100%)!important;
    -webkit-border-image: -webkit-linear-gradient(left, #fe004e 0%, #8700cd 100%)!important;
    border-image: linear-gradient(to left, #fe004e 0%, #8700cd 100%)!important;
    border-image-slice: 1!important;
    font-family: 'avertape-bolduploaded_file';
    color: #3d3b44;
    text-transform: uppercase;
    padding-top: 7px;
    padding-bottom: 9px;
    background-color: #fff;
    font-size: 11px;
    background-image: none!important;
    font-size: 12px!important;
    padding-top: 10px!important;
    padding-bottom: 10px!important;
    padding-left: 30px;
    padding-right: 30px;
    letter-spacing: -0.5px;
    width: 100%;
	margin-top:10px;
}
.container {
    width: 100% !important;
    margin-right: auto !important;
    margin-left: auto !important;
    padding-right: 50px !important;
    padding-left: 50px !important;
}
</style>
<div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login login_form ">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
							  <div class="login active" id="login-form-link">
								  <a href="#" class="active" >Login</a>
							  </div>
							</div>
							<div class="col-xs-6">
							   <div class="login" id="register-form-link">
								  <a href="#" >Sign Up</a>
							   </div>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div id="login-form" style="display: block;">
									<?php 
										$url = get_site_url().'/my-account/';
										if(is_user_logged_in()){
											wp_redirect( $url );
										}else{
											echo do_shortcode("[pms-login]"); 
										}
									?>
								</div>
								<div id="register-form" style="display: none;">
									<?php 
										if(is_user_logged_in()){
											wp_redirect( $url );
										}else{
											echo do_shortcode("[pms-register plans_position='bottom' ]"); 
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
	

get_footer();
?>	
<script>
$(function() {

$('#login-form-link').click(function(e) {
	$("#login-form").delay(100).fadeIn(100);
	 $("#register-form").fadeOut(100);
	$('#register-form-link').removeClass('active');
	$(this).addClass('active');
	e.preventDefault();
});
$('#register-form-link').click(function(e) {
	$("#register-form").delay(100).fadeIn(100);
	 $("#login-form").fadeOut(100);
	$('#login-form-link').removeClass('active');
	$(this).addClass('active');
	e.preventDefault();
});

});
</script>