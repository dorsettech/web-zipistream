<?php
/*
** Template Name: Add Post From Frontend
*/
get_header();

if(is_user_logged_in())
{
    //print_r($_POST); 
    if(isset($_POST['ispost']))
	{
		global $current_user;
		wp_get_current_user();
		//print_r($_POST); die;
		$user_login = $current_user->user_login;
		$user_email = $current_user->user_email;
		$user_firstname = $current_user->user_firstname;
		$user_lastname = $current_user->user_lastname;
		$user_id = $current_user->ID;



		$post_title = $_POST['title'];
		$sample_image = $_FILES['sample_image']['name'];
		$post_content = $_POST['post_content'];
		$category = array($_POST['category']);

		if (!function_exists('wp_generate_attachment_metadata'))
		{
			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			require_once(ABSPATH . "wp-admin" . '/includes/file.php');
			require_once(ABSPATH . "wp-admin" . '/includes/media.php');
		}



		$new_post = array(
			'post_title' => $post_title,
			'post_content' => $post_content,
			'post_status' => 'pending',
			'post_name' => $post_title,
			//'post_type' => 'podcast',
			'post_category' => $category
		);

		$pid = wp_insert_post($new_post);
		add_post_meta($pid, 'meta_key', true);

		if(filter_var($post_content, FILTER_VALIDATE_URL)){
				add_post_meta($pid, 'audio_file', $post_content);
				add_post_meta($pid, 'enclosure', $post_content);
				add_post_meta($pid, 'episode_type', 'audio');
		}

		if (isset($_FILES['sample_image']) AND !empty($_FILES['sample_image']['name'])) {


			$uploadedfile = $_FILES['sample_image'];
			$upload_overrides = array( 'test_form' => false );
		
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
			$imageurl = "";
			if ( $movefile && ! isset( $movefile['error'] ) ) {
				$imageurl = $movefile['url'];
				//$post_content2 =  $imageurl. '  '.$post_content;
				//$post_content2 =  $imageurl;
				add_post_meta($pid, 'audio_file', $imageurl);
				add_post_meta($pid, 'enclosure', $imageurl);
				add_post_meta($pid, 'episode_type', 'audio');
			} else {
				echo $movefile['error'];
			}	

		}
		############## Update post feature image #############################
		if (isset($_FILES['post_Fimage']) AND !empty($_FILES['post_Fimage']['name'])) {

			$uploaddir = wp_upload_dir();
			$file = $_FILES["post_Fimage"]["name"];
			$uploadfile = $uploaddir['path'] . '/' . basename( $file );

			move_uploaded_file( $_FILES["post_Fimage"]["tmp_name"] , $uploadfile );
			$filename = basename( $uploadfile );

			$wp_filetype = wp_check_filetype(basename($filename), null );

			$attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
				'post_content' => '',
				'post_status' => 'inherit'
			);
			$attach_id = wp_insert_attachment( $attachment, $uploadfile );
			set_post_thumbnail( $pid, $attach_id );
		}

		// if ($_FILES)
		// {
		// 	foreach ($_FILES as $file => $array)
		// 	{
		// 		if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK)
		// 		{
		// 			return "upload error : " . $_FILES[$file]['error'];
		// 		}
		// 		$attach_id = media_handle_upload( $file, $pid );
		// 	}
		// }
		// if ($attach_id > 0)
		// {
		// 	//and if you want to set that image as Post then use:
		// 	update_post_meta($new_post, '_thumbnail_id', $attach_id);
		// }

		$my_post1 = get_post($attach_id);
		$my_post2 = get_post($pid);
		$my_post = array_merge($my_post1, $my_post2);
        
        if ( $pid ) {
            wp_redirect('/view-podcast');
            exit;
        }

	}
}
else
{
	wp_redirect('/myaccount');
}

?>

<style>

 .viewpadcast-btn{
	 border: 2px solid transparent!important;
    border-image: linear-gradient(to left,#fe004e 0%,#8700cd 100%)!important;
    border-image-slice: 1!important;
    font-family: 'avertape-bolduploaded_file';
    color: #3d3b44;
    text-transform: uppercase;
    padding: 10px 20px;
    font-size: 14px;
    background-image: none!important;
    padding: 10px;
    text-decoration: none;
 }
 form#primaryPostForm {
    margin: 25px 0;
}
#primaryPostForm .form-control {
    border: 1px solid #ddd;
    border-radius: 0px;
    min-height: 45px;
    margin-bottom: 10px;
	 font-size: 14px;
}
#primaryPostForm .form-control:focus{
	box-shadow:none;
	border-color:#222;
}
#primaryPostForm label {
    font-size: 16px;
    color: #222;
    margin-bottom: 10px;
}
.submit-btn{
	margin-top:30px;
}
input#post_Fimage {
    background: #fff;
    padding: 10px;
    width: 100%;
    border: 1px solid #ddd;
}
.uploadpost{
	 background: #fff;
    padding: 10px;
    width: 100%;
    border: 1px solid #ddd;
}
</style>
<div class="container margin-tb">
	<div class="col-lg-8 offset-2">
		
		<div class="row">
			<div class="col-lg-6">
			   <h5>Add New Post</h5> 
			</div>
		  <div class="col-lg-6 text-right">
		    <a href="/view-podcast" class="viewpadcast-btn btn">View Podcast Post </a>
		  </div>
		</div>
		<div class="row">
		<div class="col-lg-12">
		<form class="form-horizontal" name="form" method="post" id="primaryPostForm" enctype="multipart/form-data">
			<input type="hidden" name="ispost" value="1" />
			<input type="hidden" name="userid" value="" />
			<div class="row">
			<div class="col-md-12 form-group">
				<label class="control-label">Title</label>
				<input type="text" required placeholder="Add Title" class="form-control" name="title" />
			</div>
			</div>
            <div class="row">
				<div class="form-group col-md-12">
					<label class="control-label">Post Content</label>
					<textarea class="form-control" placeholder="Upload the primary podcast file or paste the file URL here.." rows="8" name="post_content"></textarea>
				</div>
			</div>
            <div class="row">
			<div class="col-md-6 form-group">
				<label class="control-label">Choose Category</label>
				<select required name="category" class="form-control">
					<?php
					$catList = get_categories();
					foreach($catList as $listval)
					{
						echo '<option value="'.$listval->term_id.'">'.$listval->name.'</option>';
					}
					?>
				</select>
			</div>
			<div class="col-md-6 form-group">
		
				<label class="control-label" for="post_Fimage">Featured Image</label>
				<input type="file" name="post_Fimage" id="post_Fimage">
			</div>
			</div>
			<div class="row">
			<div class="col-md-6 form-group">
				<label class="control-label">Upload for Post</label>
				<input type="file" name="sample_image" class="uploadpost" />
		
			</div>
			</div>
           <div class="row">
			<div class="col-md-12">
				<input type="submit" class="btn btn-primary submit-btn" value="SUBMIT" name="submitpost" />
			</div>
			</div>
		</form>
		</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>




<?php get_footer(); ?>


<script>
jQuery(document).ready(function() {
 
 //jQuery("#primaryPostForm");

}); 
</script>