<?php
/*
** Template Name: Reset password
*/
get_header();
?>
<style>
input#pms_username_email {width: 50%;}
</style>
<div class="container margin-tb">
	<div class="col-lg-8 offset-2">
        <?php echo do_shortcode('[pms-recover-password]'); ?>
    </div>
</div>
<?php
get_footer(); 