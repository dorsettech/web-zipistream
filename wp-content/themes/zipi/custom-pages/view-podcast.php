<?php
/*
** Template Name: View Podcast
*/
get_header(); 

    $userID = get_current_user_id();
    $args = array(
        'author'        =>  $userID, 
        'orderby'       =>  'post_date',
        'order'         =>  'ASC',
        //'post_type'     => 'podcast',
        'post_status' => array('publish', 'pending', 'draft', 'private')   ,
        'posts_per_page' => -1 // no limit
        
        );
         
    $current_user_posts = get_posts( $args );
    if(is_user_logged_in())
    {
          
?>
 <style>

 .viewpadcast-btn{
	 border: 2px solid transparent!important;
    border-image: linear-gradient(to left,#fe004e 0%,#8700cd 100%)!important;
    border-image-slice: 1!important;
    font-family: 'avertape-bolduploaded_file';
    color: #3d3b44;
    text-transform: uppercase;
    padding: 10px 20px;
    font-size: 14px;
    background-image: none!important;
    padding: 10px;
    text-decoration: none;
 }
 div#view_podcast_wrapper {
    margin-top: 30px;
    margin-bottom: 30px;
}
div#view_podcast_wrapper table thead tr th{
	border:1px solid #47268b;
}
div#view_podcast_wrapper table thead tr th a{
	text-decoration:none;
}
div#view_podcast_wrapper table thead tr th a span{
	color:#fff;
	
}
div#view_podcast_wrapper table tbody tr td a{
	color:#222;
	text-decoration:none;
}
/* div#view_podcast_wrapper table tbody tr td strong select{
	width:72% !important;
} */
select#post_status {
    font-size: 16px;
    padding-left: 0px;
}
.dataTables_wrapper .dataTables_length select{
	width:39% !important;
}
table.dataTable.no-footer {
    border-bottom: 0px solid #111 !important;
}
#view_podcast_wrapper.dataTables_wrapper .dataTables_paginate .paginate_button.current, #view_podcast_wrapper.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
    color: #fff !important;
    border: 1px solid #47268b !important;
    background-color: #47268b !important;
    background: linear-gradient(to bottom, #47268b 0%, #47268b 100%) !important;
}
 </style>
<div class="container margin-tb">
    <div class="col-lg-8 offset-2">
		<div class="row">
			<div class="col-lg-6">
			   <h5>All Podcast Post</h5> 
			</div>
		  <div class="col-lg-6 text-right">
		    <a href="/add-podcast" class="btn viewpadcast-btn">Add Podcast </a>
		  </div>
		</div>
        
        <table id="view_podcast" class="table-hover table-bordered">
            <thead style="background:#47268b;">
                <tr>
                    <th scope="col" style="width:6%;">
                        <a href="#_"><span>S.No.</span></a>
                    </th>
                    <th scope="col" class="manage-column column-primary" style="width:50%;">
                        <a href="#_"> <span>Podcast</span></a>
                    </th>
                    <th scope="col" class="manage-column column-primary">
                        <a href="#_"><span>Datetime</span></a>
                    </th>
                    <th scope="col" class="manage-column column-primary">
                        <a href="#_"><span>Status</span></a>
                    </th>
                </tr>
            </thead> 
            <tbody>
                <?php 
                if($current_user_posts){
                    $i=1;
                    foreach($current_user_posts as $post){
                        // print_r($post);
                        ?>
                        
                        <tr class="accordion-toggle collapsed" id="accordion1" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= $i?>">

                            <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                <strong>
                                    <a class="row-title" href="<?php echo get_permalink($post->ID); ?>"><?php echo $i; ?></a>
                                </strong>
                            </td>
                            <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                <strong>
                                    <a class="row-title" href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>
                                </strong>
                            </td>

                            <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                <strong>
                                    <a class="row-title" href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_modified; ?></a>
                                </strong>
                            </td>
                            <td>
                                <strong>
                                    <select id="post_status" onChange="ChangeStatus(this, '<?= $post->ID;?>');">
                                        <option value="publish" <?php echo ($post->post_status == 'publish') ? 'selected' : '';?>>publish</option>
                                        <option value="pending" <?php echo ($post->post_status == 'pending') ? 'selected' : '';?>>pending</option>
                                        <option value="draft" <?php echo ($post->post_status == 'draft') ? 'selected' : '';?>>draft</option>
                                        <option value="private"<?php echo ($post->post_status == 'private') ? 'selected' : '';?>>private</option>
                                    </select> 
                                </strong>
                            </td>
                        </tr>
                        <?php  $i++; 
                    }
                }else{  
                    echo '<tr>No post found</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>




            <?php } else
{
	wp_redirect('/myaccount');
} get_footer(); ?>
<script>
jQuery(document).ready(function() {
 
  
	jQuery('#view_podcast').DataTable();
	
    
});
function ChangeStatus(status, postID){
    
    ajaxURL = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php";
    var statusVal= status.value;

    jQuery.ajax({
                url : ajaxURL,
                type : 'post',
                data : {
                    action : 'change_post_status',
                    status : statusVal,
                    postID: postID
                },
                success : function( response ) {
                    if(response == 'updated'){
                        location.reload();
                    }else{
                        alert('An error occur while update post status');
                    }
                }
            });
}

</script>