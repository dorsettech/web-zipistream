<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$allowed_html = array(
	'a' => array(
		'href' => array(),
	),
);
?>

<p>
	<?php
	printf(
		/* translators: 1: user display name 2: logout url */
		wp_kses( __( 'Hello %1$s (not %1$s? <a href="%2$s">Log out</a>)', 'woocommerce' ), $allowed_html ),
		'<strong>' . esc_html( $current_user->display_name ) . '</strong>',
		esc_url( wc_logout_url() )
	);
	?>
</p>

<p>
	<?php
	/* translators: 1: Orders URL 2: Address URL 3: Account URL. */
	$dashboard_desc = __( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">billing address</a>, and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' );
	if ( wc_shipping_enabled() ) {
		/* translators: 1: Orders URL 2: Addresses URL 3: Account URL. */
		$dashboard_desc = __( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a>, and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' );
	}
	printf(
		wp_kses( $dashboard_desc, $allowed_html ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		esc_url( wc_get_endpoint_url( 'edit-address' ) ),
		esc_url( wc_get_endpoint_url( 'edit-account' ) )
	);
	?>
</p>


<?php
	global $wpdb;

	$userID = get_current_user_id();
	$args = array(
		'author'        =>  $userID, 
		'orderby'       =>  'post_date',
		'order'         =>  'ASC',
		//'post_type'     => 'podcast',
        'post_status'   => 'publish',
		'posts_per_page' => -1 // no limit
	  );
	  
	$table_name = $wpdb->prefix.'play_stats';  
	$current_user_posts = get_posts( $args );
	//echo '<pre>';
	//print_r($current_user_posts);
	
?>

    <div class="tabmain dd">
        <div class="tab_content_outer">
            <div class="tab_content">
                <div class="innernav">
                    <select name="pdcast" class="pdcast">
                    	<option value="all">Select Podcast</option>
						<?php 	
							if($current_user_posts){
								foreach($current_user_posts as $post){
									//echo $post_id->ID;
									$result = $wpdb->get_results("SELECT * FROM $table_name where podcast_id = $post->ID"); 
									//$users = get_post_meta( $post->ID );
									foreach ($result as $key ) {
										
										$postDta   = get_post( $key->podcast_id );
										echo '<option value="'.$key->podcast_id.'" >'.$postDta->post_title.'</option>';
									} 
								}
								//print_r($users); 
							} 
						?>

                    </select>

                    <select name="pdfilter" class="pdfilter">
                    	<option value="all" >Select</option>
                    	<option value="most" >Most Running</option>
                    	<option value="least" >Least Running</option>
					</select>

                </div> 
				<div class="wp_podcastlist">                
                    <table id="usr_dashboard_tbl" class="wp-list-table table-hover table-bordered widefat fixed striped posts">
                       <thead style="background:#efefef;">
                            <tr>
								<th scope="col">#</th>
								<th scope="col" style="width:6%;">
                                    <a href="#_"><span>S.No.</span> </a>
                                </th>
                                <th scope="col" class="manage-column column-primary" style="width:50%;">
                                    <a href="#_"><span>Podcast</span></a>
                                </th>
                                <th scope="col" class="manage-column column-primary">
                                    <a href="#_"><span>Count</span></a>
                                </th>
                                <th scope="col" class="manage-column column-primary">
                                    <a href="#_"><span>Datetime</span> </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                      <?php 
                      if($current_user_posts){
						$i=1;
						foreach($current_user_posts as $post){
							//echo $post->ID;
							$result = $wpdb->get_results("SELECT * FROM $table_name where podcast_id = $post->ID"); 
							$users = get_post_meta( $post->ID, 'users_share_info', true  );

							if($result == true){
							foreach ($result as $key ) { 
								
                      			$postDta   = get_post( $key->podcast_id );
                      		?>
                            <!--tr class="iedit type-post format-standard"-->
							<tr class="accordion-toggle collapsed" id="accordion1" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= $i?>">
								<td class="expand-button"></td>
								<td class="title column-title has-row-actions column-primary" data-colname="Title">
                                    <strong>
                                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $i; ?></a>
                                    </strong>
                                </td>
                                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                    <strong>
                                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $postDta->post_title; ?></a>
                                    </strong>
                                </td>
                                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                    <strong>
                                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $key->count; ?></a>
                                    </strong>
                                </td>
                                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                    <strong>
                                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $key->datetime; ?></a>
                                    </strong>
                                </td>
                            </tr>
							<?php 
								if($users){

									foreach($users as $user){
	
										$usersinfo= get_userdata($user);
								?>
								<tr class="hide-table-padding">
								<td></td>
								<td colspan="4">
									<div id="collapse_<?= $i?>" class="collapse in p-3">
										<div class="row">
											<div class="col-6">Name: <?php echo $usersinfo->data->display_name;?></div>
											<div class="col-6">Email: <?php echo $usersinfo->data->user_email ;?></div>
										</div>
									</div>
								</td>
								</tr>
								<?php }	
								} else{	?>
									<tr class="hide-table-padding">
										<td></td>
										<td colspan="4">
											<div id="collapse_<?= $i?>" class="collapse in p-3">
											<div class="row">
												<div class="col-6">No data found</div>
											</div>
										</td>
									</tr>
								<?php } $i++; 
								}
							} else{  //echo '<tr>No post found</tr>';
							 }
							}
						 } else { echo '<tr>No post found</tr>'; } ?>
                        
                        </tbody>
                    </table>
               </div>
            </div>
         
        </div>

    </div>
    
    <style type="text/css">
        .tab_content_outer .tab_content{display:none;}
        .tab_nav{float:left;width:100%;border-bottom:1px solid #ccc;}
        .tab_nav li{float:left;padding:7px 20px;background:#e5e5e5;border-bottom:none !important;margin-right:12px;border:1px solid #ccc;margin-bottom:0;border-radius:2px 2px 0 0;}
        .tab_nav li.selected{border-bottom:1px solid #f1f1f1 !important;margin-bottom:-1px;background:#f1f1f1 !important}
        .tab_nav li a{font-size:14px;line-height:1.71428571;font-weight:600;text-decoration:none;color:#555;}
        .tab_nav li:first-child{margin-left:12px;}
        .tab_nav ul{float:left;width:100%;padding:0;margin:0;}
        .tab_content_outer .tab_content:first-child{display:block;}
        .tabmain{margin-top:25px;}
        .tab_content_outer{display:inline-block;width:100%;padding:12px;box-sizing:border-box;}
        .tab_content_outer h4{margin-top:0 !important}
        .innernav{display:inline-block;width:100%;}
        .innernav ul{margin:0;padding:0;float:left;width:100%;}
        .innernav ul li{float:left;}
        .innernav li a{font-size:15px;line-height:1.71428571;font-weight:600;text-decoration:none;display:inline-block;color:#555;padding:14px 14px;color:#0073aa;border:1px solid #ccc;}
        .widefat td{padding: 12px 10px !important}
        .pdcast{width:20%; float: left; margin: 20px 20px;}
		.pdfilter{width: 20%; margin: 20px 20px;}
		.entry-content a{color: #47268b;}
		.hide-table-padding td { padding: 0 !important;}
		.expand-button { position: relative; }
		.accordion-toggle .expand-button:after { position: absolute;  left:.75rem; top: 50%; transform: translate(0, -50%); content: '-'; }
		.accordion-toggle.collapsed .expand-button:after { content: '+'; }
		.wp_podcastlist #collapse_1 .col, .wp_podcastlist #collapse_1 .wp-block-column, .wp_podcastlist #collapse_1 [class*=col-] {
    padding-right: 0px;
    padding-left: 0;
}
.wp_podcastlist div#collapse_1{
margin-left:30px;}
    </style>

    <script type="text/javascript">
			
			jQuery(document).ready(function() {
				jQuery('#usr_dashboard_tbl').DataTable();
			});

    	jQuery(".pdcast").change(function(){
    		ajaxURL = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php";
    		var podcastID = jQuery(this).val();
    		if(podcastID == 'all'){
    			location.reload();
			} else {
				jQuery.ajax({
	            	url : ajaxURL,
	            	type : 'post',
	            	data : {
	                	action : 'get_podcast_callback',
	                	podcastID : podcastID,
						front_dash : 'front_dash'
	            	},
	            	success : function( response ) {
						//console.log(response);
	                	jQuery("#usr_dashboard_tbl tbody").html(response);
	            	}
	        	});
			}
    	});

    	jQuery(".pdfilter").change(function(){
    		ajaxURL = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php";
    		var pdfilter = jQuery(this).val();
    		if(pdfilter == 'all'){
    			location.reload();
			}else{
				jQuery.ajax({
	            	url : ajaxURL,
	            	type : 'post',
	            	data : {
	                	action : 'get_podcast_filter_callback_frontend',
	                	pdfilter : pdfilter,
						frontDashboard: 'frontDashboard'
	            	},
	            	success : function( response ) {
						//console.log(response);
	                	jQuery("#usr_dashboard_tbl tbody").html(response);
	            	}
	        	});
			}
    	});

	</script>
<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
