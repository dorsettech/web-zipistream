<?php
require_once("../../../wp-load.php");    

if(isset($_POST['data']))
{
     parse_str($_POST['data'], $data);

     $postId = $data['podcast_id'];
     $userId = $data['userId'];
     $checkUser = get_post_meta( $postId, 'Is_claimed', true );

     if(empty($checkUser)){
          $checkUser =array();
     }
          if(!empty($checkUser) && in_array($userId, $checkUser)){
               
               echo "You have Already claimed, Your claim is under review !";
               die;
          }else{

               array_push($checkUser, $userId);
               update_post_meta( $postId, "Is_claimed", $checkUser);
          }

    //$to = $_POST['email']; //sendto@example.com
    $post = get_post($data['podcast_id']); //assuming $id has been initialized
    
    $content= "Hi Admin,<br><br> A new user has claimed for '" . $post->post_title . "' Podcast <br><br> Users details: <br><br> Name: '" .$data['full_name']. "'<br> Email: '". $data['email'] ."' <br><br> '".$data['message']."'";
    $to = 'zipistream@dorset.tech';
    //$to = 'sahilbatra.betasoft@outlook.com';
    $subject = 'Claim Podcast';
    $body = $content;
    $headers = array('Content-Type: text/html; charset=UTF-8');

   if(wp_mail( $to, $subject, $body, $headers )){

        echo "Email sent to Admin for review your Podcast Claim";
   }
   else{

        echo "Email sent failed please contact to Admin";
   }
}

?>