== Change Log ==

= 1.2.1 =
* Added: Full Support for Buzzsprout (Theme Options -> Podcast -> Player)
* Improved: Custom area functionality

= 1.2 =

* Power Add: Users can pick and add more Custom areas (Theme Options -> Front Page -> General -> Number of custom content sections )
* Power Add: Users can display content from desired page inside Custom area (Theme Options -> Front Page -> Custom content -> Pull custom content from )
* Added: Several icons for podcast platforms which can be recognized and displayed in the Subscribe menu(iHeart, TuneIN, Overcast, Ausha, Anchor, Megaphone.fm, iVoox, Simplecast, BuzzSprout )
* Added: Support for Simplecast, iHeart, TuneIN, Anchor.fm, Ausha embeds (Theme Options -> Podcast -> Player)
* Added: Option to have custom order of Shows (Theme Options -> Podcast -> Shows -> Shows to display)
* Added: Option to limit description on Shows (Theme Options -> Podcast -> Shows -> Display description -> Description excerpt limit)
* Added: Authors page template option to display guests (Page template: Authors -> Display data from: Host authors / Guests authors) 
* Fixed: Pagination problem, including infinite scroll pagination problem
* Fixed: Multiple authors problem on mobile
* Fixed: Several minor styling issues including IE fixes

= 1.1 =

* Added: Option to switch logic which separates episodes from regular posts if you are using Podlove or Seriously simple podcasting WordPress plugins (Theme Options -> Podcast -> General)
* Added: Option to modify the player height (Theme Options -> Podcast -> Player)
* Added: Option to display Download button for audio episodes inside the player (Theme Options -> Podcast -> Player)
* Added: Several icons for podcast platforms which can be recognized and displayed in the Subscribe menu (Mixcloud, Spotify, Stitcher, Podbean, Libsyn, Spreaker, Cadence13, Podomatic, Blog Talk Radio )
* Added: Another "blank" section on the home page which can display any custom content (Theme Options -> Front Page)
* Added: Options to choose alternative background color for Featured area and Welcome area on front page (Theme Options -> Front Page)
* Added: Support for Megaphone.fm, BuzzSprout and iVoox embeds (Theme Options -> Podcast -> Player)
* Added: Option to enable the play button/icon over the episode thumbnail in all episode listing layouts (Theme Options -> Podcast -> Episode Listing Layouts)
* Fixed: Play button not working on the page template which listed all shows
* Fixed: Several minor styling issues

= 1.0.1 =
* Fixed: Several minor styling issues
* Fixed: template-functions.php throwing errors on specific server/PHP configurations

= 1.0 =
* Initial release