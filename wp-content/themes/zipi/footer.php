<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
 <style>
* {
  box-sizing: border-box;
}

.columns {
  float: left;
  width: 33.3%;
  padding: 8px;
margin-bottom: 40px;
}
.pum-theme-2790 .pum-content+.pum-close, .pum-theme-lightbox .pum-content+.pum-close{
    background-color: rgb(93 13 197);
}
.button-bg{
    margin:20px;
}
.pum-theme-2790 .pum-container, .pum-theme-lightbox .pum-container{
   box-shadow:none !important;
   border:0px !important;
}
.price {
  list-style-type: none;
  border: 1px solid #e8e8e8;
  margin: 0;
  padding: 0;
  -webkit-transition: 0.3s;
  transition: 0.3s;
}

.price:hover {
  box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
}

.price .header {
  background-color: rgb(75, 34, 135);
  color: white;
  font-size: 25px;
}

.price li {
  border-bottom: 1px solid #e8e8e8;
  padding: 20px;
  text-align: center;
font-size:14px;
}
.price li:last-child {
    border-bottom: 0px solid #eee;
	}
.price .grey {
  font-size: 20px;
}
.col.text-center {
    margin-bottom: 20px;
}
a.button {
 border: 2px solid transparent!important;
    border-image: linear-gradient(to left, #fe004e 0%, #8700cd 100%)!important;
    border-image-slice: 1!important;
    font-family: 'avertape-bolduploaded_file';
    color: #3d3b44;
    text-transform: uppercase;
    padding: 10px 20px;
    font-size: 14px;
    background-image: none!important;
	padding:10px;
	text-decoration:none;
}
.button-bg{
margin:20px;}
@media only screen and (max-width: 600px) {
  .columns {
    width: 100%;
  }
}
</style>
<input type="hidden" id="checkUserlogin" value="<?php echo (is_user_logged_in()) ? '1' : '0';?>">
<!-- Plans Modal -->
<div class="modal fade" id="plans" tabindex="-1" role="dialog" aria-labelledby="plansTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
		<div class="modal-headers">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
      <div class="modal-body">
		<div class="columns">
			<ul class="price">
				<li class="header">Basic</li>
				<li class="grey">$ <span style="font-size: 30px;color: #ff006c;">0</span> / year</li>
				<li>Can't upload Podcast</li>
				<li>Adds show</li>
				<li>Zero Download</li>
				<!-- <li class="grey button-bg"><a href="<?= get_site_url()?>/myaccount/" class="button">Sign Up</a></li> -->
			</ul>
		</div>

		<div class="columns">
			<ul class="price">
				<li class="header" style="background-color:#ff006c">Pro</li>
				<li class="grey">$ <span style="font-size: 30px;color: #ff006c;">24.99</span> / year</li>
				<li>can upload upto 5 Podcast Daily</li>
				<li>No Adds</li>
				<li>Unlimited Download</li>
				<!-- <li class="grey button-bg"><a href="<?= get_site_url()?>/myaccount/" class="button">Sign Up</a></li> -->
			</ul>
		</div>

		<div class="columns">
			<ul class="price">
				<li class="header">Premium</li>
				<li class="grey">$ <span style="font-size: 30px;color: #ff006c;">49.99</span> / year</li>
				<li>can upload upto 10 Podcast Daily</li>
				<li>No Adds</li>
				<li>Unlimited Download</li>
				
			</ul>
		</div>
		<div class="col text-center">
			<a href="<?= get_site_url()?>/myaccount/" class="button text-center">Get Your Plan</a>
		</div>	
      </div>
    </div>
  </div>
</div>
<?php if ( megaphone_get( 'display', 'footer' ) ) : ?>

	<?php if ( megaphone_get( 'footer', 'subscribe' ) ) : ?>
		<?php get_template_part( 'template-parts/footer/subscribe' ); ?>
	<?php endif; ?>

	<?php if ( megaphone_get( 'footer', 'instagram' ) ) : ?>
		<?php get_template_part( 'template-parts/footer/instagram' ); ?>
	<?php endif; ?>

	<?php get_template_part( 'template-parts/ads/above-footer' ); ?>

	<footer class="megaphone-footer">
			
			<?php if ( megaphone_get( 'footer', 'widgets' ) ) : ?>
				<div class="container">			
					<?php get_template_part( 'template-parts/footer/widgets' ); ?>		
				</div>
			<?php endif; ?>

			<?php if ( megaphone_get( 'footer', 'copyright' ) ) : ?>
				<div class="megaphone-copyright">
					<p class="cpyL"><?php echo do_shortcode( wp_kses_post( megaphone_get( 'footer', 'copyright' ) ) ); ?></p>
                    <p class="cpyR"><a href="/privacy-policy/">Privacy Policy</a><span class="pipe">|</span><a href="/terms-of-service/">Terms of Service</a></p>
				</div>
			<?php endif; ?>


	</footer>
<?php endif; ?>

<div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        	<div class="alert alert-success message_div" role="alert" style="display: none;">
			  Thanks for your feedback!
			</div>
         	<div class="msg">
			  can we let the podcast owner know that you've listened to their podcast?
			</div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger notify_owner" data-value="1">Yes</button>
          <button type="button" class="btn btn-danger notify_owner" data-value="0">No</button>
        </div>
        
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
	jQuery( document ).ready(function(){
		var userStatus = jQuery('#checkUserlogin').val();
		if ((jQuery('body').hasClass('home')) && userStatus == 0 ) {
        	jQuery("#plans").modal('show');
		}	
		
		// jQuery('#premium').click(function(){

		// window.location.href='https://zipi.honesttech.co.uk/myaccount/';
		// });
		
		var ajaxurl = "<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php";

		jQuery(".megaphone-play").click(function(){
			if(!jQuery(this).hasClass( "megaphone-is-playing")){
				var playID = jQuery(this).attr('data-play-id');
				var ajaxData = {
						'action': 'add_play_stats',
						'playID': playID,
				}
				jQuery.post(ajaxurl, ajaxData, function(response){
				var obj = JSON.parse(response);
					if(obj.status == true){
							console.log(obj.message);
					}
				});
			}
		});
		setInterval(function(){
			if(jQuery('.mejs-currenttime').text() > '05:00'){
				//alert('yes');
				jQuery('.message_div').hide();
				var playID = jQuery('.mejs-currenttime').closest('#meks-ap-player').attr("data-playing-id");
				var ajaxData = {
						'action': 'get_notify_status',
						'post_id': playID,
				}
				jQuery.post(ajaxurl, ajaxData, function(response) {
					//console.log(response);
					var obj = JSON.parse(response);
					if(obj.status == 200){
						$('.notify_owner').attr("data-play-id",playID);
						jQuery('#myModal').modal('show');
					}
				});
			}
		},10000);

		jQuery('.notify_owner').click(function(){
			jQuery('.msg').hide();
			jQuery('.modal-footer').hide();
			var user_response = jQuery(this).data('value');
			var playID = jQuery(this).attr("data-play-id");
			var ajaxData = {
				'action': 'notify_provider',
				'post_id': playID,
				'user_response': user_response
			}
			jQuery.post(ajaxurl, ajaxData, function(response){
				//console.log(response);// return false;
				var obj = JSON.parse(response);
				if(obj.status == 200){
					jQuery('.message_div').show();
					setTimeout(function(){
						jQuery('#myModal').modal('hide');
					},3000);
					
				}
			});
		})

	});
</script>


<?php if ( megaphone_get( 'footer', 'popup' ) ) : ?>
	<?php get_template_part( 'template-parts/footer/gallery-placeholder' ); ?>
<?php endif; ?>

<?php if ( megaphone_get( 'footer', 'go_to_top' ) ) : ?>
	<a href="javascript:void(0)" id="megaphone-goto-top" class="megaphone-goto-top"><i class="mf mf-chevron-up"></i></a>
<?php endif; ?>


<?php get_template_part( 'template-parts/footer/overlay' ); ?>
<?php get_template_part( 'template-parts/footer/hidden-sidebar' ); ?>

<?php wp_footer(); ?>
</body>

</html>