<?php
get_header();
global $wp_query;
?>
<div class="wapper">
  <div class="contentarea clearfix">
    <div class="content" style="width: 30%; margin: 5% auto;">
      	<div class="megaphone-content">
			<div class="section-head">
		        <h5 class="section-title searchTitle h2">ZipiStream Search</h5>
			</div>
	        <p class="searchSub">Enhance your listening experience. Find the best podcasts, search by popularity or subject.</p>

			<?php //get_search_form(); 
				echo do_shortcode('[wpdreams_ajaxsearchlite]');
			?>
		</div>
	</div>
  </div>
</div>
<?php get_footer(); ?>