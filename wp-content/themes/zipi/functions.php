<?php

/* Define theme version */
define( 'MEGAPHONE_THEME_VERSION', '1.2.1' );

/* Helpers and utility functions */	
include_once get_parent_theme_file_path( '/core/helpers.php' );

/* Include translation strings */
include_once get_parent_theme_file_path( '/core/translate.php' );

/* Default options */
include_once get_parent_theme_file_path( '/core/default-options.php' );

/* Load frontend scripts */
include_once get_parent_theme_file_path( '/core/enqueue.php' );

/* Template functions */
include_once get_parent_theme_file_path( '/core/template-functions.php' );

/* Menus */
include_once get_parent_theme_file_path( '/core/menus.php' );

/* Sidebars */
include_once get_parent_theme_file_path( '/core/sidebars.php' );


/* Extensions (hooks and filters to add/modify specific features ) */
include_once get_parent_theme_file_path( '/core/extensions.php' );

/* Main theme setup hook and init functions */
include_once get_parent_theme_file_path( '/core/setup.php' );


if ( is_admin() || is_customize_preview() ) {

	/* Admin helpers and utility functions  */
	include_once get_parent_theme_file_path( '/core/admin/helpers.php' );

	/* Load admin scripts */
	include_once get_parent_theme_file_path( '/core/admin/enqueue.php' );

	if ( is_customize_preview() ) {
		/* Theme Options */
		include_once get_parent_theme_file_path( '/core/admin/options.php' );
	}

	/* Include plugins - TGM init */
	include_once get_parent_theme_file_path( '/core/admin/plugins.php' );

	/* Include AJAX action handlers */
	include_once get_parent_theme_file_path( '/core/admin/ajax.php' );

	/* Extensions ( hooks and filters to add/modify specific features ) */
	include_once get_parent_theme_file_path( '/core/admin/extensions.php' );

	/* Custom metaboxes */
	include_once get_parent_theme_file_path( '/core/admin/metaboxes.php' );

	/* Demo importer panel */
	include_once get_parent_theme_file_path( '/core/admin/demo-importer.php' );

}

function wpdocs_theme_name_scripts() {
	//wp_enqueue_style( 'bootstrap-style', "https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css",array(),rand(1000,9999),true );

	//wp_enqueue_script( 'bootstraps-popper', "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js", array(),rand(1000,9999),true );
    //wp_enqueue_script( 'bootstrap-style', "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js",array(),rand(1000,9999),true );
   //wp_enqueue_script( 'bootstraps-modal', "https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js", array(),rand(1000,9999),true );
   	wp_enqueue_style( 'style', 'https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css' );
	wp_enqueue_script( 'script', 'https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js', array ( 'jquery' ), 1.1, true);
 }

add_action( 'admin_enqueue_scripts', 'wpdocs_theme_name_scripts' );
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

function add_cors_http_header(){
    header("Access-Control-Allow-Origin: *");
}
add_action('init','add_cors_http_header');


/**
 * Add custom taxonomies
 */
function themename_custom_taxonomies() {
	// Channel
	$coach_method = array(
		'name' => _x( 'Channel', 'channel' ),
		'singular_name' => _x( 'Channel', 'channel' ),
		'search_items' =>  __( 'Search in Channel' ),
		'all_items' => __( 'All Channel' ),
		'most_used_items' => null,
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Channel' ), 
		'update_item' => __( 'Update Channel' ),
		'add_new_item' => __( 'Add new Channel' ),
		'new_item_name' => __( 'New Channel' ),
		'menu_name' => __( 'Channel' ),
	);
	$args = array(
		'hierarchical' => true,
		'labels' => $coach_method,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'channel' ),
		'show_in_rest' => true,
	);
    register_taxonomy( 'channel','post', $args );
}
add_action( 'init', 'themename_custom_taxonomies', 0 );

function add_custom_headers() {

    add_filter( 'rest_pre_serve_request', function( $value ) {
        header( 'Access-Control-Allow-Headers: Authorization, X-WP-Nonce,Content-Type, X-Requested-With');
        header( 'Access-Control-Allow-Origin: *' );
        header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
        header( 'Access-Control-Allow-Credentials: true' );

        return $value;
    } );
}
add_action( 'rest_api_init', 'add_custom_headers', 15 );

add_filter('kses_allowed_protocols', function($protocols) {
    $protocols[] = 'ionic';
    return $protocols;
});
 
function add_custom_search_api(){
    register_rest_route( 'api/v1', '/search', array(
        'methods' => 'GET',
        'callback' => 'get_custom_search_data',
    ));
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', '/get/search', array(
		'methods' => 'GET',
		'callback' => 'get_search'
	)
);
});
function get_search($data){
$final_data = [];
$query_args = array( 's' => $data['query'], 'post_type' => 'podcast','posts_per_page' => -1);
$query = new WP_Query( $query_args );
	foreach($query->posts as $post){
		//print_r($post);die;
		$post->image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		$users = get_post_meta( $post->ID, 'liked_user_ids', true );
		if(count($users) && in_array($data['user_id'], $users)){
			$post->isLiked = 'yes';
		}else{
			$post->isLiked = 'no';
		}
		
		$added_to_playlist = get_post_meta( $post->ID, 'added_to_playlist', true );
		if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
			$post->isAddedToPlaylist = 'yes';
		}else{
			$post->isAddedToPlaylist = 'no';
		}
		$tags = wp_get_post_tags($post->ID);
		$post->tags = $tags;
		$author_id = $post->post_author;
		$post->authorName = get_user_meta( $author_id, 'nickname' ,true );
		
		$post->songUrl = get_post_meta( $post->ID, 'audio_file', true );
		
		
		$meta_key = 'notify_provider_'.$data['user_id'];
		$category_data = get_post_meta($post->ID,$meta_key,'true');
		$post->is_provider_notified = $category_data; 
		
		$final_data[] = $post;
	}
	//print_r($final_data);die;
	$response['status'] = 'success';
	$response['data'] = $final_data;
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'like/song', array(
		'methods' => 'GET',
		'callback' => 'like_song'
	)
);
});
function like_song($data){
	
	//echo "<pre>";print_r($data);die;
	if($data['user_id']){
	if ( metadata_exists( 'post', $data['post_id'], 'liked_user_ids' ) ) {
		$users = get_post_meta( $data['post_id'], 'liked_user_ids', true );
		if(count($users)){
			array_push($users, $data['user_id']);
		}else{
			$users = [$data['user_id']];
		}
	}else{
		$users = [$data['user_id']];
	}
	update_post_meta( $data['post_id'], "liked_user_ids", $users);
	$response['status'] = 'success';
	}else{
		$response['status'] = 'error';
		$response['message'] = 'User ID is required';
		$response['data'] = [];
	}
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'unlike/song', array(
		'methods' => 'GET',
		'callback' => 'unlike_song'
	)
);
});

function unlike_song($data){
	if($data['user_id']){
		
	if ( metadata_exists( 'post', $data['post_id'], 'liked_user_ids' ) ) {
		$users = get_post_meta( $data['post_id'], 'liked_user_ids', true );
		if(count($users) && in_array($data['user_id'], $users)){
			$pos = array_search($data['user_id'], $users);
			unset($users[$pos]);
		}else{
			$users = [];
		}
	}else{
		$users = [];
	}
	update_post_meta( $data['post_id'], "liked_user_ids", $users);
	$response['status'] = 'success';
	}else{
		$response['status'] = 'error';
		$response['message'] = 'User ID is required';
		$response['data'] = [];
	}
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'add/to/playlist', array(
		'methods' => 'GET',
		'callback' => 'add_to_playlist'
	)
);
});
function add_to_playlist($data){
	if($data['user_id']){
		
	if ( metadata_exists( 'post', $data['post_id'], 'added_to_playlist' ) ) {
		$users = get_post_meta( $data['post_id'], 'added_to_playlist', true );
		if(count($users)){
			array_push($users, $data['user_id']);
		}else{
			$users = [$data['user_id']];
		}
	}else{
		$users = [$data['user_id']];
	}
	update_post_meta( $data['post_id'], "added_to_playlist", $users);
	$response['status'] = 'success';
	}else{
		$response['status'] = 'error';
		$response['message'] = 'User ID is required';
		$response['data'] = [];
	}
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'remove/from/playlist', array(
		'methods' => 'GET',
		'callback' => 'remove_from_playlist'
	)
);
});

function remove_from_playlist($data){
	if($data['user_id']){
		if ( metadata_exists( 'post', $data['post_id'], 'added_to_playlist' ) ) {
			$users = get_post_meta( $data['post_id'], 'added_to_playlist', true );
			if(count($users) && in_array($data['user_id'], $users)){
				$pos = array_search($data['user_id'], $users);
				unset($users[$pos]);
				//echo"<pre>";print_r($users);die;
			}else{
				$users = [];
			}
		}else{
			$users = [];
		}
		update_post_meta( $data['post_id'], "added_to_playlist", $users);
		$response['status'] = 'success';
	}else{
		$response['status'] = 'error';
		$response['message'] = 'User ID is required';
		$response['data'] = [];
	}
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'liked/song/list', array(
		'methods' => 'GET',
		'callback' => 'liked_song_list'
	)
);
});

function liked_song_list($data){
	
	$args = array(
        'post_type'     => 'podcast',
        'post_status'   => 'publish',
		'posts_per_page' => -1,
        'meta_query'    => array(
			array(
				'key'   => 'liked_user_ids',
				'value' => $data['user_id'],
				'compare'   => 'LIKE'
			)
		)
    );

    $final_data = get_posts($args);
	$final = [];
	foreach($final_data as $post){
		$post->image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		$users = get_post_meta( $post->ID, 'liked_user_ids', true );
		if(count($users) && in_array($data['user_id'], $users)){
			$post->isLiked = 'yes';
		}else{
			$post->isLiked = 'no';
		}
		
		$added_to_playlist = get_post_meta( $post->ID, 'added_to_playlist', true );
		if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
			$post->isAddedToPlaylist = 'yes';
		}else{
			$post->isAddedToPlaylist = 'no';
		}
		$tags = wp_get_post_tags($post->ID);
		$post->tags = $tags;
		$author_id = $post->post_author;
		$post->authorName = get_user_meta( $author_id, 'nickname' ,true );
		
		$post->songUrl = get_post_meta( $post->ID, 'audio_file', true );
		
		$meta_key = 'notify_provider_'.$data['user_id'];
		$category_data = get_post_meta($post->ID,$meta_key,'true');
		$post->is_provider_notified = $category_data;  
		
		$final[] = $post;
	}
	//print_r($final);die;
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'search/clicked', array(
			'methods' => 'GET',
			'callback' => 'search_clicked'
		)
	);
});

function search_clicked($data){
	$meta_key = "recently_searched_by_".$data['user_id'];
	update_post_meta( $data['post_id'], $meta_key, strtotime(date('d-m-Y h:i:s')));
	$final_data = wp_get_single_post($data['post_id']);
	$final_data->image = wp_get_attachment_url( get_post_thumbnail_id( $final_data->ID ) );
	
	$users = get_post_meta( $final_data->ID, 'liked_user_ids', true );
	if(count($users) && in_array($data['user_id'], $users)){
		$final_data->isLiked = 'yes';
	}else{
		$final_data->isLiked = 'no';
	}
		
	$added_to_playlist = get_post_meta( $final_data->ID, 'added_to_playlist', true );
	if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
		$final_data->isAddedToPlaylist = 'yes';
	}else{
		$final_data->isAddedToPlaylist = 'no';
	}
	
	$tags = wp_get_post_tags($final_data->ID);
	$final_data->tags = $tags;
	$author_id = $final_data->post_author;
	$final_data->authorName = get_user_meta( $author_id, 'nickname' ,true );
	$post->songUrl = get_post_meta( $final_data->ID, 'audio_file', true );
	
	$meta_key = 'notify_provider_'.$data['user_id'];
	$category_data = get_post_meta($final_data->ID,$meta_key,'true');
	$post->is_provider_notified = $category_data;  
	
	//print_r($final_data);die;
	$response['status'] = 'success';
	$response['data'] = $final_data;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'recent/searched', array(
			'methods' => 'GET',
			'callback' => 'recent_searched'
		)
	);
});

function recent_searched($data){
		$meta_key = "recently_searched_by_".$data['user_id'];
		$args = array(
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'post_type' => 'podcast',
			'meta_key' => $meta_key,
			'orderby' => 'meta_value_num',
			'order' => 'DESC'
		);

		$final_data = get_posts($args);
		$final = [];
		foreach($final_data as $post){
			$post->image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
			$users = get_post_meta( $post->ID, 'liked_user_ids', true );
			if(count($users) && in_array($data['user_id'], $users)){
				$post->isLiked = 'yes';
			}else{
				$post->isLiked = 'no';
			}

			$added_to_playlist = get_post_meta( $post->ID, 'added_to_playlist', true );
			if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
				$post->isAddedToPlaylist = 'yes';
			}else{
				$post->isAddedToPlaylist = 'no';
			}
			$tags = wp_get_post_tags($post->ID);
			$post->tags = $tags;
			$author_id = $post->post_author;
			$post->authorName = get_user_meta( $author_id, 'nickname' ,true );
			$post->songUrl = get_post_meta( $post->ID, 'audio_file', true );
			
			$meta_key = 'notify_provider_'.$data['user_id'];
			$category_data = get_post_meta($post->ID,$meta_key,'true');
			$post->is_provider_notified = $category_data;  
			
			$final[] = $post;
		}
		$response['status'] = 'success';
		$response['data'] = $final;
		return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'song/played', array(
			'methods' => 'GET',
			'callback' => 'song_clicked'
		)
	);
});

function song_clicked($data){
	$meta_key = "recently_played_by_".$data['user_id'];
	update_post_meta( $data['post_id'], $meta_key, strtotime(date('d-m-Y h:i:s')));
	$data = wp_get_single_post($data['post_id']);
	$response['status'] = 'success';
	$response['data'] = $data;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'recent/played', array(
			'methods' => 'GET',
			'callback' => 'recent_played'
		)
	);
});

function recent_played($data){
	$meta_key = "recently_played_by_".$data['user_id'];
	$args = array(
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'post_type' => 'podcast',
		'meta_key' => $meta_key,
		'orderby' => 'meta_value_num',
		'order' => 'DESC'
	);

   // $data = get_posts($args);
	$final_data = get_posts($args);
	$final = [];
	foreach($final_data as $post){
		$post->image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		$users = get_post_meta( $post->ID, 'liked_user_ids', true );
		if(count($users) && in_array($data['user_id'], $users)){
			$post->isLiked = 'yes';
		}else{
			$post->isLiked = 'no';
		}
		
		$added_to_playlist = get_post_meta( $post->ID, 'added_to_playlist', true );
		if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
			$post->isAddedToPlaylist = 'yes';
		}else{
			$post->isAddedToPlaylist = 'no';
		}
		$tags = wp_get_post_tags($post->ID);
		$post->tags = $tags;
		$author_id = $post->post_author;
		$post->authorName = get_user_meta( $author_id, 'nickname' ,true );
		$post->songUrl = get_post_meta( $post->ID, 'audio_file', true );
		
		$meta_key = 'notify_provider_'.$data['user_id'];
		$category_data = get_post_meta($post->ID,$meta_key,'true');
		$post->is_provider_notified = $category_data;
		
		$final[] = $post;
	}
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'featured/posts', array(
			'methods' => 'GET',
			'callback' => 'featured_posts'
		)
	);
});

function featured_posts($data){
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'podcast',
		'post_per_page' => -1,
		'meta_key' => '_wpfp_featured_post',
		'meta_value' => '1',
		'order' => 'DESC'
	);

    $final_data = get_posts($args);
	$final = [];
	foreach($final_data as $post){
		$post->image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		$users = get_post_meta( $post->ID, 'liked_user_ids', true );
		if(count($users) && in_array($data['user_id'], $users)){
			$post->isLiked = 'yes';
		}else{
			$post->isLiked = 'no';
		}
		
		$added_to_playlist = get_post_meta( $post->ID, 'added_to_playlist', true );
		if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
			$post->isAddedToPlaylist = 'yes';
		}else{
			$post->isAddedToPlaylist = 'no';
		}
		$tags = wp_get_post_tags($post->ID);
		$post->tags = $tags;
		$author_id = $post->post_author;
		$post->authorName = get_user_meta( $author_id, 'nickname' ,true );
		$post->songUrl = get_post_meta( $post->ID, 'audio_file', true );
		
		$meta_key = 'notify_provider_'.$data['user_id'];
		$category_data = get_post_meta($post->ID,$meta_key,'true');
		$post->is_provider_notified = $category_data;
		
		$final[] = $post;
	}
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'all/songs', array(
			'methods' => 'GET',
			'callback' => 'all_songs'
		)
	);
});

function all_songs($data){
	
	$args = array(
        'post_type'     => 'podcast',
        'post_status'   => 'publish',
		'posts_per_page'=> -1
    );

    $final_data = get_posts($args);
	$final = [];
	foreach($final_data as $post){
		//print_r($post);die;
		$post->image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		$users = get_post_meta( $post->ID, 'liked_user_ids', true );
		if(count($users) && in_array($data['user_id'], $users)){
			$post->isLiked = 'yes';
		}else{
			$post->isLiked = 'no';
		}
		
		$added_to_playlist = get_post_meta( $post->ID, 'added_to_playlist', true );
		if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
			$post->isAddedToPlaylist = 'yes';
		}else{
			$post->isAddedToPlaylist = 'no';
		}
		$tags = wp_get_post_tags($post->ID);
		$post->tags = $tags;
		$author_id = $post->post_author;
		$post->authorName = get_user_meta( $author_id, 'nickname' ,true );
		$post->songUrl = get_post_meta( $post->ID, 'audio_file', true );
		
		$meta_key = 'notify_provider_'.$data['user_id'];
		$category_data = get_post_meta($post->ID,$meta_key,'true');
		$post->is_provider_notified = $category_data; 
		$final[] = $post;
	}
	//print_r($final);die;
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'category/songs', array(
			'methods' => 'GET',
			'callback' => 'category_songs'
		)
	);
});

function category_songs($data){
	
	$args = array(
        'post_type'     => 'podcast',
        'post_status'   => 'publish',
		'category_name' => $data['name'],
    );

    $final_data = get_posts($args);
	$final = [];
	foreach($final_data as $post){
		$post->image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		$users = get_post_meta( $post->ID, 'liked_user_ids', true );
		if(count($users) && in_array($data['user_id'], $users)){
			$post->isLiked = 'yes';
		}else{
			$post->isLiked = 'no';
		}
		
		$added_to_playlist = get_post_meta( $post->ID, 'added_to_playlist', true );
		if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
			$post->isAddedToPlaylist = 'yes';
		}else{
			$post->isAddedToPlaylist = 'no';
		}
		$tags = wp_get_post_tags($post->ID);
		$post->tags = $tags;
		$author_id = $post->post_author;
		$post->authorName = get_user_meta( $author_id, 'nickname' ,true );
		$post->songUrl = get_post_meta( $post->ID, 'audio_file', true );
		
		$meta_key = 'notify_provider_'.$data['user_id'];
		$category_data = get_post_meta($post->ID,$meta_key,'true');
		$post->is_provider_notified = $category_data;
		
		$final[] = $post;
	}
	//print_r($final);die;
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'podcast/songs', array(
			'methods' => 'GET',
			'callback' => 'podcast_songs'
		)
	);
});

function podcast_songs($data){
	
	$args = array(
			'post_type' => 'podcast',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'series',
					'field' => 'ID', //can be set to ID
					'terms' => $data['term_id']
				)
			)
		);
		$final_data[] = get_posts($args);
	$dd = array_flatten($final_data);
	$final = [];
		foreach($dd as $post){
			$post->image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
			$users = get_post_meta( $post->ID, 'liked_user_ids', true );
			if(!empty($users) && in_array($data['user_id'], $users)){
				$post->isLiked = 'yes';
			}else{
				$post->isLiked = 'no';
			}

			$added_to_playlist = get_post_meta( $post->ID, 'added_to_playlist', true );
			if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
				$post->isAddedToPlaylist = 'yes';
			}else{
				$post->isAddedToPlaylist = 'no';
			}
			$tags = wp_get_post_tags($post->ID);
			$post->tags = $tags;
			$author_id = $post->post_author;
			$post->authorName = get_user_meta( $author_id, 'nickname' ,true );
			//$post->authorName = get_the_author_meta( 'display_name' , $author_id );
			$post->songUrl = get_post_meta( $post->ID, 'audio_file', true );

			$meta_key = 'notify_provider_'.$data['user_id'];
			$category_data = get_post_meta($post->ID,$meta_key,'true');
			$post->is_provider_notified = $category_data;
			
			$post->categories =  wp_get_object_terms( $post->ID, 'series', array( 'fields' => 'names' ) ); 
			$final[] = $post;
		}
	//print_r($final_data);die;
	//$data = array_map("unserialize", array_unique(array_map("serialize", $final)));
	//print_r($final);die;
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'podcast/categories', array(
			'methods' => 'GET',
			'callback' => 'categories'
		)
	);
});

function categories($data){


$tags = get_tags();
//$postslist =  get_posts(array('post_type' => 'podcast','tag' => 'amazing'));
	//print_r($tags);die;
	$tax_terms = get_terms("series", array('hide_empty' => false));
	foreach ( $tax_terms as $term ) {
		$args = array(
			'post_type' => 'podcast',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'series',
					'field' => 'slug', //can be set to ID
					'terms' => $term->slug
				)
			)
		);
		$final_data[] = get_posts($args);
	}
	$dd = array_flatten($final_data);
	$featured_categoreis = [];
	foreach($dd as $key => $val){
		$featured_categoreis[] =  wp_get_object_terms( $val->ID,  'channel' );
	}
	$cat1 = array_flatten($featured_categoreis);
	$final_data = [];
	foreach($tags as $k => $v){
		$final_data[$k]['term_id'] = $v->term_id;		
		$final_data[$k]['name'] = $v->name;
		$final_data[$k]['parent_id'] = $v->term_id;
		
		

	}
	$data = array_map("unserialize", array_unique(array_map("serialize", $final_data)));
	//print_r($data);die;
	$response['status'] = 'success';
	$response['data'] = $data;
	return $response;exit;
}

function array_flatten($array) { 
  if (!is_array($array)) { 
    return FALSE; 
  } 
  $result = array(); 
  foreach ($array as $key => $value) { 
    if (is_array($value)) { 
      $result = array_merge($result, array_flatten($value)); 
    } 
    else { 
      $result[$key] = $value; 
    } 
  } 
  return $result; 
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'categories/podcast', array(
			'methods' => 'GET',
			'callback' => 'categories_podcast'
		)
	);
});

function categories_podcast($data){
	//isme category k podcast aayenge
	//print_r($postslist);die;
	//get_term_by('id', 12, 'category')
	//$final_data = get_term_by('id',$data['parent_id'],'channel');
	$final_data =  get_posts(array('post_type' => 'podcast','tag_id' => $data['parent_id']));
	$categories = [];
	foreach($final_data as $k => $v){
		$final = wp_get_post_terms($v->ID ,'series');
		$final[0]->image = get_term_meta($final[0]->term_id, '_megaphone_meta',true)['image'];
		$categories[] = $final;
	}
	$categories = array_flatten($categories);
	$data = array_map("unserialize", array_unique(array_map("serialize", $categories)));
	//print_r($data);die;
	$response['status'] = 'success';
	$response['data'] = $data;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'featured/podcasts', array(
			'methods' => 'GET',
			'callback' => 'featured_podcasts'
		)
	);
});

function featured_podcasts($data){
	
	$tax_terms = get_terms("series", array('hide_empty' => false));
	//print_r($tax_terms);die;
	foreach ( $tax_terms as $term ) {
		$args = array(
			'post_type' => 'podcast',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'series',
					'field' => 'slug', //can be set to ID
					'terms' => $term->slug
				)
			)
		);
		$final_data[] = get_posts($args);
	}
	//print_r($final_data);
	$dd = array_flatten($final_data);
	
	$featured_categoreis = [];
	foreach($dd as $key => $val){
		$featured_category =  wp_get_object_terms( $val->ID,  'series' );
	
		$featured_category[0]->image = get_term_meta($featured_category[0]->term_id, '_megaphone_meta',true)['image'];
		//print_r($featured_categoreis);die;
		$featured_categoreis[] = $featured_category;
	}
	
	$cat1 = array_flatten($featured_categoreis);
	//print_r($cat1);die;
	$final_data = [];
	foreach($cat1 as $k => $v){
		$final_data[] = get_term_by('id',$v->parent,'series');

	}
	$data = array_map("unserialize", array_unique(array_map("serialize", $cat1)));
	//print_r($data);die;
	$response['status'] = 'success';
	$response['data'] = $data;
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'playlist/songs', array(
		'methods' => 'GET',
		'callback' => 'playlist_songs'
	)
);
});

function playlist_songs($data){
	
	$args = array(
        'post_type'     => 'podcast',
        'post_status'   => 'publish',
		'posts_per_page' => -1, 
        'meta_query'    => array(
			array(
				'key'   => 'added_to_playlist',
				'value' => $data['user_id'],
				'compare'   => 'LIKE'
			)
		)
    );

    $final_data = get_posts($args);
	$final = [];
	foreach($final_data as $post){
		$post->image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		$users = get_post_meta( $post->ID, 'liked_user_ids', true );
		if($users && count($users) && in_array($data['user_id'], $users)){
			$post->isLiked = 'yes';
		}else{
			$post->isLiked = 'no';
		}
		
		$added_to_playlist = get_post_meta( $post->ID, 'added_to_playlist', true );
		if($added_to_playlist && count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
			$post->isAddedToPlaylist = 'yes';
		}else{
			$post->isAddedToPlaylist = 'no';
		}
		$tags = wp_get_post_tags($post->ID);
		$post->tags = $tags;
		$author_id = $post->post_author;
		$post->authorName = get_user_meta( $author_id, 'nickname' ,true );
		$post->songUrl = get_post_meta( $post->ID, 'audio_file', true );

		$meta_key = 'notify_provider_'.$data['user_id'];
		$category_data = get_post_meta($post->ID,$meta_key,'true');
		$post->is_provider_notified = $category_data;
		
		$final[] = $post;
	}
//print_r($final);die;
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'social/login', array(
		'methods' => 'GET',
		'callback' => 'social_login'
	)
);
});

function social_login($data){
	$exists = email_exists($data['email']);
	$parts = explode("@", $data['email']);
	$login_name = $parts[0];
  	if ( !$exists ){

		$user_id = wp_insert_user( array(
		  'user_login' => $login_name.time(),
		  'user_pass' => 'tZjJ-:!4gYU-%TF',
		  'user_email' => $data['email'],
		  'display_name' => $data['name']
		));
		
		if(isset($data['fbid']) && $data['fbid']){
			update_user_meta( $user_id, "fbid", $data['fbid']);
		}
		
		if(isset($data['googleid']) && $data['googleid']){
			update_user_meta( $user_id, "googleid", $data['googleid']);
		}
		update_user_meta( $user_id, "provider", $data['provider']);
		
		if ( ! is_wp_error( $user_id ) ) {
			$user_data = get_userdata($user_id);
			$user_data->fbid = get_user_meta( $user_id, "fbid", true);
			$user_data->googleid = get_user_meta( $user_id, "googleid", true);
			$user_data->provider = get_user_meta( $user_id, "provider", true);
			$user_data->data->id = $user_id;
			$user_data->email = $user_data->user_email;
			$user_data->displayname = $user_data->display_name;
			//$response['data'] = $user_data->data;
			
		}else{
			$response['data'] = [];
			$response['message'] = $user_id->get_error_message();
		}
	}else{
		
		$user_data = get_user_by_email($data['email']);
		if(isset($data['fbid'])){
			update_user_meta( $user_data->ID, "fbid", $data['fbid']);
			update_user_meta( $user_data->ID, "googleid", '');
		}
		if(isset($data['googleid'])){
			update_user_meta( $user_data->ID, "googleid", $data['googleid']);
			update_user_meta( $user_data->ID, "fbid", '');
		}
		update_user_meta( $user_data->ID, "provider", $data['provider']);
		//update_user_meta( $user_data->ID, "device_type", $data['device_type']);
		$user_data->fbid = get_user_meta( $user_data->ID, "fbid", true);
		$user_data->googleid = get_user_meta( $user_data->ID, "googleid", true);
		$user_data->provider = get_user_meta( $user_data->ID, "provider", true);
		$user_data->data->id = $user_data->ID;
		$user_data->email = $user_data->user_email;
		$user_data->displayname = $user_data->display_name;
		//$response['data'] = $user_data->data;
	}
	$resposne['user'] = $user_data->data;
	return $resposne;die;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'podcast/search', array(
		'methods' => 'GET',
		'callback' => 'podcast_search'
	)
);
});
function podcast_search($data){
	
	$final = [];
	if($data['search'] != ''){
		$terms = get_terms( 'series', array(
			'name__like' => $data['search']
		) );
		
		foreach($terms as $post){
			$post->image = get_term_meta($post->term_id, '_megaphone_meta',true)['image'];
			$added_to_playlist = get_term_meta( $post->term_id, 'channel_added_to_playlist', true );
			if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
				$post->isAddedToPlaylist = 'yes';
			}else{
				$post->isAddedToPlaylist = 'no';
			}
			
	
			$meta_key = 'notify_provider_'.$data['user_id'];
			$category_data = get_term_meta($post->term_id,$meta_key,'true');
			$post->is_provider_notified = $category_data;
			
			$final[] = $post;
		}
	}
	
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'user/listening/podcast', array(
		'methods' => 'GET',
		'callback' => 'user_listening_podcast'
	)
);
});

function user_listening_podcast($data){
	if($data['user_id']){
	$meta_key = 'notify_provider_'.$data['user_id'];	
	// if ( metadata_exists( 'term', $data['series_id'], 'users_listening' ) ) {
	// 	$users = get_term_meta( $data['series_id'], 'users_listening', true );
	// 	if(count($users)){
	// 		array_push($users, $data['user_id']);
	// 	}else{
	// 		$users = [$data['user_id']];
	// 	}
	// }else{
	// 	$users = [$data['user_id']];
	// }
	// update_term_meta( $data['series_id'], "users_listening", $users);
	update_post_meta( $data['post_id'], $meta_key, $data['notify_provider']);	
		$response['status'] = 'success';
	}else{
		$response['status'] = 'error';
		$response['message'] = 'User ID is required';
		$response['data'] = [];
	}
	return $response;exit;
}


add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'add/channel/to/playlist', array(
		'methods' => 'GET',
		'callback' => 'add_channel_to_playlist'
	)
);
});
function add_channel_to_playlist($data){
	if($data['user_id']){
		
	if ( metadata_exists( 'term', $data['series_id'], 'channel_added_to_playlist' ) ) {
		$users = get_term_meta( $data['series_id'], 'channel_added_to_playlist', true );
		if(count($users)){
			array_push($users, $data['user_id']);
		}else{
			$users = [$data['user_id']];
		}
	}else{
		$users = [$data['user_id']];
	}
	update_term_meta( $data['series_id'], "channel_added_to_playlist", $users);
		
	$args = [
		'post_type' => 'podcast',
		'posts_per_page' => -1,
		'tax_query' => [
			[
				'taxonomy' => 'series',
				'terms' => $data['series_id'],
			],
		],
	];	
	$post_data = get_posts($args);
		foreach($post_data as $v){
			if ( metadata_exists( 'post', $v->ID, 'added_to_playlist' ) ) {
				$users1 = get_post_meta( $v->ID, 'added_to_playlist', true );
				
				if(count($users1)){
					array_push($users1, $data['user_id']);
				}else{
					$users1 = [$data['user_id']];
				}
			}else{
				
				$users1 = [$data['user_id']];
			}
			//print_r($users1);die;
			update_post_meta( $v->ID, "added_to_playlist", $users1);
		}
		


		$response['status'] = 'success';
	}else{
		$response['status'] = 'error';
		$response['message'] = 'User ID is required';
		$response['data'] = [];
	}
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'channel/in/playlist', array(
		'methods' => 'GET',
		'callback' => 'channel_in_playlist'
	)
);
});
function channel_in_playlist($data){
	$args = array(
		'taxonomy'   => 'series',
		'hide_empty' => false,
		'meta_query' => array(
			 array(
				'key'       => 'channel_added_to_playlist',
				'value'     => $data['user_id'],
				'compare'   => 'LIKE'
			 )
		)
	);
	
	$final_data = get_terms($args);
	$final = [];
	foreach($final_data as $post){
		$post->image = get_term_meta($post->term_id, '_megaphone_meta',true)['image'];
		$added_to_playlist = get_term_meta( $post->term_id, 'channel_added_to_playlist', true );
		if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
			$post->isAddedToPlaylist = 'yes';
		}else{
			$post->isAddedToPlaylist = 'no';
		}
		
		// $meta_key = 'notify_provider_'.$data['user_id'];
		// $category_data = get_post_meta($post->term_id,$meta_key,'true');
		// $post->is_provider_notified = $category_data;
		
		$final[] = $post;
	}
	//print_r($final);die;
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'podcast/search/clicked', array(
			'methods' => 'GET',
			'callback' => 'podcast_search_clicked'
		)
	);
});

function podcast_search_clicked($data){
	$meta_key = "recently_searched_by_".$data['user_id'];
	update_term_meta( $data['series_id'], $meta_key, strtotime(date('d-m-Y h:i:s')));
	$response['status'] = 'success';
	//$response['data'] = $final;
	return $response;exit;
}


add_action('rest_api_init', function () {
		register_rest_route('wp/v2', 'recent/searched/podcasts', array(
			'methods' => 'GET',
			'callback' => 'recent_searched_podacsts'
		)
	);
});

function recent_searched_podacsts($data){
	$meta_key = "recently_searched_by_".$data['user_id'];
	$args = array(
	  'taxonomy' => 'series',
	  'orderby' => 'meta_value_num',
	  'order' => 'DESC',
	  'hide_empty' => false,
	  'hierarchical' => false,
	  //'parent' => 0,
	  'meta_query' => [[
	    'key' => $meta_key,
	    'type' => 'NUMERIC',
	  ]],
	);

	$final_data = get_terms($args);
	$final = [];
	foreach($final_data as $post){
		$post->image = get_term_meta($post->term_id, '_megaphone_meta',true)['image'];
		$added_to_playlist = get_term_meta( $post->term_id, 'channel_added_to_playlist', true );
		if(count($added_to_playlist) && in_array($data['user_id'], $added_to_playlist)){
			$post->isAddedToPlaylist = 'yes';
		}else{
			$post->isAddedToPlaylist = 'no';
		}
		
		// $meta_key = 'notify_provider_'.$data['user_id'];
		// $category_data = get_term_meta($post->term_id,$meta_key,'true');
		// $post->is_provider_notified = $category_data;
		$final[] = $post;
	}
	//print_r($final);die;
	$response['status'] = 'success';
	$response['data'] = $final;
	return $response;exit;
}

add_action('rest_api_init', function () {
	register_rest_route('wp/v2', 'remove/channel/from/playlist', array(
		'methods' => 'GET',
		'callback' => 'remove_channel_to_playlist'
	)
);
});


function remove_channel_to_playlist($data){
	if($data['user_id']){
		
	if ( metadata_exists( 'term', $data['series_id'], 'channel_added_to_playlist' ) ) {
		
			$users = get_term_meta( $data['series_id'], 'channel_added_to_playlist', true );
		
			if(count($users) && in_array($data['user_id'], $users)){
				$pos = array_search($data['user_id'], $users);
				unset($users[$pos]);
			}else{
				$users = [];
			}
		}else{
			$users = [];
		}
	update_term_meta( $data['series_id'], "channel_added_to_playlist", $users);
		
	$args = [
		'post_type' => 'podcast',
		'posts_per_page' => -1,
		'tax_query' => [
			[
				'taxonomy' => 'series',
				'terms' => $data['series_id'],
			],
		],
	];	
	$post_data = get_posts($args);
		//print_r($post_data);die;
		foreach($post_data as $v){
			if ( metadata_exists( 'post', $v->ID, 'added_to_playlist' ) ) {
				$users1 = get_post_meta( $v->ID, 'added_to_playlist', true );
				//print_r($users1);die;
				if(count($users1) && in_array($data['user_id'], $users1)){
					$pos = array_search($data['user_id'], $users1);
					unset($users1[$pos]);
					update_post_meta( $v->ID, "added_to_playlist", $users1);
				}
			}else{
				update_post_meta( $v->ID, "added_to_playlist", []);
			}
			
		}


$response['status'] = 'success';
	}else{
		$response['status'] = 'error';
		$response['message'] = 'User ID is required';
		$response['data'] = [];
	}
	return $response;exit;
}




add_action( 'admin_menu', 'zipistreams_play_stats_menu' );
function zipistreams_play_stats_menu() {
  add_menu_page( 'Play Stats', 'Play Stats', 'manage_options', 'play-stats', 'play_stats', 'dashicons-welcome-widgets-menus', 90 );
}

function play_stats(){
	global $wpdb;
    $table_name = $wpdb->prefix.'play_stats';
    $result = $wpdb->get_results("SELECT * FROM $table_name"); ?>

    <div class="tabmain">
        <div class="tab_content_outer">
            <div class="tab_content">
                <div class="innernav">
                    <!-- <ul>
                        <li><a href="#">Year</a></li>
                        <li><a href="#">This Month</a></li>
                        <li><a href="#">Last 7 Days</a></li>
                    </ul> -->

                    <select name="pdcast" class="pdcast">
                    	<option value="all">Select Podcast</option>
                    	<?php foreach ($result as $key ) {
                    		$postDta   = get_post( $key->podcast_id );
                    		echo '<option value="'.$key->podcast_id.'" >'.$postDta->post_title.'</option>';
                    	} ?>

                    </select>

                    <select name="pdfilter" class="pdfilter">
                    	<option value="all" >Select</option>
                    	<option value="most" >Most Running</option>
                    	<option value="least" >Least Running</option>
					</select>

                </div> 
				<div class="wp_podcastlist">                
                    <table class="wp-list-table widefat fixed striped posts">
                       <thead style="background:#efefef;">
                            <tr>
                                 <th scope="col" style="width:6%;">
                                    <a href="#_">
                                        <span>S.No.</span>
                                    </a>
                                </th>
                                <th scope="col" class="manage-column column-primary" style="width:50%;">
                                    <a href="#_">
                                        <span>Podcast</span>
                                    </a>
                                </th>
                                <th scope="col" class="manage-column column-primary">
                                    <a href="#_">
                                        <span>Count</span>
                                    </a>
                                </th>
                                <th scope="col" class="manage-column column-primary">
                                    <a href="#_">
                                        <span>Datetime</span>
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                      <?php 
                      	if($result == true){  $i=1;
                      		foreach ($result as $key){ 
                      			$postDta   = get_post( $key->podcast_id );
                      		?>
                            <tr class="iedit type-post format-standard">
                                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                    <strong>
                                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $i; ?></a>
                                    </strong>
                                </td>
                                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                    <strong>
                                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $postDta->post_title; ?></a>
                                    </strong>
                                </td>
                                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                    <strong>
                                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $key->count; ?></a>
                                    </strong>
                                </td>
                                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                                    <strong>
                                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $key->datetime; ?></a>
                                    </strong>
                                </td>
                            </tr>
                           <?php $i++; } } else { 'No post found'; } ?>
                        
                        </tbody>
                    </table>
               </div>
            </div>
         
        </div>

    </div>
    
    <style type="text/css">
        .tab_content_outer .tab_content{display:none;}
        .tab_nav{float:left;width:100%;border-bottom:1px solid #ccc;}
        .tab_nav li{float:left;padding:7px 20px;background:#e5e5e5;border-bottom:none !important;margin-right:12px;border:1px solid #ccc;margin-bottom:0;border-radius:2px 2px 0 0;}
        .tab_nav li.selected{border-bottom:1px solid #f1f1f1 !important;margin-bottom:-1px;background:#f1f1f1 !important}
        .tab_nav li a{font-size:14px;line-height:1.71428571;font-weight:600;text-decoration:none;color:#555;}
        .tab_nav li:first-child{margin-left:12px;}
        .tab_nav ul{float:left;width:100%;padding:0;margin:0;}
        .tab_content_outer .tab_content:first-child{display:block;}
        .tabmain{margin-top:25px;}
        .tab_content_outer{display:inline-block;width:100%;padding:12px;box-sizing:border-box;}
        .tab_content_outer h4{margin-top:0 !important}
        .innernav{display:inline-block;width:100%;}
        .innernav ul{margin:0;padding:0;float:left;width:100%;}
        .innernav ul li{float:left;}
        .innernav li a{font-size:15px;line-height:1.71428571;font-weight:600;text-decoration:none;display:inline-block;color:#555;padding:14px 14px;color:#0073aa;border:1px solid #ccc;}
        .widefat td{padding: 12px 10px !important}
    </style>

    <script type="text/javascript">
    	jQuery(".pdcast").change(function(){
    		ajaxURL = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php";
    		var podcastID = jQuery(this).val();
    		if(podcastID == 'all'){
    			location.reload();
			} else {
				jQuery.ajax({
	            	url : ajaxURL,
	            	type : 'post',
	            	data : {
	                	action : 'get_podcast_callback',
	                	podcastID : podcastID
	            	},
	            	success : function( response ) {
	                	jQuery(".wp_podcastlist").html(response);
	            	}
	        	});
			}
    	});

    	jQuery(".pdfilter").change(function(){
    		ajaxURL = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php";
    		var pdfilter = jQuery(this).val();
    		if(pdfilter == 'all'){
    			location.reload();
			}else{
				jQuery.ajax({
	            	url : ajaxURL,
	            	type : 'post',
	            	data : {
	                	action : 'get_podcast_filter_callback',
	                	pdfilter : pdfilter
	            	},
	            	success : function( response ) {
	                	jQuery(".wp_podcastlist").html(response);
	            	}
	        	});
			}
    	});

    </script>
<?php }


function add_play_stats(){
	global $wpdb;
	$playID = $_POST['playID'];
	$table_name = $wpdb->prefix.'play_stats';

	$rt = $wpdb->get_results("SELECT * FROM $table_name WHERE `podcast_id` = $playID");
	if($rt == true){
		$pid = $rt[0]->id;
		$pdcount = $rt[0]->count + 1;
		$pupdate = $wpdb->query("UPDATE  $table_name SET count = $pdcount WHERE id = $pid");
		if($pupdate == true){
			$data = array("status" => true, "message"=> 'update successfully');
		}else{
			$data = array("status" => true, "message"=> 'please try again');
		}
	} else {
		$result = $wpdb->insert($table_name, array(
			'podcast_id' => $playID,
			'count' => 1,
			'datetime' => date('Y-m-d H:i:s')
		));

		if($result == true){
			$data = array("status" => true, "message"=> 'added successfully');
		}else{
			$data = array("status" => true, "message"=> 'please try again');
		}
	}

	echo json_encode($data);

die();

}
add_action("wp_ajax_add_play_stats", "add_play_stats");
add_action("wp_ajax_nopriv_add_play_stats", "add_play_stats");

add_action('rest_api_init', function () { 
	register_rest_route('wp/v2', 'add-play-stats', array(
		'methods' => 'GET',
		'callback' => 'add_play_stats_callback'
	)
);
});

function add_play_stats_callback($data){
	global $wpdb;
	$playID = $data['playID'];
	$table_name = $wpdb->prefix.'play_stats';

	$rt = $wpdb->get_results("SELECT * FROM $table_name WHERE `podcast_id` = $playID");
	if($rt == true){
		$pid = $rt[0]->id;
		$pdcount = $rt[0]->count + 1;
		$pupdate = $wpdb->query("UPDATE  $table_name SET count = $pdcount WHERE id = $pid");
		if($pupdate == true){
			$data = array("status" => true, "message"=> 'update successfully');
		}else{
			$data = array("status" => true, "message"=> 'please try again');
		}
	} else {
		$result = $wpdb->insert($table_name, array(
			'podcast_id' => $playID,
			'count' => 1,
			'datetime' => date('Y-m-d H:i:s')
		));

		if($result == true){
			$data = array("status" => true, "message"=> 'added successfully');
		}else{
			$data = array("status" => true, "message"=> 'please try again');
		}
	}

	echo json_encode($data);

die();

}
// function notify_provider()
// {
// 	if(is_user_logged_in()){
// 		$user_id = get_current_user_id();	
// 		$meta_key = 'notify_provider_'.$user_id;
// 		$notify_provider = ($_POST['user_response'] == 1) ? 'yes' : 'no'; 
// 		update_post_meta( $_POST['post_id'], $meta_key, $notify_provider);	
// 	}
// 	echo json_encode(['status' => 200]);die();
// }

// add_action("wp_ajax_notify_provider", "notify_provider");
// add_action("wp_ajax_nopriv_notify_provider", "notify_provider");

function get_podcast_callback(){
	global $wpdb;
	$table_name = $wpdb->prefix.'play_stats';
	$podcastID = $_POST['podcastID'];
	$front_dash= $_POST['front_dash'];

	$result = $wpdb->get_results("SELECT * FROM $table_name WHERE `podcast_id` = $podcastID"); 

      	if($result == true){  
      			$postDta   = get_post( $podcastID );
      		?>
            <tr class="iedit type-post format-standard">
				<?php if($front_dash == 'front_dash'){ echo '<td></td>'; }?>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($podcastID ); ?>">1</a>
                    </strong>
                </td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($podcastID ); ?>"><?php echo $postDta->post_title; ?></a>
                    </strong>
                </td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($podcastID ); ?>"><?php echo $result[0]->count; ?></a>
                    </strong>
                </td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($podcastID ); ?>"><?php echo $result[0]->datetime; ?></a>
                    </strong>
                </td>
            </tr>
           <?php  } else { 'No post found'; } 
die();
}
add_action("wp_ajax_get_podcast_callback", "get_podcast_callback");
add_action("wp_ajax_nopriv_get_podcast_callback", "get_podcast_callback");


function get_podcast_filter_callback_frontend(){
	global $wpdb;
	$table_name = $wpdb->prefix.'play_stats';
	$pdfilter = $_POST['pdfilter'];

	$userID = get_current_user_id();
    $args = array(
        'author'        =>  $userID, 
        'orderby'       =>  'post_date',
        'order'         =>  'ASC',
        'post_status' => array('publish')   ,
        'posts_per_page' => -1 // no limit
        
        );
         
	$current_user_posts = get_posts( $args );
	//echo '<pre>';
	//print_r($current_user_posts); die;
	$result = array();
	if($pdfilter == 'most' ){
		foreach($current_user_posts as $post){

			$res = $wpdb->get_row("SELECT * FROM $table_name where podcast_id = $post->ID ORDER BY count  DESC ");

			if(!empty($res)){
				array_push($result, $res);
			}
		}
		$results = json_decode(json_encode($result), true);

			$price = array();
			foreach ($results as $key => $row)
			{
				$price[$key] = $row['count'];
			}
			array_multisort($price, SORT_DESC, $results);
	}else{	
		foreach($current_user_posts as $post){
			
			$res = $wpdb->get_row("SELECT * FROM $table_name where podcast_id = $post->ID ORDER BY count  ASC ");	
			
			if(!empty($res)){
				array_push($result, $res);
			}
		}
		$results = json_decode(json_encode($result), true);

			$price = array();
			foreach ($results as $key => $row)
			{
				$price[$key] = $row['count'];
			}
			array_multisort($price, SORT_ASC, $results);
	} 
		  if($results == true){  $i=1;
			
			// echo '<pre>';
			// print_r($results); die;
      		foreach ($results as $key){ 
      			$postDta   = get_post( $key['podcast_id'] );
      		?>
            <tr class="iedit type-post format-standard">
				<td></td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink ($key['podcast_id']); ?>"><?php echo $i; ?></a>
                    </strong>
                </td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($key['podcast_id']); ?>"><?php echo $postDta->post_title; ?></a>
                    </strong>
                </td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($key['podcast_id']); ?>"><?php echo $key['count']; ?></a>
                    </strong>
                </td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($key['podcast_id']); ?>"><?php echo $key['datetime']; ?></a>
                    </strong>
                </td>
            </tr>
           <?php $i++; } } else { 'No post found'; } 
die();
}
add_action("wp_ajax_get_podcast_filter_callback_frontend", "get_podcast_filter_callback_frontend");
add_action("wp_ajax_nopriv_get_podcast_filter_callback_frontend", "get_podcast_filter_callback_frontend");

function get_podcast_filter_callback(){
	global $wpdb;
	$table_name = $wpdb->prefix.'play_stats';
	$pdfilter = $_POST['pdfilter'];

	if($pdfilter == 'most' ){
		$result = $wpdb->get_results("SELECT * FROM $table_name ORDER BY count  DESC ");	
	}else{
		$result = $wpdb->get_results("SELECT * FROM $table_name ORDER BY count  ASC ");	
	} 
      	if($result == true){  $i=1;
      		foreach ($result as $key){ 
      			$postDta   = get_post( $key->podcast_id );
      		?>
            <tr class="iedit type-post format-standard">
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $i; ?></a>
                    </strong>
                </td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $postDta->post_title; ?></a>
                    </strong>
                </td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $key->count; ?></a>
                    </strong>
                </td>
                <td class="title column-title has-row-actions column-primary" data-colname="Title">
                    <strong>
                        <a class="row-title" href="<?php echo get_permalink($key->podcast_id); ?>"><?php echo $key->datetime; ?></a>
                    </strong>
                </td>
            </tr>
           <?php $i++; } } else { 'No post found'; } 
die();
}
add_action("wp_ajax_get_podcast_filter_callback", "get_podcast_filter_callback");
add_action("wp_ajax_nopriv_get_podcast_filter_callback", "get_podcast_filter_callback");

function get_notify_status_callback(){
	$status = 400;
	if(is_user_logged_in()){
		$user_id = get_current_user_id();
		$meta_key = 'notify_provider_'.$user_id;
		if ( !metadata_exists( 'post', $_POST['post_id'], $meta_key ) ) {
			$status = 200;
		}
	}
	echo json_encode(['status' => $status]);die();
}

add_action("wp_ajax_get_notify_status", "get_notify_status_callback");
add_action("wp_ajax_nopriv_get_notify_status", "get_notify_status_callback");

function notify_provider_callback()
{
	global $wpdb;
    $table_name = $wpdb->prefix.'postmeta';
	
	// if(is_user_logged_in()){
	// 	$user_id = get_current_user_id();	
	// 	$meta_key = 'notify_provider_'.$user_id;
	// 	$notify_provider = ($_POST['user_response'] == 1) ? 'yes' : 'no'; 
	// 	update_post_meta( $_POST['post_id'], $meta_key, $notify_provider);	
	// }
	// echo json_encode(['status' => 200]);die();
	
	if(is_user_logged_in()){
		$user_id = get_current_user_id();
		
		$postId	= $_POST['post_id'];
		$checkUser = get_post_meta( $postId, 'users_share_info', true );
		$meta_key = 'notify_provider_'.$user_id;
		$notify_provider = ($_POST['user_response'] == 1) ? 'yes' : 'no'; 
		update_post_meta( $_POST['post_id'], $meta_key, $notify_provider);

		if($_POST['user_response'] == 1){
		
			if(empty($checkUser) && !in_array($user_id, $checkUser)){
				$checkUser =array();

					array_push($checkUser, $user_id);
					add_post_meta( $postId, 'users_share_info', $checkUser );
					echo json_encode(['status' => 200]);die();
		   	}
			if(!empty($checkUser) && !in_array($user_id, $checkUser)){ 

				array_push($checkUser, $user_id);
				update_post_meta( $postId, 'users_share_info', $checkUser);
				echo json_encode(['status' => 200]);die();	
			
			}
		}

		echo json_encode(['status' => 200]);die();
		
		
	}
	
}

add_action("wp_ajax_notify_provider", "notify_provider_callback");
add_action("wp_ajax_nopriv_notify_provider", "notify_provider_callback");



add_action('admin_menu', 'claim_podcast_menu_pages');

function claim_podcast_menu_pages(){

	add_menu_page('Claim Podcast', 'Claim Podcast', 'manage_options', 'claim-podcast', 'claim_podcast', 'dashicons-welcome-widgets-menus', 90 );

}
function change_podcast_author(){

	$post_id = $_POST['podcastID'];
	$user_id 	= $_POST['uid'];

	$arg = array(
		'ID' => $post_id,
		'post_author' => $user_id,
	);
	
	if(wp_update_post( $arg )){
		
		update_post_meta( $post_id, "Broadcast", $user_id);
		echo "Author updated successfully";

	}else{

		echo "An error occur while update Author";
	}
	

}

add_action("wp_ajax_change_podcast_author", "change_podcast_author");
add_action("wp_ajax_nopriv_change_podcast_author", "change_podcast_author");

function claim_podcast(){
	
	global $wpdb;
    $table_name = $wpdb->prefix.'postmeta';
	$result = $wpdb->get_results("SELECT * FROM $table_name where meta_key = 'Is_claimed'"); 
	
	?>

    <div class="tabmain">
        <div class="tab_content_outer">
            <div class="tab_content">
				<div class="claim-title">
					<h1>Claim Podcast</h1>
				</div>
				<div class="wp_podcastlist">                
                    <table id="claimTable" class="wp-list-table widefat fixed striped posts">
                       <thead style="background: #23282d;padding:5px;">
                            <tr>
                                 <th scope="col" style="width:10%;">
                                    <a href="#_">
                                        <span>S.No.</span>
                                    </a>
                                </th>
                                <th scope="col" class="manage-column column-primary">
                                    <a href="#_">
                                        <span>Podcast Name</span>
                                    </a>
                                </th>
                            </tr>
						
                        </thead>
						
                        <tbody>
							
							   
                      <?php 
						  if($result == true){  $i=1;

                      		foreach ($result as $key){ 
								  $postDta   = get_post( $key->post_id );
								  $users = get_post_meta( $key->post_id, 'Is_claimed', true );
								  $content = substr($postDta->post_content, 0, 60);
								  $checkUser = get_post_meta( $postDta->ID, 'Broadcast', true );
								 // print_r($users);
                      		?>
							 <tr> 
							<td>
							     <h3><?php echo $i; ?></h3>
							   </td>
							<td>  
							<button type="button" class="collapsible"><?php echo $postDta->post_title; ?> <img src="https://zipi.honesttech.co.uk/wp-content/uploads/down-arrow-1.png"/></button>
							<div class="content claim-detail">
							<h3>Claimed Users Detail</h3> 
							<?php foreach($users as $user) {  

										
										$dd = get_userdata($user);
										if(!empty($checkUser) && $checkUser == $user){
											?>
											<div class="deatil_claim_users">
											 <p><input type="radio" checked class="radio userID" onchange="changePodcastAuth(<?php echo $user.','. $postDta->ID ?>)" name="claim_assign" value="" ></p>
												 <ul>
													 <li><strong>Name:</strong> <?php echo $dd->display_name; ?></li>
													 <li><strong>Email:</strong> <?php echo $dd->user_email; ?></li>
												 </ul>
											 </div>
										<?php }else{ ?>
										<div class="deatil_claim_users">
											<p><input type="radio" class="radio userID" onchange="changePodcastAuth(<?php echo $user.','.$postDta->ID ?>)" name="claim_assign" value=""></p>
											<ul>
												<li><strong>Name:</strong> <?php  echo $dd->display_name; ?></li>
											    <li><strong>Email:</strong> <?php echo $dd->user_email ; ?></li>
											</ul>
										</div>
									<?php		
										}
								 }?>
							</div>
							</td>
						</tr>
                           <?php $i++; } } else { 'No post found'; } ?>
                        
                        </tbody>
                    </table>
               </div>
            </div>
         
        </div>

    </div>
    
    <style type="text/css">
		.collapsible {
	background-color: #0073aa;
    color: white;
    cursor: pointer;
    padding: 10px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    margin-bottom: 10px;
		}
		.claim-detail h3{
			margin:0;
			line-height:32px;
		}
		.widefat tr th{
			padding:10px;
		}
		.widefat tr th a{
			color:#fff;
			text-decoration:none;
			font-size: 16px;
		}
		.widefat tr td{
			border-bottom:1px solid #ddd;
		}
		.deatil_claim_users {
			display: flex;
    width: 30%;
    float: left;
    margin-right: 10px;
		}
		.widefat td p {
    margin: 0px 8px 0em;
}
.widefat td h3{
	margin:0;
}
		.deatil_claim_users ul {
			border: 1px solid #ddd;
		}
		.deatil_claim_users ul li{
			border-bottom: 1px solid #ddd;
			padding:10px;
			background:#fff;
			margin-bottom:0;
		}
		.deatil_claim_users ul li:last-child{
			border-bottom:0px;
		}
		.deatil_claim_users p {
			line-height: 100px !important;
			margin-right: 10px;
		}
		.collapsible img {
			width: 18px;
			float: right;
		}
           .claim-title h1{		 
				line-height: 52px;
				font-size: 23px;
				font-weight: 400;
				margin: 0;
				padding: 9px 0 4px 0;
		   }
		.active, .collapsible:hover {
		background-color: #555;
		}

		.content {
		padding: 20px;
		display: none;
		overflow: hidden;
			background-color: #f1f1f1;
			border: 1px solid #d2caca;
			margin-bottom: 10px;
		}
		div#claimTable_length select {
			width: 60px;
		}
		div#claimTable_length {
			margin-bottom: 10px;
		}
    </style>

    <script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#claimTable').DataTable();
	} );
	var coll = document.getElementsByClassName("collapsible");
		var i;

		for (i = 0; i < coll.length; i++) {
		coll[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var content = this.nextElementSibling;
			if (content.style.display === "block") {
			content.style.display = "none";
			} else {
			content.style.display = "block";
			}
		});
		}
		function changePodcastAuth(uid, podcastID){
			
				if (confirm("Are you sure you wanna assgin Podcast to this user?")){
					console.log(uid);
					console.log(podcastID);

					ajaxURL = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php";
						
						if(podcastID != null && uid != null){

							jQuery.ajax({
								url : ajaxURL,
								type : 'post',
								data : {
									action : 'change_podcast_author',
									podcastID : podcastID,
									uid : uid
								},
								success : function( response ) {
									//console.log(response);
									alert(response);
								}
							});
						}
				}
		}
    	 

    </script>
<?php 
}

add_action('wp_logout','auto_redirect_after_logout');

function auto_redirect_after_logout(){

	$url = $url = get_site_url().'/myaccount/';	
  	wp_redirect( $url );
  	exit();

}

function action_woocommerce_after_customer_login_form( $evolve_woocommerce_after_customer_login_form ) { 
	// make action magic happen here... 
	//echo do_shortcode("[pms-login]");
}; 
         
// add the action 
add_action( 'woocommerce_after_customer_login_form', 'action_woocommerce_after_customer_login_form', 10, 1 ); 



####################### Change Post Staus from Front end #####################################
function change_post_status(){

	$status = $_POST['status'];
	$postid = $_POST['postID'];

	wp_update_post(array(
        'ID'    =>  $postid,
        'post_status'   =>  $status
		));
	
	echo 'updated';
	die();
}


add_action("wp_ajax_change_post_status", "change_post_status");
add_action("wp_ajax_nopriv_change_post_status", "change_post_status");

####################### END Change Post Staus from Front end #####################################